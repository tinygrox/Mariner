import { SEA_EVENT_SLOTS } from "../../slots_helpers.mjs";
import { minChance, generateScaffholding } from "./sea_events_helpers.mjs";

export function generateSicknessSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("sickness");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "An Unwholesome Air",
    tablereqs: { "mariner.weathermeta.sickly": 1 },
    slots: SEA_EVENT_SLOTS(),
    startdescription:
      "Normaly the sea air is brisk, hale and salt-laden, but today the effluvium of industry has followed us from the city into the sea. The vile retchings of smoke stacks clogs the air like bitter miasm and me must be hearty to survive it. [Approach this challenge with Heart or Grail.]",
    linked: [
      { id: `${PREFIX}.outcome.success`, chance: minChance("heart") },
      { id: `${PREFIX}.outcome.success`, chance: minChance("grail") },
      { id: `${PREFIX}.outcome.failure` },
    ],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.success`,
    warmup: 20,
    label: "Through the Miasma",
    startdescription:
      "Through nurtering our private spaces, and keeping care to keep the air in the ship free of stain, we make it through the area without any lasting sickness.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "A Rattling Cough",
    startdescription:
      "The foul fog is clogging up our rooms, our mouths our longs. Some of my crew is lost to a coughing fit they just cannot shake, even after we leave the area. they will be bed-ridden and feverish for the journey remaining at least.",
    furthermore: [
      {
        target: "~/tabletop",
        mutations: {
          "[mariner.crew] && [mariner.crew.traits.frail] && [mariner.crew.exhausted]<1": {
            mutate: "mariner.crew.exhausted",
            level: 5,
            limit: 30,
          },
        },
      },
      {
        target: "~/tabletop",
        mutations: {
          "[mariner.crew] && [mariner.crew.exhausted]<1": {
            mutate: "mariner.crew.exhausted",
            level: 5,
            limit: 3,
          },
        },
      },
    ],
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
