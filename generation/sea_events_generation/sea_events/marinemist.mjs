import { SEA_EVENT_SLOTS, SHIP_MANOEUVERING_SLOTS } from "../../slots_helpers.mjs";
import { BASE_SUCCESS_CHANCE } from "../generate_seaevents.mjs";
import { generateScaffholding, minChance } from "./sea_events_helpers.mjs";

export function generateMarineMistEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("marinemist");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "Marine Mists",
    startdescription:
      "Today the marine layer is thick as pea soup, and the feeble ailing sun is not enough to cut through it. We can pierce it with Lantern or intuit the route with Knock.",
    slots: SHIP_MANOEUVERING_SLOTS(),
    linked: [
      { id: `${PREFIX}.outcome.success`, chance: minChance("lantern") },
      { id: `${PREFIX}.outcome.success`, chance: minChance("knock") },
      { id: `${PREFIX}.outcome.success`, chance: BASE_SUCCESS_CHANCE },
      { id: `${PREFIX}.outcome.fogfailure` },
      { id: `${PREFIX}.outcome.failure` },
    ],
  });

  // Outcomes
  generateSimpleSuccessOutcome({
    label: "Navigate Through",
    startdescription:
      "Guided by maps, and tools and what we can see with our eyes and beyond, we have found our way out of the mistbank without harm.",
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.fogfailure`,
    warmup: 20,
    tablereqs: { "mariner.weathermeta.misty": 1 },
    label: "The Deeper Mists",
    startdescription:
      "Where are we? Where are we going? Why are we trying to get there? In this bleak and forlorn silence, any ambition or meaning seems quite far away. My half-heart aches.",
    effects: { "mariner.experiences.rattling.standby": 1 },
    mutations: [
      {
        filter: "mariner.ship",
        mutate: "mariner.ship.damage",
        level: 1,
        additive: true,
      },
    ],
    linked: [{ id: "mariner.sailing.postevent" }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "A Scrape on the Bow",
    startdescription:
      "A horrid groan, and our worries were proven true. We had guessed our location incorrectly, or the sandbanks shifted further than we thought. Now we are oriented again, though our ship's hull bleeds.",
    mutations: [
      {
        filter: "mariner.ship",
        mutate: "mariner.ship.damage",
        level: 1,
        additive: true,
      },
    ],
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
