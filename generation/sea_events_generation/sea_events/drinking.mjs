import { SEA_EVENT_SLOTS } from "../../slots_helpers.mjs";
import { BASE_SUCCESS_CHANCE } from "../generate_seaevents.mjs";
import { minChance, generateScaffholding } from "./sea_events_helpers.mjs";

export function generatedrinkingEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("drinking");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "Drunken Mates",
    startdescription:
      "It is known that sailors like a drink when they are on shore, but when that behavior follows the ship to sea, trouble comes in its wake. My ship stinks of gin and malcontent. I could solve this with stern authority of Winter or the opposition of Edge.",
    slots: SEA_EVENT_SLOTS(),
    linked: [
      { id: `${PREFIX}.outcome.success`, chance: minChance("edge") },
      { id: `${PREFIX}.outcome.success`, chance: minChance("winter") },
      { id: `${PREFIX}.outcome.success`, chance: BASE_SUCCESS_CHANCE },
      { id: `${PREFIX}.outcome.drinkingfailure` },
    ],
  });

  // Outcomes
  generateSimpleSuccessOutcome({
    label: "The Specter of Discipline",
    startdescription:
      "I have smashed every bottle I could find, and have made those responsible pour over the deck for the shattered glass. They may despise me, but the ship will run with military precision.",
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.drinkingfailure`,
    warmup: 20,
    label: "Spirits on Deck",
    startdescription:
      "They may nod along to my speeches, but they all have a bottle stashed for their shifts. The discipline has been broken, and it will take time to rebuild it.",
    furthermore: [
      {
        target: "~/tabletop",
        mutations: {
          "[mariner.crew] && [mariner.crew.traits.boozy] && [mariner.crew.exhausted]<1": {
            mutate: "mariner.crew.exhausted",
            level: 5,
            limit: 30,
          },
        },
      },
      {
        target: "~/tabletop",
        mutations: {
          "[mariner.crew] && [mariner.crew.exhausted]<1": {
            mutate: "mariner.crew.exhausted",
            level: 5,
            limit: 3,
          },
        },
      },
    ],
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
