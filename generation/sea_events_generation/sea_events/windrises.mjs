import { SIGNALS } from "../../generate_signal_aspects.mjs";
import { DISTANCE_SCALED } from "../../sea_generation/generate_sailing_routes.mjs";
import { SHIP_MANOEUVERING_SLOTS } from "../../slots_helpers.mjs";
import { BASE_SUCCESS_CHANCE } from "../generate_seaevents.mjs";
import { generateScaffholding, minChance } from "./sea_events_helpers.mjs";

export function generateWindRisesEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("windrises");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "The Wind Rises",
    startdescription:
      "The flag stands straight horizontal, and a constant wind tempts the kite to turn its way. We need to tame this weather with Forge, or survive it with Heart.",
    slots: SHIP_MANOEUVERING_SLOTS(),
    linked: [
      {
        id: `${PREFIX}.highwindcheck.forge`,
        chance: minChance("forge", 10),
      },
      {
        id: `${PREFIX}.highwindcheck.heart`,
        chance: minChance("heart", 10),
      },
      { id: `${PREFIX}.outcome.success`, chance: minChance("forge") },
      { id: `${PREFIX}.outcome.success`, chance: minChance("heart") },
      { id: `${PREFIX}.outcome.success`, chance: BASE_SUCCESS_CHANCE },
      { id: `${PREFIX}.outcome.failure` },
    ],
  });

  // Critical situation check
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.highwindcheck.forge`,
    tablereqs: { "mariner.weathermeta.windy": 1 },
    linked: [{ id: `${PREFIX}.outcome.criticalsuccess` }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.highwindcheck.heart`,
    tablereqs: { "mariner.weathermeta.windy": 1 },
    linked: [{ id: `${PREFIX}.outcome.criticalsuccess` }],
  });

  // Outcomes
  generateSimpleSuccessOutcome({
    label: "Control the Vessel",
    startdescription:
      "The wind plays with us, but we manoeuvre, turn, dance and fight against it. It will not cost us any time on our travel.",
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.criticalsuccess`,
    warmup: 20,
    label: "Strong Tailwind",
    startdescription:
      "We leash the wind, and let it serve us instead. With this we will arrive at our destination sooner than expected.",
    effects: { "mariner.sailing.distance": -DISTANCE_SCALED(0.8) },
    linked: [{ id: "mariner.sailing.postevent" }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Blown off Course",
    startdescription: "We fight against the wind, but at some point we have to bend, and give ourselves.",
    effects: { "mariner.sailing.distance": DISTANCE_SCALED(0.8) },
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
