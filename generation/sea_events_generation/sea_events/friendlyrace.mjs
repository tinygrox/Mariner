import { DISTANCE_SCALED } from "../../sea_generation/generate_sailing_routes.mjs";
import { SEA_EVENT_SLOTS } from "../../slots_helpers.mjs";
import { BASE_SUCCESS_CHANCE } from "../generate_seaevents.mjs";
import { generateScaffholding, minChance } from "./sea_events_helpers.mjs";

export function generateFriendlyRaceEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("friendlyrace");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "A Friendly Race",
    startdescription:
      "Another vessel sails the same course as us. As is natural for us seamen, we decide on a friendly competition, to test our skills. [Approach this challenge with Heart or Edge.]",
    slots: SEA_EVENT_SLOTS(),
    linked: [
      { id: `${PREFIX}.outcome.windsuccess` },
      { id: `${PREFIX}.outcome.success`, chance: minChance("edge") },
      { id: `${PREFIX}.outcome.success`, chance: minChance("heart") },
      { id: `${PREFIX}.outcome.success`, chance: BASE_SUCCESS_CHANCE },
      { id: `${PREFIX}.outcome.terriblefailure`, chance: 10 },
      { id: `${PREFIX}.outcome.failure` },
    ],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.windsuccess`,
    warmup: 20,
    tablereqs: { "mariner.weathermeta.windy": 1 },
    label: "Victory and Reward",
    startdescription:
      "The competition pushes our efforts, and the wind rewards us. We sail the distance in half the time expected.",
    effects: { "mariner.sailing.distancetravelled": DISTANCE_SCALED(1), "mariner.thrill": 1 },
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  generateSimpleSuccessOutcome({
    label: "Victory!",
    startdescription:
      "We surpass our rivals, and leave them in our wake. They leave us with visceral emotions however, known to hunters, sailors and adventurers alike.",
    effects: { "mariner.thrill": 1 },
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.terriblefailure`,
    warmup: 20,
    label: "A Crash!",
    startdescription:
      "All efforts focussed on the race, both parties do not wish to give eachother the space to make their turn. Our ships graze eachother before pulling apart. While the crew flings insults, the captains simply share a look of pained embarrassment.",
    mutations: [
      {
        filter: "mariner.ship",
        mutate: "mariner.ship.damage",
        level: 1,
        additive: true,
      },
    ],
    linked: [{ id: "mariner.sailing.postevent" }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Second Place",
    startdescription:
      "We give it our all, but our competors edge out the distance. As we trail behind, they wave their good byes from the bow, and we give them a joyous salute.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
