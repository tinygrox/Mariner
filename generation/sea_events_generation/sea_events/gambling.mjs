import { BASE_SUCCESS_CHANCE } from "../generate_seaevents.mjs";
import { generateScaffholding, minChance } from "./sea_events_helpers.mjs";

export function generateGamblingEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("gambling");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "Gambling on Deck",
    startdescription:
      "The rattle of the dice are separated with bouts of laughter and shouts of despair. I should reestablish discipline with Edge or Winter before this ends in blows, or I could put in some Funds and try to earn some money of my crew.",
    slots: [
      {
        id: `help1`,
        label: "Help",
        required: {
          tool: 1,
          "mariner.song": 1,
          "mariner.crew": 1,
          "mariner.weather": 1,
        },
        forbidden: {
          "mariner.crew.exhausted": 1,
          "mariner.tool.broken": 1,
        },
      },
      {
        id: `help2`,
        label: "Help",
        required: {
          tool: 1,
          "mariner.song": 1,
          "mariner.crew": 1,
          "mariner.weather": 1,
        },
        forbidden: {
          "mariner.crew.exhausted": 1,
          "mariner.tool.broken": 1,
        },
      },
      {
        id: `help3`,
        label: "Help or Stakes",
        required: {
          tool: 1,
          "mariner.song": 1,
          "mariner.crew": 1,
          "mariner.weather": 1,
          funds: 1,
        },
        forbidden: {
          "mariner.crew.exhausted": 1,
          "mariner.tool.broken": 1,
        },
      },
    ],
    linked: [
      { id: `${PREFIX}.outcome.successgambling`, chance: 50 },
      { id: `${PREFIX}.outcome.failuregambling` },
      { id: `${PREFIX}.outcome.success`, chance: minChance("edge") },
      { id: `${PREFIX}.outcome.success`, chance: minChance("winter") },
      { id: `${PREFIX}.outcome.success`, chance: BASE_SUCCESS_CHANCE },
      { id: `${PREFIX}.outcome.failure` },
    ],
  });

  // Outcomes
  generateSimpleSuccessOutcome({
    label: "The Silence of Discipline",
    startdescription: "The dice thrown overboard and the cards burned to cinders. Order has returned to decks of my ship.",
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "A Clamor and Row",
    startdescription:
      "At first it is just the shuffling cards and tick-tack of ceramic on deck. But after that comes the exchange of coins, or worse, an exchange of blows. ",
    furthermore: [
      {
        target: "~/exterior",
        mutations: {
          "[mariner.crew] && [mariner.crew.exhausted]<1": {
            mutate: "mariner.crew.exhausted",
            level: 5,
            limit: 3,
          },
        },
      },
    ],
    linked: [{ id: "mariner.sailing.postevent" }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.successgambling`,
    warmup: 20,
    requirements: { funds: 1 },
    label: "Play for Keeps",
    startdescription: "I join in on the game, like officers ought not to do, but leaders sometimes must.",
    effects: { funds: 2 },
    linked: [{ id: "mariner.sailing.postevent" }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failuregambling`,
    warmup: 20,
    requirements: { funds: 1 },
    label: "Sour Luck",
    startdescription:
      "I play a few rounds, and lose each one. My crew will smile at my face for a long while now, and snicker behind my back. But the air has passed.",
    effects: { funds: -1 },
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
