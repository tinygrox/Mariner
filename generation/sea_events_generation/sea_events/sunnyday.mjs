import { generateScaffholding } from "./sea_events_helpers.mjs";

export function generateSunnyDayEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("sunnyday");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "A Pleasant Journey",
    tablereqs: { "mariner.weathermeta.stormy": -1, "mariner.weathermeta.sickly": -1, "mariner.weathermeta.sandstorm": 1 },
    startdescription: "The winds are good, the sea is calm, there is nothing pressing to do except enjoy the journey.",
    linked: [{ id: `${PREFIX}.outcome.sunsuccess` }, { id: `${PREFIX}.outcome.success` }],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.sunsuccess`,
    warmup: 20,
    tablereqs: { "mariner.weathermeta.sunny": 1 },
    label: "A Golden Afternoon",
    startdescription:
      "The sunlight casts the sea with diamonds, glittering on the cresting waves. The sea breeze steady in the sails, and cooling on the skin. Joy it is said, is hard to recall, but you will treasure this feeling in your heart a little while.",
    effects: { contentment: 1 },
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.success`,
    warmup: 20,
    label: "Pleasant Travels",
    startdescription: "Sometimes the journey passing is so uneventful, there is nothing left to do except enjoy yourself.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
