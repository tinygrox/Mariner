import { generateScaffholding, minChance } from "../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../slots_helpers.mjs";
export function generateNemesisSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("nemesis");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "Nemesis Crux",
    jumpstart: true,
    tablereqs: { "mariner.weathermeta.stormy": 1 },
    startdescription: "On certain days all is battle. Then hot air, incongruous with the cold, leaves the clouds themselves ripe with muzzle flash and mortar strike. Gulls battle the winds below, and lower still, the waves shift in an infinite battle field of rise and fall. With enough Edge I can challenge shackling fate itself, and have an Encounter with a nemesis. if I do not direct this energy, the battle is bound to break out on the ship itself.",
    linked: [{ id: `${PREFIX}.outcome.firstoutcome`, chance: minChance("edge", 10) }, { id: `${PREFIX}.outcome.failure` }],
    slots: SEA_EVENT_SLOTS()
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.firstoutcome`,
    warmup: 20,
    tablereqs: {},
    label: "Fated Meeting",
    startdescription: "When the Colonel has set a design, many are trapped in its lines, when the Lionsmith forges a course, much is pushed in disarray, Our encounter here may have been willed.",
    linked: [{ id: "mariner.sailing.postevent" }], //link to encounter with the Nemesis
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Gnaw At Its Tail", //todo broken
    startdescription: "The gulls cries ring with echoes of wolfs howls, and my crew prowls, ready to devour itself.",
    linked: [{ id: "mariner.sailing.postevent" }],
    furthermore: [
      {
        target: "~/exterior",
        mutations: {
          "[mariner.crew] && [mariner.crew.traits.boisterous] && [mariner.crew.exhausted]<1": {
            mutate: "mariner.crew.exhausted",
            level: 5,
            limit: 30,
          },
        },
      },
      {
        target: "~/exterior",
        mutations: {
          "[mariner.crew] && [mariner.crew.exhausted]<1": {
            mutate: "mariner.crew.exhausted",
            level: 5,
            limit: 2,
          },
        },
      },
    ],
  });
}
