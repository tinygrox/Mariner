import { generateScaffholding } from "../sea_events_helpers.mjs";

export function generateBleedingSunsetSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("bleedingsunset");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    jumpstart: true,
    label: "A Bleeding Sunset",
    startdescription:
      "The sun hangs red and low and as it graces the waves, it start to bleed. Every lapping wave sips a little more of the red, and every lap pales the sun a little futher. An Ending is assured, and an impossible choice lays at my feet. Unless I wish to divest my self of choice, but regardless, sunset will take its toll.",
    linked: [{ id: `${PREFIX}.outcome.choice` }, { id: `${PREFIX}.outcome.nochoice` }],
    slots: [
      {
        id: "victim",
        label: "Victim",
        description: "The Sun must have its toll. if I do not pick any, it will pick for me.",
        required: { "mariner.crew": 1 },
      },
    ],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.choice`,
    warmup: 20,
    requirements: { "mariner.crew": 1 },
    label: "A Beautiful Ending",
    startdescription: "like a bellstrike; \n a resounding silence now \n marred by one dull hit",
    effects: { "mariner.crew": -1 },
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.nochoice`,
    warmup: 20,
    requirements: { "mariner.crew": -1 },
    label: "A Beautiful Ending",
    startdescription: "like a bellstrike; \n a resounding silence now \n marred by one dull hit",
    slots: [
      {
        id: "victim",
        label: "Victim",
        description: "The Sun must have its toll. if I do not pick any, it will pick for me.",
        required: { "mariner.crew": 1 },
        greedy: true,
      },
    ],
    linked: [{ id: `${PREFIX}.outcome.death` }, { id: `${PREFIX}.outcome.choice` }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.death`,
    warmup: 20,
    requirements: { "mariner.crew": -1 },
    label: "A Beautiful Ending",
    startdescription: "No one to stand in, and the Sunset will have its price. The day is cold, and beautiful, and final.",
    ending: "deathofthebody",
    signalendingflavour: "Melancholy",
  });
}
