import { generateScaffholding, minChance } from "../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../slots_helpers.mjs";

export function generateOperationLeadEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("operation_lead");

  mod.initializeAspectFile("aspects.seaevents", ["sailing", "seaevents"]);
  mod.setHiddenAspect("aspects.seaevents", { id: `mariner.flag.operation.lead.set` });
  mod.setHiddenAspect("aspects.seaevents", { id: `mariner.flag.operation.silver.set` });
  mod.setHiddenAspect("aspects.seaevents", { id: `mariner.flag.operation.gold.set` });

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: {
      "root/mariner.flag.operation.lead.set": -1,
      "~/exterior : mariner.weathermeta.stormy": 1,
    },
    label: "The Operations of Sky: The Lead Heaven",
    startdescription:
      "Homer called the heavens iron, though tonight she is formed of a duller metal. Great clouds are stacked in agitated discontentment. Horomachisters and Ithastrists alike aknowledge the power kept there in potentia. If I expand a Trapping of Edge and sufficient Forge or Moth, I could stir this potential into a higher state, the next time I encounter a potent sky.",
    linked: [
      { id: `${PREFIX}.outcome.success`, chance: minChance("edge", 6) },
      { id: `${PREFIX}.outcome.success`, chance: minChance("forge", 6) },
      { id: `${PREFIX}.outcome.failure` },
    ],
    slots: SEA_EVENT_SLOTS(),
  });

  // Outcomes
  generateSimpleSuccessOutcome({
    label: "The Stirring of Potential",
    startdescription: "Black yields to silver, as the night yields to dawn and a storm clearing leaves the air crisp and fresh.",
    furthermore: [
      {
        rootAdd: {
          "mariner.flag.operation.lead.set": 1,
        },
      },
    ],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Baser Weathers",
    startdescription:
      "We could not meet the demands to wield the airs of potential. The pressure shifts, the moment fades, and we drift back into less leaden weather patterns.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
