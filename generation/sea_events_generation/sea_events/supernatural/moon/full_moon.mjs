import { generateScaffholding, minChance } from "../../sea_events_helpers.mjs";

export function generateFullMoonsEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("full_moon");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: { "~/exterior : mariner.moon.basic.5": 1 },
    jumpstart: true,
    label: "<Full Moon Rising>",
    startdescription:
      "On a cloudless full moon night, one might say that it is bright as day outside. But nothing so garish is true. It's not quite a kinder light, nor a colder one, but it is the opposite of cruel and hot. Be enlightened. Savor it. Bath in it, dance with its pale beams as a partner or let it guide you in lunacy. Let this night of nights be the succor that replenishes the toll of countless days.",
    linked: [
      { id: `${PREFIX}.outcome.simple` }, //waits on implimentaiton of song levels
    ],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.simple`,
    warmup: 20,
    label: "From Whom we do not Turn",
    startdescription: "<What is worn away by days can be recovered in one perfect night>",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
