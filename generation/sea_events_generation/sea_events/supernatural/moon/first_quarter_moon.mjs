import { generateScaffholding, minChance } from "../../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../../slots_helpers.mjs";

export function generatefirstMoonsEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("first_moon");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: { "~/exterior : mariner.moon.basic.1": 3 },
    jumpstart: { effects: { "mariner.influences.moth": 10 } },
    label: "<Twins' Moon Rising>",
    startdescription:
      "Tonight the moon calls for companionship, and thus she stirs those passions within the lesser hearts of mortals. Homesickness, sailors' plight, might reach a fevers pitch. however, others may find the call of the ocean pining for her sisters pale kiss stir in them. If I am lucky, a passenger may deeper longing stir and wish to join my crew, with sufficient Moth or Grail.",
    linked: [
      { id: `${PREFIX}.outcome.action`, chance: minChance("grail", 10) },
      { id: `${PREFIX}.outcome.action`, chance: minChance("moth", 10) },
      { id: `${PREFIX}.outcome.inaction` },
    ],
    slots: SEA_EVENT_SLOTS(),
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.action`,
    warmup: 20,
    label: "<Action>",
    startdescription:
      "<The longing for the human to the sea is an old one, burried deep in most of us. A sudden transformation has overcome come one of my sailors, and now they realized this wish a life at the sea now more.>",
    requirements: { "mariner.passenger": 1 },
    effects: { "mariner.passenger": -1 },
    linked: [
      { id: `${PREFIX}.outcome.basic`, chance: 50 },
      { id: `${PREFIX}.outcome.manual`, chance: 50 },
      { id: `${PREFIX}.outcome.officer` },
    ],
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: "mariner.crew",
            mutate: "mariner.crew.longing",
            level: "Random(0, 1) * 10",
            additive: false,
          },
        ],
      },
    ],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.basic`,
    warmup: 20,
    linked: [{ id: "mariner.sailing.postevent" }],
    effects: { "mariner.crew.basic.opportunity.free": 1 },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.manual`,
    warmup: 20,
    linked: [{ id: "mariner.sailing.postevent" }],
    deckeffects: {
      "mariner.decks.specialists.opportunities.manual.free": 1,
    },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.officer`,
    warmup: 20,
    linked: [{ id: "mariner.sailing.postevent" }],
    deckeffects: {
      "mariner.decks.specialists.opportunities.intellectual.free": 1,
    },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.inaction`,
    warmup: 20,
    label: "Inaction",
    startdescription:
      "<The ocean pushes, the moon pulls. Adventure draws, but home is an aching tether. All will find themselves swayed towards where their hearts true allegiances lie.>",
    linked: [{ id: "mariner.sailing.postevent" }],
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: "mariner.crew",
            mutate: "mariner.crew.longing",
            level: "Random(0, 1) * 10",
            additive: false,
          },
        ],
      },
    ],
  });
}
