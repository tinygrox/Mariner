import { SEA_EVENT_SLOTS } from "../../../../slots_helpers.mjs";
import { generateScaffholding, minChance } from "../../sea_events_helpers.mjs";

export function generateThirdMoonsEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("third_moon");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: { "~/exterior : mariner.moon.basic.7": 1 },
    jumpstart: { effects: { "mariner.influences.winter": 10 } },
    label: "<Mirror Moon Rising>",
    startdescription:
      "Tonight in between the twin infinities of the sea and the sky, below the mirror-bladed third quarter moon, our ship might meet her mirror self, strayed in from an adjacant history. As we crest the surface, the image will scramble, and when it becomes clear again, our ship may resemble another configuration. If we wish to cling to this reality, we must armor ourself with sufficient Winter, else we will see how the Kite meets us in the morning.",
    linked: [{ id: `${PREFIX}.outcome.success`, chance: minChance("winter", 10) }, { id: `${PREFIX}.outcome.failure` }],
    slots: SEA_EVENT_SLOTS(),
  });

  // Outcomes
  generateSimpleSuccessOutcome({
    label: "<Rejected>",
    startdescription: "<A moment of power was here, and now that moment is history. We sail on.>",
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Reflected",
    startdescription:
      "<One may wonder in the morning, was this always like this? Fools say it wasn't, the learned know it is, and adepts know it never was.>",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
