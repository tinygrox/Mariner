import { generateScaffholding, minChance } from "../../sea_events_helpers.mjs";

export function generateGluttonsMoonsEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("gluttons_moon");

  mod.setRecipe(RECIPES_FILE, {
    grandReqs: { "~/exterior : mariner.moon.basic.4": 1 },
    jumpstart: { effects: { "mariner.rations": 1 } },
    id: EVENT_START,
    warmup: 20,
    label: "<Glutton's Moon Rising>",
    startdescription:
      "How mortal passions stir under the waxing Gibbous Moon is a matter of folklore and almanacs, however, how it stirs hunger in immortals is a certainty. If I provide my crew a fecund feast, something new may bud and blossom. But I will have to beware of the stirring hunger of spirits tonight.",
    linked: [{ id: `${PREFIX}.outcome.success` }, { id: `${PREFIX}.outcome.failure` }],
    slots: [
      {
        id: "rations",
        label: "Rations",
        description: "A feast to celebrate the glutting moon",
        required: {
          "mariner.rations": 1,
        },
      },
    ],
  });

  // Outcomes
  generateSimpleSuccessOutcome({
    label: "<Feast>",
    requirements: { "mariner.rations": 1 },
    startdescription: "<Satisfaction is rarely a gluttons reward, but new dimensions follow new appetitetes.>",
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: "mariner.crew",
            mutate: "grail",
            level: 2,
            limit: 2,
          },
        ],
      },
    ],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Famine",
    startdescription: "<Hunger too, is an altering sensation. Cravings bloom under a Glutton moon>",
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: "mariner.crew",
            mutate: "mariner.crew.traits.cannibal",
            level: 1,
            limit: 3,
            additive: false,
          },
        ],
      },
    ],
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
