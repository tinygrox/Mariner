import { generateScaffholding, minChance } from "../../sea_events_helpers.mjs";
import { SIGNALS } from "../../../../generate_signal_aspects.mjs";
import { SEA_EVENT_SLOTS } from "../../../../slots_helpers.mjs";

export function generateChangelingMoonsEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("changeling_moon");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: { "~/exterior : mariner.moon.basic.1": 2 },
    jumpstart: { effects: { "mariner.influences.moth": 10 } },
    label: "<Changeling's Moon Rising>",
    startdescription:
      "The moon bears many faces, but perhaps in that she is a mask shared and shackled. Under a Changeling Moon it may be possible to rid oneself of past guises like a centipede sheds her skin. With enough Moth channeled at a willing participant they may come to shed the old and don a new persona.",
    linked: [{ id: `${PREFIX}.outcome.disrobe`, chance: minChance("moth", 10) }, { id: `${PREFIX}.outcome.failure` }],
    slots: SEA_EVENT_SLOTS(),
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.disrobe`,
    warmup: 20,
    label: "<Guise Disrobbed>",
    startdescription:
      "<Saint Columba, allegedly, suggests a bath in molten snow to free yourself from your past, but saint Nympha suggests one only needs to bath in moonlight. One of my crew has peeled of a layer of themselves and found something new beneath.>", //change how traits are assigned so we can do a a reassign. if we simply add a recipe clearing all traits before they are assigned, we can call the same rescipe again to reassign traits. (this would enable clearing ... of fading crewmembers)
    furthermore: [
      {
        mutations: {
          "mariner.crew": {
            mutate: "mariner.crew.exhausted",
            level: 5,
          },
        },
      },
      { movements: { "~/tabletop": ["mariner.crew"] } },
    ],
    linked: [{ id: `${PREFIX}.outcome.select` }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.select`,
    warmup: 20,
    label: "<Target>",
    startdescription: "<Someone to change>", //change how traits are assigned so we can do a a reassign. if we simply add a recipe clearing all traits before they are assigned, we can call the same rescipe again to reassign traits. (this would enable clearing ... of fading crewmembers)
    slots: [
      {
        id: "target",
        label: "Crewmember",
        description: "Someone to change",
        greedy: true,
        required: { "mariner.crew": 1 },
      },
    ],
    linked: [{ id: `${PREFIX}.outcome.wipe` }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.wipe`,
    aspects: { [SIGNALS.WIPE_ALL_TRAITS]: 1 },
    linked: [{ id: "mariner.assigntraitstocrew.start" }],
    addCallbacks: {
      RETURN_FROM_ASSIGN_TRAITS_TO_CREW: "mariner.sailing.postevent",
    },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "This Too Passes",
    startdescription: "<A moment of power was here, and now that moment is history. we sail on.>",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
