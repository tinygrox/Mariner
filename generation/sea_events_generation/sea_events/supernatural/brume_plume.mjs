import { generateScaffholding, minChance } from "../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../slots_helpers.mjs";
import { SIGNALS } from "../../../generate_signal_aspects.mjs";

export function generateBrumePlumeSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("brumeplume");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "Brume Plume",
    tablereqs: { "mariner.weathermeta.misty": 1, "mariner.weathermeta.numa": -1 },
    startdescription: "A waft of mist that smells of potential has drifted onto our path. A memory of a season not often seen in this world. The unlight, where it touches the skyline, paints new vista's in shades of mystery, delight and promise. Do we steer into this weather phenominon, and enjoy all the advantages the Ninth season offers and attempt to navigate into the unkown with Moth or Knock>?",
    linked: [{
      id: `${PREFIX}.outcome.firstoutcome`, chance: minChance("moth", 7)
    },
    { id: `${PREFIX}.outcome.firstoutcome`, chance: minChance("knock", 7) },
    { id: `${PREFIX}.outcome.failure` }],
    slots: SEA_EVENT_SLOTS(),
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.firstoutcome`,
    warmup: 20,
    label: "Rosewards; Moonways",
    startdescription: "With an unnamed wind in our backs, we sail an undiscovered degree towards a forgotten lattitude.",
    furthermore: [
      {
        target: "~/exterior",
        aspects: { [SIGNALS.BRUME_SET_NUMA]: 1 },
      },
    ],
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Rejected",
    startdescription: "A moment of power was here, and now that moment is history. we sail on.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
