import { generateScaffholding, minChance } from "../../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../../slots_helpers.mjs";
import { DISTANCE_SCALED } from "../../../../sea_generation/generate_sailing_routes.mjs";

export function generateShipOfKnockEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } =
    generateScaffholding("ShipOfKnock");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: {
      "[~/exterior : { [mariner.crew] } : knock] + [ ~/local: { [mariner.ship.kite] } : knock]": 10,
    },
    label: "<Ship of Dreams>",
    startdescription:
      "These nights the moon has been devoured until there isn't a sliver left and we sail in complete darkness. With no celestial observer and no land in sight we sway in crucim between anuwhere and Nowhere. We could cling to the skin of the world we know with sufficient heart, or catch the currents the sea conjures when the moon is absent.",
    linked: [{ id: `${PREFIX}.outcome.end` }],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.end`,
    warmup: 20,
    label: "<Ship of Dreams: Serpents at the Treshold>",
    startdescription: "<>",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
