import { generateScaffholding, minChance } from "../../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../../slots_helpers.mjs";
import { DISTANCE_SCALED } from "../../../../sea_generation/generate_sailing_routes.mjs";

export function generateShipOfMothEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } =
    generateScaffholding("ShipOfMoth");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: {
      "[~/exterior : { [mariner.crew] } : moth] + [ ~/local: { [mariner.ship.kite] } : moth]": 10,
    },
    label: "<Crecuspular Ship>",
    startdescription: "On wings on canvas we glide through the night...",
    linked: [{ id: `${PREFIX}.outcome.end` }],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.end`,
    warmup: 20,
    label: "Crecuspular Ship: Fly into the night",
    startdescription:
      "<On wings on canvas we glide through the night,  each contemplating the change wrought form each landing and each departure. it is not only theseus ship that was slowly changed, it's crew too sheds perceptions, truths, aspirations, lies, with each new experience displaying one before. in this night, we are renewed.>",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
