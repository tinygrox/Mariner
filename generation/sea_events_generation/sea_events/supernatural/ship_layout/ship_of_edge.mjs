import { generateScaffholding, minChance } from "../../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../../slots_helpers.mjs";
import { DISTANCE_SCALED } from "../../../../sea_generation/generate_sailing_routes.mjs";

export function generateShipofEdgeEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } =
    generateScaffholding("ShipOfEdge");


  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: {
      "[~/exterior : { [mariner.crew] } : edge] + [~/local : { [mariner.ship.kite] } : edge]": 10,
      "[~/exterior : mariner.crew.traits.rival] ": -1,

    },
    label: "<Ship of War>",
    startdescription: "<Tight bonds shatter in the strongest effect...>",
    linked: [{ id: `${PREFIX}.outcome.end` }],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.end`,
    warmup: 20,
    label: "<Ship of War: The Sustenance of Emnity>",
    startdescription:
      "<Tight bonds shatter in the strongest effect, and betrayal is as old as the sea, twice as cruel and thrice as bitter. Two among my crew bonds have been reforged into hatred. They have been sharepened by hatred, and now burn hot or simmer cold. The lines are drawn, an invisible border across the ship. Two of my crew have been elevated by their enmity. Now, unless I balance their loyalty and rivalry on a knives edge, I may one or both of them before the next port. >",
    linked: [{ id: "mariner.sailing.postevent" }],
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: "mariner.mortal",
            mutate: "mariner.crew.traits.rival",
            limit: 2,
            level: 1,
            additive: true,
          },
        ],
      },
    ],
  });
}
