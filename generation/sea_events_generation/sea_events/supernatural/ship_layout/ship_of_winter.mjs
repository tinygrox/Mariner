import { generateScaffholding, minChance } from "../../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../../slots_helpers.mjs";
import { DISTANCE_SCALED } from "../../../../sea_generation/generate_sailing_routes.mjs";

export function generateShipOfWinterEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } =
    generateScaffholding("ShipOfWinter");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: {
      "[~/exterior : { [mariner.crew] } : moth] + [ ~/local: { [mariner.ship.kite] } : moth]": 10,
    },
    label: "<The Patient Ship>",
    startdescription: "Time on board can become a crawl...",
    linked: [{ id: `${PREFIX}.outcome.end` }],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.end`,
    warmup: 20,
    label: "The Patient Ship: Each Journey A Moment",
    startdescription:
      "<Time on board can become a crawl as change itself fears to disturb the captains discipline. misfortune quiets, homesickness is smothered uder the weight of pallid acceptance. Our end may come, onde day, but until that day my crew fate is shackled to mine. On a journey like this one, the call of the shore quiets like a shout through a showflurry, and I will not lose my crew to longing.>",
    linked: [{ id: "mariner.sailing.postevent" }],
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: "mariner.crew",
            mutate: "mariner.crew.longing",
            limit: 20,
            level: -2,
            additive: true,
          },
        ],
      },
    ],
  });
}
