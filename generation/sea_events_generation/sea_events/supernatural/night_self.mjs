import { generateScaffholding } from "../sea_events_helpers.mjs";
import { SIGNALS } from "../../../generate_signal_aspects.mjs";

export function generateNightSelfSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("nightself");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "His Night Self",
    startdescription:
      "Even at night, when the sun cannot grace the sky, his proxy still ensures our deference. As the reflection rises through the sky as inconstant as his eternity, toonight, the reflections double stil, as the moon dances on the ocean.",
    linked: [{ id: `${PREFIX}.outcome.firstoutcome` }],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.firstoutcome`,
    warmup: 20,
    label: "What is Within, Without",
    startdescription:
      "The moons face is marred, and thus, so are we. what we attempt to bottle up inside bubbles out, and that too doubles beneath the stars.",
    furthermore: [
      {
        target: "~/exterior",
        aspects: { [SIGNALS.DUPLICATE_EXPERIENCES]: 1 },
      },
    ],
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
