import { generateScaffholding } from "../sea_events_helpers.mjs";
import { CALL_FUNCTION } from "../../../generate_function_calls.mjs";

export function generatePromiseofDawnEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("promiseofdawn");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "The Promise of Dawn",
    startdescription:
      "<The darkness is deepest in the Hour before dawn, but there lies also strongest promise of light. On this morning I wait for the fading of the stars and the peaking light of another day.>",
    linked: [{ id: `${PREFIX}.outcome.firstoutcome` }],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.firstoutcome`,
    warmup: 20,
    label: "Normalcy Continued",
    startdescription:
      "It has been said that struggle is the engine of the world, or that an invisible heartbeat pounds behind the sky. But there is one hour who is said to keep a measure of our heartbeats, and she too resolves that the sun will rise.",
    furthermore: CALL_FUNCTION("addgroundedness.30"),
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
