import { generateScaffholding, minChance } from "../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../slots_helpers.mjs";

export function generateOperationSilverEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("operation_silver");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: {
      "root/mariner.flag.operation.silver.set": -1,
      "root/mariner.flag.operation.lead.set": 1,
      "~/exterior : mariner.weathermeta.rainy": 1,
    },
    label: "The Operations of the Sky: That Silver Gale",
    startdescription:
      "Homer called the heavens copper, but tonight she is formed of a paler metal. Rain comes down, not as a whisper, nor a torrent, but with the intent of knives. None of the crew relishes their jobs on deck today, but I can feel that the sky once again holds that power in potentia. With a trapping of Moth and a significant power of Lantern or Forge I could brighten her, and elevate the next time I encounter the Operations of the Sky.",
    linked: [
      { id: `${PREFIX}.outcome.success`, chance: minChance("lantern", 7) },
      { id: `${PREFIX}.outcome.success`, chance: minChance("forge", 7) },
      { id: `${PREFIX}.outcome.failure` },
    ],
    slots: SEA_EVENT_SLOTS(),
  });

  // Outcomes
  generateSimpleSuccessOutcome({
    label: "The Elevation of Potential",
    startdescription: "Silver yields to gold, as past always yields to future and worth is only refined in greed or in strife.",
    furthermore: [
      {
        rootAdd: {
          "mariner.flag.operation.silver.set": 1,
        },
      },
    ],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Baser Weathers",
    startdescription:
      "We could not meet the demands to wield the airs of potential. The pressure shifts, the moment fades, and we drift back into less leaden weather patterns.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
