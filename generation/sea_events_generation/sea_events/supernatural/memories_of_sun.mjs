import { generateScaffholding, minChance } from "../sea_events_helpers.mjs";

export function generateMemoriesOfSunSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("memoriesofthesun");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "Memories of the Sun",
    startdescription:
      "The sky holds is breath as the sun's parabolic arc is chased by the shallower course of her counterpart on the waters canvas below. Then, at its perfect zenith, it vanishes from the sky. Only the reflection remains. The sky shuddders in loss, and we must shield our soul with Heart or Lantern less we shatter along with it.",
    linked: [
      { id: `${PREFIX}.outcome.firstoutcome`, chance: minChance("lantern", 7) },
      { id: `${PREFIX}.outcome.firstoutcome`, chance: minChance("heart", 7) },
      { id: `${PREFIX}.outcome.failure` },
    ],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.firstoutcome`,
    warmup: 20,
    tablereqs: {},
    label: "Much is lost, Much abides",
    startdescription: "We regain what we have, and sail back into our flat world under our counterfeit sun.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Scar in the Sky",
    startdescription:
      "Three of the crew drop, eyes up at the sky, staring at the gap of the sun. we can feel it everyday that something is missing, but on this strange tide, we are confronted with it. my lack heart too, is bleeding, echoing with the thrumming lack.",
    linked: [{ id: "mariner.sailing.postevent" }],
    furthermore: [
      {
        target: "~/exterior",
        mutations: {
          "[mariner.crew] && [mariner.crew.exhausted]<1": {
            mutate: "mariner.crew.exhausted",
            level: 5,
            limit: 3,
          },
        },
      },
    ],
  });
}
