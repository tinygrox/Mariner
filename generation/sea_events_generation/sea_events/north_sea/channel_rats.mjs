import { generateScaffholding } from "../sea_events_helpers.mjs";
import { SIGNALS } from "../../../generate_signal_aspects.mjs";

export function generateChannelRatsEvent() {
    const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } =
        generateScaffholding("channelrats");

    mod.setRecipe(RECIPES_FILE, {
        id: EVENT_START,
        warmup: 20,
        label: "Channel Rats",
        grandReqs: {
            "~/exterior : mariner.weathermeta.stormy": -1
        },
        startdescription:
            "A mounting pressure has collected in the vast expanse of sky above the vast expance of western ocean. Only the slender neck of the channel is available as an escape. Thorugh that funnel, fallow winds and middling waves gets whipped up into a stampede, and we, mere mortals upon the sea, have to bear its thunder.",
        linked: [
            { id: `${PREFIX}.outcome.success` },
        ],
        furthermore: [
            {
                target: "~/exterior",
                aspects: { [SIGNALS.CHANNELRATS_SET_STORM]: 1 },
            },
        ],
    });

    // Outcomes
    generateSimpleSuccessOutcome({
        label: "Tides in the Sky",
        startdescription:
            "The clouds are the roiling black of crawling vermin. if the wind are their screetches, and the lightning their flickering fangs, in our moments of darkest humor we speculate waht the downpour might be.",
    });
}
