import { generateScaffholding } from "../sea_events_helpers.mjs";
import { SIGNALS } from "../../../generate_signal_aspects.mjs";

export function generateBlizzardEvent() {
    const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } =
        generateScaffholding("blizzard");

    mod.setRecipe(RECIPES_FILE, {
        id: EVENT_START,
        warmup: 20,
        label: "Blizzard",
        grandReqs: {
            "~/exterior : mariner.weathermeta.blizzard": -1
        },
        startdescription:
            "'The coldest winds are not always North winds, but they always Howl' And today, a Howl is rising up in the Arctic, and it's hurrying down to this soupbowl of a sea.",
        linked: [
            { id: `${PREFIX}.outcome.success` },
        ],
        furthermore: [
            {
                target: "~/exterior",
                aspects: { [SIGNALS.BLIZZARD_SET_BLIZZARD]: 1 },
            },
        ],
    });

    // Outcomes
    generateSimpleSuccessOutcome({
        label: "Fury At Sea",
        startdescription:
            "When Winter stirs in Rage, the sailors job becomes the hardest. When the blizzard rages, the fisherman stays home and the trawler remains in harbor.",
    });
}
