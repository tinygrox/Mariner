export function minChance(aspect, value) {
  return `([${aspect}]/${value ?? 7})*100`;
}

function _generateScaffholding(eventType, eventId) {
  // Initial recipe
  const RECIPES_FILE = `recipes.${eventType}.${eventId}`;
  const PREFIX = `mariner.sailing.${eventType}.${eventId}`;
  const EVENT_START = `mariner.sailing.${eventType}.start.${eventId}`;

  mod.initializeRecipeFile(RECIPES_FILE, ["sailing", eventType, eventId]);
  return {
    RECIPES_FILE,
    PREFIX,
    EVENT_START,
    generateSimpleSuccessOutcome(outcome) {
      generateSimpleSuccessOutcome(RECIPES_FILE, PREFIX, outcome);
    },
  };
}

export function generateScaffholding(eventId) {
  return _generateScaffholding("seaevents", eventId);
}

export function generateSpecialScaffholding(eventId) {
  return _generateScaffholding("specialseaevents", eventId);
}

export function generateSimpleSuccessOutcome(recipesFile, recipePrefix, outcome) {
  mod.setRecipe(recipesFile, {
    id: `${recipePrefix}.outcome.success`,
    warmup: 20,
    ...outcome,
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
