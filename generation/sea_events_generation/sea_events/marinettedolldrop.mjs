import { QUEST_FLAG_ID } from "../../global_flags_generation.mjs";
import { generateSpecialScaffholding } from "./sea_events_helpers.mjs";

export function generateMarinetteDropSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateSpecialScaffholding("marinettedolldrop");

  mod.setRecipe(RECIPES_FILE, {
    id: `${EVENT_START}`,
    warmup: 20,
    tablereqs: {
      "mariner.weathermeta.windy": 1,
      "mariner.weathermeta.rainy": 1,
      "mariner.marinettedoll": 1,
    },
    label: "Letting Go of a Gift",
    startdescription:
      "Mast-high waves, deluge-announcing rain, almost dense enough to drown or crush mere mortals. Here, I drop the small doll, offering it to the hungry water. As it hits the surface, the disturbances around it seem to embrace it, cradling its shape, then accompany it down below. A heartbeat later, it is gone.",
    purge: { "mariner.marinettedoll": 1 },
    inductions: [{ id: `${PREFIX}.discovercharacterwound` }],
    linked: [{ id: "mariner.sailing.postevent" }],
    updateCodexEntries: {
      "codex.agdistis": {
        add: ["codex.agdistis.throwndoll"],
      },
    },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.discovercharacterwound`,
    actionId: "discovercharacterwound",
    warmup: 30,
    label: "Catalyst",
    startdescription:
      "As I watch the doll disappear, Agdistis story resonates in my mind. We share this wound: he was trapped, immobile, unable to choose to go backwards of forwards. He needed my help to take this first step. Now I need to set a step of my own.",
    effects: { "mariner.mysteries.howtomendcharacter": 1 },
    slots: [
      {
        id: "temptation",
        label: "Temptation",
        description: "I learned something new about my path.",
        greedy: true,
        required: {
          "mariner.ascension.twins.temptation": 1,
        },
      },
    ],
    linked: [{ id: `${PREFIX}.discovercharacterwound.applyaspect` }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.discovercharacterwound.applyaspect`,
    label: "Catharsis",
    description:
      "Now I know my first step of becoming whole: I need to mend that inside me that grants the capacity for meaningful choice. As always I will turn to the world to teach me. Bards and skalds through the ages will have found the answer, and recorded it in their tales.",
    mutations: {
      "mariner.ascension.twins.temptation": {
        mutate: "mariner.ascension.twins.character.uncovered",
        level: 1,
      },
    },
    rootAdd: {
      [QUEST_FLAG_ID("marinettedoll", "completed")]: 1,
    },
    unlockCodexEntries: ["codex.ascensions.constellate.character"],
  });
}
