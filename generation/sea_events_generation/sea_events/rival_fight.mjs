import { generateScaffholding, minChance } from "./sea_events_helpers.mjs";

export function generateRivalFightEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } =
    generateScaffholding("ShipOfEdge");


  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    grandReqs: {
      "mariner.crew.traits.rival": 2
    },
    label: "<Rivalries Boil Over>",
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: "mariner.crew.traits.rival",
            mutate: "mariner.crew.exhausted",
            limit: 2,
            level: 1,
            additive: true,
          },
        ],
      },
    ],
    linked: [{ id: `${PREFIX}.main` }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "<Rivalries Boil Over>",
    startdescription: "<The Rivals on board have come out to clash. I need to either quell there struggle before it begins with Winter, or make sure to limit the injuries with intervening Heart.>",
    linked: [
      { id: `${PREFIX}.outcome.failure`, chance: 10 },
      { id: `${PREFIX}.outcome.success`, chance: minChance("winter") },
      { id: `${PREFIX}.outcome.success`, chance: minChance("heart") },
      { id: `${PREFIX}.outcome.failure` }
    ],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.success`,
    warmup: 20,
    label: "<Quelled, For Now>",
    startdescription:
      "<The battle has not reached it's final conclusion, and the rivals remain on board a little longer.>",
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "<Concluded, evermore>",
    startdescription:
      "<satisfaction, elation, depression, disappiointment. who can say what prevails in our remaning rivals shadowed art.>",
    linked: [{ id: "mariner.sailing.postevent" }],
    furthermore: [
      {
        effects: {
          "mariner.crew.traits.rival": -1
        }
      },
    ]
  });
}
