import { generateScaffholding, minChance } from "../sea_events_helpers.mjs";

export function generateTouristsEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("stray signals");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "Stray Signals",
    tablereqs: { "mariner.weathermeta.numa": 1 },
    startdescription:
      "A vessel sails in our wake. it has been sailing in our wake for three days, following our course exactly. on the deck are a motly lot flamboyantly dressed. Vageries on their pilgrimage, having caught up with what they see as a highlight of your obseciences. If we engage with grail or lantern, they might share something useful, but probably will just slow us down and attract attention. I can always attempt to shake them with Moth or Edge.",
    linked: [
      { id: `${PREFIX}.outcome.talk`, chance: minChance("lantern") },
      { id: `${PREFIX}.outcome.talk`, chance: minChance("grail") },
      { id: `${PREFIX}.outcome.flee`, chance: minChance("edge") },
      { id: `${PREFIX}.outcome.flee`, chance: minChance("moth") },
      { id: `${PREFIX}.outcome.failure` },
    ],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.talk`,
    warmup: 20,
    label: "Tittle-tattle or Tit for Tat",
    startdescription:
      "We deal with the Vageries, and we deal with having to deal with them, we will be delayed, and we might attract attention, but we will gather something from the encounter.",
    linked: [{ id: `${PREFIX}.outcome.outcome.talk.router` }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.talk.router`,
    warmup: 20,
    linked: [
      { id: `${PREFIX}.outcome.talk.reward.rumour`, chance: 33 },
      { id: `${PREFIX}.outcome.talk.reward.scrap`, chance: 50 },
      { id: `${PREFIX}.outcome.talk.reward.gifts` },
    ],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.talk.reward.rumour`,
    warmup: 20,
    effects: { "mariner.rumour": 1 },
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.talk.reward.scrap`,
    warmup: 20,
    effects: { "mariner.scrapofinformation": 1 },
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.talk.reward.gifts`,
    warmup: 20,
    effects: { "mariner.trappings.moth": 1 },
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.flee`,
    warmup: 20,
    label: "Flight",
    startdescription:
      "These cultists may venerate an Hour of travel, but a sailing crew they do not make. We easily put distance between us and them again. They will probably still spin a tale out of the encounter.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "<>",
    startdescription: "<>",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
