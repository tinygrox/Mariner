import { INJURE_CREWMEMBER } from "../../../generation_helpers.mjs";
import { BASE_SUCCESS_CHANCE } from "../../generate_seaevents.mjs";
import { generateScaffholding, minChance } from "./../sea_events_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import { TIMER_ELAPSED } from "../../../global_timers_generation.mjs";
import { RESET_TIMER } from "../../../global_timers_generation.mjs";
import { SEA_EVENT_SLOTS } from "../../../slots_helpers.mjs";

export function generateShadowEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("shadow");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: {
      ...TIMER_ELAPSED("northsea.randomevent.shadow", 25),
      [READ_QUEST_FLAG("noonwater", "stolen")]: -1,
      [READ_QUEST_FLAG("noonwater", "complete")]: -1,
      [READ_QUEST_FLAG("bewitch", "completed")]: 1,
    },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.shadow") },
    label: "A Shadow on Deck",
    startdescription:
      "Those of us in our cots are awoken by a muffled cry of alarm. An intruder on board? We rise to help, but all lamps have been snuffed, and we must maneuver the decks in the disorienting dark of a midnight ship. We need a spark to light our way or else rely on our instinct, and the base sense of peril. [use Lantern or Moth to navigate this challenge]",
    slots: SEA_EVENT_SLOTS(),
    linked: [
      { id: `${PREFIX}.outcome.success`, chance: minChance("moth", "8+([root/mariner.galantha.growing])") },
      { id: `${PREFIX}.outcome.success`, chance: minChance("lantern", "8+([root/mariner.galantha.growing])") },
      { id: `${PREFIX}.outcome.success`, chance: BASE_SUCCESS_CHANCE },
      { id: `${PREFIX}.outcome.failure` },
    ],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.success`,
    label: "Path found!",
    startdescription: "Darkness is no hindrence when your surroundings are as familiar as your own skin.",
    warmup: 15,
    linked: [{ id: "mariner.sailing.seaevents.start.sightlessfight" }],
    furthermore: [
      {
        target: "~/tabletop",
        mutations: {
          "[mariner.crew] && [mariner.crew.exhausted]<1": {
            mutate: "mariner.crew.exhausted",
            level: 3,
            limit: 2,
          },
        },
      },
    ],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Stumbling",
    startdescription:
      "I stumble on the ship, as senseless as if  had never crossed it before, it is only the cries of our crew that bring us to the culprit, later then we would have wished.",
    linked: [{ id: "mariner.sailing.seaevents.start.sightlessfight" }],
    furthermore: [
      {
        target: "~/tabletop",
        mutations: {
          "[mariner.crew] && [mariner.crew.exhausted]<1": {
            mutate: "mariner.crew.exhausted",
            level: 3,
            limit: 3,
          },
        },
      },
    ],
  });
}
