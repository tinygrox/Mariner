import { generateScaffholding } from "../sea_events_helpers.mjs";
import { SIGNALS } from "../../../generate_signal_aspects.mjs";

export function generateMistralEvent() {
    const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } =
        generateScaffholding("mistral");

    mod.setRecipe(RECIPES_FILE, {
        id: EVENT_START,
        warmup: 20,
        label: "Mistral",
        grandReqs: {
            "~/exterior : mariner.weathermeta.windy": -1
        },
        startdescription:
            "Through the Rhone valley, the winds pass like a corridor down into the bay. The winds bay and howl and wail and sanp at eachothers necks. The wind is picking up, and it will not suddenly die down.",
        linked: [
            { id: `${PREFIX}.outcome.success` },
        ],
        furthermore: [
            {
                target: "~/exterior",
                aspects: { [SIGNALS.MISTRAL_SET_WIND]: 1 },
            },
        ],
    });

    // Outcomes
    generateSimpleSuccessOutcome({
        label: "Symphony in Cacophony",
        startdescription:
            "We settle in for a orchestra of bellowing winds to accompany our now animated struggle with the ship.",
    });
}
