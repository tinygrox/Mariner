import { generateScaffholding } from "../sea_events_helpers.mjs";
import { SIGNALS } from "../../../generate_signal_aspects.mjs";

export function generateHarmattanEvent() {
    const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } =
        generateScaffholding("harmattan");

    mod.setRecipe(RECIPES_FILE, {
        id: EVENT_START,
        warmup: 20,
        label: "Harmattan",
        grandReqs: {
            "~/exterior : mariner.weathermeta.sandstorm": -1
        },
        startdescription:
            "The low wind lashes like daggers. The Harmattan carries what remains after dust is filed to a raisor's edge.",
        linked: [
            { id: `${PREFIX}.outcome.success` },
        ],
        furthermore: [
            {
                target: "~/exterior",
                aspects: { [SIGNALS.HARMATTAN_SET_SANDSTORM]: 1 },
            },
        ],
    });

    // Outcomes
    generateSimpleSuccessOutcome({
        label: "Dust to Dust",
        startdescription:
            "After the Harmattan has risen, we know dust will remain in our sky, our mouths and our future's for a while.",
    });
}
