import { generateScaffholding, minChance } from "../sea_events_helpers.mjs";

export function generatePiratesEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("pirates");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "Pirates",
    tablereqs: { "mariner.weathermeta.numa": 1 },
    startdescription:
      "Sailors cannot rely on black flags and crossed bones that myths and stories supply.  But some times as a ship approaches me on the western mediteranians, you might just have a shiver up your spine and even without that sixth sense, the ship call would make it clear soon enough. We have encountered a vessel that is planning to board, and we have to decide if we fight or flee.",
    linked: [
      { id: `${PREFIX}.outcome.moth`, chance: minChance("moth") },
      { id: `${PREFIX}.outcome.edge`, chance: minChance("edge") },
      { id: `${PREFIX}.outcome.grail`, chance: minChance("grail") },
      { id: `${PREFIX}.outcome.failure` },
    ],
  });

  // Outcomes //these outcomes will link to the fight, flight and barter routers for the ships.
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.moth`,
    warmup: 20,
    label: "Fight",
    startdescription: "<>",
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.edge`,
    warmup: 20,
    label: "Flee",
    startdescription: "<>",
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.barter`,
    warmup: 20,
    label: "Flee",
    startdescription: "<>",
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Flounder",
    startdescription: "whatever there was to hear, we do not. whatever we might have learned, we do not.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
