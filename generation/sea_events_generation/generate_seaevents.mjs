// basic Sea events
import { generateFairWeatherEvent } from "./sea_events/fairweather.mjs";
import { generateMarineMistEvent } from "./sea_events/marinemist.mjs";
import { generateWindRisesEvent } from "./sea_events/windrises.mjs";
import { generateFireEvent } from "./sea_events/fire.mjs";
import { generatedrinkingEvent } from "./sea_events/drinking.mjs";
import { generateGamblingEvent } from "./sea_events/gambling.mjs";
import { generateFightingEvent } from "./sea_events/fightondeck.mjs";
import { generateFlotsamEvent } from "./sea_events/flotsam.mjs";
import { generateFriendlyRaceEvent } from "./sea_events/friendlyrace.mjs";
import { generateSunnyDayEvent } from "./sea_events/sunnyday.mjs";
import { generateMarinetteDropSeaEvent } from "./sea_events/marinettedolldrop.mjs";
import { generateColdsnapSeaEvent } from "./sea_events/coldsnap.mjs";
import { generateDrownedeaEvent } from "./sea_events/drownedman.mjs";
import { generateJammingSeaEvent } from "./sea_events/jamming.mjs";
import { generateRatsSeaEvent } from "./sea_events/rats.mjs";
import { generateShapesSeaEvent } from "./sea_events/shapesinthewater.mjs";
import { generateShipwreckSeaEvent } from "./sea_events/shipwreck.mjs";
import { generateSicknessSeaEvent } from "./sea_events/sickness.mjs";
import { generateSkySeaEvent } from "./sea_events/skies.mjs";
import { generateRivalFightEvent } from "./sea_events/rival_fight.mjs";

//north sea events
import { generateChannelRatsEvent } from "./sea_events/north_sea/channel_rats.mjs";
import { generateBlizzardEvent } from "./sea_events/north_sea/blizzard.mjs";
import { generateNorthernLightsSeaEvent } from "./sea_events/north_sea/northern_lights.mjs";
import { generateStraySingalsEvent } from "./sea_events/north_sea/stray_signals.mjs";

//medse events
import { generateSiroccoEvent } from "./sea_events/med_sea/sirocco.mjs";
import { generateYachtsEvent } from "./sea_events/med_sea/yachts.mjs";
import { generatePiratesEvent } from "./sea_events/med_sea/pirates.mjs";
import { generateBoraEvent } from "./sea_events/med_sea/bora.mjs";
import { generateHarmattanEvent } from "./sea_events/med_sea/harmattan.mjs";
import { generateMistralEvent } from "./sea_events/med_sea/mistral.mjs";

//supernatural sea events
import { generateOperationGoldEvent } from "./sea_events/supernatural/operation_gold.mjs";
import { generateOperationSilverEvent } from "./sea_events/supernatural/operation_silver.mjs";
import { generateOperationMercuryEvent } from "./sea_events/supernatural/operation_mercury.mjs";
import { generateOperationLeadEvent } from "./sea_events/supernatural/operation_lead.mjs";

import { generateNewMoonsEvent } from "./sea_events/supernatural/moon/new_moon.mjs";
import { generateChangelingMoonsEvent } from "./sea_events/supernatural/moon/changeling_moon.mjs";
import { generatefirstMoonsEvent } from "./sea_events/supernatural/moon/first_quarter_moon.mjs";
import { generateGluttonsMoonsEvent } from "./sea_events/supernatural/moon/glutton_moon.mjs";
import { generateFullMoonsEvent } from "./sea_events/supernatural/moon/full_moon.mjs";
import { generateScholarMoonsEvent } from "./sea_events/supernatural/moon/scholars_moon.mjs";
import { generateThirdMoonsEvent } from "./sea_events/supernatural/moon/third_quarter_moon.mjs";
import { generateThievesMoonsEvent } from "./sea_events/supernatural/moon/thieves_moon.mjs";

import { generateDayUndoneSeaEvent } from "./sea_events/supernatural/day_undone.mjs";
import { generatePromiseofDawnEvent } from "./sea_events/supernatural/promise_of_dawn.mjs";
import { generateMemoriesOfSunSeaEvent } from "./sea_events/supernatural/memories_of_sun.mjs";
import { generateBleedingSunsetSeaEvent } from "./sea_events/supernatural/sunset.mjs";
import { generateNightSelfSeaEvent } from "./sea_events/supernatural/night_self.mjs";
import { generateNemesisSeaEvent } from "./sea_events/supernatural/nemesis_crux.mjs";
import { generateWhiteWestSeaEvent } from "./sea_events/supernatural/white_in_the_west.mjs";
import { generateEffluviaSeaEvent } from "./sea_events/supernatural/effluvia.mjs";
import { generateBrumePlumeSeaEvent } from "./sea_events/supernatural/brume_plume.mjs";

import { generateShipOfHeartEvent } from "./sea_events/supernatural/ship_layout/ship_of_heart.mjs";
import { generateShipOfWinterEvent } from "./sea_events/supernatural/ship_layout/ship_of_winter.mjs";
import { generateShipofEdgeEvent } from "./sea_events/supernatural/ship_layout/ship_of_edge.mjs";
import { generateShipOfGrailEvent } from "./sea_events/supernatural/ship_layout/ship_of_grail.mjs";
import { generateShipOfForgeEvent } from "./sea_events/supernatural/ship_layout/ship_of_forge.mjs";
import { generateShipOfMothEvent } from "./sea_events/supernatural/ship_layout/ship_of_moth.mjs";
import { generateShipOfLanternEvent } from "./sea_events/supernatural/ship_layout/ship_of_lantern.mjs";



export function generateSeaEventsSystem() {
  generateWindRisesEvent(); //allways available 1 
  generateFairWeatherEvent(); // only weather meta clear
  generateMarineMistEvent(); // allways available 2 
  generateFireEvent(); //always available 3 
  generatedrinkingEvent(); //always available 4 
  generateGamblingEvent(); //always available 5 
  generateFightingEvent(); //always available 6 
  generateFlotsamEvent(); //always available 7 
  generateFriendlyRaceEvent(); //always available 8 
  generateSunnyDayEvent(); //always available 9 
  generateColdsnapSeaEvent(); //always available 10 
  generateDrownedeaEvent(); //always available 11 
  generateJammingSeaEvent(); //always available 12
  generateRatsSeaEvent(); //always available 13 
  generateShapesSeaEvent(); //always available 14 
  generateShipwreckSeaEvent(); //always available 15 
  generateSicknessSeaEvent(); // only when weather meta sickly
  generateSkySeaEvent(); //always available 16
  generateRivalFightEvent //only available when you have a rival

  //northsea
  generateChannelRatsEvent(); //always available 17
  generateBlizzardEvent(); //always available 18
  generateStraySingalsEvent(); //always available 19
  generateNorthernLightsSeaEvent(); // only when weather meta is clear

  // special event north sea
  generateMarinetteDropSeaEvent(); //special event

  //medsea 
  generateHarmattanEvent(); //always available 17
  generateMistralEvent(); //always available  18
  generateBoraEvent(); //always available  19
  generateSiroccoEvent(); //always available 20
  generateYachtsEvent(); // only when weather meta is clear
  generatePiratesEvent(); //always available 22


  //supernatural 
  generateOperationGoldEvent(); //supernatural event requirement sunny and lead and silver done 
  generateOperationSilverEvent(); //supernatural event requirement rainy and lead done 
  generateOperationMercuryEvent(); // general supernatural event after gold done.
  generateOperationLeadEvent(); // only when weather meta is stormy
  generateNewMoonsEvent(); // only when new moon
  generateChangelingMoonsEvent(); // only when changeling moon
  generatefirstMoonsEvent(); // only when first quarter moon
  generateGluttonsMoonsEvent(); // only when gluttons mooon
  generateFullMoonsEvent(); //only when full moon
  generateScholarMoonsEvent(); //only when scholars moon
  generateThirdMoonsEvent(); // only when third quarter moon
  generateThievesMoonsEvent(); //only when thieves moon

  generateDayUndoneSeaEvent(); //always available supernatural 1 
  generatePromiseofDawnEvent(); //always available supernatural 2
  generateMemoriesOfSunSeaEvent(); //always available supernatural 3
  generateBleedingSunsetSeaEvent(); //always available supernatural 4
  generateNightSelfSeaEvent(); //always available supernatural 5
  generateNemesisSeaEvent(); //only available in storm
  generateWhiteWestSeaEvent(); //only availabe in snow
  generateEffluviaSeaEvent(); //only available in smog
  generateBrumePlumeSeaEvent(); //only avialable in mist

  generateShipOfHeartEvent(); //crew stat dependant
  generateShipOfWinterEvent(); //crew stat dependant
  generateShipofEdgeEvent(); //crew stat dependant
  generateShipOfGrailEvent(); //crew stat dependant
  generateShipOfForgeEvent(); //crew stat dependant
  generateShipOfMothEvent(); //crew stat dependant
  generateShipOfLanternEvent(); //crew stat dependant
}








export const BASE_SUCCESS_CHANCE = 10;
