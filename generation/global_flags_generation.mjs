const ASPECTS_FILE = "aspects.global_flags";

export function FLAG_ID(id) {
  const flagId = `mariner.globalflags.${id}`;
  if (!mod.aspects[ASPECTS_FILE]) mod.initializeAspectFile(ASPECTS_FILE, ["flags"]);
  if (!mod.getAspect(ASPECTS_FILE, flagId)) mod.setHiddenAspect(ASPECTS_FILE, { id: flagId });
  return flagId;
}

export function READ_FLAG(id) {
  return `root/${FLAG_ID(id)}`;
}

export function QUEST_FLAG_ID(id, step) {
  return FLAG_ID(`quest.${id}.${step}`);
}

export function READ_QUEST_FLAG(id, step) {
  return `root/${QUEST_FLAG_ID(id, step)}`;
}
