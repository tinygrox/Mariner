import { generateStoryTrading } from "./trading_helper.mjs";

export function generateBarStoryTrading() {
  generateStoryTrading({
    id: "generic.bar",
    story: {
      label: "A Courting Story",
      icon: "apolloandmarsyas",
      description:
        "One person meets another. They circle eachother like wild dogs, strut like a peacock, bare their teeth. For this night, the pair does not need to be lonely.",
      aspects: {
        grail: 1,
        moth: 1,
        "mariner.storyaspect.dionysian": 1,
        "mariner.storyaspect.romantic": 1,
        "mariner.storyaspect.whimsical": 1,
      },
    },
    requirements: { "mariner.bar": 1 },
    label: "Trade My Story With Others in Bar",
    startdescription:
      "In many bars one can find many different stories, but there is one you can always find. This one is spoken not in words, but in postures, and stances and glances.",
    description:
      "I watch a dialogue in body language, a story older than spoken language. Over the night, I watch the blooming of a firefly relationship, I watch it rise, shine, and blink out in the night.",
  });
}
