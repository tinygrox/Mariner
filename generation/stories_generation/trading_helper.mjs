import { SIGNALS } from "../generate_signal_aspects.mjs";

export function generateStoryTrading({
  recipeFile,
  path,
  id,
  story,
  requirements,
  label,
  startdescription,
  description,
}) {
  const RECIPE_FILE = recipeFile ?? `recipes.tradestories.${id}`;
  const STORY_ID = `mariner.stories.${id}`;
  mod.initializeRecipeFile(RECIPE_FILE, path ?? ["stories", "trading"]);
  mod.setElement("stories", {
    id: STORY_ID,
    icon: story.icon ?? STORY_ID,
    label: story.label,
    description: story.description,
    aspects: {
      ...story.aspects,
      "mariner.story": 1,
      "mariner.local": 1,
    },
    xtriggers: {
      [SIGNALS.EXHAUST_STORY]: [{ morpheffect: "destroy" }],
    },
  });

  mod.setRecipe(RECIPE_FILE, {
    id: `mariner.tradestory.${id}`,
    craftable: true,
    actionId: "mariner.navigate",
    requirements: {
      ...requirements,
      "mariner.story": 1,
    },
    label,
    startdescription,
    warmup: 20,
    description,
    furthermore: [
      {
        aspects: { [SIGNALS.EXHAUST_STORY]: 1 },
      },
      {
        effects: { [STORY_ID]: 1 },
      },
    ],
  });
}
