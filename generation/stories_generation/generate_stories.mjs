import { generateCodexEntry } from "../codex_generation/codex_generation.mjs";
import { SIGNALS } from "../generate_signal_aspects.mjs";
import { ME_MUTATE, ME_SET_MUTATION } from "../generation_helpers.mjs";
import { MOON_INFLUENCE } from "../song_generation/generate_influences.mjs";
import { generateBarStoryTrading } from "./bar_trading.mjs";
import { generateWharfStoryTrading } from "./wharf_trading.mjs";

export const STORY_ASPECT_MOON_BONUS = (id, level = 1) => `mariner.moonbonus.${id}.${level}`;
export const STORY_ASPECT = (id) => `mariner.storyaspect.${id}`;
export function STORY_ASPECTS() {
  return Object.fromEntries([...arguments].map((id) => [STORY_ASPECT(id), 1]));
}
const EXPERIENCE_CLEARED_BY_ASPECT_ID = (id) => `mariner.experiencefixedbystoryaspect.${id}`;

export function EXPERIENCE_CLEARED_BY() {
  return Object.fromEntries([...arguments].map((id) => [EXPERIENCE_CLEARED_BY_ASPECT_ID(id), 1]));
}

function generateCollectableStory({ id, lores, label, description, icon, pages, additionalReadingProperties }) {
  generateCodexEntry(`codex.story.${id}`, `[Story] ${label}`, pages.join("\n\n"));
  mod.setHiddenAspect("aspects.stories", { id: `mariner.uqg.story.${id}` });
  mod.setElements(
    "stories",
    {
      id: `mariner.stories.${id}`,
      label,
      description,
      icon,
      aspects: { "mariner.sing_allowed": 1, "mariner.story": 1, ...lores },
      unique: true,
      uniquenessgroup: `mariner.uqg.story.${id}`,
      xtriggers: {
        [SIGNALS.EXHAUST_STORY]: `mariner.stories.${id}.exhausted`,
      },
    },
    {
      id: `mariner.stories.${id}.exhausted`,
      label: `${label} [exhausted]`,
      description,
      icon,
      aspects: {
        "mariner.sing_allowed": 1,
        "mariner.story.exhausted": 1,
        ...lores,
      },
      unique: true,
      uniquenessgroup: `mariner.uqg.story.${id}`,
      xtriggers: {
        [SIGNALS.UNEXHAUST_STORY]: `mariner.stories.${id}`,
      },
    }
  );
  for (let i = 0; i < pages.length - 1; i++) {
    const page = pages[i];
    mod.setRecipe("recipes.readstories", {
      actionId: "mariner.sing",
      id: `mariner.readstories.${id}.page.${i + 1}`,
      craftable: i === 0,
      requirements:
        i === 0
          ? {
            [`mariner.stories.${id}`]: 1,
            "mariner.theater": -1,
            "mariner.local": -1,
          }
          : undefined,
      label: `Recounting "${label}"`,
      startdescription: i === 0 && page.length > 1 ? `<i>${page}</i>` : page,
      description: i === pages.length - 2 ? pages[i + 1] : undefined,
      warmup: 20,
      linked: i < pages.length - 2 ? [{ id: `mariner.readstories.${id}.page.${i + 2}` }] : undefined,
      ...(i === pages.length - 2 ? { unlockCodexEntries: [`codex.story.${id}`] } : {}),
      ...((i === 0 && additionalReadingProperties) || {}),
    });
  }
}

function generateStoryAspect({ id, label, description, moonBonus, moonLabel, moonDescription }) {
  mod.setAspects(
    "aspects.stories",
    {
      id: STORY_ASPECT(id),
      label,
      description,
      xtriggers: {
        [SIGNALS.APPLY_MOON_BONUS_ASPECTS]: [ME_SET_MUTATION(STORY_ASPECT_MOON_BONUS(id), 1, `(${STORY_ASPECT(id)} >= 3) * 100`)],
      },
    },
    {
      id: EXPERIENCE_CLEARED_BY_ASPECT_ID(id),
      //TODO: improve with a nice overlay or something
      icon: STORY_ASPECT(id),
      label: `Experience of type: ${label}`,
      description,
    },
    {
      id: STORY_ASPECT_MOON_BONUS(id),
      label: moonLabel,
      description: moonDescription,
      xtriggers: {
        [SIGNALS.INSTALL_MOON_BONUSES]: Object.entries(moonBonus ?? {}).map(([id, level]) => ME_MUTATE(id, level)),
        [SIGNALS.CLEAR_MOON_BONUSES]: [
          ...Object.entries(moonBonus ?? {}).map(([id, level]) => ME_MUTATE(id, -level)),
          ME_SET_MUTATION(STORY_ASPECT_MOON_BONUS(id), 0),
        ],
      },
    }
  );

  mod.setRecipes(
    "recipes.clearexperience",
    {
      id: `mariner.clearexperience.with.${id}.hint`,
      actionId: "talk",
      hintOnly: true,
      requirements: {
        "mariner.bar": 1,
        [EXPERIENCE_CLEARED_BY_ASPECT_ID(id)]: 1,
        [STORY_ASPECT(id)]: -1,
      },
      label: `<Coming to Terms with Experience via the Parallels of ${id} Story?>`,
      startdescription: "<>",
    },
    {
      id: `mariner.clearexperience.with.${id}`,
      actionId: "talk",
      craftable: true,
      requirements: {
        "mariner.bar": 1,
        [EXPERIENCE_CLEARED_BY_ASPECT_ID(id)]: 1,
        [STORY_ASPECT(id)]: 1,
      },
      warmup: 60,
      label: `<Coming to Terms with Experience via the Parallels of ${id} Story>`,
      startdescription: "<>",
      aspects: { [SIGNALS.EXHAUST_STORY]: 1 },
      effects: { "mariner.experience": -1 },
    }
  );
}

export function generateStories() {
  mod.initializeAspectFile("aspects.stories", ["stories"]);
  mod.initializeElementFile("stories", ["stories"]);
  mod.initializeRecipeFile("recipes.readstories", ["stories"]);
  mod.initializeRecipeFile("recipes.clearexperience", ["experiences"]);

  mod.setAspects("aspects.stories", {
    id: "mariner.story.exhausted",
    label: "Tarnished Story",
    icon: "exhausted",
    description:
      "I feel like I have exhausted this story of its substance by telling it. I should move, experience new things, meet with a different crowd in order to give it new colours.",
  });

  generateStoryAspect({
    id: "grim",
    label: "Grim",
    description: "What explains that urge within the human heart, to seek the darkness even when life is light?",
    moonBonus: { edge: 2 },
    moonLabel: "<Under a Bitter Moon>",
    moonDescription: "<The moon bears witness to all lives challenges and injustices.>",
  });
  generateStoryAspect({
    id: "rousing",
    label: "Rousing",
    description:
      "It's the stories of great excitement that make children wish they could grow up already, and for adults remember the vibrancy of youth.",
    moonBonus: { forge: 2 },
    moonLabel: "<Under a Blazing Moon>",
    moonDescription: "<The Moon lights a path to whatever dawn I seek to forge.>",
  });
  generateStoryAspect({
    id: "wise",
    label: "Wise",
    description: "Some truths are so simple they can only be made clear in fables and fantasies.",
    moonBonus: { lantern: 2 },
    moonLabel: "<Under the Sage moon>",
    moonDescription: "<>",
  });
  generateStoryAspect({
    id: "whimsical",
    label: "Whimsical",
    description: "Reality can be a taxing exercise. Everyone sometimes deserves a more pleasant alternative.",
    moonBonus: { moth: 2 },
    moonLabel: "<Under a Smiling moon>",
    moonDescription: "<On some nights, the moon itself dreams of cheese and pattering rabbits' feet.>",
  });
  generateStoryAspect({
    id: "witty",
    label: "Witty",
    description:
      "Humor is the sword that cuts kings and the flame that ignites itself. Laughter can unite without language.",
    moonBonus: { grail: 2 },
    moonLabel: "<Under a Laughing Moon>",
    moonDescription: "<Our antics, our plots and ploys are beneath her, but oh she watches with such joy>",
  });
  generateStoryAspect({
    id: "occult",
    label: "Occult",
    description: "All stories enact the passions of the Hours in some guise, but few are as blatant as this one is.",
    moonBonus: { knock: 2 },
    moonLabel: "<Under the Sun's Night-self>",
    moonDescription: "<>",
  });
  generateStoryAspect({
    id: "romantic",
    label: "Romantic",
    icon: "romanticinterest",
    description: "Those things that soften the soul, from a pastoral idyll to simmering passion.",
    moonBonus: { heart: 2 },
    moonLabel: "<Under a Lovesick Moon>",
    moonDescription: "<Slowly, coyly, the moon turn her face away from her lover, until she is coaxed to show the ocean her face again.>",
  });
  generateStoryAspect({
    id: "tragic",
    label: "Tragic",
    description: "Fates unavoidable, hubris unbearable, peaks unreachable. Still, we tell it anyway.",
    moonBonus: { winter: 2 },
    moonLabel: "<Under a Weeping Moon>",
    moonDescription: "<moonstones, it is said, are the tears the moon sheds from her cracks-eyes in her mirror-face>",
  });
  generateStoryAspect({
    id: "apollonian",
    label: "Apollonian",
    description: "All that is touched and tinted by that immortal light above.",
    moonBonus: { [MOON_INFLUENCE("lantern")]: 1 },
    moonLabel: "<Under a Luminous Moon>",
    moonDescription: "<Even conversation sparkles brighter when the moon is in attendance>",
  });
  generateStoryAspect({
    id: "dionysian",
    label: "Dionysian",
    description: "The low feelings of the underbrush, the undercurrent, the underbelly.",
    moonBonus: { [MOON_INFLUENCE("moth")]: 1 },
    moonLabel: "<Under a Conspiratorial Moon>",
    moonDescription: "<The night is for lovers, thieves and rebels. The moon will keep their secrets save.>",
  });
  generateStoryAspect({
    id: "mortal",
    label: "Mortal",
    description: "What Passions burn in these fire-fly lives, can burn brighter than the evening sky.",
    moonBonus: { [MOON_INFLUENCE("heart")]: 1 },
    moonLabel: "<Under A Sympathetic Moon>",
    moonDescription: "<Our chaperone through darkness, our companion in delight.>",
  });
  generateStoryAspect({
    id: "cthonic",
    label: "C'thonic",
    description: "Beyond the doors to death, below the shifting earth, before the nights' veil.",
    moonBonus: { [MOON_INFLUENCE("edge")]: 1 },
    moonLabel: "<Under an Wicked Moon>",
    moonDescription: "<the blade and the witch, the cyclle and eclipse, >",
  });
  generateStoryAspect({
    id: "horomachistry",
    label: "Horomachistry",
    description: "The Passions and Movements of the Hours, Names and all the higher Players on the worlds Stage.",
    moonBonus: { [MOON_INFLUENCE("winter")]: 1 },
    moonLabel: "<Under A Pharisaic Moon>",
    moonDescription: "<>",
  });

  generateBarStoryTrading();
  generateWharfStoryTrading();

  mod.setElement("stories", {
    id: "mariner.stories.sailorstories",
    label: "Sailor Stories",
    description:
      "Any well-traveled person collects stories on their travels, but no well is as full of anecdotes as the ocean. Regale your audience with a swell of partial truths and full lies and whisk them away to far-off half-real places.",
    icon: "voyagesfo",
    aspects: {
      "mariner.story": 1,
      "mariner.local": 1,
      heart: 1,
      grail: 1,
      ...STORY_ASPECTS("mortal", "dionysian", "rousing"),
    },
    xtriggers: {
      [SIGNALS.EXHAUST_STORY]: [{ morpheffect: "destroy" }],
    },
  });

  generateCollectableStory({
    id: "centipede",
    lores: {
      winter: 2,
      knock: 2,
      ...STORY_ASPECTS("cthonic", "apollonian", "occult", "witty", "whimsical"),
    },
    label: "The Travels of the Centipede",
    description:
      "This Story tells of the travels of the Centipede, the places she visits High and Low, True and Real, and the Antics that got her Banned from the House of the Sun.",
    icon: "bookofcentipede",
    pages: [
      "Once there was a curious centipede...",
      "Who wished to see all the things the world had to offer, especially those places people did not want her to go. First she crawled through all the lands of the earth, from her Hometown of Miah, through the land of the Great River, to the Plateau of Leng, and the Eastern Mountain Spine, and the great Plains.",
      "Then She crawled all through the house of the Sun, through the Courts Gold and Red, circled the Orchard Trees, burrowed in the shifting Sands, twisted her body in knots in the Heart-chamber, and then swam through that work of art her other self would paint.",
      "The Painted River ran out of the Mansus, and landed behind the Rain. But the Centepede wanted to go deeper, to the Kingdom of Black Nephrite Lamps. She distracted the Doorwarden, and slipped through the crack under the door. Down, Down she buried, and heared the keening prophecies of the forgotten dead.",
      "She learned much there, in the lightless underplaces. But the knowledge can be blemish, if it's contents are inky black. The Centipedes crimes, it is said, were as numerous as her tar-stained feet. The Doorwarden barred entrance to the Mansus, but her travels have continued, between the stars and below the skin of the world, places high and low, true and real, all except the one.",
    ],
  });
  generateCollectableStory({
    id: "anansi",
    lores: {
      moth: 2,
      edge: 2,
      knock: 2,
      ...STORY_ASPECTS("apollonian", "wise", "witty"),
    },
    label: "How Anansi The Spider Came To Own All Stories",
    description:
      "Once, there were no stories told in the world. The Skyfather, Sun of Suns, held them all. Anansi bartered for them, and though the Skyfather set an impossible price, Anansi delivered. He tricked wise Snake, caught the familial Hornets, trapped the fierce Leopard and fooled the furtive fairy. With these prizes Anansi earned the respect of the Skyfather, and all the stories of the world.",
    icon: "locksmithsdream5",
    pages: [
      "Once there were no stories in the world...",
      "Nyame the Skyfather, ruler of all the sunrays touch, was in posession of all the stories, unwilling to share. Anansi decided that he will change this, and goes to Nyame, and asks him for his price, boasting he will pay anything, for he cannot be stayed, and he cannot be exhausted.",
      "Nyame was doubtful, but intrigued, and named his price, impossibly high. He wanted four of his most dangerous subjects to be put in their place, and be brought to him. Onini the Python, the Mmoboro Hornets, Osebo the Leopard, and the Fairy Mmoatia. Anansi Smiled, bowed his head and traveled back to earth, to plot.",
      "First Anansi captured wise Onini, by tricking her to measure her length through loops of vines he had prepaired. Next were the Mmoboro hornets, for whom he prepaired a hollow gourd. He offered it to them for shelter from the rain, but closed the lid and caught them.",
      "Then on to Fierce Osebo, who he trapped in a pit, and cudgeled on the head, just when he was offered an escape by Anansi. Last the Furtive fairy, who he lured with sweet drink and a fake face. With all his prizes presented to Nyame, Anansi was awarded all the stories from all the histories. That is why to this day, it is spidersilk from which the threads of The Histories are woven.",
    ],
  });
  generateCollectableStory({
    id: "lorelei",
    lores: {
      grail: 2,
      edge: 2,
      heart: 2,
      ...STORY_ASPECTS("cthonic", "grim"),
    },
    label: "The Maiden of the Rhine",
    description:
      "The Maiden sits on Lorelei Rock, and combs her golden hair. Her eyes are aimed beyond, and as ships break on the clips beneath her and men break their necks trying to reach her, she does not spare them a glace. Where does she go, when she slips beneath the waves?",
    icon: "locksmithsdream4",
    pages: [
      "The Lorelei sits on her namesake rock, combs her hair and sings...",
      "Seemingly unwittingly, she distracts sailors on the Rhine below, who smash their boats onto the clips. Only then she deigns to look down. She collects the souls of the drowning, and swims down, down into the Kingdom Below River and Hill.",
      "She meets up with the Nixes and Undines from all of the Streams of Westphalia and beyond. They dance around the throne of their holly-crowned queen, attended to by the souls of those they have lead astray.",
      "It ends on the chilling note that the direction of Death is down, and one who dwells their sends up many gifts.",
    ],
  });
  generateCollectableStory({
    id: "mermaid",
    lores: {
      heart: 2,
      ...STORY_ASPECTS("apollonian", "romantic", "tragic"),
    },
    label: "Voiceless Devotion",
    description:
      "A young mermaid traded her voice for feet, so she could win the love of an air-breathing prince. She danced for his enjoyment, never faltering despite her painful limbs. When she was inevitably refused by the prince, her sisters urge her to take revenge... but she decides on a path of self-sacrifice.",
    icon: "locksmithsdream5",
    pages: [
      "A princess from below the waves fell in love with a prince from the wooded shores...",
      "Aided by the Matron of the Seas, she is persuaded to trade her voice for a transformation of her being. Thus her tail is split and she coughs up brine and gasps for air.",
      "The Prince takes her in, and in her silent devotion, she attempts to win his heart. She danced for his delight, despite the knife-sharp pain she feels with every step, her passion maintains her rhythms.",
      "But to no avail... The prince discards her in favor of a marriage alliance with lady from the Scarlet Church. And as the Prince and his betrothed tie their bonds on doorstep of the church, our heroine is pulled back to the sea. Her sisters call to her, and have brought a knife for her to cut out the heart of the Prince, and snap their marriage ribbons.",
      "But the mermaid decides to accept her fate, and dissolve into seafoam rather a harming the Prince and his Realm. But in her sacrificicial leap down the cliff she is elevated instead. She ascends as a mighty Eidolon of the Skies, and will earn her soul by serving the world and protecting the virtuous.",
    ],
  });
  generateCollectableStory({
    id: "debellismurorum",
    lores: {
      edge: 2,
      ...STORY_ASPECTS("horomachistry", "rousing", "occult"),
    },
    label: "War of the Walls",
    description:
      "In hedges and in edges, on sea-dikes and on cavewalls, the many-sided battle for the Borderlands rages. Though factions may shift and alignments are malleable, some things are certain; The One-Two Joins and the Horned One Distinguishes.",
    icon: "debellismurorum",
    pages: [
      "It's a war of many sides, with few winners but many casualities...",
      "Striges fight Undines in the borderlands that seperates the sea from the sky, as fate twines to keep mortal paramours apart or to unite them in seamless unity. The horizon itself is a battlefield.",
      "The War of the Walls can be told many ways, with different combatents and different battlefields, but the sides are always the same.",
      "The Two-One who Dissolve, Horned Axe who Divides. There is no ending, not truly, not until the sea swallows the shore, or Night unwinds from the Day. ",
    ],
  });
  generateCollectableStory({
    id: "kerys",
    lores: {
      grail: 2,
      lantern: 2,
      knock: 2,
      ...STORY_ASPECTS("horomachistry", "apollonian", "occult", "tragic"),
    }, // or dangerous lovers?
    label: "The Doom of Ker Ys",
    description:
      "There once was a low city, which wrenched its existance from the waters of a bitter sea. This is the story of its final days, before it was drowned with all souls but one.",
    icon: "locksmithsdream5",
    pages: [
      "There once was a city crowned by the dikes, on the edge of a bitter sea...",
      "It was that sea that they relied upon for their lifelyhood, and that sea that took from them the lives of many of their fishermen. ",
      "But the king's daughter was thaught in the ways of sorcery in the House of Albion, and she shared her arts with her father. Together they supplicated the sea, and the city prospered. But as their ambitions grew, so grew the sea's hunger. So more and more they gave. Of all these dark tidings, even the birds took note.",
      "One day, a stranger came to town, a foreign royal from beyond the sea. He dazzled the king and courted the princess, and then he would leave again, coming and going with the tides. As the seasons passed, their courtship formalized, but then in the height of summer he vanished. Six moons passed without a word.",
      "The princess was griefstricken and mad with longing, pining away in her palace. She was unable to perform her duties to her father or to the sea. Then, under the first quarter moon, a kite landed in her window, and delivered a message from her love. She rushed into the nights, to meet him at the sea-gate locks.",
      "A whisper came to her from beyond the gate. She heeded her heart and did not think twice... not until the white heads of waves came rushing in. The Sea, denied of her ministrations in her lovesick folly, rushed into the cities streets and courtyards, ravenous and cruel. Even the Princess was dragged down into their cruel embrace.",
      "She would have drowned there, in the seas roiling stomach, as was the lot of all her kith and kin. But a moon beam pierced the water, as the mother of mercy took pity on her. She was plucked from the waters, and served the Lady of Wounds in both this world and beyond.",
    ],
  });
  generateCollectableStory({
    id: "pineknight", //Suffering and Surviving or Guiding Goddess? It does not rhyme in this tongue, but it's meter was carefully preserved. A romantic ballad telling the more ancient story of the Pine knight, and the Concord of Queens.
    lores: {
      grail: 2,
      heart: 2,
      moth: 2,
      ...STORY_ASPECTS("mortal", "dionysian", "romantic", "grim"),
    },
    label: "The Pine Knight and the Queens",
    description:
      "It follows the story of a devoted knight, and his quest for the Cinnabar Cup. His duty gets challenged by romance however, and it ends, as always, in tragedy.",
    icon: "orchidtransfigurations2",
    pages: [
      "In the old days, before the great forests had been logged...",
      "A great knight sought to find duty in service. His focus was the Cinnabar Cup, sought after by many, in the command of none. He ignored the blowing winds of war, steel against stone, and focussed only on his quest.",
      "But duty can be fickle when it comes to matters of the Heart. The Pine-Knight met a Maiden Queen, in a forest deep and dark. Their romance was honey, but honey drawn from foxglove and thornapples. For the winds of war were turning to a storm of death, as one, two, three, four, five kings fell, and still the war raged on.",
      "The Knight's own Red Queen, when she heard of his dalliance, became irate, for she had kept great plans for him. She weighed her options, her emotions, her problems. Knotting tassles in her hand as she thought. With grim delight, she wrought a new scheme. She cut the knot from her shawll, and send it to the Hawthorn Queen of Stone",
      "A pact it was, a pact to end the bloodshed. The hawthorne queen did anguish on these tidings. It would mean the end retribution for her fallen sibling kings. But this war was tearing the lands apart, and as a queen, it was her duty to keep it safe. She twisted the knot as she thought, twisted it into new shapes of compliance.",
      "A messanger came to the Honey Queen too. It proclaimed with joy that the war had ended, and that all it would cost was one more life. And that the quest of the Pine-Knight was at an end too, because Duty had found him.",
      "The Maiden Queen trembled like the leaves as she considered what she should do. She could save her lover, quieting him away in the hidden glades of her kingdom. But the cost would be so great, both for her and the world. She decided the only mercy she could offer was for him to die at her side, where he had known most happiness.",
      "So in her innermost kingdom, upon these stormwrecked coasts, the honey queen tied the final knot, and the tree branches swung, even though the wind dared not to breath. So ended the Pine-Knights Quest for the Cup, and so ended the Wars of Stone, in a tripple knotted noose.",
    ],
  });
  generateCollectableStory({
    id: "something", //Suffering and Surviving or Guiding Goddess? It does not rhyme in this tongue, but it's meter was carefully preserved. A romantic ballad telling the more ancient story of the Pine knight, and the Concord of Queens.
    lores: {
      moth: 2,
      ...STORY_ASPECTS("mortal", "dionysian", "romantic", "grim"),
    },
    label: "<something>",
    description: "<>",
    icon: "orchidtransfigurations2",
    pages: ["<>"],
  });
  generateCollectableStory({
    id: "moon", //Suffering and Surviving or Guiding Goddess? It does not rhyme in this tongue, but it's meter was carefully preserved. A romantic ballad telling the more ancient story of the Pine knight, and the Concord of Queens.
    lores: {
      moth: 2,
      ...STORY_ASPECTS("mortal", "dionysian", "romantic", "grim"),
    },
    label: "<City Unbuild>",
    description: "<>",
    icon: "orchidtransfigurations2",
    pages: ["<>"],
  });
  generateCollectableStory({
    id: "serapeum", //Suffering and Surviving or Guiding Goddess? It does not rhyme in this tongue, but it's meter was carefully preserved. A romantic ballad telling the more ancient story of the Pine knight, and the Concord of Queens.
    lores: {
      knock: 2,
      ...STORY_ASPECTS("mortal", "dionysian", "romantic", "grim"),
    },
    label: "<something>",
    description: "<>",
    icon: "orchidtransfigurations2",
    pages: ["<>"],
  });
  generateCollectableStory({
    id: "imago", //Suffering and Surviving or Guiding Goddess? It does not rhyme in this tongue, but it's meter was carefully preserved. A romantic ballad telling the more ancient story of the Pine knight, and the Concord of Queens.
    lores: {
      knock: 2,
      ...STORY_ASPECTS("mortal", "dionysian", "romantic", "grim"),
    },
    label: "<something>",
    description: "<>",
    icon: "orchidtransfigurations2",
    pages: ["<>"],
  });
}
