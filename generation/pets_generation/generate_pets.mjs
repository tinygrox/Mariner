import { SIGNALS } from "../generate_signal_aspects.mjs";
import { ME_RECIPE, buildAdvancedRefinementBlock } from "../generation_helpers.mjs";

function generatePetHandlingSystem() {
  mod.setAspect("aspects.pets", {
    id: "mariner.petsitter",
    label: "Petsitter",
    description: "This one has an affinity with pets. They could accept to keep one of mine for a while, for a small fee.",
  });
  mod.setHiddenAspect("aspects.pets", {
    id: "mariner.petsitter.filled",
    label: "Petsitter: Already Petsitting",
    description: "This one is already petsitting one of my little companions.",
  });

  // Give pet to petsitter
  mod.setRecipe("recipes.pets", {
    id: "mariner.pets.givepet",
    hintOnly: true,
    actionId: "talk",
    requirements: {
      "mariner.petsitter": 1,
      "mariner.pet": 1,
      "mariner.petsitter.filled": 1,
    },
    label: "<Give Pet to Petsit for a while?>",
    startdescription: "<This person already petsits for me>",
  });

  mod.setRecipe("recipes.pets", {
    id: "mariner.pets.givepet",
    actionId: "talk",
    craftable: true,
    requirements: {
      "mariner.petsitter": 1,
      "mariner.pet": 1,
      "mariner.petsitter.filled": -1,
      funds: 1,
    },
    label: "<Give Pet to Petsit for a while>",
    startdescription: "<>",
    description: "<>",
    warmup: 60,
    // Trigger the xtrigger on the pet card. It will run the proper recipe to store the pet and its bond level.
    aspects: {
      [SIGNALS.GIVE_PET]: 1,
    },
  });

  // Retrieve pet
  mod.setRecipe("recipes.pets", {
    id: "mariner.pets.givepet",
    actionId: "talk",
    craftable: true,
    // THESE REQUIREMENTS ARE PROBABLY WRONG. HOW DO YOU GET THEM BACK? FUNDS? SOME SPECIFIC CARD? No idea yet.
    requirements: {
      "mariner.petsitter": 1,
      "mariner.petsitter.filled": 1,
    },
    label: "<Retrieve Pet?>",
    startdescription: "<Wanna retrive the little precious? Or maybe talk about something?>",
    warmup: 60,
    // Trigger the xtrigger on the pet aspect mutated onto the petsitter. It will run the proper recipe to retrieve the pet and its bond level.
    aspects: {
      [SIGNALS.RETRIEVE_PET]: 1,
    },
  });

  // Generic part of the pet offering
  mod.setRecipe("recipes.pets", {
    id: `mariner.offerpet.waitanswer`,
    warmup: 60,
    slots: [{ id: "answer", required: { "mariner.halfheart": 1 } }],
    linked: [{ id: "mariner.offerpet.check" }, { id: "mariner.offerpet.leave" }],
  });
  mod.setRecipe("recipes.pets", {
    id: `mariner.offerpet.check`,
    requirements: { "mariner.halfheart": 1 },
    linked: [{ id: "mariner.offerpet.nospace" }],
  });

  mod.setRecipe("recipes.pets", {
    id: `mariner.offerpet.nospace`,
    tablereqs: { "mariner.pet": 1 },
    description: "<no space on board. I'll keep it there for now>",
    aspects: { [SIGNALS.GIVE_PET]: 1 },
  });
  mod.setRecipe("recipes.pets", {
    id: `mariner.offerpet.leave`,
    aspects: { [SIGNALS.GIVE_PET]: 1 },
  });
}

function generatePet({ id, stages }) {
  mod.initializeRecipeFile(`recipes.pets.${id}`, ["pets"]);
  // Aspect version
  mod.setAspect("aspects.pets", {
    id: `mariner.givenpet.${id}`,
    icon: `pets/${id}`,
    label: "<A Furry Companion>",
    description: "<We left our furry companion in the company of this person for a bit.>",
    xtriggers: {
      [SIGNALS.RETRIEVE_PET]: ME_RECIPE(`mariner.retrievepet.${id}`),
    },
  });

  // Card version
  const firstStage = stages.shift();
  const reversedStages = [...stages].reverse();
  mod.setElement("pets", {
    id: `mariner.pets.${id}`,
    icon: `pets/${id}/${id}`,
    label: buildAdvancedRefinementBlock(
      reversedStages.map((s, index) => ({
        aspect: "mariner.petbond",
        value: (reversedStages.length - index) * 30,
        text: s.label,
      })),
      firstStage.label
    ),
    description: buildAdvancedRefinementBlock(
      reversedStages.map((s, index) => ({
        aspect: "mariner.petbond",
        value: (reversedStages.length - index) * 30,
        text: s.description,
      })),
      firstStage.description
    ),
    aspects: { "mariner.pet": 1, "mariner.topic": 1 },
    xtriggers: {
      [SIGNALS.GIVE_PET]: ME_RECIPE(`mariner.givepet.${id}`),
      [SIGNALS.UPDATE_PET]: stages.map((s, index) => ME_RECIPE(`mariner.updatepet.${id}.${index + 1}`)),
    },
  });

  for (let i = 0; i < stages.length; i++) {
    const aspects = stages[i].aspects ?? [];
    mod.setRecipe(`recipes.pets.${id}`, {
      id: `mariner.updatepet.${id}.${i + 1}`,
      grandReqs: {
        [`[~/exterior : mariner.petbond]`]: (i + 1) * 30,
        [`[~/exterior : {${aspects.map((aspect) => `[${aspect}] < ${i + 1}`).join(" && ")}} : mariner.pets.${id}]`]: 1,
      },
      furthermore: [
        {
          target: "~/exterior",
          mutations: aspects.map((aspect, index) => ({
            filter: `mariner.pets.${id}`,
            mutate: aspect,
            level: i + 1,
          })),
        },
      ],
    });
  }

  // Recipe to give this pet
  mod.setRecipe(`recipes.pets.${id}`, {
    id: `mariner.givepet.${id}`,
    actionId: "talk",
    craftable: true,
    requirements: {
      "mariner.petsitter": 1,
      "mariner.petsitter.filled": -1,
      "mariner.pet": 1,
    },
    warmup: 30,
    label: "<Give pet to petsit>",
    startdescription: "<Give pet to petsit>",
    // 1. Assign the busy petsitter aspect to the npc,
    // 2. Assign the proper petsitted pet aspect,
    // 4. Store the pet card
    mutations: [
      {
        filter: "mariner.petsitter",
        mutate: "mariner.petsitter.filled",
        level: 1,
      },
      {
        filter: "mariner.petsitter",
        mutate: `mariner.givenpet.${id}`,
        level: 1,
      },
    ],
    movements: {
      "*/mariner.decks.storedpets_draw": ["mariner.pet"],
    },
  });

  // Recipe to get pet back
  mod.setRecipe(`recipes.pets.${id}`, {
    id: `mariner.retrievepet.${id}`,
    actionId: "talk",
    craftable: true,
    requirements: {
      "mariner.petsitter": 1,
      "mariner.petsitter.filled": 1,
      "mariner.wanderlust": 1,
    },
    tablereqs: { "mariner.pet": -1 },
    warmup: 30,
    label: "<Retrieve pet>",
    startdescription: "<Retrieve pet>",
    // 1. Remove the busy petsitter aspect to the npc,
    // 2. Remove the proper petsitted pet aspect,
    // 3. Create the pet card
    // 4. Retrieve the pet bond level from
    furthermore: [
      {
        mutations: [
          {
            filter: "mariner.petsitter",
            mutate: "mariner.petsitter.filled",
            level: 0,
          },
          {
            filter: "mariner.petsitter",
            mutate: `mariner.givenpet.${id}`,
            level: 0,
          },
        ],
      },
      {
        target: "*/mariner.decks.storedpets_draw",
        movements: {
          "~/local": [`mariner.pets.${id}`],
        },
      },
    ],
  });

  // How to make npcs hand out pets?
  // 1. Mutate the "petsitter" aspect onto them.
  // 2. Spawn the pet card.
  // 3. Display a warmup expecting some card, maybe a heart.
  // 4. Check if there's space on the table. If not, trigger SIGNALS.GIVE_PET.
  // This recipe-chain can be called as the last one of a chain, to handle that automatically. It also supports a callback.
  mod.setRecipe(`recipes.pets.${id}`, {
    id: `mariner.offerpet.${id}`,
    mutations: [
      {
        filter: "mariner.npc",
        mutate: "mariner.petsitter",
        level: 1,
      },
    ],
    effects: { [`mariner.pets.${id}`]: 1 },
    linked: [{ id: "mariner.offerpet.waitanswer" }],
  });
}

export function generatePets() {
  mod.initializeAspectFile("aspects.pets", ["pets"]);
  mod.initializeElementFile("pets", ["pets"]);
  mod.initializeRecipeFile("recipes.pets", ["pets"]);
  mod.initializeDeckFile("decks.pets", ["pets"]);

  mod.setDeck("decks.pets", {
    id: "mariner.decks.storedpets",
    resetOnExhaustion: false,
    spec: [],
  });

  mod.setAspect("aspects.pets", {
    id: "mariner.pet",
    label: "Pet",
    description: "<>",
  });
  mod.setAspect("aspects.pets", {
    id: "mariner.petbond",
    label: "Pet Bond",
    description: "<>",
  });

  generatePetHandlingSystem();

  // Generate the pet element, and a "petsit" aspect version.
  for (const pet of PETS) {
    generatePet(pet);
  }
}

export function generatePetsittingOverlays() {
  return PETS.map((pet) => ({
    image: `pets/${pet.id}/${pet.id}_petsitted`,
    expression: `[mariner.givenpet.${pet.id}]`,
  }));
}

const PETS = [
  {
    id: "ferret",
    stages: [
      {
        label: "A Watchful Ferret",
        description:
          "Sneaky shadow, claw sounds on the planks, pinky snout peeking from under the furnitures. Ever watching, ever expecting something to happen. Some could argue all these are just a play of their imagination, were it not for the fact that it leaves little food stashes all over the ship.",
      },
      {
        label: '"Hadashi"',
        description:
          'Someone named it "Hadashi", and it stuck since. It accepts little nuggets of food, and likes to follow the crew around, at a distance. It seems to always be aware of anything happening an instant before we do.',
        aspects: ["knock", "lantern"],
      },
      {
        label: "Hadashi, Ever Watching",
        description: "something something",
        aspects: ["knock", "lantern"],
      },
    ],
  },
];
