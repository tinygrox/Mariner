import { SIGNALS } from "./generate_signal_aspects.mjs";
import { EXPERIENCE_CLEARED_BY } from "./stories_generation/generate_stories.mjs";

function generateExperience({ id, label, description, lifetime, aspects, apply }) {
  const decayedId = `mariner.experiences.${id}.decayed`;
  mod.setElement("experiences", {
    id: `mariner.experiences.${id}`,
    icon: `experiences/${id}`,
    label,
    description,
    aspects: { "mariner.experience": 1, ...(aspects ?? {}) },
    lifetime: lifetime ?? 480,
    decayTo: decayedId,
    xtriggers: {
      [SIGNALS.DUPLICATE_EXPERIENCES]: {
        morpheffect: "spawn",
        id: `mariner.experiences.${id}`,
      },
    },
    slots: [
      {
        id: "story",
        ifAspectsPresent: { "mariner.bar": 1 },
        label: "Story",
        description: "<related story to clear exp>",
        actionId: "talk",
        addsSlots: true,
        required: {
          "mariner.story": 1,
        },
      },
    ],
  });
  mod.setElement("experiences", {
    id: `mariner.experiences.${id}.standby`,
    // TODO: improve with a nice overlay or something
    overlays: [{ image: `experiences/${id}`, grayscale: true, color: "#A6A6AA" }],
    label: `${label} [stand by]`,
    description,
    aspects: {
      "mariner.experience": 1,
      "mariner.experience.standby": 1,
      ...(aspects ?? {}),
    },
    xtriggers: {
      [SIGNALS.ENABLE_EXPERIENCE_DECAY]: `mariner.experiences.${id}`,
      [SIGNALS.DUPLICATE_EXPERIENCES]: {
        morpheffect: "spawn",
        id: `mariner.experiences.${id}.standby`,
      },
    },
  });
  mod.setElement("experiences", {
    id: decayedId,
    label: `${label} [decayed]`,
    overlays: [{ image: `experiences/${id}`, grayscale: true, color: "#000000" }],
    description,
    aspects: { "mariner.experience.decayed": 1, ...(aspects ?? {}) },
    xtriggers: {
      [SIGNALS.DUPLICATE_EXPERIENCES]: {
        morpheffect: "spawn",
        id: decayedId,
      },
    },
  });

  apply({ id, decayedId });
}

export function generateExperiences() {
  mod.initializeAspectFile("aspects.wounds", ["experiences"]);
  mod.setAspects(
    "aspects.wounds",
    {
      id: "mariner.groundedness",
      isAspect: true,
      label: "Groundedness",
      description:
        "The mundane things ground me in the present and help me overcome the Black Dog and the visions. Actions, especially the banal, place my feet back on the ground and moving forward, back into the world.",
    },
    {
      id: "mariner.wound",
      icon: "follower_wound",
      label: "Experience",
      description: "The Tally of the Soul. Three of these will rend my Heart Asunder.",
    },
    //TODO: this text doesn't really fit what it is?
    {
      id: "mariner.experience",
      label: "An Experience",
      description: '"I strayed too far from the placid island of ignorance mortal minds can handle."',
    },
    {
      id: "mariner.experience.standby",
      label: "No Time To Reflect",
      description:
        "We're in the middle of something. Let's stay focused...But this will affect us later, once our focus softens and the periphery comes creeping in.",
    },
    {
      id: "mariner.experience.lifelesson",
      label: "Life lesson",
      description: "Experiences that paint perception in perpetuity.",
    },
    {
      id: "mariner.experience.decayed",
      isHidden: true,
    }
  );

  mod.initializeElementFile("experiences", ["experiences"]);
  mod.initializeRecipeFile("recipes.applyexperiences", ["experiences"]);

  const CONSUME_AND_WOUND_SIGNAL_AND_LINK = (decayedId, signal, quantity = 1) => ({
    furthermore: [
      {
        target: "~/exterior",
        effects: { [decayedId]: -1 },
        aspects: { [signal]: quantity },
      },
    ],
    linked: [
      {
        id: "mariner.season.wounds.loop",
      },
    ],
  });

  generateExperience({
    id: "rattling",
    label: "A Rattling Experience",
    description:
      '"What I have lived through has shaken me too my core. Traces of it keep echoing in the hollow spaces of my soul, and in each reverberation renew its disquiet."',
    aspects: EXPERIENCE_CLEARED_BY("grim"),
    apply: ({ id, decayedId }) => {
      mod.setRecipe("recipes.applyexperiences", {
        id: `mariner.season.wounds.apply.${id}`,
        tablereqs: { [decayedId]: 1 },
        ...CONSUME_AND_WOUND_SIGNAL_AND_LINK(decayedId, SIGNALS.WOUND_LH),
      });
    },
  });

  generateExperience({
    id: "dreadful",
    label: "A Dreadful Experience",
    description:
      '<"A deep gloom has settled over my being after the past events. Soon, it might end up staining me until only darkness filters through to my heart.">',
    aspects: EXPERIENCE_CLEARED_BY("grim"),
    apply: ({ id, decayedId }) => {
      mod.setRecipe("recipes.applyexperiences", {
        id: `mariner.season.wounds.apply.${id}`,
        tablereqs: { [decayedId]: 1 },
        ...CONSUME_AND_WOUND_SIGNAL_AND_LINK(decayedId, SIGNALS.WOUND_LH, 2),
      });
    },
  });

  generateExperience({
    id: "unsettling",
    label: "An Unsettling Experience",
    description:
      "<My heart rate will not return to its regular beating. At the slightest sound my thoughts spiral away in unruly patterns.>",
    aspects: EXPERIENCE_CLEARED_BY("whimsical"),
    apply: ({ id, decayedId }) => {
      mod.setRecipe("recipes.applyexperiences", {
        id: `mariner.season.wounds.apply.${id}`,
        tablereqs: { [decayedId]: 1 },
        ...CONSUME_AND_WOUND_SIGNAL_AND_LINK(decayedId, SIGNALS.WOUND_HH),
      });
    },
  });

  generateExperience({
    id: "vivid",
    label: "An Vivid Experience",
    description:
      "<I have peeked past the veil that covers the black seas of existence, and the sights I’ve seen will not leave me. When I close my eyes, the sights return.>",
    aspects: EXPERIENCE_CLEARED_BY("whimsical"),
    apply: ({ id, decayedId }) => {
      mod.setRecipe("recipes.applyexperiences", {
        id: `mariner.season.wounds.apply.${id}`,
        tablereqs: { [decayedId]: 1 },
        ...CONSUME_AND_WOUND_SIGNAL_AND_LINK(decayedId, SIGNALS.WOUND_HH, 2),
      });
    },
  });

  generateExperience({
    id: "unraveling",
    label: "An Unraveling Experience",
    description:
      "<I have been rent. The strain of what occurred has caused a fraying at my edges, and I fear what part of me may get torn away.>",
    aspects: EXPERIENCE_CLEARED_BY("grim", "whimsical"),
    apply: ({ id, decayedId }) => {
      mod.setRecipe("recipes.applyexperiences", {
        id: `mariner.season.wounds.apply.${id}`,
        tablereqs: { [decayedId]: 1 },
        furthermore: [
          {
            target: "~/exterior",
            effects: { [decayedId]: -1 },
            aspects: { [SIGNALS.WOUND_HH]: 1, [SIGNALS.WOUND_LH]: 1 },
          },
        ],
        linked: [
          {
            id: "mariner.season.wounds.loop",
          },
        ],
      });
    },
  });

  generateExperience({
    id: "divisive",
    label: "A Divisive Experience",
    description:
      "<What has occurred has not been easy on the crew. If I do not rally them, tempers will rise and some will seek less straining employ.>",
    aspects: EXPERIENCE_CLEARED_BY("rousing"),
    apply: ({ id, decayedId }) => {
      mod.setRecipe("recipes.applyexperiences", {
        id: `mariner.season.wounds.apply.${id}`,
        tablereqs: { [decayedId]: 1 },
        furthermore: [
          {
            target: "~/exterior",
            effects: { [decayedId]: -1 },
            mutations: [
              {
                filter: "mariner.crew",
                mutate: "mariner.crew.longing",
                level: 3,
              },
            ],
          },
        ],
        linked: [
          {
            id: "mariner.season.wounds.loop",
          },
        ],
      });
    },
  });

  generateExperience({
    id: "sobering",
    label: "A Sobering Experience",
    description:
      "<Sometimes the most horrible shock is a shift in perspective. What value lies in what I chase behind the waves>",
    aspects: EXPERIENCE_CLEARED_BY("rousing"),
    apply: ({ id, decayedId }) => {
      mod.setRecipe("recipes.applyexperiences", {
        id: `mariner.season.wounds.apply.${id}`,
        tablereqs: { [decayedId]: 1 },
        effects: {},
        furthermore: [
          {
            target: "~/exterior",
            effects: { [decayedId]: -1, "mariner.thrill": -10 },
            aspects: { [SIGNALS.WOUND_LH]: 1 },
          },
        ],
        linked: [
          {
            id: "mariner.season.wounds.loop",
          },
        ],
      });
    },
  });

  generateExperience({
    id: "sophorific",
    label: "A Sophorific Experience",
    description:
      "<The aftermath of the event still clings to me like nettles or old perfume. I may wish for nothing more than to be rid of it, or nothing more than to experience the more, the more, the more.>",
    aspects: EXPERIENCE_CLEARED_BY("rousing"),
    apply: ({ id, decayedId }) => {
      mod.setRecipe("recipes.applyexperiences", {
        id: `mariner.season.wounds.apply.${id}`,
        tablereqs: { [decayedId]: 1 },
        furthermore: [
          {
            target: "~/exterior",
            effects: { [decayedId]: -1, "mariner.wanderlust": 3 },
            aspects: { [SIGNALS.WOUND_HH]: 1 },
          },
        ],
        linked: [
          {
            id: "mariner.season.wounds.loop",
          },
        ],
      });
    },
  });

  generateExperience({
    id: "pain",
    label: "An Experience of Pain",
    description:
      "<I have been hurt, and now hurt is echoing through my systems, turning my sensens to plinters, and my heartbeat to a dull throb.>",
    lifetime: 10,
    apply: ({ id, decayedId }) => {
      mod.setRecipe("recipes.applyexperiences", {
        id: `mariner.season.wounds.apply.${id}`,
        tablereqs: { [decayedId]: 1 },
        furthermore: [
          {
            target: "~/exterior",
            effects: { [decayedId]: -1 },
            aspects: { [SIGNALS.WOUND_LH]: 1 },
          },
          {
            target: "~/extant",
            aspects: { [SIGNALS.EXHAUST_LH]: 1 },
          },
        ],
        linked: [
          {
            id: "mariner.season.wounds.loop",
          },
        ],
      });
    },
  });
  generateExperience({
    id: "injury",
    label: "The Experience of Impairment",
    description:
      "<I have been wounded, and my limbs do not serve me as they ought too. And as a captain I know that if one part fails, the whole is in peril.>",
    lifetime: 10,
    apply: ({ id, decayedId }) => {
      mod.setRecipe("recipes.applyexperiences", {
        id: `mariner.season.wounds.apply.${id}`,
        tablereqs: { [decayedId]: 1 },
        furthermore: [
          {
            target: "~/exterior",
            effects: { [decayedId]: -1 },
            aspects: { [SIGNALS.WOUND_HH]: 1 },
          },
          {
            target: "~/extant",
            aspects: { [SIGNALS.EXHAUST_HH]: 1 },
          },
        ],
        linked: [
          {
            id: "mariner.season.wounds.loop",
          },
        ],
      });
    },
  });
}
