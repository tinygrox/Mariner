import { probabilityChallengeTemplates } from "../probability_challenge_templates.mjs";
import { generateProbabilityChallengeTask } from "../vault_generation/vault_generation.mjs";
import { SIGNALS } from "../generate_signal_aspects.mjs";
export function generateCleanHostileTheaterAction() {
  const recipesFile = "recipes.cleanhostile.theater";
  mod.initializeRecipeFile(recipesFile, ["hostility"]);

  generateProbabilityChallengeTask({
    recipesFile,
    actionId: "explore",
    task: {
      id: "mariner.cleanhostile.theater",
      label: "Blackened Name",
      description:
        "My name is infamous, and staging me might spell more trouble than the theater director thinks is worth. I may be able to convince them that any publicity is good publicity, however.  [Adres this challenge with Grail or Knock.]",
      craftable: true,
      warmup: 0,
      aspects: { [SIGNALS.USE_HEART]: 1 },
      requirements: {
        "mariner.theater": 1,
        "mariner.location.hostile": 1,
      },
    },
    success: {
      label: "False Witness",
      description:
        "we vow to make a show, critiques be damned. the director says they hope they won't regret this. I do not say that I am confident they will",
      aspects: { "mariner.location.makenothostile": 1 },
    },
    failure: {
      label: "Careful Heads Prefale",
      description:
        '"They regret to inform me" -Of course, they always regret. "that they cannot engage me at this moment." I am assured that they fully support me, ofcourse, that I simply must return when public opinion has turned.',
      // aspects: { "mariner.location.makenothostile": 1 },
      effects: { "mariner.notoriety": 1 },
      linked: [{ id: "mariner.trouble.start" }],
    },
    template: probabilityChallengeTemplates.FORKED_TONGUE,
  });
}
