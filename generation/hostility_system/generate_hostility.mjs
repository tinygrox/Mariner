import { generateCleanHostileBarAction } from "./bar.mjs";
import { generateCleanHostileMarketAction } from "./market.mjs";
import { generateCleanHostileNpcAction } from "./npc.mjs";
import { generateCleanHostileTheaterAction } from "./theater.mjs";
import { generateTroubleToken } from "./trouble_token.mjs";
import { generateCleanHostileWharfAction } from "./wharf.mjs";

export function generateHostilitySystem() {
    generateCleanHostileBarAction();
    generateCleanHostileWharfAction();
    generateCleanHostileMarketAction();
    generateCleanHostileNpcAction();
    generateCleanHostileTheaterAction();
    generateTroubleToken();
}