/*
- DONE heart has 3 xtriggers pickoption1, pickoption2, pickoption3 that just apply pickedoption1, pickedoption2, pickedoption3 to the heart
- DONE these three aspects auto-remove themselves (autocatalyst, setmutation level 0)
- DONE display the choice proposal recipe, with [1-3] choice slots, with the "xtrigger": "pickoption1", ... property, and only halfheart as the acceptable card. It has to display a description (in case cards are put and then removed from the slots and the alt texts disappear)
- DONE An alt recipe for each of the possible choices (use: "grandReqs": { "[~/situation/slots/option1:$count] || [pickedoption1]": 1 }), linking to the actual outcome (at the end of the warmup, before linking, the heart's mutation will clean itself out)
- DONE a potential default recipe end description and link.
*/

import { SIGNALS } from "./generate_signal_aspects.mjs";

const autoClearingAspect = (aspect) => ({
  [aspect]: {
    morpheffect: "setmutation",
    id: aspect,
    level: 0,
  },
});

const autoClearItself = (number) => autoClearingAspect(`mariner.pickedoption${number}`);

export function generateOptionSelectionRecipe() {
  if (!mod.aspects["aspects.optionsselection"]) mod.initializeAspectFile("aspects.optionsselection", ["optionsselection"]);

  mod.setHiddenAspect("aspects.optionsselection", {
    id: "mariner.pickedoption1",
    xtriggers: autoClearItself(1),
  });
  mod.setHiddenAspect("aspects.optionsselection", {
    id: "mariner.pickedoption2",
    xtriggers: autoClearItself(2),
  });
  mod.setHiddenAspect("aspects.optionsselection", {
    id: "mariner.pickedoption3",
    xtriggers: autoClearItself(3),
  });
}

export function optionSelectionRecipe({
  id,
  label,
  craftable,
  actionId,
  startdescription,
  description,
  linked,
  grandReqs,
  options,
}) {
  options = options.slice(0, 3);
  return [
    {
      id,
      actionId,
      craftable,
      grandReqs,
      label,
      startdescription,
      description,
      warmup: 10,
      linked,
      slots: options.map((o, index) => ({
        id: `option${index + 1}`,
        label: o.slot?.label ?? `Option ${index + 1}`,
        description: o.slot?.description ?? `Option ${index + 1}`,
        required: { "mariner.halfheart": 1 },
        xtrigger: SIGNALS[`PICK_OPTION_${index + 1}`],
      })),
      alt: options.map((o, index) => generateOptionAltRecipes(id, o, index + 1)).flat(),
    },
  ];
}

function generateOptionAltRecipes(id, option, index) {
  if (option.recipe) {
    return [
      {
        id: `${id}.pickoption${index}`,
        grandReqs: {
          [`[~/situation/slots/option${index}:$count] || [mariner.pickedoption${index}]`]: 1,
        },
        movements: {
          "~/tabletop": ["mariner.halfheart"],
          ...option.recipe.movements,
        },
        ...option.recipe,
      },
    ];
  }
  if (option.recipes) {
    return option.recipes.map((recipe, recipeNb) => ({
      id: `${id}.pickoption${index}.${recipeNb + 1}`,
      ...recipe,
      grandReqs: {
        [`[~/situation/slots/option${index}:$count] || [mariner.pickedoption${index}]`]: 1,
        ...recipe.grandReqs,
      },
      movements: {
        "~/tabletop": ["mariner.halfheart"],
        ...recipe.movements,
      },
    }));
  }

  return [];
}
