import { LORE_ASPECTS } from "./generation_helpers.mjs";

export const ENUM_UNSET = 0;
export const ENUMS = {};
export const ENUM_LABELS = {};

export function generateRootEnum(enumId, dic) {
  mod.initializeAspectFile(`enum.${enumId}`, ["enums"]);
  const newEnum = {};
  const newEnumLabels = {};
  for (const [id, value] of Object.entries(dic)) {
    mod.setAspect(`enum.${enumId}`, { id: `mariner.enum.${enumId}.${id}` });
    newEnum[id] = value;
    newEnumLabels[id] = `mariner.enum.${enumId}.${id}`;
  }
  ENUMS[enumId] = newEnum;
  ENUM_LABELS[enumId] = newEnumLabels;
}

export function generateRootEnums() {
  mod.initializeAspectFile("enum.defaults", ["enums"]);
  mod.setHiddenAspect("enum.defaults", { id: "mariner.enum.unset" });
  generateRootEnum("principles", Object.fromEntries(LORE_ASPECTS.map((lore, index) => [lore, index + 1])));

  mod.readRecipesFile("recipes.firstarrival", ["sailing"]);
  const injectionRecipe = mod.getRecipe("recipes.firstarrival", "mariner.firstnavigation");
  injectionRecipe.rootAdd = {
    ...(injectionRecipe.rootAdd ?? {}),
    ...generateRootEffect(),
  };
}

export function generateRootEffect() {
  const rootEffect = { "mariner.enum.unset": 0 };
  for (const [enumId, enumObject] of Object.entries(ENUMS)) {
    for (const [id, value] of Object.entries(enumObject)) {
      rootEffect[`mariner.enum.${enumId}.${id}`] = value;
    }
  }
  return rootEffect;
}
