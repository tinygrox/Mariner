import Chance from "../chance.js";

export const r = new Chance("packages");

export function randomInArray(arr) {
  if (!arr || arr.length === 0) return undefined;
  if (typeof arr === "string") return arr;
  return arr[r.natural({ max: arr.length - 1 })];
}

export function randomEntry(obj) {
  return Object.values(obj)[r.natural({ max: Object.keys(obj).length - 1 })];
}

export function defineQuestionChance(questionId, chance) {
  const recipe = mod.getRecipe("recipes.getpackage", "mariner.getpackage.loop");
  const suspiciousRecipe = mod.getRecipe(
    "recipes.getpackage",
    "mariner.getpackage.loop.manypackages"
  );

  recipe.linked[0].chances = {
    ...recipe.linked[0].chances,
    [questionId]: chance,
  };
  suspiciousRecipe.linked[0].chances = {
    ...suspiciousRecipe.linked[0].chances,
    [questionId]: chance,
  };
}
