import { generateAspectBasedQuestions } from "./aspect_based_questions.mjs";

export function generateQuestions() {
  generateAspectBasedQuestions();
}
