import { randomInArray } from "./helpers.mjs";

export const PACKAGE_DETAILS = {
    stained: pd => {
        pd.stained = randomInArray(["speckled", "spotted", "smeared"]);
        pd.stainMaterial = randomInArray(["blood", "black mold", "green mold", "mud"]);
    },
    wrapped: pd => {
        pd.wrapped = randomInArray(["many layers", "crumpled paper", "thick expensive paper"]);
        pd.wrappingType = randomInArray(["with care", "hastily", "partially covered by shellfish"]);
    },
    stamped: pd => {
        pd.stamped = randomInArray(["unknown stamps", "occult stamps", "no stamp"]);
        pd.stampType = randomInArray(["beautiful", "disturbing", "old"]);
    },
    tagged: pd => {
        pd.tagged = randomInArray(["wrong address", "no address", "handwritten note", "partially erased address"])
    },
    engraved: pd => {
        pd.engraved = randomInArray(["rich motives", "formulas", "strange symbols", "words from an unknown language"])
    },
    stinking: pd => {
        pd.odour = randomInArray(["sea water", "perfume", "decomposition"])
        pd.odourStrength = randomInArray(["subtle", "reeking"])
    },
    weight: pd => {
        pd.weight = randomInArray(["abnormally light", "abnormally heavy", "quite heavy", "quite light"])
    }
}
