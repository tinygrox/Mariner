import { randomEntry, randomInArray } from "./helpers.mjs";

const note = (g) => ({
    filter: g.filter,
    generate: (pd) => `${randomEntry(NOTE_DETAIL).generate(pd, g.generate(pd))}`
})

const NOTE_DETAIL = {
    /*
    stained: (m, pd) => {
        pd.stained = randomInArray(["speckled", "spotted", "smeared"]);
        pd.stainMaterial = randomInArray(["blood", "black mold", "green mold", "mud"]);
    },
    wrapped: (m, pd) => {
        pd.wrapped = randomInArray(["many layers", "crumpled paper", "thick expensive paper"]);
        pd.wrappingType = randomInArray(["with care", "hastily", "partially covered by shellfish"]);
    },
    stamped: (m, pd) => {
        pd.stamped = randomInArray(["unknown stamps", "occult stamps", "no stamp"]);
        pd.stampType = randomInArray(["beautiful", "disturbing", "old"]);
    },
    tagged: (m, pd) => {
        pd.tagged = randomInArray(["wrong address", "no address", "handwritten note", "partially erased address"])
    },
    engraved: (m, pd) => {
        pd.engraved = randomInArray(["rich motives", "formulas",  "strange symbols", "words from an unknown language"])
    },
    stinking: (m, pd) => {
        pd.odour = randomInArray(["sea water", "perfume", "decomposition"])
        pd.odourStrength = randomInArray(["subtle", "reeking"])
    }
    */
    default: {
        filter: () => true,
        generate: (pd, clue) => `There is a note sticked to the package. It reads: "${clue}"`
    }
}

export const CLUE_SENTENCES = {
    /*
    Provided stuff :
    - location
    - other locations in the sea this might have gone before going here
    - known languages
    - used names
    - used initials
    - gender
    - what carriers/senders should say to do with it
    Clue types:
    - stamps from all over the sea but the [destination port]
    - note saying something about:
        - the gender
        - what to do with it
        - something about a special variation of a language the person knows about (engraved symbols, language,)
        - something about the profession of the person
        - something about the place of the person
        - something about partially erased name
        - something about initials
    */
    stampsFromPreviousLocations: {
        filter: (pd) => pd.previousPackageLocations && pd.stamped && pd.stamped !== "no stamp",
        generate: (pd) => `This package has travelled a lot. Looking at the stamps on it, it already went to several places in this sea alone : I can see ${pd.previousPackageLocations.join(', and ')}.`
    },

    noteGender: note({
        filter: (pd) => pd.gender,
        generate: (pd) => `We took a lot of risks for this to get to you, ${pd.gender === "female" ? "Madam" : "Sir"}. ${randomInArray(["Take care of it.", "Do not contact us again.", "Don't talk about this to anyone !"])}`
    }),

    languageRecipient: {
        filter: (pd) => pd.knownLanguages,
        generate: (pd) => `I can see the top words of the attached note. It appears to be written in ${randomInArray(pd.knownLanguages)}.`
    },

    adressedToInitials: {
        filter: (pd) => pd.usedInitials,
        generate: (pd) => `The only sign of an adress to the recipient is the marking of the initials ${randomInArray(pd.usedInitials)}.`
    },

    instructions: {
        filter: (pd) => pd.whatToDoWithIt,
        generate: (pd) => `A note comes attached to the package, adressed to the courier. It reads: ${randomInArray(pd.whatToDoWithIt)}.`
    },

    specificMorland: {
        filter: (pd) => pd.whatToDoWithIt && pd.recipientId === "morland",
        generate: (m, pd) => randomInArray([
            `A stack of books, neatly wrapped. "for the shop", read the attached note.`,
            `A square package. "Hush house wanted to acquire this, but I believe you can find a much more loving home for it. Careful who you sell it to." `,
            `The note of this package is adressed to the deliverer: "I know she does not have a return policy, but I simply could not keep this in my house anymore.`
        ])
    },

    specificAgdistis: {
        filter: (pd) => pd.whatToDoWithIt && pd.recipientId === "agdistis",
        generate: (m, pd) => randomInArray([
            `An oddly heavy box marked only as 'Costumes'`,
            `A small bundle of papers with a note "For my old partner, an update on the Lady of Wires"`
        ])
    },

    specificHarald: {
        filter: (pd) => pd.whatToDoWithIt && pd.recipientId === "harald",
        generate: (m, pd) => randomInArray([
            `There is a note attached to stack of papers "Get these files to the Maestro."`,
            `When I move this box, a soft tinkling music can be heard. The note attached only says "To inspire him again".`
        ])
    },

    specificVeiledLady: {
        filter: (pd) => pd.whatToDoWithIt && pd.recipientId === "veiledlady",
        generate: (m, pd) => randomInArray([
            `A small, delicate box, possibly for jewelry. "Tell her she broke my heart, and that I can't wait to see her again."`
        ])
    },
}