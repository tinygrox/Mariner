const injectId = (obj) => {
  return Object.fromEntries(
    Object.entries(obj).map(([id, template]) => [
      id,
      { id: id.toLowerCase(), ...template },
    ])
  );
};

export const probabilityChallengeTemplates = injectId({
  // MENTAL
  PERCEIVE: {
    base: {
      label: "Sight and Insight ",
      description:
        "A challenge of perception or understanding, which I can tackle with the bright Lantern or the intuitive Knock.",
    },
    aspects: {
      lantern: {
        low: {
          label: "Amber Eyes",
          description:
            "Behind my eyes, a candle, and it's flickers reveal the true path forward. I may succeed.",
        },
        med: {
          label: "Silver Eyes",
          description:
            "Behind my eyes, a lamp. Ony truth withstands my gaze. I will likely succeed.",
        },
        high: {
          label: "Golden Eyes",
          description:
            "Behind my eyes, a sun. I need not look, for the truth is in me allready. I will probably succeed.",
        },
      },
      knock: {
        low: {
          label: "Creeping Insight",
          description:
            "Inside my skull, a door stands ajar, and sometimes insights slip through. I may find what I am looking for.",
        },
        med: {
          label: "Deep Insight",
          description:
            "Inside my skull, a door stands open, and I know what is obscured or lost. I will likely succeed.",
        },
        high: {
          label: "Hallowed Insight",
          description:
            "Inside my skull, a door is shattered. The path is clear. I will probably succeed.",
        },
      },
    },
  },
  STUDY: {
    base: {
      label: "A Problem to Study",
      description:
        "This task requires time and deep thought. I can approach it either with studiousness of Lantern or the intensity of Winter.",
    },
    aspects: {
      lantern: {
        low: {
          label: "A Singular Focus",
          description:
            "I will shape my mind into lens to process information. If it does not crack, I may succeed.",
        },
        med: {
          label: "A Shining Focus",
          description:
            "I will shape my mind into lens to process information. Clear and strong, I will probably succeed.",
        },
        high: {
          label: "A Brilliant Focus",
          description:
            "I will shape my mind into lens to process information. It brightens the information that passes through it. I will probably succeed.",
        },
      },
      winter: {
        low: {
          label: "Quiet Concentration",
          description:
            "Inside my mind, thoughts settle like snowflakes. I may succeed.",
        },
        med: {
          label: "Silent Concentration",
          description:
            "Inside my mind, thoughts sing like settling ice. I will likely succeed.",
        },
        high: {
          label: "Still Concentration",
          description:
            "Inside my mind, thoughts crystalize in perfect form. I will probably succeed.",
        },
      },
    },
  },
  DESIGN: {
    base: {
      label: "A Bright Idea is Needed",
      description:
        "For what I face now, no solution is apparent. However, I could create one. I can approach this challenge with either the brilliance of Lantern or the ingeniuity of Forge.",
    },
    aspects: {
      lantern: {
        low: {
          label: "A Flicker of Brilliance",
          description:
            "One can find the right solution to any problem. If I cast out the doubtful shadows from my thoughts, I will find that solution. We may yet succed.",
        },
        med: {
          label: "A Glimmmer of Brilliance",
          description:
            "One can find the proper solution to any problem. If I think without mercy and without mistakes, I will find that solution. We will likely succeed.",
        },
        high: {
          label: "A Flare of Brilliance",
          description:
            "One can find the true solution to any problem. Like the Watchman, I will navigate towards that Truth. We will probably succeed.",
        },
      },
      forge: {
        low: {
          label: "A Spark of Inspiration",
          description:
            "My mind is like a smelter, which inputs sensations and outputs ideas. We may yet succeed.",
        },
        med: {
          label: "A Flash of Inspiration",
          description:
            "My mind is like a smelter- no concept that enters it remains unchanged. We will likely succeed.",
        },
        high: {
          label: "A Blaze of Inspiration",
          description:
            "My mind is like a smelter. I blaze like the Malleary, and if I speak I might spit flame. We will probably succeed.",
        },
      },
    },
  },
  //PHYSICAL
  ENDURE: {
    base: {
      label: "Withstand a Challenge",
      description:
        "This challenge is going to take its toll. A lesser person may fall, but I have withstood worse. I could approach this with the vim of Heart or the seniority of Edge.",
    },
    aspects: {
      edge: {
        low: {
          label: "Straining Scars",
          description:
            "I have weathered worse before. I'll remember the tricks I learned from those encounters. We may yet succed.",
        },
        med: {
          label: "Resilient Scars",
          description:
            "I have survived worse before, and in my bones and in my soul I carry scars that cannot be deepened. We will likely succeed.",
        },
        high: {
          label: "Indomitable scars",
          description:
            "I have conquered worse before. The Colonel's lessons are carved upon my skin. We will almost certainly succeed. ",
        },
      },
      heart: {
        low: {
          label: "Unbowed",
          description:
            "Like the battered pines, who bleed resin, I will weather this storm. We may yet succeed.",
        },
        med: {
          label: "Unbent",
          description:
            "Like the shifting reeds, who bend but do not break, I'll weather this storm. We will likely succeed.",
        },
        high: {
          label: "Unbroken",
          description:
            "Like the grand Ring-Yew, who encircles and renews, I will weather this storm. We will almost certainly succeed. ",
        },
      },
    },
  },
  OVERCOME: {
    base: {
      label: "Enforce my Designs",
      description:
        "There is someone or something between me and my desires. Time to show them my superiority. I could approach this with the force of Forge or the strength of Edge.",
    },
    aspects: {
      edge: {
        low: {
          label: "A Desire For Struggle",
          description:
            "I will match my skill to the challenge. We may yet succed.",
        },
        med: {
          label: "A Desire For Victory",
          description:
            "I will surpass the challenge. In my blood lie the roots of victory. We will likely succeed.",
        },
        high: {
          label: "A Desire For Dominion",
          description:
            "I will rise to the challenge, like the Spider rises towards the Glory. We will probably succeed.",
        },
      },
      forge: {
        low: {
          label: "Stronger than Brass",
          description:
            "I grow stronger. Only through testing my limits, can I discover how to surpass them. We may yet succed.",
        },
        med: {
          label: "Stronger than Iron",
          description:
            "I grow stronger. I'll display my ingenuity against men or nature. We will likely succeed.",
        },
        high: {
          label: "Stronger than Steel",
          description:
            "I've grown strong. Like the Lionsmith, I'll grow stronger still. We will probably succeed.",
        },
      },
    },
  },
  AVOID: {
    base: {
      label: "Avoid Opposion",
      description:
        "Something seeks us, chases us or challenges us. We could outsmart it with the cunning of Edge or sneak past it with the blessings of the Moth.",
    },
    aspects: {
      edge: {
        low: {
          label: "Tricky Tactics",
          description:
            "With dirty tricks and quick action, we can outsmart our opposition. We may yet succeed.",
        },
        med: {
          label: "Sly Tactics",
          description:
            "With foul play and decisive action, we can outsmart our opposition. We may yet succed.",
        },
        high: {
          label: "Devious Tactics",
          description:
            "With skullduggery and stirring action, We can outsmart our opposition. We will almost certainly succeed.",
        },
      },
      moth: {
        low: {
          label: "Silken Footsteps",
          description:
            "If we move with care and stick to the shadows, we can avoid their grasp. We may yet succeed.",
        },
        med: {
          label: "Shadowed Steps",
          description:
            "If we move with care and cling to the shadows, we can avoid their gaze. We are likely to succeed.",
        },
        high: {
          label: "Steps No More",
          description:
            "If we move with care and meld with the shadows, we can avoid their attention. We will almost certainly succeed.",
        },
      },
    },
  },
  // SKILLED/PRACTICAL
  OPERATE: {
    base: {
      label: "Perform an Operation",
      description:
        "Be it delicate machinery, intricate lock or the fineries of flesh and sinew, this is a task that requires Precision of Knock or the Ingenuity of Forge.",
    },
    aspects: {
      forge: {
        low: {
          label: "Clever Fingers",
          description:
            "For an applied entusiast, the imagination offers endless approaches. With luck, their skills may be up to task as well.  We may yet succeed.",
        },
        med: {
          label: "Apt Fingers",
          description:
            "For a skilled craftsman, the tools are an extension of the self. I wield them with the certiainty of a knight their sword and armor. We will likely succeed. ",
        },
        high: {
          label: "Gifted Fingers",
          description:
            "For a true artisan, the hands are all the tools you need. I will adjust reality to my precise designs. We will almost certainly succeed.",
        },
      },
      knock: {
        low: {
          label: "Insight and Care",
          description:
            "The world operates by reliable systems, that can be modified by calm hand and a burglers' demeanor.  We may yet succeed.",
        },
        med: {
          label: "Intuition and Precision",
          description:
            "The world operates by a clockwrights' logic, that can be callibrated with a steady hand and a surgeons' nerves. The world operates by a We will likely succeed. ",
        },
        high: {
          label: "Instinct and Clarity",
          description:
            "The world operates by higher laws, that can be adjusted with a deft hand and heirophants' compassion. We will almost certainly succeed.",
        },
      },
    },
  },
  BREAK_IN: {
    base: {
      label: "Pass a Barrier",
      description:
        "There is a barrier that opposes us. We can overcome it with the force of Forge or guile of Knock.",
    },
    aspects: {
      forge: {
        low: {
          label: "Sulphur and Salt",
          description:
            "Mix the reagents into an unstable mixture and apply it with trembling fingers. We may yet succeed.",
        },
        med: {
          label: "Nitrate and Chloride",
          description:
            "Mix the reagents into a volatile mixture and apply it with anticipating fingers. We will likely succeed.",
        },
        high: {
          label: "Fire and Acid",
          description:
            "Mix the reagents into a glorious mixture and apply it certain fingers. We will almost certainly succeed.",
        },
      },
      knock: {
        low: {
          label: "Through the Treshhold",
          description:
            "Like thieves, we will coax movement through locks and barriers. We may yet succeed.",
        },
        med: {
          label: "Through the Keyhole",
          description:
            "Like snakes, we will find paths through locks and barriers. We will likely succeed.",
        },
        high: {
          label: "Through the Cracks",
          description:
            "Like princes, we will demand passage through locks and barriers. We will almost certainly succeed.",
        },
      },
    },
  },
  TAMPER: {
    base: {
      label: "Tamper with an Apparatus",
      description:
        "These designs have an intended way of working. I have other intentions in in mind. We can accomplish this with discordant Moth or entropic Winter.",
    },
    aspects: {
      winter: {
        low: {
          label: "Crack and Curdle",
          description:
            "All things falter. My touch is cold, my actions calculated. After me this machine will be less than it was. We may yet succeed.",
        },
        med: {
          label: "Bleach and Blister",
          description:
            "All things fail. My touch is hatred. After me, there will be a whimper and a whine and then a gurgling rattle of death.  We will likely succeed.",
        },
        high: {
          label: "Rust and Rot",
          description:
            "All things end. My touch is bitter snow, and after me, only silence. We will almost certainly succeed.",
        },
      },
      moth: {
        low: {
          label: "The Movements of Mischief",
          description:
            "All things desire change. The engineer might have dreamt up a great contraption, but it would just as gladly be parts. And gladly we will make it so. We may yet succeed.",
        },
        med: {
          label: "The Maneuvres of Abbandon ",
          description:
            "All things yearn to change. Any moment the smallest particles could scatter like a swarm at a slight agitation. And we can be very agitating. We will likely succeed. ",
        },
        high: {
          label: "The Roads That Chaos Walk",
          description:
            "All things ache for change. Strain no more, little ones, we will set you free. We will almost certainly succeed.",
        },
      },
    },
  },
  // SOCIAL
  HONEYED_TONGUE: {
    base: {
      label: "The Honeyed Tongue",
      description:
        "A bright smile can disarm many conflicts, and sweetened words may leave many a man vulnerable. I can approach this task with the allure of Grail or the charm of Heart.",
    },
    aspects: {
      grail: {
        low: {
          label: "Words like Dark Syrup",
          description:
            "There is an art to this. We trick the mind with a trickle of flattery and faked interest. We may yet succeed.",
        },
        med: {
          label: "Words like Red Port",
          description:
            "There is an art to this. We smother the mind with the precise words it wishes to hear. Our adulation is assisted by other, more sensual tactics. We will probably succeed.",
        },
        high: {
          label: "Words like Holy Wine",
          description:
            "There is an art to this. We drown the mind in a torrent of affection strong and deep like the effusion of the Red Grail Herself. We will almost certainly succeed.",
        },
      },
      heart: {
        low: {
          label: "A Kindly Smile",
          description:
            "Much can be accomplished with a winning smile and the right attitude. Act cheerful and unassuming, and people tend to go along. We may yet succeed.",
        },
        med: {
          label: "An Infectious Smile",
          description:
            "Great things can be accomplished with a winning smile and the right attitude. By being pleasing and pleasant, people are inclined to please you. We will probably succeed.",
        },
        high: {
          label: "An Unstoppable Smile",
          description:
            "All things can be accomplished with a winning smile and the right attitude. We are as undeniable as the rising moon, and their passions rise to meet us. We will almost certainly succeed.",
        },
      },
    },
  },
  FORKED_TONGUE: {
    base: {
      label: "The Forked Tongue",
      description:
        "They say the pen is mightier than the sword, and my tongue is pen of a million, million words. We will affront or attack or lead astray. We can approach this task with the dominance of Grail or the venom of Knock.",
    },
    aspects: {
      grail: {
        low: {
          label: "Lead Them Astray",
          description:
            "The trick is for them not to notice when you plant the seeds for their demise. We may yet succeed.",
        },
        med: {
          label: "Lead Them Down the Wrong Path",
          description:
            "The trick is for them not to be aware that you are pointing them in the wrong direction. We will likely succeed.",
        },
        high: {
          label: "Lead Them to a Pitfall",
          description:
            "The trick is to make them act unwittingly selfless. It is better to please you than vex you, after all. We will probably succeed.",
        },
      },
      knock: {
        low: {
          label: "Venomous Words",
          description:
            "Our conversation is laced with malice and ill intention. They may not hear, and they may walk the path we set out. We may yet succeed.",
        },
        med: {
          label: "Venomous Phrases",
          description:
            "Our conversation is coated with malevolence and bitterness. Our tone may etch stone, and it will shock and stagger those we speak to. We will likely succeed.",
        },
        high: {
          label: "Venomous Speech",
          description:
            "Our conversation is dripping with malefaction and bile. As if they were a shepherd's flock, they will fear our scorn and act as we desire. We will probably succeed.",
        },
      },
    },
  },
  SILVER_TONGUE: {
    base: {
      label: "The Silver Tongue",
      description:
        "With the right attitude and the proper poise, you can be anyone, and you can make anything seem true. We can approach this task with the distractions of Grail or the shrewdness of Moth.",
    },
    aspects: {
      grail: {
        low: {
          label: "Not to Be Doubted",
          description:
            "I may be a rose, but I can pass for a bramble. I will hide my true self like a pearl in its shell and pass for anything. We may yet succeed.",
        },
        med: {
          label: "Not to Be Challenged",
          description:
            "We may be a fox, but we can act as lambs. If I cannot be noticed, I cannot be touched. We will likely succeed.",
        },
        high: {
          label: "Not to Be Looked at",
          description:
            "We may be regal, but we can act as the rabble. I am a face in the crowd, inseparable from the others. We will probably succeed.",
        },
      },
      moth: {
        low: {
          label: "Don A New Posture",
          description:
            "Just like changing a coat, one can change their bearing. We may yet succeed.",
        },
        med: {
          label: "Don A New Identity",
          description:
            "Just like changing one's attire, one can change their essence. I will likely succeed.",
        },
        high: {
          label: "Don A New Face",
          description:
            "Just like changing a hat, one can change their skin. The Vagabond has five Masks, but I can don many more. We will probably succeed. ",
        },
      },
    },
  },
  // NAVAL
  OVERTAKE: {
    base: {
      label: "Tame The Waves",
      description:
        "The sea is fickle and prone to change. Sometimes she is calm and gentle. At the moment, she is not. She will devour me gladly, if I offer myself to her. I can approach her using the vigor of Heart or the prowess of Forge.",
    },
    aspects: {
      heart: {
        low: {
          label: "Steady Sailing",
          description:
            "The sea is my partner, and when she bucks and shivers, I will follow her pace. We may yet succeed",
        },
        med: {
          label: "Stable Sailing",
          description:
            "The sea is my partner, and our union is a dance; the rise, the fall, the push, the pull. We will likely succed.",
        },
        high: {
          label: "Ceaseless Sailing",
          description:
            "The sea is my partner, and even if she lashes me, I will follow ceaselessly, ceaselessly, the sea, the she. We will probably succeed. ",
        },
      },
      forge: {
        low: {
          label: "Break The Waves",
          description:
            "She challenges me, but akin to how man tamed fire, I can bend her to my will. I may yet succeed.",
        },
        med: {
          label: "Crush The Waves",
          description:
            "She challenges me, but how men shattered stones, I will break through the barriers she throws at me.  I will likely succeed.",
        },
        high: {
          label: "Bind The Waves",
          description:
            "She challenges me, but as men mastering metal, I will reshape her currents to carry me, and I will ride her as as my steed. I will probably succeed. ",
        },
      },
    },
  },
  COMMAND: {
    base: {
      label: "Command My Crew",
      description:
        "This is the time where leadership is needed. This is the time where a failure of any crewmember could mean a failure of my mission. I could evoke the balanced Winter or the undeniable Edge.",
    },
    aspects: {
      winter: {
        low: {
          label: "Quiet Authority",
          description:
            "If I cannot be unbalanced, so too can my crew not get flustered. We may yet succeed",
        },
        med: {
          label: "Wordless Authority",
          description:
            "I need not speak, just a glance and the crew falls into place. We will likely succeed.",
        },
        high: {
          label: "Auroral Authority",
          description:
            "Conducted to my command, my ship is as certain  as rhyme and as unstuppable as dawns advances. We will almost certainly succeed.",
        },
      },
      edge: {
        low: {
          label: "Fierce Command",
          description:
            "Not a moment my crew can question me. Together, we will conquer hell and high water. We may yet succeed",
        },
        med: {
          label: "Unifying Command",
          description:
            "They cannot doubt me, if they are to be my many limbs. And the dosen arms of my body are storng, and growing stronger still. We will likely succed.",
        },
        high: {
          label: "Venerated Command",
          description:
            "They cannot hesitate. If they all act with my intent, our ship will turn into a creature with unbridled ferocity. We will almost certainly succeed.",
        },
      },
    },
  },
  // TRANSITIONAL
  DELVE: {
    base: {
      label: "Delving To Hidden Depths.",
      description:
        "Our path leads through c'thonic depths. There is a way down, even if I must make it myself. Approach this with the practicality of Forge or the voluptuousness of Grail.",
    },
    aspects: {
      grail: {
        low: {
          label: "Soft Demands",
          description:
            "We wriggle our way down, where our greed and curiosity lead us.  We may yet succeed",
        },
        med: {
          label: "Yielding Touches",
          description:
            "We ply ourselves into the earth, and like so many others, it yields to our touch. We will likely succeed.",
        },
        high: {
          label: "Ministrations of Parting",
          description:
            "We coax ourselves into the earth. The earth shall open to us as it does to the Beachcomber. We will almost certainly succeed.",
        },
      },
      forge: {
        low: {
          label: "The Coal Roads",
          description:
            "Down is the direction of earthly wealth. If obstacles are in my way, I will break them.  We may yet succeed",
        },
        med: {
          label: "The Graphite Roads",
          description:
            "Down is the direction of secret fire. If there is no path, I shall forge one.  We will likely succeed.",
        },
        high: {
          label: "The Diamond Road",
          description:
            "Down is the direction of the molten core. If anything is in my way, I shall shatter it like the Forge shattered the Flint. We will almost certainly succeed.",
        },
      },
    },
  },
  SNEAK_IN: {
    base: {
      label: "Sneak In",
      description:
        "Sometimes, entry can be more comfortable if one goes unnoticed. This is one of those times. I can apply either the skills of Knock or the wiles of Moth.",
    },
    aspects: {
      knock: {
        low: {
          label: "Unexpected Paths",
          description:
            "We take the road less traveled by. Hopefully this will leave us undetected. We may yet succeed.",
        },
        med: {
          label: "Improbable Paths",
          description:
            "We find the room that others neglect. We move through odd corners and narrow cracks. We will probably succeed.",
        },
        high: {
          label: "Impossible Paths",
          description:
            "Geometry bends around us. We move through the spaces between spaces. we will almost certainly succeed.",
        },
      },
      moth: {
        low: {
          label: "Skulking Movements",
          description:
            "Wolven steps and feline grace. We stalk quietly. We may yet succeed.",
        },
        med: {
          label: "Hidden Movements",
          description:
            "With the ants tenecacity and the moths subtlety, we will enter unseen where we are not wanted. We will probably succeed.",
        },
        high: {
          label: "Just the Wind",
          description:
            '"Who is there?" "Relax, there is nothing but the night out there..." We will almost certainly succeed.',
        },
      },
    },
  },
  DIVE: {
    base: {
      label: "Into The Abyss",
      description:
        "Below us lies a fathomless expanse of freezing darkness and suffocating pressure. To here we must venture forth. We can approach this with the wayfaring Knock or the stoic Winter.",
    },
    aspects: {
      knock: {
        low: {
          label: "A Barren Path",
          description:
            "The depths are dark and lonely, but I will not be blinded. With open eyes I peer into open emptyness. We may yet succeed.",
        },
        med: {
          label: "A Guided Path",
          description:
            "The depths may be dark and empty, but I will not be alone. I will be open to all experiences and beings here. I will likely succeed.",
        },
        high: {
          label: "A Piercing Path",
          description:
            "The depths may be dark and empty, but so am I. I am an empty vessel to be filled, no different from the void outside my tank. We will almost certainly succeed.",
        },
      },
      winter: {
        low: {
          label: "A Steady Demeanor",
          description:
            "As I sink down, I settle into myself as the light fades and the temperature drops. I may yet succeed.",
        },
        med: {
          label: "An Unmarred Demeanor",
          description:
            "As I sink down, I crystalise as the darkness overtakes me and the pressure rises. I will likely succeed.",
        },
        high: {
          label: "An Implaccable Demeanor",
          description: '"...". I almost certainly succeed.',
        },
      },
    },
  },
  // SPIRITUAL/OCCULT
  WARD: {
    base: {
      label: "Ward off an Influence",
      description:
        "There is an oppressive presence here, or an outside intention effecting me and mine. I can use declining Winter or the vivaceous Heart.",
    },
    aspects: {
      heart: {
        low: {
          label: "An Interruptive Beat",
          description:
            "My snapping fingers can't be stilled. My swaying steps shake what clings to me. We may yet succeed.",
        },
        med: {
          label: "A Counter-Charm",
          description:
            "My heart is a drum, and it drowns out the whatever is attempts to take hold of me. We will likely succeed.",
        },
        high: {
          label: "A Syncopation Of The Soul ",
          description:
            "My being is music, my body is joy. Within the rhythms of my life there is no space for opposition.",
        },
      },
      winter: {
        low: {
          label: "Fading Light",
          description:
            "Compared to the solumnity that has settled in my soul, all other things fade to grey. We may yet succeed.",
        },
        med: {
          label: "Dwindling Light",
          description:
            "Under my setting-sun gaze, stains wither until only barren beauty remains. We will probably succeed.",
        },
        high: {
          label: "The Inclination Towards The End",
          description:
            "Inexorably, all things follow the course of the Sun. By my authority, now is the time for a final ending. We wil almost certainly succeed.",
        },
      },
    },
  },
  COMMUNE: {
    base: {
      label: "Commune With A Presence",
      description:
        "If I cast my mind out, and bear my soul to my suroundings, I could contact the conscience which resides here. I could use the uniting heart or the mistifying moth.",
    },
    aspects: {
      heart: {
        low: {
          label: "An Open Connection",
          description:
            "I open like a blossom and the whipping winds may whisper secrets to my soul. We may yet succeed.",
        },
        med: {
          label: "An Open Communication",
          description:
            "I open like a shell, and the worldsoul moves my mind the the sea polishes pearls. We will almost certainly succeed.",
        },
        high: {
          label: "Unending, Unlimited, Unbounding",
          description:
            "My tongue tastes the air, and the air touches the world around. Through this path there are no barriers between what is within me and what is without. We will almost certainly succeed.",
        },
      },
      moth: {
        low: {
          label: "Half-Heard Whispers",
          description:
            "The stirring of leaves tells me the opinions of the wind. of course, the wind is a notorious liar. We may yet succeed.",
        },
        med: {
          label: "Half-Understood Messages",
          description:
            "Raindrops write their tales on the water. It is folly to read them, so I will read them perfectly. We will almost certainly succeed.",
        },
        high: {
          label: "Half-Fealt Truths",
          description:
            "I speak with the tongue of the birds, like mages and madmen before me. We wil almost certainly succeed.",
        },
      },
    },
  },
  STRATEGIZE: {
    base: {
      label: "Plan The Slaying Of A Beast",
      description:
        "Something unnatural opposes us, half within this world, half without. Pedestrian means of murder may not be enough. I could employ the knowledge of Lantern of the armaments of Edge.",
    },
    aspects: {
      lantern: {
        low: {
          label: "Unlikely Tactics",
          description:
            "Old stories and forgotten myths. Snippets hint at practices and tools that are of use here. We may yet succeed.",
        },
        med: {
          label: "Arcane Tactics",
          description:
            "Themes in folktales reveal underlying truths. I am illuminated by knowledge and armed with daimonography. We will likely succeed.",
        },
        high: {
          label: "Eldritch Tactics",
          description:
            "The laws of the House of Sun still apply to this entity. I know their ways and I know their bounds. We will almost certainly succeed.",
        },
      },
      edge: {
        low: {
          label: "The Right Tools",
          description:
            "When a normal blade won't do, I carry sterling silver of blackened oak. We may yet succeed.",
        },
        med: {
          label: "The Perfected Tools",
          description:
            "The operations of murder reveal the many practices that prepare our many tools for our many jobs. We will likely succeed.",
        },
        high: {
          label: "The Sacred Tools",
          description:
            "My dagger could be rusted, my axe could be flint, in my hand they are the teeth of wolf and they unmake, they unmake, they unmake. We will almost certainly succeed.",
        },
      },
    },
  },
  PLEAD: {
    base: {
      label: "Plead to a Greater Power",
      description:
        "There is a being here, who holds mortal authority over me or whose regard in the House of the Sun is higher than mine. I can approach this task with the logic of Lantern of the appeal of Grail.",
    },
    aspects: {
      lantern: {
        low: {
          label: "Reason Like a Knife",
          description:
            "Statements follow statements: My reasoning is build up like a light through a prism, one truth cast in many shades of logic and rhetoric. We may yet succeed.",
        },
        med: {
          label: "Reason Like a Razor ",
          description:
            "Arguments follow Arguments. One leads to the next, like the colors of the dawn dividing, until our truth is clear as day. We will likely succeed.",
        },
        high: {
          label: "Reason Like a Scalpel",
          description:
            "Coda after coda, I weave an appeal with a poet's grace and a mathematical complexity, but throughout all a single glorious truth shines. We will almost certainly succeed.",
        },
      },
      grail: {
        low: {
          label: "Appeal To Their Wants",
          description:
            "Be it warriors, poets, princes, all men are not so different, regardless if they are from the wake or from the Manse. Find what they desire, and play them like a musicbox. We may yet succeed.",
        },
        med: {
          label: "Appeal To Their Needs",
          description:
            "Be it monsters, mages, Names, all beings high and low will have their goals to reach. Approach them as an equal and play them like a betting table. We will likely succeed.",
        },
        high: {
          label: "Appeal To Their Core",
          description:
            "Be it constabulary, royalty, divinity, all hold certain secret values close to what passes for their hearts. I will play them like a musician plays their strings. We will almost certainly succeed.",
        },
      },
    },
  },
});
