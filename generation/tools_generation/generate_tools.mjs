import { SIGNALS } from "../generate_signal_aspects.mjs";
import { LORE_ASPECTS, ME_RECIPE } from "../generation_helpers.mjs";
import { generateRepairToolAction } from "./repair_tools.mjs";
import tools_definitions from "./tools_definitions.mjs";
import { generateUpgradeToolActions } from "./upgrade_tools.mjs";

export const MAXED_NOTCHES_FORMULA = "[~/source : mariner.tool.level] * 8";

export const SMITH_TYPES = {
  TOOLSMITH: {
    id: "toolsmith",
    handles: ["forge", "knock"],
  },
};

const upgradeTool = (nextToolId) => [
  {
    morpheffect: "setmutation",
    id: "mariner.tool.notch",
    level: 0,
  },
  {
    morpheffect: "setmutation",
    id: "mariner.tool.readytoupgrade",
    level: 0,
  },
  {
    morpheffect: "transform",
    id: nextToolId,
  },
];

const requiredTrappingSlot = (lore, number = 1) => ({
  id: `tool.upgrade.${number}`,
  label: "Trapping",
  description: "<>",
  required: { [`mariner.trappings.${lore}`]: 1 },
  ifAspectsPresent: { "mariner.tool.readytoupgrade": 1 },
});

const specialisationTrappingSlot = (loreA, loreB) => ({
  id: `tool.specialisation`,
  label: "Specialisation",
  description: "<>",
  required: {
    [`mariner.trappings.${loreA}`]: 1,
    [`mariner.trappings.${loreB}`]: 1,
  },
  ifAspectsPresent: { "mariner.tool.readytoupgrade": 1 },
});

function generateTool(loreAspect, toolData) {
  mod.initializeElementFile(`tools.${loreAspect}`, ["tools"]);

  const icon = `tools/mariner.tool.${loreAspect}`;
  const toolLoreAspect = `mariner.tool.${loreAspect}`;
  mod.setHiddenAspect("aspects.tools", {
    id: toolLoreAspect,
    label: `${mod.helpers.capitalize(loreAspect)} Tool`,
  });

  mod.setElement(`tools.${loreAspect}`, {
    $derives: "mariner.composables.tool",
    id: `mariner.tool.${loreAspect}.1`,
    icon,
    label: toolData["1"].label,
    description: toolData["1"].description,
    aspects: {
      tool: 1,
      [toolLoreAspect]: 1,
      [loreAspect]: 3,
      "mariner.tool.level": 1,
    },
    slots: [requiredTrappingSlot(loreAspect)],
    xtriggers: {
      [SIGNALS.UPGRADE_TOOL]: upgradeTool(`mariner.tool.${loreAspect}.2`),
    },
  });

  mod.setElement(`tools.${loreAspect}`, {
    $derives: "mariner.composables.tool",
    id: `mariner.tool.${loreAspect}.2`,
    icon,
    label: toolData["2"].label,
    description: toolData["2"].description,
    aspects: {
      tool: 1,
      [toolLoreAspect]: 1,
      [loreAspect]: 4,
      "mariner.tool.level": 2,
    },
    slots: [requiredTrappingSlot(loreAspect), requiredTrappingSlot(loreAspect, 2)],
    xtriggers: {
      [SIGNALS.UPGRADE_TOOL]: upgradeTool(`mariner.tool.${loreAspect}.3`),
    },
  });

  mod.setElement(`tools.${loreAspect}`, {
    $derives: "mariner.composables.tool",
    id: `mariner.tool.${loreAspect}.3`,
    icon,
    label: toolData["3"].label,
    description: toolData["3"].description,
    aspects: {
      tool: 1,
      [toolLoreAspect]: 1,
      [loreAspect]: 5,
      "mariner.tool.level": 3,
      [`mariner.tool.specialisationpotential.${toolData["4_1"].additionalAspect}`]: 1,
      [`mariner.tool.specialisationpotential.${toolData["4_2"].additionalAspect}`]: 1,
    },
    slots: [
      requiredTrappingSlot(loreAspect),
      requiredTrappingSlot(loreAspect, 2),
      requiredTrappingSlot(loreAspect, 3),
      specialisationTrappingSlot(toolData["4_1"].additionalAspect, toolData["4_2"].additionalAspect),
    ],
    xtriggers: {
      [SIGNALS[`SPECIALISE_TOOL_WITH_${toolData["4_1"].additionalAspect.toUpperCase()}`]]: upgradeTool(
        `mariner.tool.${loreAspect}.4_1`
      ),
      [SIGNALS[`SPECIALISE_TOOL_WITH_${toolData["4_2"].additionalAspect.toUpperCase()}`]]: upgradeTool(
        `mariner.tool.${loreAspect}.4_2`
      ),
    },
  });

  mod.setElement(`tools.${loreAspect}`, {
    $derives: "mariner.composables.tool",
    id: `mariner.tool.${loreAspect}.4_1`,
    icon,
    label: toolData["4_1"].label,
    description: toolData["4_1"].description,
    aspects: {
      tool: 1,
      [toolLoreAspect]: 1,
      [loreAspect]: 6,
      [toolData["4_1"].additionalAspect]: 2,
      "mariner.tool.level": 4,
    },
    slots: [
      requiredTrappingSlot(loreAspect),
      requiredTrappingSlot(loreAspect, 2),
      requiredTrappingSlot(toolData["4_1"].additionalAspect, 3),
      requiredTrappingSlot(toolData["4_1"].additionalAspect, 4),
    ],
    xtriggers: {
      [SIGNALS.UPGRADE_TOOL]: upgradeTool(`mariner.tool.${loreAspect}.5_1`),
    },
  });

  mod.setElement(`tools.${loreAspect}`, {
    $derives: "mariner.composables.tool",
    id: `mariner.tool.${loreAspect}.4_2`,
    icon,
    label: toolData["4_2"].label,
    description: toolData["4_2"].description,
    aspects: {
      tool: 1,
      [toolLoreAspect]: 1,
      [loreAspect]: 6,
      [toolData["4_2"].additionalAspect]: 2,
      "mariner.tool.level": 4,
    },
    slots: [
      requiredTrappingSlot(loreAspect),
      requiredTrappingSlot(loreAspect, 2),
      requiredTrappingSlot(toolData["5_1"].additionalAspect, 3),
      requiredTrappingSlot(toolData["5_1"].additionalAspect, 4),
    ],
    xtriggers: {
      [SIGNALS.UPGRADE_TOOL]: upgradeTool(`mariner.tool.${loreAspect}.5_2`),
    },
  });

  mod.setElement(`tools.${loreAspect}`, {
    $derives: "mariner.composables.tool",
    id: `mariner.tool.${loreAspect}.5_1`,
    icon,
    label: toolData["5_1"].label,
    description: toolData["5_1"].description,
    aspects: {
      tool: 1,
      [toolLoreAspect]: 1,
      [loreAspect]: 7,
      [toolData["5_1"].additionalAspect]: 3,
      "mariner.tool.level": 5,
    },
    slots: [requiredTrappingSlot(loreAspect), requiredTrappingSlot(loreAspect, 2), requiredTrappingSlot(loreAspect, 3)],
  });

  mod.setElement(`tools.${loreAspect}`, {
    $derives: "mariner.composables.tool",
    id: `mariner.tool.${loreAspect}.5_2`,
    icon,
    label: toolData["5_2"].label,
    description: toolData["5_2"].description,
    aspects: {
      tool: 1,
      [toolLoreAspect]: 1,
      [loreAspect]: 7,
      [toolData["5_2"].additionalAspect]: 3,
      "mariner.tool.level": 5,
    },
    slots: [requiredTrappingSlot(loreAspect), requiredTrappingSlot(loreAspect, 2), requiredTrappingSlot(loreAspect, 3)],
  });
}

export function generateTools() {
  mod.initializeElementFile("composables.items", ["_composables"]);
  mod.setElement("composables.items", {
    id: "mariner.composables.tool",
    xtriggers: {
      [SIGNALS.USE_TOOL]: ME_RECIPE("mariner.usetool"),
      [SIGNALS.MISUSE_TOOL]: [ME_RECIPE("mariner.usetool"), { morpheffect: "break", chance: 80 }, ME_RECIPE("mariner.breaktool")],
    },
  });

  mod.initializeAspectFile("aspects.tools", ["tools"]);
  // Hidden, used by the recipes
  mod.setHiddenAspect("aspects.tools", {
    id: "mariner.smith",
    label: "<Smith>",
    description: "<This is where I can find the skilled hands able to work on some of my most precious tools.>",
  });
  mod.setAspects(
    "aspects.tools",
    {
      id: "mariner.smiths.toolsmith",
      label: "<Toolsmith>",
      description: "<Knock and Forge tools>",
    },
    {
      id: "mariner.tool.notch",
      label: "<Tool Notch>",
      description: "<Tool Notch>",
    },
    {
      id: "mariner.tool.level",
      label: "<Tool Tier/Level>",
      description: "<Tool Tier/Level>",
    },
    {
      id: "mariner.tool.readytoupgrade",
      label: "<Maxed out Notches>",
      description: "<Ready to upgrade>",
    }
  );

  for (const loreAspect of LORE_ASPECTS)
    mod.setHiddenAspect("aspects.tools", {
      id: `mariner.tool.specialisationpotential.${loreAspect}`,
    });

  mod.initializeRecipeFile("recipes.tools", ["tools"]);
  // Based on the level of notches of the tool, as well as its tier level, add a notch, or the "ready to upgrade" aspect
  mod.setRecipe("recipes.tools", {
    id: "mariner.usetool",
    grandReqs: {
      "~/source : mariner.tool.broken": -1,
      "~/source : mariner.tool.readytoupgrade": -1,
      "~/source : mariner.tool.level": -5,
    },
    furthermore: [
      {
        target: "~/source",
        mutations: [
          {
            filter: "tool",
            mutate: "mariner.tool.notch",
            level: 1,
            additive: true,
          },
        ],
      },
    ],
    linked: [{ id: "mariner.settoolreadytoupgrade" }],
  });
  mod.setRecipe("recipes.tools", {
    id: "mariner.settoolreadytoupgrade",
    grandReqs: {
      "~/source : mariner.tool.broken": -1,
      "~/source : mariner.tool.readytoupgrade": -1,
      "~/source : mariner.tool.notch": MAXED_NOTCHES_FORMULA,
    },
    furthermore: [
      {
        target: "~/source",
        mutations: [
          {
            filter: "tool",
            mutate: "mariner.tool.readytoupgrade",
            level: 1,
            additive: false,
          },
        ],
      },
    ],
  });

  // Break tool
  mod.setRecipe("recipes.tools", {
    id: "mariner.breaktool",
    furthermore: [
      {
        target: "~/source",
        mutations: [
          {
            filter: "tool",
            mutate: "mariner.tool.broken",
            level: 1,
            additive: false,
          },
        ],
      },
    ],
  });

  for (const loreAspect of LORE_ASPECTS) generateTool(loreAspect, tools_definitions[loreAspect]);

  generateRepairToolAction();
  generateUpgradeToolActions();
}
