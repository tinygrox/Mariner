import { SIGNALS } from "../generate_signal_aspects.mjs";
import { LORE_ASPECTS } from "../generation_helpers.mjs";
import { MAXED_NOTCHES_FORMULA } from "./generate_tools.mjs";

function generateRepairLowLevelToolAction() {
  mod.initializeRecipeFile("recipes.upgradetool", ["tools"]);
  // Hints: the tool doesn't have the level + maxed notches, or it is too high level
  mod.setRecipes(
    "recipes.upgradetool",
    {
      id: "mariner.upgradetool.hint.needmorenotches",
      hintOnly: true,
      actionId: "mariner.navigate",
      grandReqs: {
        "[mariner.tool.level] < 3": 1,
        [`[mariner.tool.notch] < (${MAXED_NOTCHES_FORMULA})`]: 1,
      },
      label: "<Not Enough Notches to Upgrade>",
      startdescription: "<>",
    },
    {
      id: "mariner.upgradetool.hint.toohighlevelforpersonalupgrade",
      hintOnly: true,
      actionId: "mariner.navigate",
      grandReqs: {
        "mariner.tool.level": 3,
        "mariner.tool.notch": MAXED_NOTCHES_FORMULA,
        // TODO : to be sure, also define that no smith is present in the verb
      },
      label: "<Tool Too High Level to Upgrade Myself>",
      startdescription: "<This tool is too high level to improve it myself. I'll need to talk to the proper smith to do that.>",
    }
  );

  for (const loreAspect of LORE_ASPECTS) {
    mod.setRecipes(
      "recipes.upgradetool",
      {
        id: `mariner.upgradetool.${loreAspect}.myself`,
        hintOnly: true,
        actionId: "mariner.navigate",
        grandReqs: {
          [`mariner.tool.${loreAspect}`]: 1,
          "[mariner.tool.level] < 3": 1,
          "mariner.tool.readytoupgrade": 1,
        },
        label: `<Upgrade ${loreAspect} Tool Myself: Need More Trappings>`,
        startdescription: "<>",
      },
      {
        id: `mariner.upgradetool.${loreAspect}.myself`,
        actionId: "mariner.navigate",
        craftable: true,
        grandReqs: {
          [`mariner.tool.${loreAspect}`]: 1,
          "[mariner.tool.level] < 3": 1,
          "mariner.tool.readytoupgrade": 1,
          [`[~/local : {[mariner.trapping]} : ${loreAspect}]`]: "mariner.tool.level", // as many trapping value as the tool level
        },
        label: `<Upgrade ${loreAspect} Tool Myself>`,
        startdescription: "<>",
        warmup: 60,
        effects: { "mariner.trapping": -10 },
        aspects: { [SIGNALS.UPGRADE_TOOL]: 1 },
      }
    );
  }
}

function generateToolSpecializationAction() {
  // This action only happens when upgrading a tier 3 weapon.
  // At tier 3, tools possess several mariner.tool.specialisationpotential.${lore} aspects, showing the potential specialisation paths they can follow.
  // The Eight recipes here will upgrade them. After that, they'll follow the normal upgrade process, but require to go to the toolsmith (and pay them)
  mod.initializeRecipeFile("recipes.specialisetool", ["tools"]);
  for (const loreAspect of LORE_ASPECTS) {
    mod.setRecipe("recipes.specialisetool", {
      id: `mariner.specialisetool.with.${loreAspect}`,
      craftable: true,
      grandReqs: {
        // A tool, level 3, ready to upgrade
        tool: 1,
        "[mariner.tool.level] = 3": 1,
        "mariner.tool.readytoupgrade": 1,
        // With this specialisation potential
        [`mariner.tool.specialisationpotential.${loreAspect}`]: 1,
        // And the proper trapping amount provided for the specialisation
        [`mariner.trappings.${loreAspect}`]: 2,
        // And the proper trapping value provided in total (the tool's slots filter the proper allowed kind in advance)
        "mariner.trapping": 4,
      },
      warmup: 180,
      label: `<Infuse Tool With the ${loreAspect} Specialisaton>`,
      startdescription: "<>",
      description: "<It is done.>",
      effects: { "mariner.trapping": -10 },
      aspects: {
        [SIGNALS[`SPECIALISE_TOOL_WITH_${loreAspect.toUpperCase()}`]]: 1,
      },
    });
  }
}

function generateHighLevelToolUpgradeAction() {
  for (const loreAspect of LORE_ASPECTS) {
    mod.setRecipes(
      "recipes.upgradetool",
      {
        id: `mariner.upgradetool.${loreAspect}.viasmith`,
        hintOnly: true,
        actionId: "talk",
        grandReqs: {
          "mariner.toolsmith": 1,
          [`mariner.tool.${loreAspect}`]: 1,
          "[mariner.tool.level] = 4": 1,
          "mariner.tool.readytoupgrade": 1,
        },
        label: `<Upgrade ${loreAspect} Tool Myself: Need More Trappings>`,
        startdescription: "<>",
      },
      {
        id: `mariner.upgradetool.${loreAspect}.viasmith`,
        actionId: "mariner.navigate",
        craftable: true,
        grandReqs: {
          "mariner.toolsmith": 1,
          [`mariner.tool.${loreAspect}`]: 1,
          "[mariner.tool.level] = 4": 1,
          "mariner.tool.readytoupgrade": 1,
          // 4 trapping cards in total. Slots force the type anyway.
          "mariner.trapping": 4,
        },
        label: `<Upgrade ${loreAspect} Tool Via Smith>`,
        startdescription: "<>",
        warmup: 60,
        effects: { "mariner.trapping": -10 },
        aspects: { [SIGNALS.UPGRADE_TOOL]: 1 },
      }
    );
  }
}

export function generateUpgradeToolActions() {
  generateRepairLowLevelToolAction();
  generateToolSpecializationAction();
  generateHighLevelToolUpgradeAction();
}
