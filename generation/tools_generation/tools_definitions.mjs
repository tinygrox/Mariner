export default {
  knock: {
    1: {
      label: "Lockpicks",
      description:
        "There are few honest reasons to have a set of lockpicks, but many exciting ones.",
    },
    2: {
      label: "Improved Lockpicks",
      description: "",
    },
    3: {
      label: "Glorious Lockpicks",
      description: "",
    },
    "4_1": {
      label: "Exalted Wounding Picks",
      description: "",
      additionalAspect: "edge",
    },
    "5_1": {
      label: "Mythical Wounding Picks",
      description: "",
      additionalAspect: "edge",
    },
    "4_2": {
      label: "Exalted Heart-thiefs Picks",
      description: "",
      additionalAspect: "grail",
    },
    "5_2": {
      label: "Mythical Heart-thiefs Picks",
      description: "",
      additionalAspect: "grail",
    },
  },
  grail: {
    1: {
      label: "Jewelry",
      description: "The right amount of glitter does not distract but amplify.",
    },
    2: {
      label: "Improved Jewelry",
      description: "",
    },
    3: {
      label: "Glorious Jewelry",
      description: "",
    },
    "4_1": {
      label: "Exalted Wildering Amulet",
      description: "",
      additionalAspect: "moth",
    },
    "5_1": {
      label: "Mythical Wildering Amulet",
      description: "",
      additionalAspect: "moth",
    },
    "4_2": {
      label: "Exalted Cold Jewel",
      description: "",
      additionalAspect: "winter",
    },
    "5_2": {
      label: "Mythical Cold Jewel",
      description: "",
      additionalAspect: "winter",
    },
  },
  moth: {
    1: {
      label: "Cape",
      description:
        "A coat is the first lie we show to the world. It conceals, protects, obscures.",
    },
    2: {
      label: "Improved Cape",
      description: "",
    },
    3: {
      label: "Glorious Cape",
      description: "",
    },
    "4_1": {
      label: "Exalted Skinshifters Cape",
      description: "",
      additionalAspect: "heart",
    },
    "5_1": {
      label: "Mythical Skinshifters Cape",
      description: "",
      additionalAspect: "heart",
    },
    "4_2": {
      label: "Exalted Cutthroat Coat",
      description: "",
      additionalAspect: "edge",
    },
    "5_2": {
      label: "Mythical Cutthroat Coat",
      description: "",
      additionalAspect: "edge",
    },
  },
  lantern: {
    1: {
      label: "Compass",
      description: "It does not know the way, but it helps finding it.",
    },
    2: {
      label: "Improved Compass",
      description: "",
    },
    3: {
      label: "Glorious Compass",
      description: "",
    },
    "4_1": {
      label: "Exalted Truthtelling Compass",
      description: "",
      additionalAspect: "knock",
    },
    "5_1": {
      label: "Mythical Truthtelling Compass",
      description: "",
      additionalAspect: "knock",
    },
    "4_2": {
      label: "Exalted Seas’ Wish Compass",
      description: "",
      additionalAspect: "heart",
    },
    "5_2": {
      label: "Mythical Seas’ Wish Compass",
      description: "",
      additionalAspect: "heart",
    },
  },
  forge: {
    1: {
      label: "Toolbox",
      description:
        "Any captain worth their salt has a set of their own, regardless if the ship has a carpenter.",
    },
    2: {
      label: "Improved Toolbox",
      description: "",
    },
    3: {
      label: "Glorious Toolbox",
      description: "",
    },
    "4_1": {
      label: "Exalted Brightmaker",
      description: "",
      additionalAspect: "lantern",
    },
    "5_1": {
      label: "Mythical Brightmaker",
      description: "",
      additionalAspect: "lantern",
    },
    "4_2": {
      label: "Exalted Entry Gainer",
      description: "",
      additionalAspect: "knock",
    },
    "5_2": {
      label: "Mythical Entry Gainer",
      description: "",
      additionalAspect: "knock",
    },
  },
  edge: {
    1: {
      label: "Axe",
      description: "It implies a practical use as well as a sinister one.",
    },
    2: {
      label: "Improved Axe",
      description: "",
    },
    3: {
      label: "Glorious Axe",
      description: "",
    },
    "4_1": {
      label: "Exalted Executioner's Axe",
      description: "",
      additionalAspect: "winter",
    },
    "5_1": {
      label: "Mythical Executioner's Axe",
      description: "",
      additionalAspect: "winter",
    },
    "4_2": {
      label: "Exalted Commanders Gleam",
      description: "",
      additionalAspect: "lantern",
    },
    "5_2": {
      label: "Mythical Commanders Gleam",
      description: "",
      additionalAspect: "lantern",
    },
  },
  winter: {
    1: {
      label: "Shroud",
      description:
        "This cloth is stained with bad memories, but its air cannot be denied.",
    },
    2: {
      label: "Improved Shroud",
      description: "",
    },
    3: {
      label: "Glorious Shroud",
      description: "",
    },
    "4_1": {
      label: "Exalted Singed Shroud",
      description: "",
      additionalAspect: "forge",
    },
    "5_1": {
      label: "Mythical Singed Shroud",
      description: "",
      additionalAspect: "forge",
    },
    "4_2": {
      label: "Exalted Tattered Shroud",
      description: "",
      additionalAspect: "moth",
    },
    "5_2": {
      label: "Mythical Tattered Shroud",
      description: "",
      additionalAspect: "moth",
    },
  },
  heart: {
    1: {
      label: "Wrappings",
      description:
        "This cloth is stained with bad memories, but its air cannot be denied.",
    },
    2: {
      label: "Improved Wrappings",
      description: "",
    },
    3: {
      label: "Glorious Wrappings",
      description: "",
    },
    "4_1": {
      label: "Exalted Thirsty Wrappings",
      description: "",
      additionalAspect: "grail",
    },
    "5_1": {
      label: "Mythical Thirsty Wrappings",
      description: "",
      additionalAspect: "grail",
    },
    "4_2": {
      label: "Exalted Eager Wrappings",
      description: "",
      additionalAspect: "forge",
    },
    "5_2": {
      label: "Mythical Eager Wrappings",
      description: "",
      additionalAspect: "forge",
    },
  },
};
