import { SIGNALS } from "./generate_signal_aspects.mjs";

const getSeaIdFromPortElementId = (portId) => portId.split(".")[2];
const getPortIdFromPortElementId = (portId) => portId.split(".")[3];

const getDockedPortElementFromPortElementId = (portElementId) => {
  const seaId = getSeaIdFromPortElementId(portElementId);
  return mod.getElement(`locations.${seaId}`, portElementId);
};

export function generatePassengerSystem() {
  const allPorts = [].concat.apply(
    [],
    mod.decks["decks.seas"].filter((deck) => deck.id.includes(".ports")).map((d) => d.spec)
  );
  mod.readRecipesFile("recipes.findpassengers", ["passengers"]);
  mod.initializeAspectFile("aspects.passengers", ["passengers"]);
  mod.initializeRecipeFile("recipes.convertpassengersonarrival", ["passengers"]);
  mod.setAspect("aspects.passengers", {
    id: "mariner.passenger",
    label: "Passenger",
    description:
      "A person looking to travel, with their own full life and hidden story in which the Kite is just a single passage.",
  });
  mod.setAspect("aspects.passengers", { id: "mariner.passenger.freakedout", label: "<Freaked Out>", description: "" });

  // Catalyst
  mod.setHiddenAspect("aspects.passengers", { id: "mariner.acceptclient" });
  mod.setHiddenAspect("aspects.passengers", {
    id: "mariner.passenger.opportunity",
  });
  // For each port:
  //- define an aspect on the passenger to make its destination clear
  //- define the passenger card
  //- define the recipe that gives the passenger
  // Define the recipe that converts the passenger
  for (const portDockedId of allPorts) {
    const portRegion = getSeaIdFromPortElementId(portDockedId);
    const portId = getPortIdFromPortElementId(portDockedId);
    const dockedPortElement = getDockedPortElementFromPortElementId(portDockedId);
    const uniquenessGroupPort = `mariner.locations.${portRegion}.${portId}.uqg`;
    const wantsToGoToAspect = `mariner.passenger.wantstogoto.${portRegion}.${portId}`;
    const opportunityElementId = `mariner.passengeropportunity.goingto.${portRegion}.${portId}`;
    const passengerElementId = `mariner.passenger.goingto.${portRegion}.${portId}`;

    const portLabel = dockedPortElement.label.split("[")[0].trim();
    // Aspect
    mod.setAspect("aspects.passengers", {
      id: wantsToGoToAspect,
      label: `Passenger to ${dockedPortElement.label.split("[")[0].trim()}`,
      description: `This Passenger wants to go to ${portLabel}`,
    });

    mod.setHiddenAspect("aspects.passengers", {
      id: "mariner.passenger.opportunity",
    });

    // passenger
    if (!mod.elements[`passengers.${portRegion}`]) mod.initializeElementFile(`passengers.${portRegion}`, ["passengers"]);

    mod.setElement(`passengers.${portRegion}`, {
      id: opportunityElementId,
      label: `Potential Client, Going to ${portLabel}`,
      description: `This one is looking for a ship to get them to ${portLabel}. They will reward us if we bring them to ${portLabel}, but their patience is limited.`,
      lifetime: 400,
      icon: "mariner.opportunity.passenger",
      aspects: {
        "mariner.passenger.opportunity": 1,
        "mariner.local": 1,
        modded_talk_allowed: 1,
      },
      xtriggers: {
        "mariner.acceptclient": passengerElementId,
      },
    });
    mod.setElement(`passengers.${portRegion}`, {
      id: passengerElementId,
      label: `Passenger, Going to ${portLabel}`,
      description: `Passenger, going to ${portLabel}. They will reward us if we bring them to ${portLabel}, but their patience is limited.`,
      lifetime: 600,
      icon: "generic_c_grail",
      aspects: {
        "mariner.passenger": 1,
        [wantsToGoToAspect]: 1,
        "mariner.cabinuse": 1,
      },
      xtriggers: {
        [SIGNALS.FREAK_OUT]: [{ morpheffect: "mutate", id: "mariner.passenger.freakedout", level: 1 }],
      },
      decayTo: "mariner.lastingnotoriety",
    });

    // Recipe to give passenger opportunity
    // Valid if: you're not docked to the destination, but the port exists on the table
    mod.setRecipe("recipes.findpassengers", {
      id: `mariner.findpassenger.foundpassenger.goingto.${portId}`,
      extantreqs: { [portDockedId]: -1, [uniquenessGroupPort]: 1 },
      effects: { [opportunityElementId]: 1 },
      linked: [{ id: "mariner.findpassengers.foundsomeone" }],
    });

    // Recipe to grab & convert all passengers on arrival
    mod.setRecipe("recipes.convertpassengersonarrival", {
      id: `mariner.grabpassengersonarrival.${portRegion}.${portId}`,
      actionId: "mariner.grabpassengersonarrival",
      label: "Passengers disembarking",
      startdescription: "The passengers have reached their destination, and leave me only with their fare.",
      // Has to be an extantreqs because it doesn't bring the port with it
      extantreqs: { [portDockedId]: 1 },
      tablereqs: {
        [wantsToGoToAspect]: 1,
      },
      furthermore: [
        {
          effects: {
            funds: `2*[~/exterior:${wantsToGoToAspect}]`,
            [`mariner.rumour.${portRegion}`]: `[~/exterior:${wantsToGoToAspect}]`,
            "mariner.notoriety": `[~/exterior : { [mariner.passenger.freakedout] } : ${wantsToGoToAspect}]`,
          },
        },
        {
          target: "~/exterior",
          effects: { [wantsToGoToAspect]: -20 },
        },
      ],
    });
  }
}
