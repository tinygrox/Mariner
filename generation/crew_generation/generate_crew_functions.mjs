import { SIGNALS } from "../generate_signal_aspects.mjs";
import { generateTraumaSystem } from "./generate_trauma_system.mjs";
import { generateLeftOperandTraits } from "./left_operand_traits.mjs";
import { generateRightOperandTraits } from "./right_operand_traits.mjs";

const RECIPE_FILE = "recipes.assigntraitstocrew";

// Helpers for each operand/generic functions
export function generateCosmeticTrait(operand, aspectFile, recipeFile, linked, traitId, traitLabelString) {
  mod.setHiddenAspect(aspectFile, {
    id: `mariner.crew.traits.${traitId}`,
    xtriggers: {
      [operand === "leftoperand" ? SIGNALS.FORGET_LEFT_OPERAND : SIGNALS.FORGET_RIGHT_OPERAND]: [
        { morpheffect: "transform", id: `mariner.crew.traits.erased${operand === "leftoperand" ? "left" : "right"}` },
      ],
      [SIGNALS.WIPE_ALL_TRAITS]: [{ morpheffect: "setmutation", id: `mariner.crew.traits.${traitId}`, level: 0 }],
    },
  });

  mod.setRecipe(recipeFile, {
    id: `mariner.assigntraitstocrew.${operand}.cosmetic.assign.${traitId}`,
    mutations: [
      {
        filter: "mariner.crew",
        mutate: `mariner.crew.traits.${traitId}`,
        level: 1,
      },
    ],
    linked,
  });

  mod.crewMemberTraits[operand][`mariner.crew.traits.${traitId}`] = traitLabelString ?? traitId;
}

export function generateGameplayTrait(
  operand,
  aspectFile,
  recipeFile,
  linked,
  traitId,
  traitLabel,
  traitDescription,
  traitLabelString,
  aspectsToAdd,
  canBeGiven = true,
  customIcon = false
) {
  mod.setAspect(aspectFile, {
    id: `mariner.crew.traits.${traitId}`,
    icon: customIcon ? undefined : "ability",
    label: traitLabel ?? `<${traitId}>`,
    description: traitDescription ?? `<${traitId}>`,
    xtriggers: {
      [operand === "leftoperand" ? SIGNALS.FORGET_LEFT_OPERAND : SIGNALS.FORGET_RIGHT_OPERAND]: [
        { morpheffect: "transform", id: `mariner.crew.traits.erased${operand === "leftoperand" ? "left" : "right"}` },
      ],
      [SIGNALS.WIPE_ALL_TRAITS]: [{ morpheffect: "setmutation", id: `mariner.crew.traits.${traitId}`, level: 0 }],
    },
  });
  if (canBeGiven) {
    const additionalMutations = Object.entries(aspectsToAdd ?? {}).map(([aspect, quantity]) => ({
      filter: "mariner.crew",
      mutate: aspect,
      level: quantity,
      additive: true,
    }));

    mod.setRecipe(recipeFile, {
      id: `mariner.assigntraitstocrew.${operand}.gameplay.assign.${traitId}`,
      mutations: [
        {
          filter: "mariner.crew",
          mutate: `mariner.crew.traits.${traitId}`,
          level: 1,
        },
        ...additionalMutations,
      ],
      linked,
    });
  }
  mod.crewMemberTraits[operand][`mariner.crew.traits.${traitId}`] = traitLabelString ?? traitId;
}

export const ASSIGN_PREFIX = "mariner.assigntraitstocrew";
export const ASSIGN_LEFT = `${ASSIGN_PREFIX}.leftoperand`;
export const ASSIGN_RIGHT = `${ASSIGN_PREFIX}.rightoperand`;

// This method generates the recipes to generate traits on a crewmember element
function generateApplyTraitsToCrewMemberFunction() {
  /*
    The way it works:
    -use id wildcards to select random traits, to apply the first and the second trait, used for the first and second parts of the label
    */
  mod.initializeRecipeFile(RECIPE_FILE, ["crew", "traits"]);

  // Left
  mod.setRecipe(RECIPE_FILE, {
    id: `${ASSIGN_PREFIX}.start`,
    linked: [{ id: `${ASSIGN_LEFT}.gameplay.selection`, chance: 20 }, { id: `${ASSIGN_LEFT}.cosmetic.selection` }],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: `${ASSIGN_LEFT}.gameplay.selection`,
    linked: [{ id: `${ASSIGN_LEFT}.gameplay.assign.*`, randomPick: true }],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: `${ASSIGN_LEFT}.cosmetic.selection`,
    linked: [{ id: `${ASSIGN_LEFT}.cosmetic.assign.*`, randomPick: true }],
  });

  // Right
  mod.setRecipe(RECIPE_FILE, {
    id: `${ASSIGN_RIGHT}.selection`,
    linked: [{ id: `${ASSIGN_RIGHT}.gameplay.selection`, chance: 20 }, { id: `${ASSIGN_RIGHT}.cosmetic.selection` }],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: `${ASSIGN_RIGHT}.gameplay.selection`,
    linked: [{ id: `${ASSIGN_RIGHT}.gameplay.assign.*`, randomPick: true }],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: `${ASSIGN_RIGHT}.cosmetic.selection`,
    linked: [{ id: `${ASSIGN_RIGHT}.cosmetic.assign.*`, randomPick: true }],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: `${ASSIGN_PREFIX}.end`,
    linked: [{ useCallback: "RETURN_FROM_ASSIGN_TRAITS_TO_CREW" }],
  });

  generateLeftOperandTraits();
  generateRightOperandTraits();
}

function generateComposableTraitArchetypes() {
  mod.initializeElementFile("composables.traits", ["_composables"]);
  // mod.setElements("composables.traits", {
  //   id: "mariner.composables.trait",
  //   xtriggers: {
  //     [operand === "leftoperand" ? SIGNALS.FORGET_LEFT_OPERAND : SIGNALS.FORGET_RIGHT_OPERAND]: [
  //       { morpheffect: "transform", id: `mariner.crew.traits.erased${operand === "leftoperand" ? "left" : "right"}` },
  //     ],
  //     [SIGNALS.WIPE_ALL_TRAITS]: [{ morpheffect: "setmutation", id: `mariner.crew.traits.${traitId}`, level: 0 }],
  //   },
  // })
}

export function generateCrewFunctions() {
  mod.crewMemberTraits = { leftoperand: {}, rightoperand: {} };
  generateComposableTraitArchetypes();
  generateApplyTraitsToCrewMemberFunction();
  generateTraumaSystem();
}
