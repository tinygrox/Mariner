import { generateCosmeticTrait, ASSIGN_PREFIX, generateGameplayTrait } from "./generate_crew_functions.mjs";

const RECIPE_FILE = "recipes.assigntraitstocrew.rightoperand.traits";
const ASPECT_FILE = "aspects.assigntraitstocrew.rightoperand.traits";

function generateRightOperandGameplayTraits() {
  const addGameplayTrait = (
    traitId,
    traitLabel,
    traitDescription,
    traitLabelString,
    aspectsToAdd,
    canBeGiven = true,
    customIcon = false
  ) =>
    generateGameplayTrait(
      "rightoperand",
      ASPECT_FILE,
      RECIPE_FILE,
      [{ id: `${ASSIGN_PREFIX}.end` }],
      traitId,
      traitLabel,
      traitDescription,
      traitLabelString,
      aspectsToAdd,
      canBeGiven,
      customIcon
    );

  // Gameplay traits
  addGameplayTrait("erasedright", "Erased", "Erased", "...", null, false, true);

  // Bonus passive song influences
  addGameplayTrait(
    "bright",
    "Bright",
    "A remarkable conversationalist. They will help attract a more clever sort of sailor from the bars.",
    "Bright"
  );
  addGameplayTrait("handy", "Handy", "Willing to lend a hand even when the ship is docked in the wharves.", "Handy");
  addGameplayTrait(
    "whistling",
    "Whistling",
    "This sailor knows some of the tunes of the Raconteur Network. Packages will more easily find their way to me.",
    "Whistling"
  );
  // Negative passive song influences
  addGameplayTrait("dense", "Dense", "<>", "Dense");
  addGameplayTrait(
    "dense",
    "Dense",
    "Like talking to a brick wall. that will be unappealing to clever folk who I may wish to employ.",
    "Dense"
  );
  addGameplayTrait(
    "noisy",
    "Noisy",
    "With stomping boots and endless bodily noises, it is taxing to carry a tune near them. This may affect my dealings with the Raconteur Network.",
    "Noisy"
  );
  // Can't be used in verb traits
  addGameplayTrait("mute", "Mute", "If speaking is silver and silence is gold, this sailor's tongue is gilded.", "Mute");
  addGameplayTrait("landlubber", "Landlubber", "Regardless of their skills, this one will never be a true sailor.", "Landlubber");
  addGameplayTrait(
    "singleminded",
    "Singleminded",
    "This sailor is uninterested to think beyond it's regular duties.",
    "Singleminded"
  );
  addGameplayTrait(
    "rhythmless",
    "Rhythmless",
    "Perhaps they never learned, perhaps their Chor was stripped from them, but this sailor cannot carry a tune to save their life.",
    "Rhythmless"
  );
  // Small lore bonus on element
  addGameplayTrait("envowed", "Envowed", "Envowed to the Grail's Sacrosanct.", "Envowed", { grail: 2 });
  addGameplayTrait("initiated", "Initiated", "Initiated into the Mysteries of the Moth.", "Initiated", { moth: 2 });
  addGameplayTrait("inspired", "Inspired", "Inspired by the Teachings of The Lantern.", "Inspired", { lantern: 2 });
  addGameplayTrait("inducted", "Inducted", "Inducted into the Practices of the Forge.", "Inducted", { forge: 2 });
  addGameplayTrait("conscripted", "Conscripted", "Constripted to the Cause of the Edge.", "Conscripted", { edge: 2 });
  addGameplayTrait("consecrated", "Consecrated", "Consecrated under Winter's Rule.", "Consecrated", { winter: 2 });
  addGameplayTrait("oathbound", "Oathbound", "Oathbound into Heart's Chorus.", "Oathbound", { heart: 2 });
  addGameplayTrait("sanctified", "Sanctified", "Sanctified by the ministrations of Knock.", "Sanctified", { knock: 2 });
  // Always involved in fights
  addGameplayTrait("boisterous", "Boisterous", "Unable to pass up on a fight.", "Boisterous");
  addGameplayTrait("cannibal", "<Cannibal>", "<Have not only tasted, but acquired a taste.>", "<Cannibal>");
  addGameplayTrait("rival", "<Rival>", "<Competition is the engine of exaltation>", "<Rival>", { edge: 2 });
}

function generateRightOperandCosmeticTraits() {
  const addCosmeticTrait = (traitId, traitLabelString) =>
    generateCosmeticTrait("rightoperand", ASPECT_FILE, RECIPE_FILE, [{ id: `${ASSIGN_PREFIX}.end` }], traitId, traitLabelString);

  // Cosmetic traits
  addCosmeticTrait("bald", "Bald");
  addCosmeticTrait("bellowing", "Bellowing");
  addCosmeticTrait("boisterous", "Boisterous");
  addCosmeticTrait("bright-eyed", "Bright-eyed");
  addCosmeticTrait("crosseyed", "Crosseyed");
  addCosmeticTrait("dense", "Dense");
  addCosmeticTrait("elegant", "Elegant");
  addCosmeticTrait("large", "Large");
  addCosmeticTrait("gaunt", "Gaunt");
  addCosmeticTrait("handsome", "Handsome");
  addCosmeticTrait("heavyset", "Heavyset");
  addCosmeticTrait("hulking", "Hulking");
  addCosmeticTrait("keeneyed", "Keen-eyed");
  addCosmeticTrait("lanky", "Lanky");
  addCosmeticTrait("petite", "Petite");
  addCosmeticTrait("portly", "Portly");
  addCosmeticTrait("sallowfaced", "Sallow-faced");
  addCosmeticTrait("short", "Short");
  addCosmeticTrait("silverhaired", "Silver-haired");
  addCosmeticTrait("small", "Small");
  addCosmeticTrait("stout", "Stout");
  addCosmeticTrait("stocky", "Stocky");
  addCosmeticTrait("sunkissed", "Sunkissed");
  addCosmeticTrait("tall", "Tall");
  addCosmeticTrait("tanned", "Tanned");
  addCosmeticTrait("thick", "Thick");
  addCosmeticTrait("toothless", "Toothless");
  addCosmeticTrait("veteran", "Veteran");
  addCosmeticTrait("wellgroomed", "Well-groomed");
  addCosmeticTrait("wildeyed", "Wild-eyed");
  addCosmeticTrait("young", "Young");
  addCosmeticTrait("youthful", "Youthful");
  addCosmeticTrait("slim", "Slim");
  addCosmeticTrait("skinny", "Skinny");
  addCosmeticTrait("pimpled", "Pimpled");
  addCosmeticTrait("pockmarked", "Pockmarked");
  addCosmeticTrait("scarred", "Scarred");
  addCosmeticTrait("wellbuilt", "Well-built");
  addCosmeticTrait("frail", "Frail");
  addCosmeticTrait("pudgy", "Pudgy");
  addCosmeticTrait("skeletal", "Skeletal");
  addCosmeticTrait("towering", "Towering");
  addCosmeticTrait("tiny", "Tiny");
  addCosmeticTrait("jaundiced", "Jaundiced");
  addCosmeticTrait("wrinkled", "Wrinkled");
  addCosmeticTrait("dewey", "Dewey");
  addCosmeticTrait("oily", "Oily");
  addCosmeticTrait("cracked", "Cracked");
  addCosmeticTrait("dirty", "Dirty");
  addCosmeticTrait("clean", "Clean");
  addCosmeticTrait("rough", "Rough");
  addCosmeticTrait("smooth", "Smooth");
  addCosmeticTrait("bushybrowed", "Bushy-browed");
  addCosmeticTrait("hawknosed", "Hawk-nosed");
  addCosmeticTrait("brutish", "Brutish");
  addCosmeticTrait("unassuming", "Unassuming");
  addCosmeticTrait("cultured", "Cultured");
  addCosmeticTrait("fashionable", "Fashionable");
  addCosmeticTrait("westislander", "West-islander");
  addCosmeticTrait("channelborn", "Channel-born");
  addCosmeticTrait("mainlander", "Mainlander");
  addCosmeticTrait("northerner", "Northener");
  addCosmeticTrait("cityslicker", "Cityslicker");
  addCosmeticTrait("freshfaced", "Fresh-faced");
  addCosmeticTrait("seasoned", "Seasoned");
  addCosmeticTrait("eastcoast", "East-coast");
  addCosmeticTrait("provincial", "provincial");
  addCosmeticTrait("heavilyaccented", "Heavily-accented");
  addCosmeticTrait("wellspoken", "Well-spoken");
  addCosmeticTrait("unblinking", "Unblinking");
}

export function generateRightOperandTraits() {
  mod.initializeRecipeFile(RECIPE_FILE, ["crew", "traits"]);
  mod.initializeAspectFile(ASPECT_FILE, ["crew", "traits"]);

  generateRightOperandGameplayTraits();
  generateRightOperandCosmeticTraits();
}
