import { generateCosmeticTrait, ASSIGN_RIGHT, generateGameplayTrait } from "./generate_crew_functions.mjs";

const RECIPE_FILE = "recipes.assigntraitstocrew.leftoperand.traits";
const ASPECT_FILE = "aspects.assigntraitstocrew.leftoperand.traits";

function generateLeftOperandGameplayTraits() {
  const addGameplayTrait = (
    traitId,
    traitLabel,
    traitDescription,
    traitLabelString,
    aspectsToAdd,
    canBeGiven = true,
    customIcon = false
  ) =>
    generateGameplayTrait(
      "leftoperand",
      ASPECT_FILE,
      RECIPE_FILE,
      [{ id: `${ASSIGN_RIGHT}.selection` }],
      traitId,
      traitLabel,
      traitDescription,
      traitLabelString,
      aspectsToAdd,
      canBeGiven,
      customIcon
    );

  // Gameplay traits
  addGameplayTrait("erasedleft", "Erased", "Erased", "A ...", null, false, true);

  // Bonus passive song influences
  addGameplayTrait(
    "charming",
    "Charming",
    "A charming sailor is quickly befriended, rarely trusted but easily forgiven.",
    "A Charming"
  );
  addGameplayTrait(
    "shifty",
    "Shifty",
    "This sailor has a nose for deals that are lucrative, clandestine and less than legal.",
    "A shifty"
  );
  addGameplayTrait(
    "engaging",
    "Engaging",
    "A winning smile and a wicked sense of humor. This sailor will help attract a more adventurous crew.",
    "An Engaging"
  );
  addGameplayTrait(
    "goodfortuned",
    "Good-Fortuned",
    "There is something unsettling about a sailor without tales of hardships. But we are willing to forgive them to gain some of that quiet for our coming voyages.",
    "A Good-fortuned"
  );
  addGameplayTrait("entertaining", "Entertaining", "Quick to laugh and make other laugh in turn.", "An Entertaining");
  // Negative passive song influences
  addGameplayTrait("unpleasant", "Unpleasant", "One who isn't liked is easy to blame.", "An Unpleasant");
  addGameplayTrait("crude", "Crude", "A social lack of subtlety can be the death of many a business deal.", "A Crude");
  addGameplayTrait("clumsy", "Clumsy", "As likely to cause damage as to repair it.", "A Clumsy");
  addGameplayTrait(
    "cowardly",
    "Cowardly",
    "With one who has a reputation like this on my ship, I am unlikely to attract a more adventurous sort of sailor.",
    "A Cowardly"
  );
  addGameplayTrait("unlucky", "Unlucky", "When this crewmember takes a watch, fate is more keen to act.", "An Unlucky");
  addGameplayTrait("rambunctious", "Rambunctious", "This sailor will not improve my standing in ports.", "A Rambunctious");
  // Can't be used in verb traits
  addGameplayTrait(
    "craven",
    "Craven",
    "This sailor has strong opinions on where a sailor's nose should and should not belong. They will refuse to Explore the unfamiliar.",
    "A Craven"
  );
  // Longing malus
  addGameplayTrait("flighty", "Flighty", "This one's attention wanders. They won't last long on my crew.", "A Flighty");
  // No longing
  addGameplayTrait(
    "loyal",
    "Loyal",
    "Now that this sailor has joined the crew, only death could convince them to part.",
    "A Loyal"
  );
  // Drunk
  addGameplayTrait("boozy", "Boozy", "Easily tempted by chinking glass.", "A Boozy");
  // 1 more sea event
  addGameplayTrait("illomened", "Ill-omened", "A cloud hangs over this one, and the ships they voyaged with.", "An Ill-omened");
  // One less fighter? Never fights?
  addGameplayTrait("harmonious", "Harmonious", "One who keeps the peace can be invaluable in close quarders.", "An Harmonious");
  // Counts as notoriety
  addGameplayTrait(
    "wanted",
    "Wanted",
    "This crewmember has the furtive eyes of someone chased, though they will not share their story with me.",
    "A Wanted"
  );
}

function generateLeftOperandCosmeticTraits() {
  const addCosmeticTrait = (traitId, traitLabelString) =>
    generateCosmeticTrait(
      "leftoperand",
      ASPECT_FILE,
      RECIPE_FILE,
      [{ id: `${ASSIGN_RIGHT}.selection` }],
      traitId,
      traitLabelString
    );

  // Cosmetic traits
  addCosmeticTrait("arrogant", "An Arrogant");
  addCosmeticTrait("beautiful", "A Beautiful");
  addCosmeticTrait("bold", "A Bold");
  addCosmeticTrait("brave", "A Brave");
  addCosmeticTrait("brazen", "A Brazen");
  addCosmeticTrait("calm", "A Calm");
  addCosmeticTrait("careful", "A Careful");
  addCosmeticTrait("careless", "A Careless");
  addCosmeticTrait("choleric", "A Choleric");
  addCosmeticTrait("commanding", "A Commanding");
  addCosmeticTrait("crass", "A Crass");
  addCosmeticTrait("cruel", "A Cruel");
  addCosmeticTrait("dark", "A Dark");
  addCosmeticTrait("devout", "A Devout");
  addCosmeticTrait("dour", "A Dour");
  addCosmeticTrait("eager", "An Eager");
  addCosmeticTrait("easygoing", "An Easygoing");
  addCosmeticTrait("fearful", "A Fearful");
  addCosmeticTrait("flighty", "A Flighty");
  addCosmeticTrait("focussed", "A Focussed");
  addCosmeticTrait("foulmouthed", "A Foul-mouthed");
  addCosmeticTrait("goodnatured", "A Good-natured");
  addCosmeticTrait("grave", "A Grave");
  addCosmeticTrait("harmonious", "A Harmonious");
  addCosmeticTrait("heavyhearted", "A Heavyhearted");
  addCosmeticTrait("helpful", "A Helpful");
  addCosmeticTrait("hotheaded", "A Hotheaded");
  addCosmeticTrait("humorless", "A Humorless");
  addCosmeticTrait("humourous", "A Humourous");
  addCosmeticTrait("jolly", "A Jolly");
  addCosmeticTrait("loquatious", "A Loquatious");
  addCosmeticTrait("kind", "A Kind");
  addCosmeticTrait("kindhearted", "A Kindhearted");
  addCosmeticTrait("melancholic", "A Melancholic");
  addCosmeticTrait("mildmannered", "A Mild-mannered");
  addCosmeticTrait("mousy", "A Mousy");
  addCosmeticTrait("nervous", "A Nervous");
  addCosmeticTrait("oderous", "An Oderous");
  addCosmeticTrait("authentic", "An Authentic");
  addCosmeticTrait("ominous", "An Ominous");
  addCosmeticTrait("overeager", "An Overeager");
  addCosmeticTrait("pensive", "A Pensive");
  addCosmeticTrait("pious", "A Pious");
  addCosmeticTrait("proud", "A Proud");
  addCosmeticTrait("sad", "A Sad");
  addCosmeticTrait("sarcastic", "A Sarcastic");
  addCosmeticTrait("softspoken", "A Softspoken");
  addCosmeticTrait("somber", "A Somber");
  addCosmeticTrait("steadfast", "A Steadfast");
  addCosmeticTrait("taciturn", "A Taciturn");
  addCosmeticTrait("vile", "A Vile");
  addCosmeticTrait("talkative", "A Talkative");
  addCosmeticTrait("wellintentioned", "A Well-intentioned");
  addCosmeticTrait("wellmannered", "A Well-mannered");
  addCosmeticTrait("unlikable", "A Unlikable");
  addCosmeticTrait("vengeful", "A Vengeful");
  addCosmeticTrait("thickheaded", "A Thickheaded");
  addCosmeticTrait("wild", "A Wild");
  addCosmeticTrait("compassionate", "A Compassionate");
  addCosmeticTrait("considerate", "A Considerate");
  addCosmeticTrait("diligent", "A Diligent");
  addCosmeticTrait("energetic", "A Energetic");
  addCosmeticTrait("exuberant", "A Exuberant");
  addCosmeticTrait("likable", "A Likable");
  addCosmeticTrait("nice", "A Nice");
  addCosmeticTrait("outgoing", "A Outgoing");
  addCosmeticTrait("passionate", "A Passionate");
  addCosmeticTrait("persistent", "A Persistent");
  addCosmeticTrait("polite", "A Polite");
  addCosmeticTrait("principled", "A Principled");
  addCosmeticTrait("productive", "A Productive");
  addCosmeticTrait("homesick", "A Homesick");
}

export function generateLeftOperandTraits() {
  mod.initializeRecipeFile(RECIPE_FILE, ["crew", "traits"]);
  mod.initializeAspectFile(ASPECT_FILE, ["crew", "traits"]);

  generateLeftOperandGameplayTraits();
  generateLeftOperandCosmeticTraits();
}
