import { ME_RECIPE, buildRefinementBlock } from "../generation_helpers.mjs";
export const SUMMON_TYPES = [];

//types: Devourer, Deciever, Incorporeal, Faciemorph
// the creatureSummon function is a function that helps quickly generate all the logic associated with the summons, so they just have to be filled with the content.
//inside createSummon the following parameters are expected:
// label, filled with the label
// description, filled with the description (should there also be more states of this? recuperating)
// types: the types of summons are specific features/archetypes they have. which come as aspects that allow certain actions/behaviors/interactions. I expect we will have a set of 5 to 7 different types, taht we can mix and match across the different summons.
// aspects, all the regular, additional aspectst that the summon has, seperate of the regular aspect each npc has.

//inside the genrateSummons function, we define the behavior for each of the types, as well as each of the aspects, as well as the generic functions to dismiss and or bolster the cummon.
//i think the summons need some more states. perhaps recuperating, and perhaps an option to break free, either when you usmmon it or later.

function createSummon(id, label, description, aspects) {
  SUMMON_TYPES.push(label.toLowerCase());

  mod.setElement("summon", {
    id: `mariner.crew.summon.${id}`,
    label,
    description,
    aspects: {
      "mariner.summon": 1,
      "mariner.crew": 1,
      ...aspects,
    },
    icon: `mariner.summons.${id}`,
    useBigPicture: true,
    useAlternativeTextStyle: true,
  });
}

export function generateSummonBodies() {
  mod.initializeDeckFile("decks.summon", ["crew"]);
  mod.setDeck("decks.summon", {
    id: "mariner.decks.summons.wild",
    spec: [],
    resetonexhaustion: true,
    shuffleAfterDraw: true,
  });

  mod.initializeAspectFile("summon_aspects", ["crew"]);
  mod.setAspect("summon_aspects", {
    id: "mariner.summon",
    icon: "summoned",
    label: "<A Summon>",
    description:
      "<An Entity from behind starlight and or below the skin of the world. This entity is bound by me by the power of my Songs, but that bond is not eternal, and not unbreakable.>",
    xtriggers: {
      // TODO: wth is this signal doing here?
      // [SIGNALS.UPDATE_DISPLAYED_REPUTATION]: ME_RECIPE("mariner.updatedisplayedreputation.default"),
    },
  });
  mod.setAspect("summon_aspects", {
    id: "mariner.construct",
    label: "<Construct>",
    description: "<an Animated simulacum of the mortal form>",
  });
  mod.setAspect("summon_aspects", {
    id: "mariner.monster",
    label: "<Monster>",
    description: "<Not all myth has fled the world just yet.>",
  });
  mod.setAspect("summon_aspects", {
    id: "mariner.spirit",
    label: "<Spirit>",
    description: "<From behind the rain and beyond the sky, this is being native to the House of the Sun.>",
  });

  mod.initializeElementFile("summon", ["crew"]);

  createSummon(
    "caligine",
    "Caligine",
    'The misbegotten spawn of the Forge, sublimating from the baser operations of the Malleary. "They say all creations of The Forge-of-Days are sublime, with no refuse or regret. How then, does one explain the Caligine? --Xenodice"',
    {
      forge: 6,
      moth: 4,
      winter: 2,
      "mariner.crew.traits.incorporeal": 1,
      "mariner.crew.traits.fingerless": 1,
      "mariner.crew.traits.canrebel": 1,
      "mariner.crew.traits.freak": 1,
    }
  );
  createSummon(
    "maidinthemirror",
    "Maid In the Mirror",
    'Those that ascend in the house of the I may never enter, enter into the the service of that cold and distant Sun. "To summon one, you must know their name, but never speak it. You must be steeled as a cold and hard master, but never let their cruelty stain your intentions. They never forget their life, but they do not remember it kindly."-- Alexia ',
    {
      edge: 6,
      lantern: 4,
      winter: 2,
      "mariner.crew.traits.invulnerable": 1,
      "mariner.crew.traits.incorporeal": 1,
      "mariner.crew.traits.unsociable": 1,
      "mariner.crew.traits.fingerless": 1,
      "mariner.crew.traits.canrebel": 1,
      "mariner.crew.traits.freak": 1,
    }
  );
  createSummon(
    "strixling",
    "Strixling",
    'The Strix serve either the Lionsmith or the Colonel, or both. It has been said they served a different mistress before them, and now this one serves me. "Both vegeance and victory come winged and clawed"--Kitty Marazine',
    {
      edge: 5,
      knock: 5,
      "mariner.crew.traits.invulnerable": 1,
      "mariner.crew.traits.incorporeal": 1,
      "mariner.crew.traits.unsociable": 1,
      "mariner.crew.traits.fingerless": 1,
      "mariner.crew.traits.canrebel": 1,
    }
  );
  createSummon(
    "didumos",
    "Didumos",
    'There is an undeniable kinship. It too, feels misplaced, untethered, lost in a world so flat and grey and wrong. And the stars too, sing. "In our dreams our Twin-selves pursue what we cannot express, or are pursued by it. But Half-Hearts cannot dream, and Didumos are only dream. "--Agdistis',
    {
      lantern: 6,
      knock: 4,
      heart: 2,
      "mariner.crew.traits.incorporeal": 1,
      "mariner.crew.traits.unsociable": 1,
      "mariner.crew.traits.fingerless": 1,
      "mariner.crew.traits.canrebel": 1,
    }
  );
  createSummon(
    "burgeoningrisen",
    "Burgeoning Risen",
    'A walking treshold, with the fecundity of the Woods spilling forth into gut and limb and orifice. The parts make the whole, and the whole is happy, for a while. "A seed planted in winter will bud in spring, a seed planted in spring will rise in summer. But what about a seed planted in mercy, or despair?" --Harald Blomkwist',
    {
      moth: 6,
      winter: 4,
      grail: 2,
      "mariner.crew.traits.nonliving": 1,
      "mariner.crew.traits.unsociable": 1,
      "mariner.crew.traits.candecay": 1,
      "mariner.crew.traits.freak": 1,
    }
  );
  createSummon(
    "shatteredrisen",
    "Shattered Risen",
    'A corpse risen under the auspicise of the raised knife. The powers that animate a Shattered Risen do not preserve it well: Each step splinters, each turn rips, each lunge shatters. "Sweet succor it may be, to die supplicant to the Sun. But if in Death a knife was raised, service has just begun." --Aunt Mopsies book of proverbs',
    {
      edge: 6,
      winter: 4,
      forge: 2,
      "mariner.crew.traits.nonliving": 1,
      "mariner.crew.traits.unsociable": 1,
      "mariner.crew.traits.candecay": 1,
      "mariner.crew.traits.freak": 1,
    }
  );
  createSummon(
    "voicelessrisen",
    "Voiceless Risen",
    'Not all corpses are eligable to rise as a Voiceless Risen. But when one rises they are a servant that ceaseless, cunning and utterly quiet. "Those Dead that pass the White Door leave their speech behind, like a sword surrendered to a host" -Christopher Illopolli.',
    {
      heart: 6,
      winter: 4,
      lantern: 2,
      "mariner.crew.traits.nonliving": 1,
      "mariner.crew.traits.unsociable": 1,
      "mariner.crew.traits.candecay": 1,
      "mariner.crew.traits.freak": 1,
    }
  );
  createSummon("poppet", "Poppet", "<>", {
    heart: 4,
    forge: 4,
    "mariner.crew.traits.nonliving": 1,
    "mariner.crew.traits.candecay": 1,
    "mariner.crew.traits.freak": 1,
  });
  createSummon(
    "iarella",
    "Iarella",
    'pearl-born, witch-panther, hunter-sister, abyss-eye. It twins behind ships, shedding its skin to walk drowning in air upon the shoreline. "Behind the waves there is a city, below it lies its twin. Only when the water is still the two are one"--Ferishun',
    {
      moth: 2,
      grail: 6,
      edge: 4,
      "mariner.crew.traits.fingerless": 1,
      "mariner.crew.traits.canrebel": 1,
      "mariner.crew.traits.unsociable": 1,
    }
  );
  createSummon(
    "cousinscousin",
    "Cousin's Cousin",
    'In low light, with enough layers, a careless observer might mistake this for a human. They themselves will ensure you they are kin to mankind, by our Other Grandmother, twice removed. They carry themselves with the triplicate cunning of the outcast. "More remants remain in the forgotten corners of our world. How you feel about that will determine if this is a matter of Preservation or of Hushery" -- an upcoming npc',
    {
      grail: 4,
      moth: 6,
      knock: 2,
      "mariner.crew.traits.canrebel": 1,
    }
  );
  createSummon(
    "skaptodon",
    "Skaptodon",
    'Part mole, part myth, not unlike a siege-weapon and all to much like a vendetta. A breaker of boundaries bound to my service. "How does one feel in service to a god of betrayal? Is that strength still a gift when it heralds you as a challengef or all those who wis to prove their privado" -- an upcomming npc',
    {
      heart: 4,
      edge: 6,
      forge: 4,
      "mariner.crew.traits.invulnerable": 1,
      "mariner.crew.traits.unsociable": 1,
      "mariner.crew.traits.individualistic": 1,
      "mariner.crew.traits.fingerless": 1,
      "mariner.crew.traits.holdkept": 1,
      "mariner.crew.traits.canrebel": 1,
    }
  );
  createSummon(
    "invited",
    "Invited",
    "Older then humanity, colder then the ocean, harder then scale, kinder then sunlight. \"Do you hear a call from the ocean, when the Changeling moon rises? Go on, why won't you wade in. Let's see what you discover below the surface\" --Fraser Strathcoyne.",
    {
      knock: 6,
      winter: 4,
      grail: 4,
      "mariner.crew.traits.unsociable": 1,
      "mariner.crew.traits.individualistic": 1,
      "mariner.crew.traits.canrebel": 1,
    }
  );
}
