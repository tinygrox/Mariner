import { SIGNALS } from "../generate_signal_aspects.mjs";
import { buildRefinementBlock } from "../generation_helpers.mjs";
export const CREW_TYPES = [];

function refinedCrewmemberLabel(title) {
  return `${buildRefinementBlock(mod.crewMemberTraits.leftoperand)} ${buildRefinementBlock(
    mod.crewMemberTraits.rightoperand
  )} ${title}`;
}

function createSpecialist(label, description, type, aspects) {
  let firstAspect = Object.keys(aspects)[0];
  CREW_TYPES.push(label.toLowerCase());
  mod.setElement(
    "crewmen",
    {
      //icon: `generic_a_${firstAspect}`,
      id: `mariner.crew.specialist.${label.toLowerCase()}`,
      $derives: ["mariner.composables.crew.mortal"],
      dynamicIcon: buildRefinementBlock(
        {
          "mariner.crew.wantstoleave": "mariner.crew.wantstoleave",
          "mariner.crew.exhausted": "mariner.crew.exhausted",
        },
        `generic_a_${firstAspect}` /*`mariner.crew.specialist.${label.toLowerCase()}`*/
      ),
      dynamicLabel: refinedCrewmemberLabel(label),
      description,
      aspects,
    },
    true
  );

  const cost = 2 + Object.keys(aspects).length;
  const opportunityElement = {
    id: `mariner.crew.specialist.${label.toLowerCase()}.opportunity`,
    label: `Opportunity to Recruit: ${label}`,
    description: "This one would agree to join us...for a price.",
    icon: "mariner.recruitmentopportunity.specialist",
    aspects: {
      "mariner.local": 1,
      modded_talk_allowed: 1,
      "mariner.recruitmentopportunity": 1,
      "mariner.opportunitycost": cost,
    },
    lifetime: 120,
    xtriggers: {
      "mariner.paidopportunity": `mariner.crew.specialist.${label.toLowerCase()}`,
    },
    slots: [],
  };
  const freeOpportunityElement = {
    id: `mariner.crew.specialist.${label.toLowerCase()}.opportunity.free`,
    label: `Opportunity to Recruit: ${label} [free]`,
    description: "This one would agree to join us.",
    icon: "mariner.recruitmentopportunity.specialist",
    aspects: {
      "mariner.local": 1,
      modded_talk_allowed: 1,
      "mariner.recruitmentopportunity": 1,
    },
    lifetime: 120,
    xtriggers: {
      "mariner.paidopportunity": `mariner.crew.specialist.${label.toLowerCase()}`,
    },
  };

  mod.setElements("crewmen", opportunityElement, freeOpportunityElement);
  for (let i = 1; i <= cost; i++) {
    opportunityElement.slots.push({
      id: "funds" + i,
      label: "Funds",
      actionId: "talk",
      required: { funds: 1 },
    });
  }
  let deckSpec = mod.decks["decks.crew"].find((d) => d.id === `mariner.decks.specialists.opportunities.${type}`).spec;
  deckSpec.push(`mariner.crew.specialist.${label.toLowerCase()}.opportunity`);

  deckSpec = mod.decks["decks.crew"].find((d) => d.id === `mariner.decks.specialists.opportunities.${type}.free`).spec;
  deckSpec.push(`mariner.crew.specialist.${label.toLowerCase()}.opportunity.free`);
}

function computeBasicCrewMember(suffix, traits = []) {
  return {
    id: `mariner.crew.${suffix}`,
    $derives: ["mariner.composables.crew.mortal"],
    dynamicIcon: buildRefinementBlock(
      {
        "mariner.crew.wantstoleave": "mariner.crew.wantstoleave",
        "mariner.crew.exhausted": "mariner.crew.exhausted",
      },
      "mariner.crew.basic"
    ),
    dynamicLabel: refinedCrewmemberLabel("Crewman"),
    description: "Able to do anything, good at nothing. Expendable.",
    aspects: {
      mortal: 1,
      heart: 1,
      forge: 1,
      edge: 1,
      ...Object.fromEntries(traits.map((n) => [`mariner.crew.traits.${n}`, 1])),
    },
  };
}

function generateIntroCrew() {
  mod.initializeElementFile("introcrew", ["crew"]);
  mod.setElement("introcrew", computeBasicCrewMember("intro.doc", ["loquatious", "bright"]), true);
  mod.setElement("introcrew", computeBasicCrewMember("intro.grumpy", ["brave", "boisterous"]), true);
  mod.setElement("introcrew", computeBasicCrewMember("intro.bashful", ["cowardly", "fashionable"]), true);
  mod.setElement("introcrew", computeBasicCrewMember("intro.sneezy", ["noisy", "flighty"]), true);
  mod.setElement("introcrew", computeBasicCrewMember("intro.happy", ["engaging", "bright-eyed"]), true);
  mod.setElement("introcrew", computeBasicCrewMember("intro.dopey", ["clumsy", "mute"]), true);
  mod.setElement("introcrew", computeBasicCrewMember("intro.sleepy", ["careless", "stout"]), true);
}

export function generateCrewmen() {
  mod.initializeDeckFile("decks.crew", ["crew"]);
  mod.setDeck("decks.crew", {
    id: "mariner.decks.specialists.opportunities.manual",
    spec: [],
    resetonexhaustion: true,
    shuffleAfterDraw: true,
  });
  mod.setDeck("decks.crew", {
    id: "mariner.decks.specialists.opportunities.intellectual",
    spec: [],
    resetonexhaustion: true,
    shuffleAfterDraw: true,
  });
  mod.setDeck("decks.crew", {
    id: "mariner.decks.specialists.opportunities.manual.free",
    spec: [],
    resetonexhaustion: true,
    shuffleAfterDraw: true,
  });
  mod.setDeck("decks.crew", {
    id: "mariner.decks.specialists.opportunities.intellectual.free",
    spec: [],
    resetonexhaustion: true,
    shuffleAfterDraw: true,
  });

  mod.initializeElementFile("crewmen", ["crew"]);
  mod.setElement("crewmen", computeBasicCrewMember("basic"), true);
  mod.setElements(
    "crewmen",
    {
      id: `mariner.crew.basic.opportunity`,
      label: `Opportunity to Recruit a basic crewmember`,
      description: "This one would agree to join us...for a price.",
      icon: "mariner.recruitmentopportunity.basic",
      aspects: {
        "mariner.local": 1,
        modded_talk_allowed: 1,
        "mariner.recruitmentopportunity": 1,
        "mariner.opportunitycost": 1,
      },
      lifetime: 120,
      xtriggers: {
        "mariner.paidopportunity": `mariner.crew.basic`,
      },
      slots: [
        {
          id: "funds",
          label: "Funds",
          actionId: "talk",
          required: { funds: 1 },
        },
      ],
    },
    {
      id: `mariner.crew.basic.opportunity.free`,
      label: `Opportunity to Recruit a basic crewmember [free]`,
      description: "This one would agree to join us.",
      icon: "mariner.recruitmentopportunity.basic",
      aspects: {
        "mariner.local": 1,
        modded_talk_allowed: 1,
        "mariner.recruitmentopportunity": 1,
      },
      lifetime: 120,
      xtriggers: {
        "mariner.paidopportunity": `mariner.crew.basic`,
      },
    }
  );

  generateIntroCrew();

  createSpecialist(
    "Navigator",
    '"I can read the stars, and tell you their stories. Oh, and of course I navigate."',
    "intellectual",
    { lantern: 4, knock: 4 }
  );
  createSpecialist("Canoneer", '"I just want you to show me where to point \'em."', "manual", { edge: 4, forge: 4 });
  createSpecialist("Privateer", '"I\'m good with guns."', "manual", {
    edge: 5,
  });
  createSpecialist("Carpenter", '"I\'m good with tools."', "manual", {
    forge: 5,
  });
  createSpecialist("Cook", '"I\'ll get your men through it all, hale and happy."', "manual", { heart: 4, grail: 4 });
  createSpecialist("Diver", '"Just send me down to go where no one has gone before."', "intellectual", { winter: 4, knock: 4 });
  createSpecialist("Mate", '"I\'m good on deck."', "manual", { heart: 5 });
  createSpecialist("Officer", '"I\'ll keep order on the ship."', "intellectual", { winter: 4, lantern: 4 });
  createSpecialist("Conman", "\"I've never been convicted, and I'll see it it you're not either.\"", "intellectual", {
    grail: 4,
    moth: 4,
  });
}
