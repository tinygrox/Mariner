import { SIGNALS } from "../generate_signal_aspects.mjs";

export function generateTraumaSystem() {
  mod.initializeAspectFile("aspects.crewtrauma", ["crew"]);

  // Trauma aspects used
  mod.setAspects(
    "aspects.crewtrauma",
    {
      id: "mariner.crew.trauma.shaken",
      label: "<Superstitious>",
      description: "<This one got his mind mauled by visions of death. He won't be able to participate to any violent action.>",
    },
    {
      id: "mariner.crew.trauma.superstitious",
      label: "<Shaken>",
      description:
        "<This one got his mind shattered by visions of impossible possibilities. He won't be able to participate to any action of supernatural nature, or otherworldly threat.>",
    }
  );

  // The recipes/tokens applying them
  mod.initializeRecipeFile("recipes.traumatizecrewmember", ["crew", "events"]);
  // Experience superstitious event
  mod.setRecipes(
    "recipes.traumatizecrewmember",
    {
      warmup: 1,
      id: "mariner.genericevents.traumatizecrewmember.supernatural",
      actionId: "mariner.traumatizecrewmember.supernatural",
      linked: [
        { id: "mariner.genericevents.traumatizecrewmember.supernatural.foundexpulsed" },
        { id: "mariner.genericevents.traumatizecrewmember.supernatural.grabexpulsed" },
      ],
    },
    {
      id: "mariner.genericevents.traumatizecrewmember.supernatural.foundexpulsed",
      requirements: { "mariner.crew": 1 },
      label: "<>",
      startdescription: "<Visions of impossible horrors.>",
      linked: [{ id: "mariner.genericevents.traumatizecrewmember.supernatural.triggeraction" }],
    },
    {
      id: "mariner.genericevents.traumatizecrewmember.supernatural.grabexpulsed",
      warmup: 15,
      label: "<>",
      startdescription: "<Visions of impossible horrors.>",
      requirements: { "mariner.crew": -1 },
      slots: [
        {
          id: "victim",
          label: "The Daggers of Danger",
          description: "I know this won't end well.",
          greedy: true,
          required: { "mariner.crew": 1 },
        },
      ],
      linked: [{ id: "mariner.genericevents.traumatizecrewmember.supernatural.triggeraction" }],
    },
    {
      id: "mariner.genericevents.traumatizecrewmember.supernatural.triggeraction",
      mutations: [
        {
          filter: "mariner.crew",
          mutate: "mariner.crew.targeted",
          level: 1,
          additive: false,
        },
      ],
      aspects: { [SIGNALS.WITNESS_SUPERNATURAL_TRAUMA]: 1 },
    }
  );

  mod.setRecipe("recipes.traumatizecrewmember", {
    id: "mariner.genericevents.traumatizecrewmember.supernatural.injuremortal",
    warmup: 15,
    label: "<>",
    startdescription: "<>",
    description: "<>",
    effects: { "mariner.crew": -1 },
    mutations: [{ filter: "mariner.crew", mutate: "mariner.crew.trauma.superstitious", level: 1, additive: false }],
  });

  mod.setRecipes(
    "recipes.traumatizecrewmember",
    {
      warmup: 1,
      id: "mariner.genericevents.traumatizecrewmember.horror",
      actionId: "mariner.traumatizecrewmember.horror",
      linked: [
        { id: "mariner.genericevents.traumatizecrewmember.horror.foundexpulsed" },
        { id: "mariner.genericevents.traumatizecrewmember.horror.grabexpulsed" },
      ],
    },
    {
      id: "mariner.genericevents.traumatizecrewmember.horror.foundexpulsed",
      requirements: { "mariner.crew": 1 },
      label: "<>",
      startdescription: "<Visions of impossible horrors.>",
      linked: [{ id: "mariner.genericevents.traumatizecrewmember.horror.triggeraction" }],
    },
    {
      id: "mariner.genericevents.traumatizecrewmember.horror.grabexpulsed",
      warmup: 15,
      label: "<>",
      startdescription: "<Contemplating Pure Violence.>",
      requirements: { "mariner.crew": -1 },
      slots: [
        {
          id: "victim",
          label: "The Daggers of Danger",
          description: "I know this won't end well.",
          greedy: true,
          required: { "mariner.crew": 1 },
        },
      ],
      linked: [{ id: "mariner.genericevents.traumatizecrewmember.horror.triggeraction" }],
    },
    {
      id: "mariner.genericevents.traumatizecrewmember.horror.triggeraction",
      mutations: [
        {
          filter: "mariner.crew",
          mutate: "mariner.crew.targeted",
          level: 1,
          additive: false,
        },
      ],
      aspects: { [SIGNALS.WITNESS_HORROR_TRAUMA]: 1 },
    }
  );

  mod.setRecipe("recipes.traumatizecrewmember", {
    id: "mariner.genericevents.traumatizecrewmember.horror.injuremortal",
    warmup: 15,
    label: "<>",
    startdescription: "<>",
    description: "<>",
    effects: { "mariner.crew": -1 },
    mutations: [{ filter: "mariner.crew", mutate: "mariner.crew.trauma.shaken", level: 1, additive: false }],
  });
}

// The clearing performance
export function generateCrewTraumaClearingPerformance() {
  const RECIPE_FILE = "recipes.performances.cleartrauma";
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "cleartrauma"]);
  mod.setRecipes(
    RECIPE_FILE,
    {
      id: "mariner.performances.cleartrauma",
      label: "<Erase Trauma>",
      startdescription:
        "<I will weave the tattered parts of the mind of this person, leaving less of who they were, but what will remain will be sane.>",
      warmup: 20,
      slots: [
        {
          id: "crew",
          label: "<Hurt crew>",
          description: "<>",
          required: {
            "mariner.crew.trauma.superstitious": 1,
            "mariner.crew.trauma.shaken": 1,
          },
        },
      ],
      linked: [
        // No trait remaining, turn into lunatic

        // Only one trait remaining
        "mariner.performances.cleartrauma.eraseleft.only",
        "mariner.performances.cleartrauma.eraseright.only",
        // Both remaining, removing one at random
        { id: "mariner.performances.cleartrauma.eraseleft", chance: 50 },
        "mariner.performances.cleartrauma.eraseright",
      ],
      achievements: ["mariner.achievements.firstsuccessfulperformance"],
    },
    // Turn into lunatic
    {
      id: "mariner.performances.cleartrauma.turnintolunatic",
      requirements: { "mariner.crew.traits.erasedright": 1, "mariner.crew.traits.erasedleft": 1 },
      aspects: { [SIGNALS.TURN_INTO_LUNATIC]: 1 },
    },
    // Only one trait remaining
    {
      id: "mariner.performances.cleartrauma.eraseleft.only",
      requirements: { "mariner.crew.traits.erasedright": 1, "mariner.crew.traits.erasedleft": -1 },
      linked: "mariner.performances.cleartrauma.eraseleft",
    },
    {
      id: "mariner.performances.cleartrauma.eraseright.only",
      requirements: { "mariner.crew.traits.erasedright": -1, "mariner.crew.traits.erasedleft": 1 },
      linked: "mariner.performances.cleartrauma.eraseright",
    },
    // Both remaining, removing one
    {
      id: "mariner.performances.cleartrauma.eraseleft",
      requirements: { "mariner.crew.traits.erasedleft": -1 },
      aspects: { [SIGNALS.FORGET_LEFT_OPERAND]: 1 },
    },
    {
      id: "mariner.performances.cleartrauma.eraseright",
      requirements: { "mariner.crew.traits.erasedright": -1 },
      aspects: { [SIGNALS.FORGET_LEFT_OPERAND]: 1 },
    }
  );
  return "mariner.performances.cleartrauma";
}
