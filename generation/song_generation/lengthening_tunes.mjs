import { SIGNALS } from "../generate_signal_aspects.mjs";

const RECIPE_FILE = "recipes.tunes.makelong";

const aspectpairs = [
  "forgeheart",
  "grailedge",
  "heartgrail",
  "knocklantern",
  "winterlantern",
  "winterknock",
  "mothedge",
  "mothheart",
];

export function setLengtheningTunesRecipes() {
  mod.initializeRecipeFile(RECIPE_FILE, ["singing"]);
  for (const aspectPair of aspectpairs) {
    mod.setRecipe(RECIPE_FILE, {
      id: `mariner.tunes.lengthen.${aspectPair}`,
      label: "Memorize a Tune",
      actionid: "mariner.navigate",
      craftable: true,
      requirements: {
        [`mariner.tunes.${aspectPair}`]: 1,
        [`mariner.tunes.${aspectPair}.long`]: -1,
        "mariner.story": 1,
      },
      startdescription:
        "If I expend some of my creative resources, I could commit this tune to memory, and carry it with me elswhere, and into adventure.",
      description: "",
      warmup: 10,
      effects: {
        [`mariner.tunes.${aspectPair}`]: -1,
        [`mariner.tunes.${aspectPair}.long`]: 1,
      },
      aspects: { [SIGNALS.EXHAUST_STORY]: 1 },
    });
  }
}
