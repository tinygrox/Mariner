import { LORE_ASPECTS } from "../generation_helpers.mjs";
import { SONG_COMPOSITION_TEXTS } from "./composition_texts.mjs";
import { UPGRADE_TEXTS } from "./upgrade_texts.mjs";
import { SONG_TEXTS } from "./song_texts.mjs";
import { generateSongInfluences } from "./generate_influences.mjs";

const INSPIRATION_TYPES = ["nature", "muse", "melody", "location", "instrument", "experience", "urstory"];
/**
 *
 * @param mod the mod object
 * @param aspect the aspect of the song
 * @param tiers an array containing objects describing the song's tiers. They follow the format: {label: String, description: String}
 */
function generateSongElementsOfAspect(aspect) {
  const tiers = SONG_TEXTS[aspect];
  const songLoreAspectId = `mariner.song.${aspect}`;
  const songIntensityId = `mariner.song.intensity`;

  mod.setHiddenAspect("aspects.songs", {
    id: "mariner.inspiration.upgrade",
  });
  mod.setHiddenAspect("aspects.songs", {
    id: "mariner.inspiration.downgrade",
  });

  mod.setHiddenAspect("aspects.songs", {
    id: songLoreAspectId,
    label: `Song of ${mod.helpers.capitalize(aspect)}`,
  });

  // Song tiers
  mod.setElement("songs", {
    id: `mariner.song.${aspect}.composing`,
    label: `I am composing a ${mod.helpers.capitalize(aspect)} Song`,
    isHidden: true,
    aspects: { "mariner.song": 1, [aspect]: 1, [songLoreAspectId]: 1 },
    xtriggers: {
      "mariner.inspiration": `mariner.song.${aspect}.1`,
    },
    comments:
      "Used as a 'tier 0 song' to create the first tier through the upgrade recipes. This way, the one use design with inspirations still works as intended",
  });

  for (let i = 0; i < tiers.length; i++) {
    const tier = tiers[i];
    const obj = {
      id: `mariner.song.${aspect}.${i + 1}`,
      icon: `songs/mariner.song.${aspect}.${i + 1}`,
      label: tier.label,
      description: tier.description,
      aspects: {
        "mariner.song": 1,
        [songLoreAspectId]: 1,
        [songIntensityId]: i + 1,
        [aspect]: (i + 1) * 2,
      },
      slots: [
        {
          id: "crowd",
          label: "A Crowd",
          description: "A group, with all noses pointed the same way. In their eyes, music can become miracle.",
          actionId: "mariner.sing",
          required: {
            "mariner.crowd": 1,
          },
        },
      ],
    };
    if (i > 0) {
      obj.xtriggers = {
        ...obj.xtriggers,
        "mariner.inspiration.downgrade": `mariner.song.${aspect}.${i}`,
      };
    }
    if (i < tiers.length - 1) {
      obj.xtriggers = {
        ...obj.xtriggers,
        "mariner.inspiration.upgrade": `mariner.song.${aspect}.${i + 2}`,
      };
      obj.slots.push({
        id: "inspiration",
        label: "Inspiration",
        actionId: "mariner.navigate",
        required: {
          "mariner.inspiration": 1,
          ...Object.fromEntries(INSPIRATION_TYPES.map((type) => [`mariner.inspiration.${type}`, 1])),
        },
      });
    }
    mod.setElement("songs", obj);
  }
}

/**
 *
 *
 * Generates the upgrade recipe for the song of the given type and aspect, using influence type of the same aspect
 * @param type the type of inspiration
 * @param aspect the aspect of the song
 */
function generateSongUpgradeRecipeOfTypeAndAspect(type, aspect) {
  const songLoreAspectId = `mariner.song.${aspect}`;
  const upgradeTexts = UPGRADE_TEXTS[aspect][type];
  mod.setRecipe("recipes.upgradesong", {
    id: `mariner.upgradesongwith.${type}.${aspect}`,
    actionId: "mariner.navigate",
    craftable: true,
    label: `Upgrade ${aspect} song with ${mod.helpers.capitalize(type)} Inspiration`,
    startdescription: upgradeTexts.description,
    warmup: 60,
    // Song [aspect] + inspiration [type][aspect] + not already used
    requirements: {
      "mariner.song": 1,
      [songLoreAspectId]: 1,
      [aspect]: 1,
      [`mariner.inspiration.${type}`]: 1,
      [`mariner.song.inspirationused.${type}`]: -1,
      // "mariner.song.inspirationused.nature":-1 // Not needed because we use maxexecutions
    },
    effects: {
      "mariner.inspirationtype.consumable": -1,
      [`mariner.influences.${aspect}`]: 6,
    },
    aspects: {
      "mariner.inspiration.upgrade": 1,
    },
    mutations: [
      {
        filter: "mariner.song",
        mutate: `mariner.song.inspirationused.${type}`,
        level: 1,
      },
    ],
    achievements: [`mariner.achievements.firstused${type}inspiration`],
  });
}

/**
 * Generates the composition recipe for the song of the given aspect, using the right aspected tune and any influence of the same aspect.
 * @param mod the mod object
 * @param aspect the aspect of the tune/song
 * @param songLabel the name of the song that will be composed
 */
function generateSongCompositionRecipeOfAspect(aspect) {
  const compositionTexts = SONG_COMPOSITION_TEXTS[aspect];
  const songLoreAspectId = `mariner.song.${aspect}`;
  const inspirationLoreAspectId = `mariner.inspiration.${aspect}`;
  mod.setRecipe("recipes.composesong", {
    id: `mariner.composesong.${aspect}`,
    label: compositionTexts.label,
    actionId: "mariner.navigate",
    startdescription: compositionTexts.description,
    craftable: true,
    warmup: 60,
    effects: {
      "mariner.tune": -1,
      [`mariner.song.${aspect}.composing`]: 1,
    },
    extantreqs: {
      [songLoreAspectId]: -1,
    },
    requirements: {
      "mariner.tune": 1,
      "mariner.inspiration": 1,
      [inspirationLoreAspectId]: 1,
      [aspect]: 5,
    },
    linked: mod.recipes["recipes.upgradesong"]
      .filter((r) => r.requirements[aspect] !== undefined)
      .map((r) => ({ id: r.id, chance: 100 })),
  });
}

/**
 * Generates the singing recipes for the given lore aspect
 * @param mod
 * @param aspect
 */
function generateSingingRecipesOfAspect(loreAspect, startingTiersInfo) {
  const songLoreAspectId = `mariner.song.${loreAspect}`;
  const songIntensityId = `mariner.song.intensity`;
  const loreSingRecipesFile = `recipes.sing.${loreAspect}`;

  mod.initializeRecipeFile(loreSingRecipesFile, ["singing"]);
  // 1. Success recipe
  mod.setRecipe(loreSingRecipesFile, {
    id: `mariner.sing.success.${loreAspect}`,
    actionId: "mariner.sing",
    label: "A Glamour Woven",
    startdescription:
      "When a song is sung right, the world does not stay unchanged. The air has been infused with power, and it clings on me like a perfume.",
  });

  // 2. Generate starting recipes based on the song tier (desc)
  for (let i = startingTiersInfo.length; i--; i >= 0) {
    const startInfo = startingTiersInfo[i];
    mod.setRecipe(loreSingRecipesFile, {
      actionId: "mariner.sing",
      id: `mariner.sing.${loreAspect}.${i + 1}.start`,
      craftable: true,
      requirements: {
        "mariner.crowd": 1,
        [songLoreAspectId]: 1,
        [songIntensityId]: i + 1,
        "mariner.location.hostile": -1,
      },
      label: "A Special Song",
      startdescription: "The second note follows the first, and soon they become more than a sequence of sounds.",
      warmup: 30,
      effects: { [`mariner.influences.${loreAspect}`]: songIntensityId },
      linked: [
        { id: `mariner.sing.success.${loreAspect}`, chance: (i + 2) * 10 }, // for tier 1, 20%, 2, 30%, etc.
        { id: "mariner.sing.failure" },
      ],
    });
  }
}

/**
 * Generates all the song-singing system
 * @param mod
 */
function generateSingingRecipes() {
  // Common failure recipe
  mod.initializeRecipeFile("recipes.sing", ["singing"]);
  mod.setRecipe("recipes.sing", {
    id: "mariner.sing.failure",
    actionId: "mariner.sing",
    label: "The Crowd Turns",
    description:
      "At first it is slight. People shift on their feet. They eye me, than eachother. They don't know how, but they know they are being duped somehow. Soon the crowd will move against me, and inevitably, the town will follow. They sense the influence I summoned over it.",
    effects: { "mariner.notoriety": 1 },
  });

  generateSingingRecipesOfAspect("heart", [{}, {}, {}]);
  generateSingingRecipesOfAspect("grail", [{}, {}, {}]);
  generateSingingRecipesOfAspect("forge", [{}, {}, {}]);
  generateSingingRecipesOfAspect("winter", [{}, {}, {}]);
  generateSingingRecipesOfAspect("edge", [{}, {}, {}]);
  generateSingingRecipesOfAspect("moth", [{}, {}, {}]);
  generateSingingRecipesOfAspect("lantern", [{}, {}, {}]);
  generateSingingRecipesOfAspect("knock", [{}, {}, {}]);
}

/*
Generate all songs for the mod
*/
export function generateSongs() {
  mod.initializeAspectFile("aspects.inspirations", ["inspirations"]);
  mod.readElementsFile("tunes", ["singing"]);
  mod.readRecipesFile("recipes.composesong", ["singing"]);
  mod.initializeAspectFile("aspects.songs", ["singing"]);
  mod.initializeRecipeFile("recipes.upgradesong", ["singing"]);

  mod.setHiddenAspect("aspects.inspirations", {
    id: "mariner.inspiration",
    label: "Inspiration",
  });

  mod.setAspect("aspects.songs", {
    id: "mariner.song.intensity",
    label: "Intensity of a Song",
    ishidden: true,
  });
  mod.setHiddenAspect("aspects.songs", {
    id: "mariner.inspirationtype.consumable",
  });
  mod.setHiddenAspect("aspects.songs", {
    id: "mariner.inspirationtype.nonconsumable",
  });

  // Hint already used nature inspiration
  mod.setRecipe("recipes.upgradesong", {
    id: `mariner.upgradesongwith.nature.alreadyused`,
    actionId: "mariner.navigate",
    label: "Using the same inspiration again?",
    startdescription:
      "The soul is not sustained on the familiar. I cannot mine the same source twice. My music might become trite, or even worse, predictable.",
    hintOnly: true,
    requirements: {
      "mariner.song": 1,
      "mariner.inspiration.nature": 1,
      "mariner.song.inspirationused.nature": 1,
    },
  });

  // Generate elements for all song tiers and all aspects
  mod.initializeElementFile("songs", ["singing"]);
  for (let aspect of LORE_ASPECTS) generateSongElementsOfAspect(aspect);

  // For each type, and each aspect, generate the recipes to upgrade them using the right type of influence of the right aspect
  for (let type of INSPIRATION_TYPES) {
    for (let aspect of LORE_ASPECTS) {
      generateSongUpgradeRecipeOfTypeAndAspect(type, aspect);
    }
  }

  // For each aspect, generate the recipes to compose them using the right tune and any influence of the right aspect
  for (let aspect of LORE_ASPECTS) generateSongCompositionRecipeOfAspect(aspect);

  mod.setRecipe("recipes.composesong", {
    id: "mariner.composesong.hint",
    label: "Compose a Song?",
    actionId: "mariner.navigate",
    startdescription:
      "Do I wish to Compose a Song? If I supply this tune with an Inspiration that matches in Aspect, I can comepose a Motifs charged with that particular quintessence.",
    craftable: false,
    hintonly: true,
    requirements: {
      "mariner.tune": 1,
    },
  });

  generateSingingRecipes();
  generateSongInfluences();
}
