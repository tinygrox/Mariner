export const SONG_COMPOSITION_TEXTS = {
  knock: {
    label: "Compose an Intuitive Song",
    description:
      "I could compose a song based on those painful revelations that are felt deeply but rarely understood.",
    descriptionUpgradeEnd:
      "The chords do not come easy. But after the right cut has been made in the right place, the music flows unhindered from some deeper place in me.",
  },
  moth: {
    label: "Compose a Song of Folly",
    description:
      "I could compose based on the patterns obscured beneath the chaotic and beyond the strange.",
    descriptionUpgradeEnd:
      "I writhe in my skin. My discomfort is only lessened when finally I press these words passed my lips.",
  },
  lantern: {
    label: "Compose a Brilliant Song",
    description:
      "I could compose a song based on the glimpes of brighter times and greater truths.",
    descriptionUpgradeEnd:
      "I stare until the vision of my inspiration has been burned into my retina. Now I close my eyes, and recite.",
  },
  forge: {
    label: "Compose a Fiery Song",
    description:
      "I could compose a song based on the moment of heat and force that destroys the old for the new.",
    descriptionUpgradeEnd:
      "I sing until my tongue is parched and my teeth spark like tinder. When I pen my music down, I fear the sheet may ignite.",
  },
  edge: {
    label: "Compose a Furious Song",
    description:
      "I could compose a song based on dangerous challenges, and the will to conquer them.",
    descriptionUpgradeEnd:
      "I am seeing red, and gnash my teeth to furious splinters. The crew avoids me until I have  penned down the final note.",
  },
  winter: {
    label: "Compose a Morose Song",
    description:
      "I could compose a song based on that quiet mourning one does when they cannot feel the sun.",
    descriptionUpgradeEnd:
      "I must be still and feel, until I am unsure if I have moved in hours. Only then I am ready to perpetrate the crimes of motion, sound and speach.",
  },
  heart: {
    label: "Compose a Joyous Song",
    description:
      "I could compose a song based on all that moves and all that gets us moving.",
    descriptionUpgradeEnd:
      "Restlessly I pace I pace, until the drumbeat of my heart has aligned with the rhytm of my words",
  },
  grail: {
    label: "Compose a Seductive Song",
    description:
      "I could compose a song based on the feelings that one is not sure they should permit themselves to feel.",
    descriptionUpgradeEnd:
      "While composing, I will abstain from food and drink and other pleasures, heightening all longing. When the taste of my own tongue turns irresistable, then I know I will have found the proper tone.",
  },
};
