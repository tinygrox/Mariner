export const MOON_INFLUENCE = (id) => `mariner.mooninfluence.${id}`;

const INFLUENCE_FORMULA = (aspect, bonustrait, malustrait) =>
  `([~/exterior : ${MOON_INFLUENCE(
    aspect
  )}] * 10) + ([~/exterior : mariner.influences.${aspect}] * 10) + ([~/exterior:mariner.crew.traits.bright] * 5) - ([~/exterior:mariner.crew.traits.dense] * 5)`;

const influenceFormula = {
  lantern:
    "([~/exterior : mariner.mooninfluence.lantern] * 10) + ([~/exterior : mariner.influences.lantern] * 10) + ([~/exterior:mariner.crew.traits.bright] * 5) - ([~/exterior:mariner.crew.traits.dense] * 5)",
  edge: "([~/exterior : mariner.mooninfluence.edge] * 10) + ([~/exterior:mariner.influences.edge] * 10) + ([~/exterior:mariner.crew.traits.engaging] * 5) - ([~/exterior:mariner.crew.traits.cowardly] * 5)",
  heart:
    "([~/exterior : mariner.mooninfluence.heart] * 10) + ([~/exterior:mariner.crew.traits.entertaining] * 5) - ([~/exterior:mariner.crew.traits.rambunctious] * 5) + ([~/exterior:mariner.influences.heart] * 10)",
  grail:
    "([~/exterior : mariner.mooninfluence.grail] * 10) + ([~/exterior:mariner.crew.traits.charming] * 5) - ([~/exterior:mariner.crew.traits.unpleasant] * 5) + ([~/exterior:mariner.influences.grail] * 10)",
  forge:
    "([~/exterior : mariner.mooninfluence.forge] * 10) + ([~/exterior:mariner.influences.forge] * 10) + ([~/exterior:mariner.crew.traits.handy] * 5) - ([~/exterior:mariner.crew.traits.clumsy] * 5)",
  moth: "([~/exterior : mariner.mooninfluence.moth] * 10) + ([~/exterior:mariner.crew.traits.shifty] * 5) - ([~/exterior:mariner.crew.traits.crude] * 5) + ([~/exterior:mariner.influences.moth] * 10)",
  knock:
    "([~/exterior : mariner.mooninfluence.knock] * 10) + ([~/exterior : mariner.crew.traits.whistling] * 5) - ([~/exterior : mariner.crew.traits.noisy] * 5) + ([~/exterior : mariner.influences.knock] * 10)",
  winter:
    "([~/exterior : mariner.mooninfluence.winter] * 10) + ([~/exterior:mariner.crew.traits.goodfortuned] * 5) - ([~/exterior:mariner.crew.traits.unlucky] * 5) + ([~/exterior:mariner.influences.winter] * 10)",
};

export const INFLUENCE_CHANCE = (lore) => influenceFormula[lore];

function generateSongInfluence({ aspect, label, description, icon, lifetime, isLocal = true }) {
  mod.setElement("influences", {
    id: `mariner.influences.${aspect}`,
    label,
    description,
    aspects: {
      "mariner.influence": 1,
      influence: 1,
      [aspect]: 1,
      ...(isLocal ? { "mariner.local": 1 } : {}),
    },
    lifetime,
    icon: icon ? icon : `influence${aspect}c`,
  });

  mod.setAspect("aspects.mooninfluences", {
    id: MOON_INFLUENCE(aspect),
    label: `<${mod.helpers.capitalize(aspect)} Moon Influence>`,
    description: "<>",
  });
}

export function generateSongInfluences() {
  mod.initializeAspectFile("aspects.mooninfluences", ["singing"]);
  mod.initializeElementFile("influences", ["singing"]);

  for (const influenceData of INFLUENCES) generateSongInfluence(influenceData);
}

const INFLUENCES = [
  {
    aspect: "heart",
    label: "A Joyous Vibration",
    description:
      "A peal of laugther, a pulse of energy, a bubbling charm. [Gathering this influence around you can protect you from the ravages of Heartache]",
  },
  {
    aspect: "grail",
    label: "A Persisting Glamour",
    description:
      "Trailed by whispers, accentuated by a gasp for breath. [Having gathered this Influence around you will aid you in finding Passengers for the ship]",
  },
  {
    aspect: "forge",
    label: "Echoes Of Action",
    description:
      "Even a cooling engine still shivers and thrums. [Having gathered this Influence around you will aid you when the Kite is wounded]",
  },
  {
    aspect: "lantern",
    label: "A Clarion Air",
    description:
      "A thoughtful hum, considered words, awed silence. [Having gathered this Influence around you will aid you in attracting clever crewmembers]",
  },
  {
    aspect: "moth",
    label: "An Edge Of Static",
    description:
      "Just beyond perception, wings beat within the skull. [Having gathered this Influence around you will aid you once Authorities have their eyes on you]",
  },
  {
    aspect: "winter",
    label: "A Deafening Repose",
    description:
      "I carry a silence that others hesitate to break. [Having gathered this Influence around you will aid you in having an uneventful voyage once you set sail]",
    lifetime: 1600,
    isLocal: false,
  },
  {
    aspect: "edge",
    label: "A Lingering Thrill",
    description:
      "If a challenge is not accepted, your domination is. [Having gathered this Influence around you will aid you attracting an adventurous crew]",
  },
  {
    aspect: "knock",
    label: "A Ring Of Insight",
    description:
      "Lies flounder, voices waver, even silence is a door to truth. [Having gathered this Influence around you will aid you collecting Packages when exploring the streets with an open heart]",
  },
];
