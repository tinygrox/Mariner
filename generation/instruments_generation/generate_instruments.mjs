import { trappingAspects } from "../generate_trappings.mjs";
import { LORE_ASPECTS } from "../generation_helpers.mjs";
import { BUY_INSTRUMENT_TEXTS } from "./buyinstrument_texts.mjs";
import { PLAIN_INSTRUMENT_TEXTS } from "./instrument_texts.mjs";

function generatePlainInstrument(loreAspect) {
  const texts = PLAIN_INSTRUMENT_TEXTS[loreAspect];
  mod.setElement("instruments.plain", {
    id: `mariner.instrument.plain.${loreAspect}`,
    label: texts.label,
    description: texts.description,
    aspects: {
      "mariner.instrument": 1,
      ...trappingAspects({
        value: 2,
        types: ["crafts"],
        aspects: { [loreAspect]: 1 },
      }),
    },
  });
}

function generatePlainInstruments() {
  for (const loreAspect of LORE_ASPECTS) {
    generatePlainInstrument(loreAspect);
  }
}

function generateBuyingInstrumentAction() {
  mod.initializeRecipeFile("recipes.buyinstrument", ["instruments"]);
  mod.setRecipe("recipes.buyinstrument", {
    id: "mariner.buyinstrument.hintnospace",
    hintOnly: true,
    actionId: "explore",
    label: "Buy An Instrument?",
    startdescription:
      '"My apologies... But if you do not have a space set up for this practice, I cannot sell you our wares. Attuned Instruments require special practice, and we take to much pride to let our work be sullied." [Keep dedicated space in the hold of your ship empty to buy another Instrument.] "',
    requirements: { "mariner.instrumentshop": 1 },
    grandReqs: {
      "[~/extant:mariner.holduse] + max(0, [~/extant:mariner.instrument]-1)": "[~/extant:mariner.ship.holdsize]",
    },
  });
  mod.setRecipe("recipes.buyinstrument", {
    id: "mariner.buyinstrument.hint",
    hintOnly: true,
    actionId: "explore",
    grandReqs: {
      "[~/extant:mariner.holduse] + max(0, [~/extant:mariner.instrument]-1)": "-[~/extant:mariner.ship.holdsize]",
    },
    label: "Buy an Attuned Instrument from Baruch ibn Yahya",
    startdescription:
      '"Let me know what holds your interest, and I shall do my best to supply." [Indicate which Aspect you would like to buy an Instrument for with that Song.]',
    requirements: { "mariner.instrumentshop": 1, "mariner.song": -1 },
  });
  for (const lore of LORE_ASPECTS) {
    const texts = BUY_INSTRUMENT_TEXTS[lore];
    mod.setRecipe("recipes.buyinstrument", {
      id: `mariner.buyinstrument.${lore}`,
      actionId: "explore",
      craftable: true,
      grandReqs: {
        "[~/extant:mariner.holduse] + max(0, [~/extant:mariner.instrument]-1)": "-[~/extant:mariner.ship.holdsize]",
      },
      requirements: {
        "mariner.instrumentshop": 1,
        [`mariner.song.${lore}`]: 1,
      },
      label: `Buy a ${mod.helpers.capitalize(lore)} Instrument`,
      startdescription: texts.startdescription,
      description: texts.description,
      warmup: 30,
      effects: {
        [`mariner.instrument.plain.${lore}`]: 1,
      },
      achievements: ["mariner.achievements.firstinstrument"],
    });
  }
}

export function generateInstrumentsContent() {
  mod.initializeElementFile("instruments.plain", ["instruments"]);
  mod.initializeAspectFile("aspects.instruments", ["instruments"]);
  mod.setAspect("aspects.instruments", {
    id: "mariner.instrument",
    label: "Instrument",
    description:
      'This isn\'t a simple tool, nor is it just a craft. It commands the air and conjures sound. [The first instrument can be stored for free, "as part of your personal space on board". Any additional instrument occupies one hold space.]',
  });
  mod.setHiddenAspect("aspects.instruments", {
    id: "mariner.instrument.plain",
  });
  mod.setHiddenAspect("aspects.instruments", { id: "mariner.instrumentshop" });
  // We don't generate the mariner.inspiration.instrument here ("inspired instrument") because it's already handwritten

  generatePlainInstruments();
  generateBuyingInstrumentAction();
}
