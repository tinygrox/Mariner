import { SIGNALS } from "./generate_signal_aspects.mjs";

export function generateSavingSystem() {
  /*
    - take moon influences into account everywhere
  */
  mod.initializeRecipeFile("recipes.saving", ["saving"]);
  mod.setRecipes(
    "recipes.saving",
    {
      id: "mariner.saving",
      actionId: "mariner.sing",
      hintOnly: true,
      grandReqs: {
        "mariner.atsea": -1,
        "[~/exterior : {[mariner.vault] = 0} : mariner.docked]": 1,
        "mariner.moon": 1,
        "[mariner.story] < (1 + [~/extant : aspectmax/desire])": 1,
      },
      label: "<Saving: More Stories Needed>",
      startdescription: "<I need to tell more stories to the moon to listen>",
    },
    {
      id: "mariner.saving",
      actionId: "mariner.sing",
      hintOnly: true,
      grandReqs: {
        "mariner.atsea": -1,
        "[~/exterior : {[mariner.vault] = 0} : mariner.docked]": 1,
        "mariner.moon": 1,
        "mariner.story": -1,
      },
      label: "<Saving ?>",
      startdescription: "<Tell stories to the moon to save>",
    },
    {
      id: "mariner.saving",
      actionId: "mariner.sing",
      craftable: true,
      grandReqs: {
        "mariner.atsea": -1,
        "[~/exterior : {[mariner.vault] = 0} : mariner.docked]": 1,
        "mariner.moon": 1,
        "mariner.story": "1 + [~/extant : aspectmax/desire]",
      },
      warmup: 10,
      label: "<Saving>",
      startdescription: "<>",
      description: "<>",
      furthermore: [
        { aspects: { [SIGNALS.CLEAR_MOON_BONUSES]: 1 } },
        { aspects: { [SIGNALS.APPLY_MOON_BONUS_ASPECTS]: 1 } },
        { aspects: { [SIGNALS.INSTALL_MOON_BONUSES]: 1, [SIGNALS.EXHAUST_STORY]: 1 } },
      ],
      saveCheckpoint: true,
    }
  );
}
