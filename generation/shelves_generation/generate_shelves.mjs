export function generateGeneralShelves() {
  mod.initializeShelfFile("shelves", ["shelves"]);
  mod.setShelf("shelves", {
    id: "mariner.shelves.crew",
    category: "other",
    name: "[Mariner] Crew",
    rows: 2,
    columns: 6,
    expression: "mariner.crew",
    background: "mariner.shelves.crew",
    style: "aligncenter",
    areas: [
      {
        id: "crew_area",
        rows: 2,
        columns: 6,
      },
    ],
  });

  mod.setShelf("shelves", {
    id: "mariner.shelves.docked",
    category: "other",
    name: "[Mariner] Docked port",
    columns: 6,
    rows: 2,
    background: "mariner.shelves.docked",
    style: "aligncenter",
    areas: [
      {
        id: "docked_port",
        expression: "[mariner.docked]",
        columns: 1,
      },
      {
        id: "local_elements",
        expression: "mariner.local",
        background: "mariner.shelves.docked",
        x: 2,
        columns: 5,
        rows: 2,
      },
    ],
  });

  singleCardShelf("shelves", "Wanderlust", "mariner.wanderlust");
  singleCardShelf("shelves", "Thrill", "mariner.thrill");
  singleCardShelf("shelves", "Weather", "mariner.weather");
  singleCardShelf("shelves", "Moon", "mariner.moon");
  singleCardShelf("shelves", "The Kite", "mariner.ship", "ship");
}

export function singleCardShelf(fileId, label, expression, id) {
  return;
  id = id ?? label.toLowerCase();
  generateShelf(fileId, {
    id: `mariner.shelves.${id}`,
    category: "other",
    name: `[Mariner] ${label}`,
    background: `mariner.shelves.${id}`,
    style: "aligncenter",
    areas: [
      {
        id,
        expression,
      },
    ],
  });
}

export function generateShelf(fileId, shelf) {
  if (!mod.shelves[fileId]) mod.initializeShelfFile(fileId, ["shelves"]);
  mod.setShelf(fileId, shelf);
}
