import {
  buildSequentialCodexEntry,
  generateCodexEntry,
} from "./codex_generation.mjs";

export function generateHourCodexEntries() {
  generateCodexEntry(
    "codex.hours",
    "The Hours",
    "The Sailors call them the Fortunes. The gnostics call them the Demiurges. The scholars call them the Hours. They're the rumour behind the waves, the refraction of solar rays, the voices conducting occult prayers. The sentient forces of nature that might actually be the hidden rulers of our world, some whisper late at night."
  );

  generateCodexEntry(
    "codex.hours.vagabond",
    "The Centipede",
    "I've met Morland. My head spins from what she told me-\n\nShe told me of one the Hours: the Centipede. She's said to be a Vagabond, barred from “The House”, where They Dwell. Everywhere else, she's been. Morland thinks following her Example could actually be a way out of this muted world and into some “Truer” place.",
    "codex.hours"
  );

  generateCodexEntry(
    "codex.hours.twins",
    '"The Twins"',
    "Agdistis told me I had a split soul, and it reminded me of a fable from lake Fucino.\n\nA pair who was born incomplete, each carrying half of the same soul. They are said to have pulled on eachother like the ocean on the moon, to tug at eachother like the waves at the shore. They sought to unite, even before they ever knew the other's name.",
    "codex.hours"
  );

  generateCodexEntry(
    "codex.hours.ringyew",
    "The Ring-Yew",
    buildSequentialCodexEntry([
      "Among sailors, there are few rules as universal as the absolute prohibition to use yew wood to build the cabins of a ship, lest they be brought to dark, moonlight-dappled woods in their dreams. It is said to anger the “Ring-Yew”, sometimes called “Queen of the Wood” or “Malachite”. She is the Tree and The Hive, the Honey and the Sting.",
      {
        "codex.hours.ringyew.1":
          "The Queen in the Woods loved deeply in the story of the Pine Knight, but I don't think the Malachite is a being of love, not like mortals know it.\n\nIn the end she chose to sacrifice her lover, complying with Red Desire and Cold Revenge. I wonder if she regretted it...",
      },
    ]),
    "codex.hours"
  );
}
