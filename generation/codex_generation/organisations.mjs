import {
    buildCodexEntryBlock,
    buildSequentialCodexEntry,
    generateCodexEntry,
} from "./codex_generation.mjs";

export function generateOrganisationCodexEntries() {
    generateCodexEntry("codex.organisations", "My organisations", "<>");
    generateCodexEntry(
        "codex.organisations.lethian",
        "<House of Lethe>",
        "all throughout the ancient cities of the Levant and the verdant crescent, a closed eye symbol shows up in half forgotten places. This sign ones heralded the house of Lethe, of whose scholarly and mystical pursuits little more is left then dust and whispers.",
        "codex.organisations"
    );
    generateCodexEntry(
        "codex.persons.galantha",
        "<Ordo Limiae>",
        "Before the Obliviates were founded, but after the last Lethian house was filled with sand and dust, the Ordo Limiea was molded from the clay of the Iberian mud. As with all forms of order, this too crumbled.",
        "codex.organisations"
    );
    generateCodexEntry(
        "codex.organisations.obliviate",
        "<The Obliviates can trace a distinguished lineage amongst the occult organisations, at least they could, if they where inclined to speak on such matters, and they found anyone inclined to hear. They seem to be a reclusive group, keeping to the kind shadows. though some members have been known, from time to time, to lend their wealth, expertese or influence to affect the affairs of the invisible world.>",
        "",
        "codex.organisations"
    );
    generateCodexEntry(
        "codex.organisations.sisterhood",
        "<The Sisterhood of the Knot>",
        "<It has been said that a man cannot serve two masters, but the Sisters of the Knot venerated three. Or perhaps they merely respected them, but venerated their union, and their unity. As trinities go, Maiden, Mother, Crone has it merits, though I do not know know the details of any of the three.>",
        "codex.organisations"
    );
    generateCodexEntry(
        "codex.organisations.solarchurch",
        "<>",
        "<>",
        "codex.organisations"
    );
}
