import { generateHourCodexEntries } from "./hours.mjs";
import { generateMyNatureCodexEntries } from "./nature.mjs";
import { generateTwinsAscensionCodexEntries } from "./twins_ascension.mjs";
import { generateVagabondAscensionCodexEntries } from "./vagabond_ascension.mjs";
import { generatePerformancesCodexEntries } from "./performances.mjs";

export function buildCodexEntryBlock(aspectsAndTexts, defaultText) {
  if (!mod.aspects["aspects.codex"])
    mod.initializeAspectFile("aspects.codex", ["codex"]);
  for (const aspect of Object.keys(aspectsAndTexts)) {
    mod.setAspect("aspects.codex", { id: aspect });
  }

  return (
    "@#" +
    Object.entries(aspectsAndTexts)
      .map(([aspect, text]) => `${aspect}|${text}`)
      .join("#") +
    ((defaultText && `#|${defaultText}`) || "") +
    "@"
  );
}

export function buildSequentialCodexEntry(blocks) {
  return blocks
    .map((b) => {
      if (typeof b === "object") return buildCodexEntryBlock(b);
      else return b;
    })
    .join("");
}

export function generateCodexEntry(entryId, label, description, codexCategory) {
  mod.setElement("codex.entries", {
    id: entryId,
    label,
    description,
    codexCategory,
  });
}

export function generateGeneralCodexEntries() {
  mod.initializeElementFile("codex.entries", ["codex"]);

  generateHourCodexEntries();
  generateMyNatureCodexEntries();
  generateTwinsAscensionCodexEntries();
  generateVagabondAscensionCodexEntries();
  generatePerformancesCodexEntries();
}
