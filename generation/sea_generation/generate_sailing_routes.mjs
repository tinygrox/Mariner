import { SIGNALS } from "../generate_signal_aspects.mjs";
import { buildRefinementBlock, ME_LINK, SET_SEA_TRACKLIST } from "../generation_helpers.mjs";
import { transformXTrigger } from "../malleary.mjs";

// Explicitely not called "SCALED_DISTANCE" because DM will prefer finding it by typing "DISTANCE"
// instead of having to remember that it starts with "SCALED".
export const DISTANCE_SCALED = (distance) => Math.floor(distance * 30);

function oppositeDirection(direction) {
  if (direction === "north") return "south";
  if (direction === "south") return "north";
  if (direction === "east") return "west";
  return "east";
}
function waypoint(fromSea, direction) {
  return `${direction}.in.${fromSea}`;
}

function route({ distance, destinationIsWaypoint }) {
  return {
    distance,
    destinationIsWaypoint: destinationIsWaypoint ?? false,
  };
}

function basicRoute(customDistance) {
  return route({ distance: customDistance });
}

const seaSizes = {
  northsea: DISTANCE_SCALED(3),
  medsea: DISTANCE_SCALED(4),
  atlanticsea: DISTANCE_SCALED(5),
  indiansea: DISTANCE_SCALED(4),
};

// In this iteration of the sailing system, this structure stores which sea is connected to which and the associated waypoint informations
export const seaMesh = {
  northsea: {
    medsea: {
      direction: "south",
      label: "To the Med. Sea through Gibraltar",
    },
  },
  medsea: {
    northsea: {
      direction: "north",
      label: "To the North Sea through Gibraltar",
    },
  },
};

const customRoutes = {
  copenhagen: {
    london: basicRoute(DISTANCE_SCALED(4)),
  },
  // coming from the southern northsea waypoint (med sea)
  [waypoint("northsea", "south")]: {
    london: basicRoute(DISTANCE_SCALED(2)),
  },
  // coming from the northern med sea waypoint (north sea)
  [waypoint("medsea", "north")]: {
    bizerta: basicRoute(DISTANCE_SCALED(1)),
    marseille: basicRoute(DISTANCE_SCALED(1)),
  },
};

function generateWaypoints(seaId) {
  mod.setHiddenAspect("aspects.waypoints", {
    id: `mariner.waypoint.leadsto.${seaId}`,
  });

  mod.setRecipe("recipes.sailing.changeseas", {
    id: `mariner.sailing.changesea.to.${seaId}`,
    requirements: {
      "mariner.waypoint": 1,
      [`mariner.waypoint.leadsto.${seaId}`]: 1,
    },
    label: `<Switching seas, direction: ${seaId}>`,
    warmup: 20,
    // Switch waypoints, then lock currently unlocked stuff, then unlock new sea stuff
    furthermore: [
      // reset waypoints
      {
        aspects: { [SIGNALS.MARK_COMING_FROM_WAYPOINT]: 1 },
      },
      {
        movements: { "~/tabletop": ["mariner.waypoint"] },
      },
      {
        target: "~/exterior",
        aspects: { [SIGNALS.RESET_WAYPOINTS]: 1 },
      },
      // Lock all previously available destinations
      {
        target: "~/exterior",
        aspects: {
          [`mariner.signal.lockport`]: 1,
        },
      },
      // Unlock the things from the new sea
      {
        target: "~/exterior",
        aspects: {
          [`mariner.signal.arriveinsea.${seaId}`]: 1,
        },
      },
      // Display the new waypoints
      {
        target: "~/exterior",
        aspects: { [`mariner.signal.displaywaypoints.${seaId}`]: 1 },
      },
    ],
    linked: [{ id: "mariner.sailing.leaving.start" }],
  });

  // Generate existing waypoints and assign xtriggers to the default waypoints
  mod.setHiddenAspect("aspects.signals", {
    id: `mariner.signal.displaywaypoints.${seaId}`,
  });

  let destinations = [];
  const waypointDefinitions = seaMesh[seaId] ?? {};
  for (const [destinationSeaId, waypointDefinition] of Object.entries(waypointDefinitions)) {
    mod.setElement("waypoints", {
      id: `mariner.waypoint.from.${seaId}.to.${destinationSeaId}`,
      label: waypointDefinition.label,
      aspects: {
        [`mariner.waypoint.leadsto.${destinationSeaId}`]: 1,
        [`mariner.locations.${seaId}.sailingthere`]: 1,
        "mariner.destination": 1,
        "mariner.waypoint": 1,
        [`mariner.waypoint.${waypointDefinition.direction}`]: 1,
      },
      // decayTo: `mariner.waypoint.${waypointDefinition.direction}.none`,
      xtriggers: {
        [SIGNALS.RESET_WAYPOINTS]: [transformXTrigger(`mariner.waypoint.${waypointDefinition.direction}.none`)],
        // TODO: fix: the recipe xtrigger requires an id, grandeffects morpheffect doesn't exist anymore
        [SIGNALS.MARK_COMING_FROM_WAYPOINT]: {
          morpheffect: "recipe",
          recipe: {
            target: "~/exterior",
            mutations: [
              {
                filter: `mariner.waypoint.${oppositeDirection(waypointDefinition.direction)}`,
                mutate: "mariner.waypoint.comingfrom",
                level: 1,
                additive: false,
              },
            ],
          },
        },
        [SIGNALS.ARRIVE_TO_DESTINATION]: ME_LINK(`mariner.sailing.changesea.to.${destinationSeaId}`),
      },
    });

    mod.getElement("waypoints", `mariner.waypoint.${waypointDefinition.direction}.none`).xtriggers[
      `mariner.signal.displaywaypoints.${seaId}`
    ] = `mariner.waypoint.from.${seaId}.to.${destinationSeaId}`;

    destinations.push({
      id: `mariner.waypoint.from.${seaId}.to.${destinationSeaId}`,
      region: seaId,
      isWaypoint: true,
      direction: waypointDefinition.direction,
      port: waypoint(seaId, waypointDefinition.direction), // this isn't actually a port, of course. It's also not a destination's shorthand like "london". But it fits what's in the customRoutes object
    });

    return destinations;
  }
}

function generateFromWaypointToDestinationRoute(originLocation, destination) {
  const fileId = `mariner.sailing.routes.${originLocation.region}`;
  if (!mod.recipes[fileId]) mod.initializeRecipeFile(fileId, ["sailing", "routes"]);
  // "originLocation.port" here is actually the waypoint element id.
  const customData = customRoutes?.[originLocation.port]?.[destination.port];
  if (!customData) return;

  const distance = customData.distance;
  // const direction = customData.originLocation.direction;
  // const comingFrom = oppositeDirection(direction);
  // console.log(
  //   "Custom [from coming-in Waypoint]",
  //   originLocation.port,
  //   "to",
  //   destination.port,
  //   distance
  // );

  // The recipe to generate is actually a "set course" recipe, because going "from waypoint to destination" happens from a context of sailing the open sea.
  mod.setRecipe(fileId, {
    id: `mariner.sailing.setcourse.from.waypoint.${originLocation.port}.to.${destination.port}`,
    warmup: 10,
    label: `<Setting course from around the waypoint ${originLocation.port} sailing towards ${destination.port} in the ${originLocation.region}>`,
    startdescription: `<Setting course from around the waypoint ${originLocation.port} sailing towards <generic>. Distance: ${distance} units.>`,
    grandReqs: {
      "mariner.ship": 1,
      "mariner.destination": 1,
      [`${destination.id}.away`]: 1,
      [`[~/exterior : {[mariner.waypoint.comingfrom] = 1} : ${originLocation.id}]`]: 1,
    },
    effects: {
      "mariner.sailing.totaldistance": distance,
    },
    linked: [{ id: "mariner.sailing.leaving.start" }],
  });
}

function generateFromPortToDestinationRoute(originLocation, destination) {
  const customData =
    customRoutes?.[originLocation.port]?.[destination.port] ?? customRoutes?.[destination.port]?.[originLocation.port];
  if (!customData) return;

  const distance = customData?.distance;

  // console.log(
  //   "Custom [from port to destination]",
  //   originLocation.port,
  //   "to",
  //   destination.port,
  //   distance
  // );

  // The recipe itself
  const fileId = `mariner.sailing.routes.${originLocation.region}`;
  if (!mod.recipes[fileId]) mod.initializeRecipeFile(fileId, ["sailing", "routes"]);

  mod.setRecipe(fileId, {
    id: `mariner.sailing.leave.${originLocation.port}.to.${destination.port}`,
    label: `<Leaving ${originLocation.port} and sailing towards ${destination.port}>`,
    startdescription: `<Leaving and sailing towards ${destination.port}. Distance: ${distance} units.>`,
    actionId: "mariner.sail",
    craftable: true,
    requirements: {
      "mariner.ship": 1,
      [destination.isWaypoint ? destination.id : `${destination.id}.away`]: 1,
    },
    tablereqs: {
      [`${originLocation.id}`]: 1,
    },
    grandReqs: {
      "~/tabletop:{verb/explore}:recipe/*": -1,
      "~/tabletop:{verb/talk}:recipe/*": -1,
      "~/tabletop:{verb/mariner.sing}:recipe/*": -1,
      "~/tabletop:{verb/mariner.navigate}:recipe/*": -1,
    },
    // furthermore: computeTravelSequence(path),
    effects: {
      "mariner.sailing.totaldistance": distance,
    },
    linked: [{ id: "mariner.sailing.leaving.start" }],
    ...SET_SEA_TRACKLIST,
  });
}

function generateCustomSailingRoutes(fileId, destinations) {
  for (const originLocation of destinations) {
    for (const destination of destinations) {
      if (originLocation === destination) continue;

      if (originLocation.isWaypoint) {
        generateFromWaypointToDestinationRoute(originLocation, destination);
      } else {
        generateFromPortToDestinationRoute(originLocation, destination);
      }
    }
  }
}

export function generateSailingRoutes() {
  const allSeas = [].concat.apply(
    [],
    mod.decks["decks.seas"].filter((deck) => deck.id.includes(".discoverable")).map((d) => d.id.split(".")[3])
  );

  mod.initializeRecipeFile("recipes.sailing.changeseas", ["sailing"]);

  mod.setAspect("aspects.waypoints", {
    id: "mariner.waypoint.comingfrom",
    label: "Coming from here",
    description: "Coming from here",
  });

  // For each sea
  for (const seaId of allSeas) {
    const fileId = `mariner.sailing.routes.${seaId}`;
    if (!mod.recipes[fileId]) mod.initializeRecipeFile(fileId, ["sailing", "routes"]);

    let destinations = generateWaypoints(seaId);

    // Find all in-sea destinations + open sea, and generate all intrasea routes
    destinations = [
      ...destinations,
      ...mod.getDeck("decks.seas", `mariner.decks.locations.${seaId}.alldestinations`).spec.map((id) => ({
        id: id,
        region: seaId,
        port: id.split(".locations.")[1].split(".")[1],
      })),
    ];

    // console.log(seaId, "destinations:", destinations);

    // RECIPES
    // 1. CUSTOM SAILING FROM PORT TO PORT DIRECTLY
    generateCustomSailingRoutes(fileId, destinations);

    // 2. GENERIC DEFAULT DISTANCE RECIPE
    // Default leave from port
    mod.setRecipe(fileId, {
      id: `mariner.sailing.leave.default.${seaId}`,
      label: `<Leaving and sailing towards ${buildRefinementBlock(
        {
          [`mariner.locations.northsea.amsterdam.away`]: "Amsterdam",
        },
        "<generic>"
      )} in the ${seaId}>`,
      startdescription: `<Leaving and sailing towards <generic>. Distance: ${seaSizes[seaId]} units.>`,
      actionId: "mariner.sail",
      craftable: true,
      requirements: {
        "mariner.ship": 1,
        "mariner.destination": 1,
      },
      tablereqs: {
        "mariner.docked": 1,
        [`mariner.locations.${seaId}.sailingthere`]: 1,
      },
      grandReqs: {
        "~/tabletop:{verb/explore}:recipe/*": -1,
        "~/tabletop:{verb/talk}:recipe/*": -1,
        "~/tabletop:{verb/mariner.sing}:recipe/*": -1,
        "~/tabletop:{verb/mariner.navigate}:recipe/*": -1,
      },
      effects: { "mariner.sailing.totaldistance": seaSizes[seaId] },
      // furthermore: computeTravelSequence(defaultPath),
      linked: [{ id: "mariner.sailing.leaving.start" }],
      ...SET_SEA_TRACKLIST,
    });

    // Leave for open sea
    mod.setRecipe(fileId, {
      id: `mariner.sailing.leave.opensea.${seaId}`,
      label: `<Leaving and sailing the open sea in the ${seaId}>`,
      startdescription: `<Leaving and sailing the open sea in the ${seaId}>`,
      actionId: "mariner.sail",
      craftable: true,
      requirements: {
        "mariner.ship": 1,
        "mariner.destination": -1,
      },
      tablereqs: {
        "mariner.docked": 1,
        [`mariner.locations.${seaId}.sailingthere`]: 1,
      },
      grandReqs: {
        "~/tabletop:{verb/explore}:recipe/*": -1,
        "~/tabletop:{verb/talk}:recipe/*": -1,
        "~/tabletop:{verb/mariner.sing}:recipe/*": -1,
        "~/tabletop:{verb/mariner.navigate}:recipe/*": -1,
      },
      linked: [{ id: "mariner.sailing.leaving.start" }],
      ...SET_SEA_TRACKLIST,
    });

    // Set course from open sea
    mod.setRecipe(fileId, {
      id: `mariner.sailing.setcourse.from.${seaId}.switchback`,
      warmup: 10,
      label: `<Sailing back to the previous sea>`,
      startdescription: `<Setting course back to the previous sea. This will take roughly 1 travel steps.>`,
      actionId: "mariner.sail",
      requirements: {
        "mariner.ship": 1,
        "mariner.destination": 1,
        "mariner.waypoint.comingfrom": 1,
        [`mariner.locations.${seaId}.sailingthere`]: 1,
      },
      effects: { "mariner.sailing.totaldistance": DISTANCE_SCALED(1) },
      linked: [{ id: "mariner.sailing.leaving.start" }],
    });
    mod.setRecipe(fileId, {
      id: `mariner.sailing.setcourse.from.${seaId}`,
      warmup: 10,
      label: `<Setting course sailing towards ${buildRefinementBlock(
        {
          [`mariner.locations.northsea.amsterdam.away`]: "Amsterdam",
        },
        "<generic>"
      )} in the ${seaId}>`,
      startdescription: `<Setting course sailing towards <generic>. This will take roughly ${seaSizes[seaId]} travel steps.>`,
      actionId: "mariner.sail",
      requirements: {
        "mariner.ship": 1,
        "mariner.destination": 1,
        [`mariner.locations.${seaId}.sailingthere`]: 1,
      },
      effects: { "mariner.sailing.totaldistance": seaSizes[seaId] },
      // furthermore: [...computeTravelSequence(defaultPath)],
      linked: [{ id: "mariner.sailing.leaving.start" }],
    });
  }

  console.log("Generated routes.");
}
