import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateMorlandEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.morland"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "northsea", "london"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `A Rare Find`,
    grandReqs: { ...TIMER_ELAPSED("northsea.randomevent.morland", 25) },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.morland") },
    startdescription:
      "I rarely browse for long at Morlands, as I find her other costumers shuffling between the shelves, but today one book catches my eye.",
    linked: [{ id: "mariner.drawreward.scholarly" }],
    tablereqs: { "mariner.locations.northsea.london.morland": 1 },
    slots: [
      {
        id: "funds",
        label: "Funds",
        description: "She requires payment for the book",
        greedy: true,
        required: {
          funds: 1,
        },
      },
    ],
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
