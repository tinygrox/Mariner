import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateVisitorEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.visitor"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "northsea", "london"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Morland Other Visitor`,
    grandReqs: { ...TIMER_ELAPSED("visitor", 25) },
    rootAdd: { ...RESET_TIMER("visitor") },
    startdescription:
      'When I visited Morland last night, she had another call on her. She turned us both away with surprising resoluteness. "I don\'t do introductions. But there is public house on the corner." We spoke... carefully, of our pursuits. It seems they were searching for a direction to follow.', //add a description of meeting the follower of your last character.
    effects: {
      "mariner.influences.edge": 2,
      "mariner.influences.lantern": 2,
    },
    tablereqs: { "mariner.locations.northsea.london.morland": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
