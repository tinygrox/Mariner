import { generatePort } from "../../../port_generation/port_generation.mjs";
import { theater, theaterSlots } from "../../../port_generation/helpers.mjs";
import { generateMarinetteDollQuest } from "./marinettedoll.mjs";
import { generateLondonStreetsEvents } from "./london_index.mjs";
import { MORLAND } from "./morland.mjs";
import { AGDISTIS } from "./agdistis.mjs";
import {
  buildCodexEntryBlock,
  buildSequentialCodexEntry,
  generateCodexEntry,
} from "../../../codex_generation/codex_generation.mjs";

export function generateLondon() {
  generatePort({
    portRegions: ["northsea"],
    portId: "london",
    label: "London",
    description:
      "The Capital, whose streets I once called home. Now I am not sure if any port can claim that fame. Still, it brings back memories.",
    recipeDescriptions: {
      docking:
        "Up the Thames, until the river is more brown than grey, and the air more noise than silence. Here we dock.",
      exploreHH:
        "Guided by rumor or by memory, I will explore the Heart of the Empire. If I apply my charm, I may find extrordinary peoples, places, or opportunities.",
      exploreLH:
        "Guided by rumor or by memory, I will explore the Heart of the Empire. If I stay open and receptive, I may find discover the peculiar sounds of the city, or be approached by a Raconteur.",
    },
    tunes: [
      "forgeheart",
      "grailedge",
      "heartgrail",
      "knocklantern",
      "winterlantern",
      "mothheart",
    ],
    storyTrading: {
      story: {
        label: "Besse of Bretnal Green",
        icon: "humoursofagentleman",
        description:
          "Henry de Montforth was a cunning tactician, who got blinded in battle. He lost his fortune and became a blind beggar at the Bretnal Green. His daughter, Besse, was clever, beautiful and engaging. Four suitors approached her for her hand, a knight, a man-of-fortune, a trader and an inn-keeper's son. She told them all that they would have to get the blessing of her father, begging on the street. All but the knight were turned off by the prospect of a such a shameful father-in-law. On their wedding feast his Besse revealed that he was a de Mormont, and that their wealth was still in tact.",
        aspects: {
          grail: 2,
          heart: 2,
          "mariner.storyaspect.mortal": 1,
          "mariner.storyaspect.witty": 1,
          "mariner.storyaspect.romantic": 1,
        },
      },
      label: "Trading Stories in the London Streets",
      startdescription:
        "Stories cling to the city like washlines over a street, or like fog on a window. If I engage with the locals, I can pluck London's story and tell it as my own.",
      description:
        "Around the London East End, folklore, myth and reality mingle as if neighbors. I listen to the story told by an old woman to breathless children on her doorstep stoop.",
    },
    storeName: "<Some Supply Store>",
    barName: "The Anchor",
    wharf: {
      cargoOptions: [
        { id: "mariner.cargo.for.northsea.local", label: "North Sea" },
        { id: "mariner.cargo.for.medsea.close", label: "Med Sea" },
        { id: "mariner.cargo.for.medsea.close", label: "Med Sea" },
      ],
    },
    portElements: [
      MORLAND,
      AGDISTIS,
      {
        notDiscoverable: true,
        type: "location",
        elementId: "unmarkedgrave",
        label: "An Unmarked Grave",
        description:
          "What remains of the dancer slowly sinks down into the soggy soil.",
        localAspects: { "mariner.location": 1, modded_explore_allowed: 1 },
        slots: [
          {
            id: "intent",
            label: "Reason to Visit",
            actionId: "explore",
            required: {
              "mariner.trapping": 1,
              "mariner.lackheart": 1,
              "mariner.locations.northsea.london.agdistis": 1,
            },
          },
        ],
      },
      {
        elementId: "ecdysisfake",
        label: "The Ecdysis Club",
        description:
          "A cabaret with an occult reputation. The club has several layers of stages, each with more exclusive than the previous, and each one more intimate. I can enter on Agdistis' invitation, but I am not permitted true access until I have proven myself in an audition.",
        slots: [
          {
            id: "verbSong",
            actionId: "mariner.sing",
            label: "Song",
            description:
              "I should begin my performance, by declaring my intentions, my method, my staging.",
            required: {
              "mariner.song": 1,
            },
          },
          {
            id: "story",
            label: "Story to trade",
            description:
              "The story I'm willing to share in order to get them to share theirs.",
            actionId: "mariner.navigate",
            required: {
              "mariner.story": 1,
            },
          },
          {
            id: "aspect1",
            actionId: "mariner.sing",
            label: "Instrument",
            required: {
              "mariner.instrument": 1,
            },
          },
        ],
        storyTrading: {
          story: {
            label: "Dressing Room Chatter",
            description:
              'Conversation between intimates can be as collaborative as a dance, as collective as a chorus. "Haven\'t you heard?"-- "Do tell..." -- "No way!". Unfinished sentences build up from meaningful silences, single words that can have the room in stitches. Conversation flows without order or direction, but all voices harmonous like the birdsong chorus that is soon to arise. Lose yourself in its dizzying pace and unaffected superficiality.',
            aspects: {
              moth: 2,
              heart: 2,
              "mariner.storyaspect.mortal": 1,
              "mariner.storyaspect.dionysian": 1,
              "mariner.storyaspect.rousing": 1,
            },
            icon: "franklinbancroftdiaries",
          },
          label: "Trade Stories at the Ecdysis Club",
          startdescription:
            "At the Ecdysis they prefer to portray their stories non-verbally on stage, but in the wings, gossip and secrets are shared in front of mirrors and make-up kits.",
          description:
            "On late nights, when the performances have ended and the dancer reapply their mortal guises, the topics drift between tabloid hearsay, midnight fancies and backstage speculation.",
        },
        localAspects: {
          "mariner.sing_allowed": 1,
          modded_explore_allowed: undefined,
          "mariner.location": 1,
        },
        localIcon: "locationcabaretclosed",
        notDiscoverable: true,
      },
      theater({
        id: "ecdysisclub",
        label: "The Ecdysis Club",
        description:
          "A cabaret with an occult reputation. The club has several layers of stages, each with more exclusive than the previous, and each one more intimate.",
        slots: theaterSlots(["mariner.instrument", "mariner.trapping"]),
        storyTrading: {
          story: {
            label: "Dressing Room Chatter",
            description:
              'Conversation between intimates can be as collaborative as a dance, as collective as a chorus. "Haven\'t you heard?"-- "Do tell..." -- "No way!". Unfinished sentences build up from meaningful silences, single words that have the room in stitches. Conversation flows without order or direction, but all voices harmonous like the birdsong chorus that is soon to arise. Lose yourself in it\'s dizzying pace and unaffected superficiality.',
            aspects: {
              moth: 2,
              heart: 2,
              "mariner.storyaspect.mortal": 1,
              "mariner.storyaspect.dionysian": 1,
              "mariner.storyaspect.rousing": 1,
            },
            icon: "franklinbancroftdiaries",
          },
          label: "Trade Stories at the Ecdysis Club",
          startdescription:
            "At the Ecdysis they prefer to portray their stories non-verbally on stage, but the rafters, gossip and secrets are shared in front of mirrors and make up kits.",
          description:
            "On late nights, when the performances have ended and the dancer reapply their mortal guises, the topics drift between tabloid hearsay, midnight fancies and backstage speculation.",
        },
        options: {
          notDiscoverable: true,
        },
      }),
    ],
  });

  generateMarinetteDollQuest();
  generateLondonStreetsEvents();

  generateCodexEntry(
    "codex.agdistis",
    "[N. Sea] The Ecdysis Club and its Manager",
    buildSequentialCodexEntry([
      "A tall, handsome, soft-spoken greek. He is the dancemaster at the Ecdysis club in London. The reputation of the club is known, but what do people know about its purveyors?\n\n\n",
      buildCodexEntryBlock(
        {
          "codex.agdistis.unlockedecdysis":
            "I have a standing invitation to perform at the Ecdysis club. Agdistis has challenged me to pursue the Orphean Arts there, and see what I can accomplish with my songs.",
        },
        "Agdistis has expressed interest in my development as an artist. If I impress him with the right songs, he will open up  the stage to me."
      ),
      "\n\n\n" +
      buildCodexEntryBlock({
        "codex.agdistis.throwndoll":
          "Agdistis requested for me to drop a treasured idol into the ocean. It has brought to my mind the importance of Character, of making decisions of your own, and not just letting the world lead you.\n\nI sense that power in me is broken. If I wish to pursue the path of becoming whole, I must find a way within my means to recover this. I am sure I will find it, out there in the world's memory.",
        "codex.agdistis.givendoll":
          "Agdistis has come to me with a request. He wishes for me to <color=yellow>drop a doll into the ocean</color>. He has told me he wishes to move forward, but from what? I dread to imagine.",
      }),
    ])
  );
}
