import {
  repeatableConversation,
  uniqueConversation,
} from "../../../port_generation/helpers.mjs";

export const AGDISTIS = {
  notDiscoverable: true,
  type: "npc",
  elementId: "agdistis",
  label: "Mr. Agdistis",
  description: "The tall and tragic stage director of the Ecdysis Club.",
  deliveryOptions: {
    previousPackageLocations: ["Amsterdam", "Copenhagen"],
    knownLanguages: ["greek"],
    usedNames: ["EVANGELOS Agdistis"],
    usedInitials: ["E. C. G. A."],
    gender: "male",
    whatToDoWithIt: ["Knock on the stage door after performances are over."],
  },
  deliveryRiddles: [
    "A follower of the Heart Relentless, passing on his knowledge to apprentice Dancers.",
    "A mover of bodies, a conductor of steps. His are the arts of motion",
    "A son of Apollo in the green man’s lands",
    "Serpent daughter's page, keeper of her unblinking kingdom",
    "Tutor of those pursuing the New Way",
  ],
  storyTrading: {
    story: {
      label: "A Boy Alone",
      description:
        "The story follows a foundling boy, who even got cast out by his foster family, and became a boy in the service of a wandering musician. The story details different cities the pair visited and hardships the pair and their pack of animals endure.",
      aspects: {
        heart: 2,
        "mariner.storyaspect.mortal": 1,
        "mariner.storyaspect.rousing": 1,
        "mariner.storyaspect.grim": 1,
      },
      icon: "historyofshadowlesskings",
    },
    label: "Trade stories with Agdistis",
    startdescription:
      '"There is one from my youth that has always stayed with me..."',
    description:
      '"My mother insisted I learn french, and this was one of the stories she would read to me... Even though the ending they gave is happy, it was always the suffering of the road that fealt the most real."',
  },
  conversations: [
    repeatableConversation({
      id: "sunkencity",
      element: "mariner.mysteries.kerys",
      label: "Asking after the Sunken City",
      startdescription: '"A city at the bottom of the sea?"',
      description:
        '"No I do not know of such a place, allthough the name sounds familiar... Perhaps I have heard it in a dream."',
    }),
    repeatableConversation({
      id: "twinstemptation",
      element: "mariner.ascension.twins.temptation",
      label: "Ask about the Path to becoming Whole",
      startdescription: '"You want to talk about your Path?"',
      description:
        '"What you are missing is not gone. You just have to find it to become whole. Step by step. Part by part."',
    }),
    repeatableConversation({
      id: "twinsdedication",
      element: "mariner.ascension.twins.dedication",
      label: "Part By Part",
      startdescription: '"On your way..."',
      description:
        '"Part by part, Step by step. You soul was torn, but you can stitch it back together. That will draw you closer to your other half. If you make it all the way, you can be as one again."',
    }),
    repeatableConversation({
      id: "vagabondtemptation",
      element: "mariner.ascension.vagabond.temptation",
      label: "Another Path?",
      startdescription: '"You wish to know about another path?"',
      description:
        '"That is another path open to Half-Hearts, not quite the intended route, I don\'t think. A lonely one, as well."',
    }),
    repeatableConversation({
      id: "vagabonddedication",
      element: "mariner.ascension.vagabond.dedication",
      label: "Another Path Taken.",
      startdescription: '"So you have pivoted in another direction."',
      description: '"I hope it brings you all that you seek."',
    }),
    repeatableConversation({
      id: "dancer",
      element: "mariner.mysteries.dancer",
      label: "Asking after a Dancer",
      startdescription:
        'He regards me coolly. "You wish to know about a performer at the Club?"',
      description:
        '"We have many performers, who come and go for many reasons. But they all appreciate my confidentiality. I will not say more to an outsider, who does not understand our Ways."',
    }),
    repeatableConversation({
      id: "lackheart",
      element: "mariner.lackheart",
      label: "Asking after my Condition",
      startdescription: '"You want to know what I know about your Condition?"',
      description:
        '"Not much, I\'m afraid. I met another long ago, who had the same look in her eyes. The same... Hunger." He taps my chest. "In there. When she lingered here I watched her wither like a caged bird, until she left again. And on and on. Coming and going like the tides. Years later I saw her oncemore, arms entwined with another like her. She said she found her other half.',
    }),
    repeatableConversation({
      id: "fakeclub",
      element: "mariner.locations.northsea.london.ecdysisfake",
      label: "Asking after the Club",
      startdescription: '"The Club? What do you want to know?"',
      description:
        '"I am not the founder, nor the owner, but I do feel a certain responsible for the club, and those who passes through. I\'d love to see you on our stage again."',
    }),
    repeatableConversation({
      id: "club",
      element: "mariner.locations.northsea.london.ecdysisclub",
      label: "Asking after the Club",
      startdescription: '"The Club? What do you want to know?"',
      description:
        '"I am not the founder, nor the owner, but I do feel a certain responsible for the club, and those who passes through. I\'d love to see you on our stage again."',
    }),
    repeatableConversation({
      id: "unmarkedgrave",
      element: "mariner.locations.northsea.london.unmarkedgrave",
      label: "How It Ended.",
      startdescription: '"Have you visited yet?"',
      description:
        'Agdistis is clearing paper snake-skins and prop vines of the stage."She was on her path. Sulochana guided her, not me. Do not ask me where it went wrong, I will not say. But we gave her the rites she deserved, in the end."',
    }),
    repeatableConversation({
      id: "musicstorm",
      element: "mariner.song.grail",
      label: "Discuss the Power of my Songs",
      startdescription: '"You wish to discuss Orphean Magic with me?"',
      description:
        '"Our Arts are even less precise than those performed by aspirants and scholars. Take the weather, for example. If your Direction in a Performance is aimed at Grail, Paired with an Intent of Edge, You can Rouse Storms from Sea and Sky. To quell storms however, both Direction and Intent change... "',
        additionalProperties: {
          unlockCodexEntries: [
            "codex.performance.conversation.agdistis.grail"
          ],
        },
      }),
    repeatableConversation({
      id: "musicwrongmoth",
      element: "mariner.song.moth",
      label: "Discuss the Power of my Songs",
      startdescription: '"You wish to discuss Orphean Magic with me?"',
      description:
        '"The Club does do Performances in the Old Path, though they focus on dance, not music. If Sulochana had been here... Perhaps you can seek out another shedder of skins to elaborate on these mysteries?"',
        additionalProperties: {
          unlockCodexEntries: [
            "codex.performance.conversation.agdistis.moth"
          ],
        },
      }),
    repeatableConversation({
      id: "musicwrongedge",
      element: "mariner.song.edge",
      label: "Discuss the Power of my Songs",
      startdescription: '"You wish to discuss Orphean Magic with me?"',
      description:
        '"We try for a particular ambiance here at the Ecdysis club, and the clanging of blades is rarely a part of that. Still, if you perform with the Intent of Edge, you can rouse many things beyond mere battle lust."',
        additionalProperties: {
          unlockCodexEntries: [
            "codex.performance.conversation.agdistis.edge"
          ],
        },
      }),
    repeatableConversation({
      id: "musicwrongheart",
      element: "mariner.song.heart",
      label: "Discuss the Power of my Songs",
      startdescription: '"You wish to discuss Orphean Magic with me?"',
      description:
        '"The Revolutions of the Heart are what I studied the deepest, though the pursuit left it scars on both my body and soul. Many of the Powers of the Wood and sea share their interest in heart, but it most closely connected to its Patron Hour, The Storm Relentless. With the right performance, the Sky can be commanded, by pairing this Direction with what is springs from."',
        additionalProperties: {
          unlockCodexEntries: [
            "codex.performance.conversation.agdistis.heart"
          ],
        },
      }),
    repeatableConversation({
      id: "musicwrongmoth",
      element: "mariner.song.moth",
      label: "Discuss the Power of my Songs",
      startdescription: '"You wish to discuss Orphean Magic with me?"',
      description:
        '"The Club does do Performances in the Old Path, though they focus on dance, not music. If Sulochana had been here... Perhaps you can seek out another shedder of skins to elaborate on these mysteries?"',
    }),
    repeatableConversation({
      id: "musicwrong",
      element: "mariner.song",
      label: "Discuss the Power of my Songs",
      startdescription: '"You wish to discuss a song of Power with me?"',
      description:
        '"I am not a specialist on such manners. I fear to misconstrue it\'s intricasies. You should seek another tutor."',
    }),
    repeatableConversation({
      id: "character",
      element: "mariner.mysteries.howtomendcharacter",
      label: "The Mending of the Heart",
      startdescription: '"You are Seeking to Mend your Character?"',
      description:
        '"Character... one of the Nine. If you seek to heal it hrough the Orphean Arts, you must find the right inspiration first. Search the stories for tales of great choices and decission points. Agency gained and lost. " He shrugs. "I cannot make the choice for you. It must be meaningfull to you."',
    }),
    repeatableConversation({
      id: "songheart",
      element: "mariner.instrument.plain.heart",
      label: "Play a song for the Dance Master",
      startdescription: '"You want to sing a song for me?"',
      description:
        "After the song is through, Agdistis sits smiling, though his eyes are liquid.\"It's a cruel joy, isn't it? To give of oneself to enact eternity. Oh, but how gladly we give.\"",
    }),
    repeatableConversation({
      id: "songwrong",
      element: "mariner.instrument",
      label: "Play a song for the Dance Master",
      startdescription: '"You want to sing a song for me?"',
      description:
        "\"It's pretty. It has potential. But it could be more. Come to the club, and I'll set up a real performance for it, with a proper stage-setting, and a proper crowd. And then you can show me what you can accomplish.\"",
    }),
  ],
};
