import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateUnnervingEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.unnerving"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "northsea", "london"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `An Unnerving Performance at the Ecdysis`,
    grandReqs: { ...TIMER_ELAPSED("unnerving", 25) },
    rootAdd: { ...RESET_TIMER("unnerving") },
    startdescription:
      "The Dance tonight is some special occasion, it seems. Agdistis does not have time to greet me today, and I watch even the unblinking owner of the club talk to the performers. The dance is serpentine, and the garments fall like the shedding of skin.",
    effects: {
      "mariner.experiences.unsettling.standby": 1,
    },
    tablereqs: { "mariner.locations.northsea.london.ecdysisclub": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
