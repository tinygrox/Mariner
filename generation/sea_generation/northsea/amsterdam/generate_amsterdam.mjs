import { generatePort } from "../../../port_generation/port_generation.mjs";
import { market } from "../../../port_generation/helpers.mjs";
import { MATA_HARI, VEILED_LADY } from "./veiledlady.mjs";
import { generateAmsterdamStreetsEvents } from "./amsterdam_index.mjs";
import {
  buildCodexEntryBlock,
  generateCodexEntry,
} from "../../../codex_generation/codex_generation.mjs";

export function generateAmsterdam() {
  generatePort({
    portRegions: ["northsea"],
    portId: "amsterdam",
    label: "Amsterdam",
    description:
      "A spiderweb of canals lies at the heart of a sea of brick and cobblestone. A jewel of commerce and mercantilism.",
    recipeDescriptions: {
      docking:
        "The Kite sails waterways sharp as a knife's cut. No people have tamed water as the Dutch have. No people should fear its escape as much. I dock in IJ-Haven, with the same prayer on my lips as all the rest.",
      exploreHH:
        "Under high gables, through small alleys I pass, Across many bridges, past wide canals, I go. If I apply my charm, I may find extrordinary peoples, places, or opportunities.",
      exploreLH:
        "Under high gables, through small alleys I pass, Across  many bridges, past wide canals, I go. If I stay open and receptive, I may find discover the peculiar sounds of the city, or be approached by a Raconteur.",
    },
    storyTrading: {
      story: {
        label: "The Brave Maid at the Keizersgracht",
        description:
          "One Night, a maid sharpening the blades of the kitchenwear, when she could hear men's voices from the allyway besides. The men where discussing how to rob the house. They decided to climb in through the kitchen window one by one. So she took up position next to the window, with two cleavers, and snipped of each head as they entered. Each time the robbers outside asked if they had gotten inside safe, and each time she mimicked a gruff confirmation. only the seventh, youngest robber did not enter. She was prazed for her bravery, and gifted a diamond ring. Soon after a handsome young men became a valet at the house, and began courting her. It was the Youngest Robber, and he had come for revenge for his brothers...",
        aspects: {
          moth: 1,
          edge: 1,
          "mariner.storyaspect.mortal": 1,
          "mariner.storyaspect.rousing": 1,
          "mariner.storyaspect.grim": 1,
        },
        icon: "thosewhodonotsleep",
      },
      label: "Trade Stories at the Streets of Amsterdam",
      startdescription:
        "Stories cling to the city like washlines over a street, or like fog on a window. If I engage with the locals, I can pluck Copenhagen's story and tell it as my own.",
      description:
        "From the scullery maids, out in the markets for the shopping, I overhear this tale of the cleverness of one of their peers from fabled pasts.",
    },
    tunes: ["grailedge", "heartgrail", "mothedge", "mothheart"],
    barName: "Het Anker",
    wharf: {
      cargoOptions: [
        { id: "mariner.cargo.for.northsea.local", label: "North Sea" },
        { id: "mariner.cargo.for.medsea.close", label: "Med Sea" },
        { id: "mariner.cargo.for.medsea.close", label: "Med Sea" },
      ],
    },
    portElements: [
      market({
        id: "vermaetdiamantenhuis",
        label: "Vermaet Diamantenhuis",
        description:
          "A gemcutting house with its roots all the way back in the sixteenth century. While they mostly purchase gems in their raw state, they are known to appreciate a rare find of jewelry as well.",
        types: ["jewelry", "crafts", "artifact"],
        storyTrading: {
          story: {
            label: "The Diamond with the Perfect Flaw",
            icon: "locksmithsdream2",
            description:
              'Once, there was a King who held a wealth of gems in his treasury. On a weekly inspection of his hoard, he discovered a previously flawless gem marred by a cut on it\'s largest facet. He asked all the master crafter and jewelers to fix this gem, but all apologized and said there was nothing to be done. Then a single craftsman stepped forward, not yet out of his apprenticeship, and said. "I cannot remove the flaw, but I can save the Gem". He went to work and king saw him carve leaves, rosebuds and blooming petals. The apprentice had seen a rose stem where other just saw a scar, and had carved to enhance the beauty he had seen.',
            aspects: {
              forge: 1,
              knock: 1,
              "mariner.storyaspect.mortal": 1,
              "mariner.storyaspect.wise": 1,
              "mariner.storyaspect.romantic": 1,
            },
          },
          label: "Collecting Stories Traded between Diamond Cutters",
          startdescription:
            "As a sometimes supplier, sometimes buyer, I am allowed in the backrooms of the Diamond House. Here men in neat suits sit bowed over velvet laced desks, assessing, weighing, cutting. Tight-bunned and dimple-cheeked Mirjam oversees the work, mindful of the clock, but there is always strong sweet coffee for a guest.",
          description:
            "The quiet industry reminds me of the ship, though here the tools are finer and the goods more precious. And the stories here have traveled as far as the diamonds, through the network of journeymen, traders, good common folk that stretch from India to Jerusalem, through the Polish plains to the canals of Amsterdam and beyond.",
        },
      }),
      VEILED_LADY,
      MATA_HARI,
    ],
  });

  generateCodexEntry(
    "codex.veiledlady",
    "[N. Sea] The Charming Creature of Amsterdam",
    buildCodexEntryBlock(
      {
        "codex.veiledlady.2":
          "Miss Morland asked me to deliver a letter to this peculiar character. I didn't hear much from her, only echoes... Some say she's an avatar of desire itself. Others that she's unnaturally graceful but that something's wrong in her eyes, like she's been beyond the Veil and never really truly returned. I don't know what to think...yet.\n\n\nI've met her. She's...troubling. She's trouble, too, no doubt about that. She barely touched the envelope I gave her and immediately requested I help her. She wants her ring back. The ring is said to have been stolen by some kind of competitor of the Desire's emanations, in a different interpretation.\n\nShe told me of where they dwell: somewhere underneath Amsterdam's sewers.\n\n\nI brought her the ring; it softened her eyes like I never saw before, before she stowed it away. She rewarded me with everything a composer could hope for.\n\n<i>Who is she, really? She's the wine. She's the infinite unveiling of a charm dancer. She's the silhouette of someone in the distance, seen through the blur of a drunken eyesight.</i>",

        "codex.veiledlady.1":
          "Miss Morland asked me to deliver a letter to this peculiar character. I didn't hear much from her, only echoes... Some say she's an avatar of desire itself. Others that she's unnaturally attractive but that something's wrong in her eyes, like she's been beyond the Veil and never really truly returned. I don't know what to think...yet.\n\n\nI've met her. She's...troubling. She's trouble, too, no doubt about that. She barely touched the envelope I gave her and immediately requested I help her. <color=yellow>She wants her ring back</color>. The ring is said to have been stolen by some kind of competitor of the Desire's emanations, in a different interpretation.\n\nShe told me of where they dwell: somewhere underneath <color=yellow>Amsterdam's sewers</color>.\n\n\n",
      },
      "Miss Morland asked me to deliver a letter to this peculiar character. I didn't hear much from her, only echoes... Some say she's an avatar of desire itself. Others that she's unnaturally attractive but that something's wrong in her eyes, like she's been beyond the Veil and never really truly returned. I don't know what to think...yet.\n\n\n"
    )
    // buildSequentialCodexEntry([
    //   "Miss Morland asked me to deliver a letter to this strange recipient. I didn't hear much from her, only echoes... Some say she's an avatar of desire itself. Others that she's unnaturally attractive but that something's wrong in her eyes, like she's been beyond the Veil and never really truly returned. I don't know what to think...yet.\n\n\n",
    //   {
    //     "codex.veiledlady.1":
    //       "I've met her. She's...troubling. She's trouble, too, no doubt about that. She barely touched the envelope I gave her and immediately requested I help her, almost like it was the intent all along. <color=yellow>She wants some ring back</color>. The ring is said to have been stolen by some kind of competitor of the Desire's emanations. A different interpretation of it.\n\nShe told me of where they dwell: <color=yellow>somewhere underneath Amsterdam's sewers</color>.\n\n\n",
    //   },
    //   {
    //     "codex.veiledlady.2":
    //       "<i>Who is she, really? She's the wine. She's the infinite unveiling of a charm dancer. She's the silhouette of someone in the distance, seen through the vision of a drunken eyesight.</i>",
    //   },
    // ])
  );

  generateAmsterdamStreetsEvents();
}
