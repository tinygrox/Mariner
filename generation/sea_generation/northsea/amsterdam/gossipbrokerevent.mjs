import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";
import { LOCAL_REPUTATION } from "../../../generation_helpers.mjs";

export function generateGossipbrokersEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.gossipbrokers"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "northsea", "amsterdam"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `A Gossipbrokers Favour`,
    grandReqs: {
      [READ_QUEST_FLAG("amsterdamsewers", "completed")]: 1,
      ...TIMER_ELAPSED("northsea.randomevent.gossipbrokers", 25),
    },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.gossipbrokers") },
    startdescription:
      "This evening, a neatly written envelop awaits me on the ship. In it is a missive of the Gossipbrokers, informing me that they noticed some of the hubbub growing up around me in town, and they decided to take care of it. It seems it is in their best interest for me to be active in Amsterdam a little longer.",
    furthermore: [
      { target: "~/exterior", effects: { "mariner.notoriety": -10 } },
    ],
    tablereqs: { "mariner.locations.northsea.amsterdam": 1 },
    grandReqs: { [LOCAL_REPUTATION]: 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
