import { generateBirdsEvent } from "./birdsevent.mjs";
import { generateDiamondEvent } from "./diamondevent.mjs";
import { generateGossipbrokersEvent } from "./gossipbrokerevent.mjs";
//import { generateHazeEvent } from "./hazeevent.mjs";
//import { generateMorgenEvent } from "./morgenevent.mjs";
import { generateLadyEvent } from "./veiledladyevent.mjs";

export function generateAmsterdamStreetsEvents() {
    generateBirdsEvent();
    generateDiamondEvent();
    generateGossipbrokersEvent();
    //generateHazeEvent();
    //generateMorgenEvent();
    generateLadyEvent();
}
