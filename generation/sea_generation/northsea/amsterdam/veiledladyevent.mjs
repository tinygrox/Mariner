import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateLadyEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.lady"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "northsea", "amsterdam"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `A Quiet Exit`,
    grandReqs: { ...TIMER_ELAPSED("northsea.randomevent.lady", 25) },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.lady") },
    startdescription:
      'I get summoned to the Veiled Lady, who explains a delicate request. "Could you do me a favor? I have a friend, who needs to get out of town... on the quick we might say. But he is very capable... might he gain a passage on the ship?"',
    effects: {
      "mariner.crew.specialist.officer.opportunity.free": 1,
    },
    tablereqs: { "mariner.locations.northsea.amsterdam.veiledlady": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
