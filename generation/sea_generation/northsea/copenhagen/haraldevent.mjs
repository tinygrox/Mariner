import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateHaraldEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.harald"
  );
  mod.initializeRecipeFile(RECIPE_FILE, [
    "locations",
    "northsea",
    "copenhagen",
  ]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `A Farewell Pyre`,
    grandReqs: { ...TIMER_ELAPSED("northsea.randomevent.harald", 25) },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.harald") },
    startdescription:
      "I wandered past the banks of the Soerne lakes, when I smelled the sting of smoke on the air. It lead me to the townhouse of Harald Blomkvist. Through the open windows of his salon, I could see him pull papers from his cubboards and table and them in a burning ton. Then he topples the burning refuse out of the window. I collect some of the ashes before I go.",
    effects: {
      "mariner.trappings.winter.1": 1,
    },
    tablereqs: { "mariner.locations.northsea.copenhagen.harald": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
