import { generatePort } from "../../../port_generation/port_generation.mjs";
import {
  theater,
  theaterSlots,
  instrumentShop,
} from "../../../port_generation/helpers.mjs";
import { generateInstrumentShopQuest } from "./unlock_instrumentshop.mjs";
import { generateCopenhagenStreetsEvents } from "./copenhagen_index.mjs";
import { generateHaraldMuseQuest } from "./harald_muse.mjs";
import { HARALD } from "./harald.mjs";
import {
  buildCodexEntryBlock,
  generateCodexEntry,
} from "../../../codex_generation/codex_generation.mjs";

export function generateCopenhagen() {
  generatePort({
    portRegions: ["northsea"],
    portId: "copenhagen",
    label: "Copenhagen",
    description:
      "Once it held full control over the passage between the North and East Sea. Those tariffs have built a beautiful bastion of copper and stone.",
    recipeDescriptions: {
      docking:
        "The sea might be unpredictable, but the District of Nyhavn is bright and inviting. The Kite comes to rest at the dock as for a comfortable hibernation.",
      exploreHH:
        "Off the ship, through colourful Nyhavn, past the Charlottenborg, and into the city I go. If I apply my charm, I may find extrordinary peoples, places, or opportunities.",
      exploreLH:
        "Off the ship, through colourful Nyhavn, past the Charlottenborg, and into the city I go. If I stay open and receptive, I may find discover the peculiar sounds of the city, or be approached by a Raconteur.",
    },
    tunes: ["heartgrail", "winterlantern", "winterknock", "mothheart"],
    storyTrading: {
      story: {
        label: "Pesta and her Broom",
        description:
          "when the Black Death set upon Scandinavia, it did so in the guise of an old crone. Her skirts where red and blue, and she carried with her a Rake and a Broom. If you would meet her on the road, you would know that township would be visited by the plague soon. If she carried here rake, some of the community would be spared, but if she she was sweeping the road as she passed you, you could be sure no one would be left once the dust settled.",
        aspects: {
          winter: 1,
          edge: 1,
          "mariner.storyaspect.cthonic": 1,
          "mariner.storyaspect.grim": 1,
          "mariner.storyaspect.whimsical": 1,
        },
        icon: "medusaslament",
      },
      label: "Trade Stories at the Streets of Copenhagen",
      startdescription:
        "Stories cling to the city like washlines over a street, or like fog on a window. If I engage with the locals, I can pluck Copenhagen's story and tell it as my own.",
      description:
        "History is the scar on the skin of the world, and tragedies leave particularly deep scars. It is baked into the bricks of buildings, sticks to our language and gets planted as seeds in childrens hearts...",
    },
    barName: "Ankeret",
    wharf: {
      cargoOptions: [
        { id: "mariner.cargo.for.northsea.local", label: "North Sea" },
        { id: "mariner.cargo.for.medsea.close", label: "Med Sea" },
        { id: "mariner.cargo.for.medsea.close", label: "Med Sea" },
      ],
    },
    portElements: [
      HARALD,
      theater({
        id: "newscandinavian",
        label: "The new 'New Scandinavian' Theater",
        description:
          "The latest incarnation of a constantly reforming theater company. Here new forms of expression are married to the old.",
        slots: theaterSlots(["mariner.story", "mariner.trapping"]),
        storyTrading: {
          story: {
            label: "A Revolution of Thought",
            icon: "burningofunburntgod",
            description:
              "How do you relate a story told without logic? Words without meaning and songs without rhythm, but Emotion, Expression, Truth.",
            aspects: {
              forge: 1,
              moth: 1,
              "mariner.storyaspect.apollonian": 1,
              "mariner.storyaspect.dionysian": 1,
              "mariner.storyaspect.whimsical": 1,
            },
          },
          label: "An Evening at the New Scandinavian",
          startdescription:
            "I could catch a show at the New Scandinavian. Few runs are very long, so every show is a surprise, and every show is a challenge.",
          description:
            "A cacophony, a revelation? These performances are never straightforward, and even rarely clear what they are alluding too.",
        },
      }),
      {
        elementId: "instrumentshopfake",
        overlayId: "instrumentshop",
        label: "Baruch and Sons, Expert Luthiers and Further.",
        description:
          "An unassuming shop nestled in the oldest streets of the oldest neighborhoods. They work for an exclusive Clientel, and do not permit entry into their workshop easily.",
        notDiscoverable: true,
        localAspects: { "mariner.location": 1 },
        canBeHostile: false,
        slots: [
          {
            id: "item",
            label: "A Request",
            description:
              "I have been recommended to Baruch not only as a client, but also as ally. He has asked me to supply him with Spiteful Resin.",
            actionId: "explore",
            required: {
              "mariner.vaults.eddanger.resin": 1,
            },
          },
        ],
      },
      instrumentShop({
        id: "instrumentshop",
        label: "Baruch and Sons, Expert Luthiers and Further.",
        description:
          "An unassuming shop nestled in the oldest streets of the oldest neighborhood of Copenhagen. The air is heavy with the smells of pinewood and with music in potentia.",
        storyTrading: {
          story: {
            label: "Joha the Wise Fool",
            icon: "bookofmasks",
            description:
              'In Joha\'s town, a rich patrician was holding a feast, inviting everyone. Joha showed up, but was turned away because of the rags he wore. So Joha dug up the finest set he had, and returned to the feast. This time he was permitted entry. At the feast, his long flowing sleeves where hanging in his cup, and swaying all across his dishes. The people whispered. "Joha! roll back your sleeves." But Joha refused! "Feast, fair sleeves! You deserve this feast as much as I do, for the people in this house respect you more than they respect me."',
            aspects: {
              knock: 2,
              "mariner.storyaspect.mortal": 1,
              "mariner.storyaspect.whimsical": 1,
              "mariner.storyaspect.wise": 1,
            },
          },
          label: "Trade Stories with Baruch ibn Yahya",
          startdescription:
            '"Come in, Come in. settle with us in the shop, if you can find an empty spot. Question is, what language should we tell it in?"',
          description:
            '"The Stories from this house come in Ladino or Hebrew, and from my brother-in-law, Yiddish. My sons have even been infected with the Danish spoken by their friends." I sit back and let the barrage of tales wash over me, with words sometimes translated thrice to make sure I follow.',
        },
        notDiscoverable: true,
      }),
    ],
  });

  generateCodexEntry(
    "codex.harald",
    "[N. Sea] Harald Blomkvist",
    buildCodexEntryBlock(
      {
        "codex.harald.voicelessdevotion":
          "I have met Harald Blomkvist, Composer-Adept who has given up the craft. In his prime he wrote several inspired pieces, but he is most well known is his debut, “Voiceless Devotion”.",
      },
      "I have met Harald Blomkvist. I remember seeing his name above the theater doors. I remember seeing it there less so, of late. Some of his performances were said to portray the Truth of Things."
    ) +
      "\n\n\n" +
      buildCodexEntryBlock(
        {
          "codex.harald.solvedmystery":
            "He has asked me to find his muse, but all I could recover was the end of her story. Her pursuit of change led her first to the Ecdysis club in London, and then to an unmarked grave in the Great North Wood.",
        },
        "He has asked me to find his muse, a Dancer-Adept, seeking to walk “The Old Way”, apparently. I still have to uncover what that is, and where this “Shedder’s Club” can be found."
      )
  );

  generateCopenhagenStreetsEvents();
  generateHaraldMuseQuest();
  generateInstrumentShopQuest();
}
