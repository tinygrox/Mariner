import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateResinEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.copenhagen.randomevent.resin"
  );
  mod.initializeRecipeFile(RECIPE_FILE, [
    "locations",
    "northsea",
    "copenhagen",
  ]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `The Endless Hunger of Artistry`,
    grandReqs: { ...TIMER_ELAPSED("northsea.randomevent.resin", 25) },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.resin") },
    startdescription:
      'As I visit mr Ibn Yahya shop and revel in the smells of sawdust and lacquer. Baruch gestures for me when the shop is empty. "I could use another batch of that ingredient you collected for me. Could you visit the woods again and bring back another batch of resin?"',
    effects: { "mariner.influences.knock": 2 }, //add myster or quest?
    tablereqs: { "mariner.locations.northsea.copenhagen.instrumentshop": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
