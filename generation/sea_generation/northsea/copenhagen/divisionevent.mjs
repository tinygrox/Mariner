import { setWanderingEventChance, wanderingEventScaffhold } from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";

export function generateDivisionEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold("northsea.randomevent.division");
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "northsea", "copenhagen"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `A Stirring Performance at the New Scandinavian`,
    grandReqs: { ...TIMER_ELAPSED("northsea.randomevent.division", 25) },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.division") },
    startdescription:
      "I caught a performance at the New Scandinavian last night. The Production was called the Smiths' Lament, and blast furnace was the performers only acompanyment. As she sang her aria's, she took apart a standing clock, and fed it to the furnace. Even over the roar of the fire, it seemed you could sense the moment that the ticking stopped.",
    effects: {
      "mariner.influences.forge": 1,
      "mariner.influences.lantern": 1,
      "mariner.influences.winter": 1,
      "mariner.experiences.vivid.standby": 1,
    },
    tablereqs: { "mariner.locations.northsea.copenhagen.newscandinavian": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
