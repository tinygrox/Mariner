import { generateSea } from "../sea_generation.mjs";
import { generateVaultLocationElements } from "../../vault_generation/vault_generation.mjs";
import { DOCKSIDE_WAREHOUSE, RESCUE_MISSION, SALVAGE_MISSION, SUNKEN_CITY } from "../../vault_generation/vault_templates.mjs";
import { generateLondon } from "./london/generate_london.mjs";
import { generateAmsterdam } from "./amsterdam/generate_amsterdam.mjs";
import { generateCopenhagen } from "./copenhagen/generate_copenhagen.mjs";
import { generateKerysQuest } from "./kerys.mjs";

export function generateNorthSea() {
  // Generate North Sea
  generateSea({
    portRegion: "northsea",
    label: "North Sea",
    rewardsByType: {
      precious: [
        "mariner.trappings.lantern.1",
        "mariner.trappings.grail.1",
        "mariner.stories.debellismurorum.source",
        "mariner.stories.pineknight.source",
        "mariner.stories.kerys.source",
        "mariner.stories.kerys.source",
        "mariner.stories.kerys.source",
      ],
      jumble: [
        "mariner.trappings.edge.1",
        "mariner.trappings.moth.1",
        "mariner.trappings.heart.1",
        "mariner.stories.lorelei.source",
        "mariner.stories.lorelei.source",
      ],
      scholarly: ["mariner.stories.debellismurorum.source", "mariner.stories.pineknight.source", "mariner.scrapofinformation"],
    },
    weather: {
      mist: 2,
      rain: 2,
      wind: 2,
      storm: 4,
      clear: 3,
      snow: 2,
      bright: 2,
      smog: 2,
    },
  });

  // Generate Ports
  generateLondon();
  generateAmsterdam();
  generateCopenhagen();

  // Vault ports
  generateVaultLocationElements({
    portRegion: "northsea",
    portId: "ekstersnest",
    label: "Amsterdam's Sewers",
    notDiscoverable: true,
    authorizeRetreat: true,
    description: "The subterranean hive of tunnels, sewers and waterways beneath Amsterdam.",
    arrivalEffects: [
      {
        effects: {
          "mariner.vaults.ekstersnest.sewerlabyrinth": 1,
        },
      },
    ],
    recipeContentOnLeave: {
      furthermore: [
        {
          rootAdd: {
            "mariner.flag.smuggler.draw": "-[root/mariner.flag.smuggler.draw]",
          },
        },
      ],
    },
    arrivalLinked: [
      // Generated in the file defining the associated desire merchant, in the bazaar layer
      { id: "mariner.updateauntieesktersdesire.*", randomPick: true },
    ],
    notorietyLabels: [
      {
        label: "Whispers Between Salesmen",
        description: "An echo of a reputation, distorted through the tunnels and dampened by the canals.",
      },
      {
        label: "The Talk of the Bazaar",
        description: "I am now an unlikely story. Unfortunately, those spread quickly among merchants.",
      },
      {
        label: "Unwelcome Guest",
        description: "The Flock is watching out for me. Eyes will be everywhere. beaklike blades will follow.",
      },
      {
        label: "Persona Non-grata",
        description: "The Nest has been sealed. I will not be getting in today.",
      },
    ],
  });

  generateVaultLocationElements({ portRegion: "northsea", ...SUNKEN_CITY });
  generateVaultLocationElements({
    portRegion: "northsea",
    portId: "eddangerforest",
    label: "Eddanger Forest",
    description:
      "On the coastline from which ones the vikings sailed, a peculiar little forest lies. Where the surrounding lands have been logged and cleared to farmland, this has remained. Strange stories are told about the creatures that dwell there, and the plants that grow.",
    arrivalEffects: [
      {
        effects: {
          "mariner.vaults.eddangerforest.wall": 1,
        },
      },
    ],
    recipeContentOnLeave: {
      furthermore: [
        {
          rootAdd: {
            "mariner.flag.forge.draw": "-[root/mariner.flag.forge.draw]",
          },
        },
        {
          rootAdd: {
            "mariner.flag.moth.draw": "-[root/mariner.flag.moth.draw]",
          },
        },
        {
          rootAdd: {
            "mariner.flag.heart.draw": "-[root/mariner.flag.heart.draw]",
          },
        },
      ],
    },
    authorizeRetreat: true,
    notorietyLabels: [
      {
        label: "Eyes In the Forest",
        description: "The forest teems with life. There is no way for us to pass it unobserved.",
      },
      {
        label: "Whispers in the Foliage",
        description:
          "Our actions have been noted. The trees are whispering to the birds, the birds are informing the woodmice, and the woodmice gossip with the hidden things that lurk between the roots.",
      },
      {
        label: "Ire of the Court",
        description: "Those dream-things that live beyond the leaves and behind the rain now follow our every move.",
      },
      {
        label: "The Queen's Attention",
        description:
          "The Ring Yew lives in every tree on earth, yet she does not live in any tree on earth. But if her presence can be fealt anywhere, it is here.",
      },
    ],
  });

  // Repeatable vaults
  generateVaultLocationElements({
    portRegion: "northsea",
    ...DOCKSIDE_WAREHOUSE,
  });
  generateVaultLocationElements({ portRegion: "northsea", ...SALVAGE_MISSION });
  generateVaultLocationElements({ portRegion: "northsea", ...RESCUE_MISSION });

  generateKerysQuest();
}
