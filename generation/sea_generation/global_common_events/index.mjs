import { generateWantedEvent} from  "./wantedevent.mjs";
import { generateStrangeEvent } from "./strangeevent.mjs";
import {generateStormEvent } from "./stormevent.mjs";
import { generateSmogEvent} from "./smogevent.mjs";
import { generateScrapEvent } from "./scrapevent.mjs";
import { generateRainEvent } from "./rainevent.mjs";
import { generatePleasantEvent } from "./pleasantevent.mjs";
import {generateNamelessEvent} from "./namelessevent.mjs";
import {generateConnectionEvent} from "./knocksongevent.mjs";
import {generateGossipEvent} from "./gossipevent.mjs";
import {generateFightEvent} from "./brawlevent.mjs";


export function generateGlobalCommonEvents() {
  generateWantedEvent();
  generateStrangeEvent();
  generateStormEvent();
  generateSmogEvent();
  generateScrapEvent();
  generateRainEvent();
  generatePleasantEvent() ;
  generateNamelessEvent();
  generateConnectionEvent();
  generateGossipEvent();
  generateFightEvent();
}
