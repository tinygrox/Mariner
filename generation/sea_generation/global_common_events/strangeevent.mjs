import { setWanderingEventChance, wanderingEventScaffhold } from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateStrangeEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold("allseas.randomevent.strange");
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Moonlit Ramblings`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.strange", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.strange") },
    startdescription:
      "Last night I found a knot of streets illuminated to unnatural contrasts. Shivering visions arose in that halflight, of a city without doors, figures made of glass and stone and ivory, and weeping stars.",
    effects: { "mariner.experiences.vivid.standby": 1 },
    tablereqs: { "mariner.moon.basic.7": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
