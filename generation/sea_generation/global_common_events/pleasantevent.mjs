import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generatePleasantEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.pleasant"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `The World Brimming`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.pleasant", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.pleasant") },
    startdescription:
      "These days are marked with all the pleasant trimmings of life, and the nights are long and engaging. Life is present in the streets for a while, and my Lack-Heart stays sated.",
    effects: { "mariner.influences.heart": 2 },
    tablereqs: { "mariner.weathermeta.sunny": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
