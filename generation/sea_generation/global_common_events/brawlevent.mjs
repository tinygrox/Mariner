import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateFightEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.fight"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Fisticuffs on the Docks`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.fight", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.fight") },
    startdescription:
      "One of my crew has a high-strung temperament, and recently it has snapped. Blows where traded, and this has not come to the good of my good name, or the health of the crew.",
    effects: { "mariner.notoriety": 1 }, //turn the bar hard to reach
    tablereqs: { "mariner.crew.traits.boisterous": 1 },
    furthermore: [
      {
        target: "~/exterior",
        mutations: {
          "[mariner.crew] && [mariner.crew.traits.boisterous] && [mariner.crew.exhausted]<1":
            {
              mutate: "mariner.crew.exhausted",
              level: 5,
              limit: 1,
            },
        },
      },
    ],
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
