import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateRainEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.rain"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `That Dreaded Deluge`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.rain", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.rain") },
    startdescription:
      "Life has retreated under these constant showers, and as people excuse themselves rather than engage, my boredom is mounting. I wish to leave here soon.",
    effects: { "mariner.wanderlust": 2 },
    tablereqs: { "mariner.weathermeta.rainy": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
