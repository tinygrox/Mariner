import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateNamelessEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.nameless"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Nameless Mantle`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.nameless", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.nameless") },
    effects: { "mariner.influences.moth": 2 },
    startdescription:
      "Today the mist is thick and choking like unspun wool. All light is dimmed and diffused, all faces obscured and all secrets kept hidden.",
    tablereqs: { "mariner.weathermeta.misty": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
