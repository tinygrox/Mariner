import { generateSea } from "../sea_generation.mjs";
import { DOCKSIDE_WAREHOUSE } from "../../vault_generation/vault_templates.mjs";
import { generateVaultLocationElements } from "../../vault_generation/vault_generation.mjs";

export function generatePacificSea() {
  // Sea
  generateSea({
    portRegion: "pacificsea",
    label: "Pacific Sea",
    rewardsByType: {
      precious: ["mariner.trappings.lantern.1"],
      jumble: ["mariner.trappings.edge.1"],
      scholarly: ["mariner.scrapofinformation"],
      ancient: ["mariner.scrapofinformation"],
    },
    weather: {
      mist: 1,
    },
  });
  // Ports

  // Vaults
  generateVaultLocationElements({
    portRegion: "medsea",
    ...DOCKSIDE_WAREHOUSE,
  });
}
