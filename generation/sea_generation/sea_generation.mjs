import { generateSeaRewards } from "../generate_sea_reward_functions.mjs";
import { SIGNALS } from "../generate_signal_aspects.mjs";
import { generateWeather } from "../generate_weather.mjs";
import { generateSeaAILogic } from "../port_generation/generate_ais/agents.mjs";

export function generateSea({ portRegion, label, rewardsByType, weather }) {
  // Create sea aspect
  mod.setAspect("aspects.seas", {
    id: `mariner.seas.${portRegion}`,
    label,
    description: `This is located in the ${label}, or connected to it.`,
  });

  mod.setAspect("aspects.signals", {
    id: `mariner.signal.arriveinsea.${portRegion}`,
  });

  // Cargo for this sea
  mod.setElements(
    "cargo",
    {
      id: `mariner.cargo.for.${portRegion}.local`,
      label: `Cargo [local, destination: ${portRegion}]`,
      aspects: {
        "mariner.cargo": 1,
        "mariner.holduse": 1,
      },
      xtriggers: {
        [SIGNALS.SELL_CARGO]: [
          {
            morpheffect: "break",
            chance: `100 - ([~/extant : mariner.locations.${portRegion}.sailingthere] * 100)`,
          },
          {
            morpheffect: "transform",
            id: "funds",
            level: 2,
          },
        ],
      },
    },
    {
      id: `mariner.cargo.for.${portRegion}.close`,
      label: `Cargo [close, destination: ${portRegion}]`,
      aspects: {
        "mariner.cargo": 1,
        "mariner.holduse": 1,
      },
      xtriggers: {
        [SIGNALS.SELL_CARGO]: [
          {
            morpheffect: "break",
            chance: `100 - ([~/extant : mariner.locations.${portRegion}.sailingthere] * 100)`,
          },
          {
            morpheffect: "transform",
            id: "funds",
            level: 3,
          },
        ],
      },
    },
    {
      id: `mariner.cargo.for.${portRegion}.distant`,
      label: `Cargo [long haul, destination: ${portRegion}]`,
      aspects: {
        "mariner.cargo": 1,
        "mariner.holduse": 1,
      },
      xtriggers: {
        [SIGNALS.SELL_CARGO]: [
          {
            morpheffect: "break",
            chance: `100 - ([~/extant : mariner.locations.${portRegion}.sailingthere] * 100)`,
          },
          {
            morpheffect: "transform",
            id: "funds",
            level: 4,
          },
        ],
      },
    }
  );
  const cargoDeck = mod.getDeck("decks.resources", "mariner.decks.cargo");
  cargoDeck.spec.push(`mariner.cargo.for.${portRegion}.local`);
  cargoDeck.spec.push(`mariner.cargo.for.${portRegion}.close`);
  cargoDeck.spec.push(`mariner.cargo.for.${portRegion}.distant`);

  // Get rumour for this sea + rumour element
  mod.setElement("rumours", {
    id: `mariner.rumour.${portRegion}`,
    label: `A rumour in the ${label}`,
    description: `The chitter chatter of the locals contains old myths, local legends and unlikely stories. This one may lead to an opportunity in the ${label}.`,
    lifetime: 480,
    aspects: {
      "mariner.rumour": 1,
      "mariner.navigate_allowed": 1,
    },
    icon: "rumour",
    slots: [
      {
        id: "information",
        label: "Information",
        actionId: "mariner.navigate",
        required: {
          "mariner.scrapofinformation": 1,
        },
      },
    ],
  });

  mod.readRecipesFile("recipes.explore.bar.getrumour");
  mod.setRecipe("recipes.explore.bar.getrumour", {
    id: `mariner.explore.bar.getrumourfor.${portRegion}`,
    label: `Heard a Rumour about the ${label}`,
    description: `I listen to the many stories shared by the many mouths of a harbor bar. Any of these might contain a seed of opportunity.`,
    effects: {
      [`mariner.rumour.${portRegion}`]: 1,
    },
    aspects: { [SIGNALS.USE_HEART]: 1 },
  });

  // Create the file for ports in this sea
  mod.initializeElementFile(`locations.${portRegion}`, ["locations", portRegion]);

  // Bar files
  mod.readElementsFile("genericbars", ["locations", "bars"]);
  mod.initializeAspectFile(`aspects.discoveredbars.${portRegion}`, ["locations", portRegion]);
  mod.initializeAspectFile(`aspects.displaylocalbar.${portRegion}`, ["locations", portRegion]);
  mod.initializeRecipeFile(`recipes.discoverbars.${portRegion}`, ["locations", portRegion]);
  mod.initializeRecipeFile(`recipes.displaylocalbar.${portRegion}`, ["locations", portRegion]);

  // Wharves files
  mod.readElementsFile("genericwharves", ["locations", "wharves"]);
  mod.initializeAspectFile(`aspects.discoveredwharves.${portRegion}`, ["locations", portRegion]);
  mod.initializeAspectFile(`aspects.displaylocalwharf.${portRegion}`, ["locations", portRegion]);
  mod.initializeRecipeFile(`recipes.discoverwharves.${portRegion}`, ["locations", portRegion]);
  mod.initializeRecipeFile(`recipes.displaylocalwharf.${portRegion}`, ["locations", portRegion]);

  // Stores
  mod.readElementsFile("genericstores", ["locations", "stores"]);
  mod.initializeAspectFile(`aspects.discoveredstores.${portRegion}`, ["locations", portRegion]);
  mod.initializeAspectFile(`aspects.displaylocalstore.${portRegion}`, ["locations", portRegion]);
  mod.initializeRecipeFile(`recipes.discoverstores.${portRegion}`, ["locations", portRegion]);
  mod.initializeRecipeFile(`recipes.displaylocalstore.${portRegion}`, ["locations", portRegion]);

  // Create the file for ports docking recipes in this sea
  mod.initializeRecipeFile(`recipes.dockto.${portRegion}`, ["sailing", "dockto"]);
  // Create the file for ports leaving recipes in this sea
  mod.initializeRecipeFile(`recipes.sailing.leaving.${portRegion}`, ["sailing", "leaving"]);

  // Create a new deck that will contain all ports in this sea
  // CURRENTLY only used to help generating stuff, not used by game content
  mod.setDeck("decks.seas", {
    id: `mariner.decks.locations.${portRegion}.ports`,
    label: `${label} Locations`,
    spec: [],
    resetonexhaustion: true,
  });
  mod.setDeck("decks.seas", {
    id: `mariner.decks.locations.${portRegion}.alldestinations`,
    label: `${label} Locations`,
    spec: [],
    resetonexhaustion: true,
  });

  // Create a new deck that will contain all discoverable ports in this sea
  mod.setDeck("decks.seas", {
    id: `mariner.decks.locations.${portRegion}.discoverable`,
    label: `${label} Locations`,
    spec: [],
    resetonexhaustion: true,
  });

  // Create the file for all port discovery decks of this sea
  mod.initializeDeckFile(`decks.locations.${portRegion}`, ["locations", portRegion]);
  // Create the file for all ports exploration recipes
  mod.initializeRecipeFile(`recipes.explore.${portRegion}`, ["locations", portRegion]);
  mod.initializeRecipeFile(`recipes.discover.${portRegion}`, ["locations", portRegion]);
  mod.initializeRecipeFile(`recipes.inspectpackages.${portRegion}`, ["packages", portRegion]);
  mod.initializeElementFile(`packages.${portRegion}`, ["packages", portRegion]);

  // Create the file containing the aspects needed to travel (lock/unlock and stuff)
  mod.initializeAspectFile(`aspects.seas.${portRegion}`, ["locations", portRegion]);

  // Aspect put into docked port versions, to tell which sea to unlock when docking there, before we arrive
  mod.setHiddenAspect(`aspects.seas.${portRegion}`, {
    id: `mariner.locations.${portRegion}.sailingthere`,
    label: `Sailing to a place connected to ${label}`,
    description: `Sailing to a place connected to ${label}, so we should unlock things of this sea when arriving`,
    decayTo: "anything really",
  });
  // Aspect to put into locked port versions, to flip them into their available version when we want to
  mod.setAspect(`aspects.seas.${portRegion}`, {
    id: `mariner.locations.${portRegion}.presentthere`,
    label: `Present in ${label}`,
    description: `Present in ${label} (unavailable, might get deleted in the future)`,
    decayTo: "anything really",
  });

  generateSeaRewards(portRegion, rewardsByType);
  generateWeather(portRegion, weather);
  generateSeaAILogic(portRegion);
}
