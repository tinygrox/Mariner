import { repeatableConversation, convertSourceConversation } from "../../../port_generation/helpers.mjs";

const PARTS = [
  // SUIT
  "judgesuit",
  "suit",
  "dress",
  "militarysuit",
  // TOP
  "judgewig",
  "shorthair",
  "longhair",
  "fez",
  "habit",
  "militaryhat",
  // FACE
  "scarredeye",
  "moustache",
  "beard",
  "monocle",
  "glasses",
];

const COSTUMES = [
  // GENTLEMAN 1
  ["shorthair", "moustache", "suit"],
  // GENTLEMAN 2
  ["beard", "suit"],
  // GENTLEMAN 3
  ["beard", "suit", "glasses", "fez"],
  // GENTLEMAN 4
  ["moustache", "fez", "suit"],
  // WOMAN 1
  ["dress", "longhair"],
  // WOMAN 2
  ["judgesuit", "longhair"],
  // NUN
  ["judgesuit", "habit"],
  // PRIEST
  ["judgesuit", "habit", "beard", "moustache"],
  // JUDGE 1
  ["judgewig", "moustache", "judgesuit"],
  // JUDGE 2
  ["judgewig", "moustache", "judgesuit", "monocle"],
  // MILITARY 1
  ["scarredeye", "militaryhat", "militarysuit", "moustache"],
  // MILIRARY 2
  ["scarredeye", "militaryhat", "militarysuit", "beard", "glasses"],
];

function buildThatDamnOverlay(isDistant = false) {
  if (!mod.aspects["touristcostume"]) {
    mod.initializeAspectFile("touristcostume", ["locations", "medsea", "constantinople"]);
    mod.setAspects(
      "touristcostume",
      { id: "mariner.touristcostumeid", label: "<Tourist costume id>" },
      {
        id: "mariner.touristcostumemanager",
        label: "<Tourist costume manager",
        timers: {
          costumechange: {
            frequency: 1000 * 60 * 4,
            morpheffects: [
              {
                morpheffect: "setmutation",
                id: "mariner.touristcostumeid",
                level: `Random(0, ${COSTUMES.length - 1})`,
                vfx: "None", //"CardSpend",
              },
            ],
          },
        },
      }
    );
  }

  const overlays = [
    { image: `locations/medsea/constantinople/tourist`, ...(isDistant ? { grayscale: true, color: "#000000" } : {}) },
  ];
  const parts = Object.fromEntries(PARTS.map((id) => [id, []]));
  for (let i = 0; i < COSTUMES.length; i++) {
    const costume = COSTUMES[i];
    for (const part of costume) {
      parts[part].push(i);
    }
  }

  for (const [partId, costumeIds] of Object.entries(parts)) {
    overlays.push({
      image: `locations/medsea/constantinople/tourist_${partId}`,
      expression: costumeIds.map((index) => `([mariner.touristcostumeid] = ${index})`).join(" or "),
      ...(isDistant ? { grayscale: true, color: "#000000" } : {}),
    });
  }

  overlays.push(`locations/medsea/constantinople/constantinople_frame`);
  return overlays;
}

export const TOURIST = {
  type: "npc",
  elementId: "tourist",
  overlays: buildThatDamnOverlay(),
  distantOverlays: buildThatDamnOverlay(true),
  label: "Count Florentin Hunyadi",
  description:
    "The last scion of an old house, who has left the management of his holdings to others as he spends his middle age in an eternal vacation.",
  localAspects: { "mariner.touristcostumemanager": 1 },
  awayAspects: { "mariner.touristcostumemanager": 1 },
  deliveryOptions: {
    previousPackageLocations: [],
    knownLanguages: [],
    usedNames: [],
    usedInitials: [],
    gender: "male",
    whatToDoWithIt: ["<>", "<>"],
  },
  deliveryRiddles: [
    "<An aristocrat who plays at Fool>",
    "<Master-in-Exile>",
    "<Pilgrim to the Goddess in the Holiest of Holy>",
    "<Follower of the Footloose, walkabouts in Europe.>",
  ],
  storyTrading: {
    story: {
      label: "The Proverbial Footloose",
      icon: "onthewhite",
      description:
        '<One day, a foolish company where traveling through the mountains. They where aiming to reach the sun, but they did not know the way. On a log, they meet a Centipede, who scold them for going the wrong way. "The Sun cannot be reached at night and to go up one must go down, even children know that." She promises to lead them the way. But with her hundred tapping feet, the group cannot keep pace, and when evening falls, and the pale moon cannot pierce the canopy, they are completely lost. Soon, creatures of the light and angles and fury dropped down from the branches. First the group cursed the Centipede, but then, the youngest among them considered her lessons. To reach heights; one must travel down. When one is lost, you should close your eyes. Led by intuition, they stumbled down the slopes, until they reached a golden disk. The moon, reflected in the cool pools of the Woods.>',
      aspects: {
        winter: 2,
      },
    },
    label: "<Trade stories with the Count>",
    startdescription: '"<Of course! What a pleasure to be asked... though I no not always wait for the invitation.>"',
    description: '<This story is a common one accross the various branches of Pilgrims and Vageries. Come! listen, and assess.>"',
  },
  conversations: [
    repeatableConversation({
      id: "songmoth",
      element: "mariner.song.moth",
      label: "<Discuss the Power of my Songs>",
      startdescription: '"Seeking to tease out the powers more felt then understood, are you?"',
      description:
        '"Moth\'s dances are not jus directed by a single dancemaster, but take inspiration from many muses. With moth as an intend, you can muddle the mind, under the Moths wild guidance, or you can hide your steps from pursuers, offering them to the Velvet for save keeping. But my favorite is the Vagabonds Dance, aimed at the Direction of The Sun, which reinacts some fragment of her revenge on Miah.".',
    }),
    repeatableConversation({
      id: "songlantern",
      element: "mariner.song.lantern",
      label: "<Discuss the Power of my Songs>",
      startdescription: '"Seeking to tease out the powers more felt then understood, are you?"',
      description:
        '"Ach, that song of Light.... Light has faded, has it not? And does the world not look dramatic when the sun is low and the shadows lengthen... It is said that in her guise of the Laughting-Trush, the vagabond was a close confidant of the Su nin Splendor. But then all guises where banned from the House. I too was banned from my House, until I was the last one left to steward it... But there too, I am letting shadows lengthen. Lantern is the direction of Authority, Rule and the High noon Sun".',
    }),
    repeatableConversation({
      id: "songedge",
      element: "mariner.song.edge",
      label: "<>Discuss the Power of my Songs",
      startdescription: '"Seeking to tease out the powers more felt then understood, are you?""',
      description:
        '"The Songs of Battle. Do you hear the thunder of the drums? It seems they sound through Europe so often now, nein? I do not miss my time as an Officer. Ofcourse, that army does not even exist anymore. Still, those drums and trumpets try to rouse something in you no? Nationalism, Idealism, Vigor. That is Edge\'s Intent.".',
    }),
    repeatableConversation({
      id: "hyksos",
      element: "mariner.vaults.damascus.glyphs.hidden",
      label: "<Discuss the Glyphs with Florentin>",
      startdescription: '"Found something of note on your ramblings?"',
      description:
        '"Well I don\'t think my answers will delight you today." Florentin smiles his beaming smile. "Have I ever encountered these? Yes. But do I know what they are? No, not really." He shrugs apologetically "All I know is that when it shows up, I am unlikely to advance much farther. I am not like my mistress yet".',
    }),
    repeatableConversation({
      id: "chest",
      element: "mariner.vaults.thebes.chest",
      label: "Discuss the Warded Chest with Florentin",
      startdescription: '"What is that? You brought me a present? I jest! I jest..."',
      description:
        '"hmm, quite thoroughly wrapped. And knowing what I know, those hooked markings mean trouble. And chains I avoid whenever I can... But I know who may be able to help. Have you met Mother Euphresme? She knows most things about most things, and she can usually be tempted with curiosity."',
    }),
    repeatableConversation({
      id: "halfheart",
      element: "mariner.halfheart",
      label: "<Ask Count Hunyadi after Half-Hearts>",
      startdescription: '"<Yes, I am familiar with the term>"',
      description:
        '"Even amongst the disorganised ranks of the Vageries, the Half-Hearts hold special significance, setting of on a pilgrimage quite singular. If you are truely on such a path, you might find yourself a point of interest to other Pilgrims on the Vagabonds path. You might find yourself becoming a port of Pilgrimage soon."',
      effects: {},
    }),
    //repeatableConversation({
    //  id: "lackheart",
    //   element: "mariner.lackheart",
    //  label: "<Ask Count Hunyadi after Emptyness>",
    //  startdescription: '"<>"',
    //  description:
    //    "\"\"",
    //  effects: {}
    //}),
    repeatableConversation({
      id: "quest.1",
      element: "mariner.song.knock",
      label: "<>",
      startdescription: '"<>"',
      description: '""',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.2",
      element: "mariner.song.knock",
      label: "<>",
      startdescription: '"<>"',
      description: '""',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.3",
      element: "mariner.song.knock",
      label: "<>",
      startdescription: '"<>"',
      description: '""',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.4",
      element: "mariner.song.knock",
      label: "<>",
      startdescription: '"<>"',
      description: '""',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.5",
      element: "mariner.song.knock",
      label: "<>",
      startdescription: '"<>"',
      description: '""',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.6",
      element: "mariner.song.knock",
      label: "<>",
      startdescription: '"<>"',
      description: '""',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.7",
      element: "mariner.song.knock",
      label: "<>",
      startdescription: '"<>"',
      description: '""',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.8",
      element: "mariner.song.knock",
      label: "<>",
      startdescription: '"<>"',
      description: '""',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.9",
      element: "mariner.song.knock",
      label: "<>",
      startdescription: '"<>"',
      description: '""',
      effects: {},
    }),
    repeatableConversation({
      id: "vagabonddedication",
      element: "mariner.ascension.vagabond.dedication",
      label: "<Talk to Count Hunyadi about my Path>",
      startdescription: '"<So you are a Halfheart, and you made your choice?>"',
      description:
        '"I am glad to hear you chose the path of the Clever Mistress. Every Pilgrimage is unique, but few will see such sites as you will. I have to admit a degree of jealousy!"',
      effects: {},
    }),
    repeatableConversation({
      id: "twinsdedication",
      element: "mariner.ascension.twins.dedication",
      label: "<Talk to Count Hunyadi about my Path>",
      startdescription: '"<So you are a Halfheart, and you made your choice?>"',
      description:
        '"A pity, such a grand voyage could have been ahead of you. But still, I wish you luck on your journey, none the less. I hope you find your Other Half."',
      effects: {},
    }),
  ],
};
