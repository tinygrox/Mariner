import { LOCAL_TO } from "../../../generation_helpers.mjs";
import { QUEST_FLAG_ID } from "../../../global_flags_generation.mjs";
import { UPDATE_QUEST } from "../../../quest_generation.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";

export function generateBuriedSecretsQuest() {
    const RECIPE_FILE = mod.initializeRecipeFile(
        "recipes.medsea.constantinople.quest.buriedsecrets",
        ["locations", "medsea", "constantinople"]
    );
    const ELEMENT_FILE = mod.initializeElementFile(
        "medsea.constantinople.quest.buriedsecrets",
        ["locations", "medsea", "constantinople"]
    );
    const QUEST_PREFIX = "mariner.medsea.constantinople.buriedsecrets";
    const MYSTERY_PREFIX = `${QUEST_PREFIX}.mystery`;

    // Beginning
    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.first`,
        label: "Old wounds Ache",
        actionId: "talk",
        startdescription:
            "I am admitted into the Sisterhood of Contemplation with stern permissiveness. Mother Euphresme’s many strange visitors are expected, tolerated, but never celebrated. She greets me in her stark cell, so unlike her workspaces. Always cluttered.",
        description:
            '"\"I wondered if you could assist me in a private matter. No reason to trouble the church patriarchs with… I would do it myself, but I do not travel so well anymore, so I am obliged to bid your assistance. After all, I aided you with your sodden treasure, so I already know you know your way around Luxor…"',
        maxexecutions: 1,
        craftable: true,
        grandReqs: { [READ_QUEST_FLAG("luxor", "cleared")]: 1, },
        requirements: {
            ...LOCAL_TO("medsea", "constantinople", "nun"),
        },
        warmup: 60,
        linked: [`${QUEST_PREFIX}.second`]
    });

    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.second`,
        label: "Secrets",
        actionId: "talk",
        description:
            '\"To ensure the safety of myself and others, I had to rid myself of some incriminating papers quickly… I buried them, in the graveyard of a coptic church in a neighborhood crowding the water\'s edge. I implored with powers sacred and hidden for it to remain hidden, and as I sit here before you safely, it has been. But now I need the papers once again, and they must be recovered. So please, would you travel to Luxor to lend some aid to an old woman? \"',
        craftable: false,
        warmup: 10,
        effects: { 'mariner.quests.buriedsecrets': 1 },
        furthermore:
            [UPDATE_QUEST("buriedsecrets", "beginnings")],
    });

    // Last
    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.last`,
        label: "A Whisper of Reunions",
        actionId: "talk",
        startdescription:
            "She grabs the stained box with reverence, and carefully removes the papers from inside her small, copper glasses pressed to her nose, she pours over the pages. \"The secret was kept, even from my own skull. But now I know; I know.\"",
        description:
            'Then she pulls out a small vial, filled with a violet liquid that is almost the gray of morning mists. “Stand back” she warns, before she pours the liquid over the box and the objects. And with a great hissing and writing of vapor, each dissolves into puddles, then dust, and then even less. “I left my secrets to the velvet, and she held them very close indeed. I could not recall their names, their faces, or where I send them. Now I know, and now I have disposed of the evidence safely. They might have stayed safer where they were but I am getting old, and I could not leave things as they where. My former sisterhood needs to come together one final time, before I am too old to organize it or enjoy it.\"',
        craftable: true,
        requirements: {
            ...LOCAL_TO("medsea", "constantinople", "nun")

        },
        grandReqs: {
            [READ_QUEST_FLAG("buriedsecrets", "secret")]: 1,
        },

        warmup: 60,
        linked: [`${QUEST_PREFIX}.last.second`]
    });

    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.last.second`,
        label: "Sisterhood: Reunion",
        actionId: "talk",
        description:
            '\"Now I am not their matron any more, and I have no power to summon them. But I can invite them. Through you, I hope. I will let you know where I send each of my sisters, but I worry I will have to rely on you for the leg work.\"',
        craftable: false,
        warmup: 10,
        effects: { "mariner.mysteries.sisterhood.medsea": 1 },
        furthermore:
            [UPDATE_QUEST("buriedsecrets", "completed"), { effects: { "mariner.quests.buriedsecrets": -1 } }]
    });
}
