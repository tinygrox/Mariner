import {
  repeatableConversation,
  convertSourceConversation,
  uniqueConversation,
} from "../../../port_generation/helpers.mjs";

export const NUN = {
  type: "npc",
  elementId: "nun",
  label: "<Mother Euphresme>",
  description:
    "<An Orthodox cloistered nun who cannot be considered to be very cloistered or orthodox in her views.>",
  deliveryOptions: {
    previousPackageLocations: [],
    knownLanguages: [],
    usedNames: [],
    usedInitials: [],
    gender: "male",
    whatToDoWithIt: ["<>", "<>"],
  },
  deliveryRiddles: [
    "<The oldest sister of the holiest city.>",
    "<Contemplative, Mischievious, Ascetic>",
    "<Linchpin in the Bizantine web of Orthodox and heterodox>",
    "<Secret Keeper, knot unfirler, hosti-eater>",
    "<she is in the habit of drinking her coffee with Cardemon>",
    "<Respected for her wisdom, remembered for her wit>",
  ],
  storyTrading: {
    story: {
      label: "The Saint of Flowers",
      icon: "onthewhite",
      description:
        "<Once, a town could be found carved in the slopes of the Troodos mountains, where a cypress grove provided for the people of the town. A vile foreigner came to town. Intent on breaking open the earth and wringing value from the stones. As he worked, the forest dwindled. The matrons of the forest came together, and vowed to to trap him and put a stop to it. The oldest sharpened her blades, and the youngest seduced him and stole his wits from him. The richest took from him all that held value, until his tools could do no more harm. When the trap sprung and he was led to the old mother and her blades, he fell to his knees and begged, wishing for redemption. But the women determined there was no redemption for him in this life, but that he still had one thing of value to give, to purify. and so the oldest cut of his head, the youngest kissed him and the richest planted a seed. And from his body grew Saint Nympha, restoring in wisdom and piety.>",
      aspects: {
        moth: 2,
        knock: 2,
        "mariner.storyaspect.horomachistry": 1,
        "mariner.storyaspect.tragic": 1,
        "mariner.storyaspect.occult": 1,
        "mariner.storyaspect.cthonic": 1,
        "mariner.storyaspect.dionysian": 1,
      },
    },
    label: "<Trade stories with Mother Euphresme>",
    startdescription:
      '"<I have collected many stories in my long years. True ones, false ones. stories valued and stories worthwile.>"',
    description:
      '<I will leave you with a story I do not tell often any more, but which I once shared with seven sisters.>"',
  },
  conversations: [
    repeatableConversation({
      id: "musicknock",
      element: "mariner.song.knock",
      label: "<Discuss the Power of my Songs>",
      startdescription:
        '"<You wish to discuss with me the Strength of Liturgy?>"',
      description:
        '"<Knock truely is the one above all, or rather, the one between. There is no message without the medium. A sherperds shouts are useless if they cannot reach his flock. Keep Knock as your intent, and your message can carry into the Lower Skies, and call to you the spirits that dwell there.>"',
    }),
    repeatableConversation({
      id: "musicwrongforge",
      element: "mariner.song.forge",
      label: "<Discuss the Power of my Songs>",
      startdescription:
        '"<You wish to discuss with me the Strength of Liturgy>"',
      description:
        '"<I may tinker now and again, but I am not an adept of the mysteries of flame, but the teachings of the Leashed Flame did not make it to constantinople, not in any History, the Sisterhood made sure of that. I heard the French kept some element of their ancient enemy alive, in secret vaults or secret verse, stolen power studied.>"',
    }),
    repeatableConversation({
      id: "musicwronglantern",
      element: "mariner.song.lantern",
      label: "<Discuss the Power of my Songs>",
      startdescription:
        '"<You wish to discuss with me the Strength of Liturgy>"',
      description:
        '"<The words of Light and Glory? Well I see why you would think of me. But my church is not dedicated to Sol Invictus, not in this history at least. I would seek those mysteries more eastwards, where the Sun\'s rule is still remembered most fiercely.>"',
    }),
    uniqueConversation({
      id: "chest",
      element: "mariner.vaults.thebes.chest",
      label: "<Ask for help with a Ward>",
      startdescription: '"<A Ward on a Chest?>"',
      description:
        '"Went to plunder the Holy Lands, did you? Oh, Egypt this time. Well even if it\'s not holy to me it is holy to someone, and has been, for a long time. And some of those sanctities still hold, such as on that treasure of yours. I will help you, if only to spare you of the terrors that befall you when you open it un supervised... and perhaps to sate my own curiosities. Bring to me the proper implements, we can open it together, here in my shop."',
      effects: {
        "mariner.vaults.thebes.chest.revealed": 1,
        "mariner.vaults.thebes.chest": -1,
      },
    }),
    repeatableConversation({
      id: "hyksos",
      element: "mariner.vaults.damascus.glyphs.hidden",
      label: "<Ask for Euphresme about the strange glyphs>",
      startdescription: '"Found a Script you are unfamiliar with?"',
      description:
        '"<I am afraid I cannot help you. My studies have focussed on greek, cyrillic and it derivates, this seems far older then my ken. "',
    }),
    repeatableConversation({
      id: "halfheart",
      element: "mariner.halfheart",
      label: "<Talk to mother Euphresme about the Half Heart>",
      startdescription: '"<Do I know about Half-Hearts?>"',
      description:
        '"The term was never uttered here, but what you describe sounds familair. One my my sisters, placed in my care against her will. She withered in captivity, pining at the window for sea and song."',
    }),
    repeatableConversation({
      id: "lackheart",
      element: "mariner.lackheart",
      label: "<Talk to Mother Euphresme about the Darkness Inside>",
      startdescription: '"<Life eats away at you, does it now?>"',
      description:
        '"Now Imagine you lived as long as me. So many edges have gotten sanded away, does this wreck of a soul still resemble the young woman I once was. But I like to view it more as the ages leaving a patia each layer adding to lustre of my soul."',
    }),
    repeatableConversation({
      id: "quest.student.1",
      element: "mariner.song.knock",
      label: "<Discuss the Case of the Missing Muse>",
      startdescription: '"<>"',
      description:
        '"I caught that story. They carry french papers at the kiosk on fridays. I buy them whenever I can to keep up with the language. One week from great gorey speculation to no facts at all… you can smell there is more going on there then they are letting out, I can tell that all the way from here."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.student.2",
      element: "mariner.song.knock",
      label: "<Discuss the Case of the Missing Muse>",
      startdescription: '"<>"',
      description:
        '"Self flagellation, self mortification, practices that have been supposed to purify and exalt people for ages. This is the first time I have heard it invoked in the name of science and knowledge. Although there is that story about the smallpox vaccine."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.stench.1",
      element: "mariner.song.knock",
      label: "<Discuss the Stench of Marseille>",
      startdescription: '"<>"',
      description:
        '"So Sereno asked you for a favor… Blackwood, Schwarzwald or something Sombre perhaps? Well, whoever he is, he may not be original, but at least he knows a modicum of the histories."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.truth.1",
      element: "mariner.song.knock",
      label: "<Discuss the Recovery of Discarded Truth>",
      startdescription: '"<>"',
      description:
        '"Desperation and skill can yield terrifying results. I will not call what I did foolish, for it was necessary, but it was ill considered. I did not think that would wrench the knowledge from myself as well, not to mention the effect it had on the city..."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.reunion.1",
      element: "mariner.song.knock",
      label: "<Discuss the Reunion of the Sisterhood>",
      startdescription: '"<>"',
      description:
        '"Let me know once you have made contact further afield in the world. I spread my seven sisters as far as my reach went, and who knows how much further they flew themselves."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.towers.1",
      element: "mariner.song.knock",
      label: "<Discuss the Reverie of the Crumbling Towers>",
      startdescription: '"<>"',
      description:
        '"Truth, serenity and divinity are such elusive goals. There are a thousand thousand paths up the mountain. But it takes a rare breed to refuse the steps, but not the climb."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.caper.1",
      element: "mariner.song.knock",
      label: "<Discuss the Caper of the Copper Manse>",
      startdescription: '"<>"',
      description:
        '"They have plans to break in where? Hmmm. How to define the ethics of robbing a robber. Never covet what is possessed by another man, of course… however, are those that make a living of others misery man or demon? I am sure it\'s not my place to say."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.wist.1",
      element: "mariner.song.knock",
      label: "<Discuss the Path to Constellation>",
      startdescription: '"<>"',
      description:
        '"Ah, The Noonwell… I have heard it was once in Eden, but has been steadily traveling west. Perhaps it seeks for the sunset to sink into its oblivion? It has left our continent for centuries hence, but it has been found here at least twice. Each time mortals found it to claim its grace."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.wist.2",
      element: "mariner.song.knock",
      label: "<Discuss the Path to Constellation>",
      startdescription: '"<>"',
      description:
        '"Obscurity is a wound for mortals, yes, not just being not famous, but being truly unknown. We have not been made to exist in isolation. To know and to be known. For none to know your being, your true self, leaves a wound upon the soul."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.wist.3",
      element: "mariner.song.knock",
      label: "<Discuss the Path to Constellation>",
      startdescription: '"<>"',
      description:
        '"I had heard of exiles from the Obliviates, but I never thought about the mechanics of that. A reversing ritual.. A mending of the strings cut between the obscured and the rest of the world. I could see the Elegiast sponsoring such a deal. He remembers all, after all."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.imago.1",
      element: "mariner.song.knock",
      label: "<Discuss the Exemplary Path>",
      startdescription: '"<>"',
      description: '""',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.imago.2",
      element: "mariner.song.knock",
      label: "<Discuss the Exemplary Path>",
      startdescription: '"<>"',
      description:
        '"The House of Lethe is a bit further back then my usual light reading, I am afraid. But I know they were spread out through the Levant and North africa. The cradle of civilisation… of this civilisation at least."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.muse.1",
      element: "mariner.song.knock",
      label: "<Discuss the Chase of the Missing Muse>",
      startdescription: '"<>"',
      description:
        '"I have heard his opera’s performed here in the cafe nocturna. It had been translated into Greek, but laced with …. In those days Greek theater was at its height! Today it is all meant to be Turkish in public of course. Not so unlike what his girl fled in Bremen perhaps?"',
      effects: {},
    }),
    //repeatableConversation({
    //  id: "<asce>",
    //  element: "mariner.ascension.vagabond.dedication",
    //  label: "<>",
    //  startdescription: '"<>"',
    //  description:
    //    "\"\"",
    //  effects: {}
    //}), repeatableConversation({
    //  id: "<>",
    //  element: "mariner.ascension.twins.dedication",
    //  label: "<>",
    //  startdescription: '"<>"',
    //  description:
    //    "\"\"",
    //  effects: {}
    //}),
  ],
};

mod.readRecipesFile("recipes.imago", ["mysteries", "imago"]);
mod.setRecipe("recipes.imago", {
  id: "repeated.translation",
  label: "Inquire for another copy of the Translation of the Imago",
  startdescription:
    "Were you careless or just greedy? No mind. I can circumvent the wards again, but this time, I ask for a donation for the supplies. This drafty old building doesn't keep itself standing, you know. ",
  description:
    "The motions are measured, the incantation precise. after painful minutes the chest can be opened once more, and once more, I must hastily copy the script. Afterwards she rubs her hands together and tells me to be sure I get what I need out of it.",
  warmup: 30,
  actionid: "talk",
  craftable: true,
  requirements: {
    "mariner.matthiasamethystpursuit": -1,
    "mariner.booktalksabout.imago": 1,
    "mariner.locations.medsea.constantinople.nun": 1,
  },
  effects: { "mariner.matthiasamethystpursuit": 1 },
  slots: [
    {
      id: "funds1",
      label: "Funds",
      description: "Recompense for services rendered. Alms offered.",
      greedy: true,
      required: {
        funds: 1,
      },
    },
    {
      id: "funds2",
      label: "Funds",
      description: "Recompense for services rendered. Alms offered.",
      greedy: true,
      required: {
        funds: 1,
      },
    },
    {
      id: "funds3",
      label: "Funds",
      description: "Recompense for services rendered. Alms offered.",
      greedy: true,
      required: {
        funds: 1,
      },
    },
  ],
});

//mod.readRecipesFile("recipes.mystery.sisterhood", ["mysteries", "sisterhood"]);
//mod.setRecipe("recipes.mystery.sisterhood", {
//  id: "recipes.sisterhood.medsea.solved",
//  label: "",
//  startdescription:
//    "",
//  description:
//    "",
//  warmup: 30,
//  actionid: "mariner.navigate",
//  craftable: true,
//  requirements: {
//    "mariner.trappingstype.crafts": 1,
//    "mariner.stories.unburned": 1,
//    "mariner.waypoint.east": 1,
//  },
//  effects: { "mariner.matthiasamethystpursuit": 1 }
//});
