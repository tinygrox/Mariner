import { LOCAL_TO } from "../../../generation_helpers.mjs";

export function generateHeistHints() {

    const RECIPE_FILE = mod.initializeRecipeFile(
        "recipes.medsea.constantinople.quest.tinyheist.hints",
        ["locations", "medsea", "constantinople"]
    );
    const QUEST_PREFIX = "mariner.medsea.constantinople.tinyheist";
    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.fulfilled.hint`,
        label: "Preparations and Reparations?",
        actionId: "talk",
        startdescription:
            "\"You have collected what I need?\"",
        hintOnly: true,
        requirements: {
            ...LOCAL_TO("medsea", "constantinople", "tourist"),
            'mariner.quests.tinyheist': 1,
            'mariner.globalflags.quests.tinyheist.salutations': 1,
            "mariner.tool.knock": -1,
            "mariner.trappings.moth": 1,
            "mariner.song.grail": 1,
            "mariner.influences.heart": 1
        },
        warmup: 60,
    });
    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.fulfilled.hint2`,
        label: "Preparations and Reparations?",
        actionId: "talk",
        startdescription:
            "\"You have collected what I need?\"",
        hintOnly: true,
        requirements: {
            ...LOCAL_TO("medsea", "constantinople", "tourist"),
            'mariner.quests.tinyheist': 1,
            'mariner.globalflags.quests.tinyheist.salutations': 1,
            "mariner.tool.knock": 1,
            "mariner.trappings.moth": -1,
            "mariner.song.grail": 1,
            "mariner.influences.heart": 1
        },
        warmup: 60,
    });
    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.fulfilled.hint3`,
        label: "Preparations and Reparations?",
        actionId: "talk",
        startdescription:
            "\"You have collected what I need?\"",
        hintOnly: true,
        requirements: {
            ...LOCAL_TO("medsea", "constantinople", "tourist"),
            'mariner.quests.tinyheist': 1,
            'mariner.globalflags.quests.tinyheist.salutations': 1,
            "mariner.tool.knock": 1,
            "mariner.trappings.moth": 1,
            "mariner.song.grail": -1,
            "mariner.influences.heart": 1
        },
        warmup: 60,
    });
    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.fulfilled.hint4`,
        label: "Preparations and Reparations?",
        actionId: "talk",
        startdescription:
            "\"You have collected what I need?\"",
        hintOnly: true,
        requirements: {
            ...LOCAL_TO("medsea", "constantinople", "tourist"),
            'mariner.quests.tinyheist': 1,
            'mariner.globalflags.quests.tinyheist.salutations': 1,
            "mariner.tool.knock": 1,
            "mariner.trappings.moth": 1,
            "mariner.song.grail": 1,
            "mariner.influences.heart": -1
        },
        warmup: 60,
    });
    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.fulfilled.hint5`,
        label: "Preparations and Reparations?",
        actionId: "talk",
        startdescription:
            "\"You have collected what I need?\"",
        hintOnly: true,
        requirements: {
            ...LOCAL_TO("medsea", "constantinople", "tourist"),
            'mariner.quests.tinyheist': 1,
            'mariner.globalflags.quests.tinyheist.salutations': 1,
            "mariner.tool.knock": -1,
            "mariner.trappings.moth": -1,
            "mariner.song.grail": 1,
            "mariner.influences.heart": 1
        },
        warmup: 60,
    });
    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.fulfilled.hint6`,
        label: "Preparations and Reparations?",
        actionId: "talk",
        startdescription:
            "\"You have collected what I need?\"",
        hintOnly: true,
        requirements: {
            ...LOCAL_TO("medsea", "constantinople", "tourist"),
            'mariner.quests.tinyheist': 1,
            'mariner.globalflags.quests.tinyheist.salutations': 1,
            "mariner.tool.knock": -1,
            "mariner.trappings.moth": 1,
            "mariner.song.grail": -1,
            "mariner.influences.heart": 1
        },
        warmup: 60,
    });
    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.fulfilled.hint7`,
        label: "Preparations and Reparations?",
        actionId: "talk",
        startdescription:
            "\"You have collected what I need?\"",
        hintOnly: true,
        requirements: {
            ...LOCAL_TO("medsea", "constantinople", "tourist"),
            'mariner.quests.tinyheist': 1,
            'mariner.globalflags.quests.tinyheist.salutations': 1,
            "mariner.tool.knock": -1,
            "mariner.trappings.moth": 1,
            "mariner.song.grail": 1,
            "mariner.influences.heart": -1
        },
        warmup: 60,
    });
    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.fulfilled.hint8`,
        label: "Preparations and Reparations?",
        actionId: "talk",
        startdescription:
            "\"You have collected what I need?\"",
        hintOnly: true,
        requirements: {
            ...LOCAL_TO("medsea", "constantinople", "tourist"),
            'mariner.quests.tinyheist': 1,
            'mariner.globalflags.quests.tinyheist.salutations': 1,
            "mariner.tool.knock": 1,
            "mariner.trappings.moth": -1,
            "mariner.song.grail": -1,
            "mariner.influences.heart": -1
        },
        warmup: 60,
    });

    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.fulfilled.hint9`,
        label: "Preparations and Reparations?",
        actionId: "talk",
        startdescription:
            "\"You have collected what I need?\"",
        hintOnly: true,
        requirements: {
            ...LOCAL_TO("medsea", "constantinople", "tourist"),
            'mariner.quests.tinyheist': 1,
            'mariner.globalflags.quests.tinyheist.salutations': 1,
            "mariner.tool.knock": 1,
            "mariner.trappings.moth": -1,
            "mariner.song.grail": 1,
            "mariner.influences.heart": -1
        },
        warmup: 60,
    });
    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.fulfilled.hint10`,
        label: "Preparations and Reparations?",
        actionId: "talk",
        startdescription:
            "\"You have collected what I need?\"",
        hintOnly: true,
        requirements: {
            ...LOCAL_TO("medsea", "constantinople", "tourist"),
            'mariner.quests.tinyheist': 1,
            'mariner.globalflags.quests.tinyheist.salutations': 1,
            "mariner.tool.knock": 1,
            "mariner.trappings.moth": 1,
            "mariner.song.grail": -1,
            "mariner.influences.heart": -1
        },
        warmup: 60,
    });
    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.fulfilled.hint11`,
        label: "Preparations and Reparations?",
        actionId: "talk",
        startdescription:
            "\"You have collected what I need?\"",
        hintOnly: true,
        requirements: {
            ...LOCAL_TO("medsea", "constantinople", "tourist"),
            'mariner.quests.tinyheist': 1,
            'mariner.globalflags.quests.tinyheist.salutations': 1,
            "mariner.tool.knock": -1,
            "mariner.trappings.moth": -1,
            "mariner.song.grail": -1,
            "mariner.influences.heart": 1
        },
        warmup: 60,
    });
    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.fulfilled.hint12`,
        label: "Preparations and Reparations?",
        actionId: "talk",
        startdescription:
            "\"You have collected what I need?\"",
        hintOnly: true,
        requirements: {
            ...LOCAL_TO("medsea", "constantinople", "tourist"),
            'mariner.quests.tinyheist': 1,
            'mariner.globalflags.quests.tinyheist.salutations': 1,
            "mariner.tool.knock": 1,
            "mariner.trappings.moth": -1,
            "mariner.song.grail": -1,
            "mariner.influences.heart": -1
        },
        warmup: 60,
    });
    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.fulfilled.hint13`,
        label: "Preparations and Reparations?",
        actionId: "talk",
        startdescription:
            "\"You have collected what I need?\"",
        hintOnly: true,
        requirements: {
            ...LOCAL_TO("medsea", "constantinople", "tourist"),
            'mariner.quests.tinyheist': 1,
            'mariner.globalflags.quests.tinyheist.salutations': 1,
            "mariner.tool.knock": 1,
            "mariner.trappings.moth": -1,
            "mariner.song.grail": -1,
            "mariner.influences.heart": -1
        },
        warmup: 60,
    });
    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.fulfilled.hint14`,
        label: "Preparations and Reparations?",
        actionId: "talk",
        startdescription:
            "\"You have collected what I need?\"",
        hintOnly: true,
        requirements: {
            ...LOCAL_TO("medsea", "constantinople", "tourist"),
            'mariner.quests.tinyheist': 1,
            'mariner.globalflags.quests.tinyheist.salutations': 1,
            "mariner.tool.knock": -1,
            "mariner.trappings.moth": -1,
            "mariner.song.grail": 1,
            "mariner.influences.heart": -1
        },
        warmup: 60,
    });
    mod.setRecipe(RECIPE_FILE, {
        id: `${QUEST_PREFIX}.fulfilled.hint15`,
        label: "Preparations and Reparations?",
        actionId: "talk",
        startdescription:
            "\"You have collected what I need?\"",
        hintOnly: true,
        requirements: {
            ...LOCAL_TO("medsea", "constantinople", "tourist"),
            'mariner.quests.tinyheist': 1,
            'mariner.globalflags.quests.tinyheist.salutations': 1,
            "mariner.tool.knock": -1,
            "mariner.trappings.moth": -1,
            "mariner.song.grail": -1,
            "mariner.influences.heart": -1
        },
        warmup: 60,
    });

}