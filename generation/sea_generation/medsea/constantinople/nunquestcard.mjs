import { generateQuest } from "../../../quest_generation.mjs";

export function generateBuriedSecretsQuestCard() {
  generateQuest({
    id: "buriedsecrets",
    // Quest card informations
    label: "The Retrieval of an Indiscretion",
    description:
      "\"I wondered if you could assist me in a private matter. No reason to trouble the church patriarchs with… I would do it myself, but I do not travel so well anymore, so I am obliged to bid your assistance. After all, I aided you with your sodden treasure, so I already know you know your way around Luxor... To ensure the safety of myself and others, I had to rid myself of some incriminating papers quickly… I buried them, in the graveyard of a coptic church in a neighborhood crowding the water's edge. I implored with powers sacred and hidden for it to remain hidden, and as I sit here before you safely, it has been. ",
    // Quest stages
    updates: [
      {
        id: "beginnings",
        label: "Old Wounds Ache",
        description:
          '"To ensure the safety of myself and others, I had to rid myself of some incriminating papers quickly… I buried them, in the graveyard of a coptic church in a neighborhood crowding the water\'s edge. I implored with powers sacred and hidden for it to remain hidden, and as I sit here before you safely, it has been. But now I need the papers once again, and they must be recovered. So please, would you travel to Luxor to lend some aid to an old woman?"',
      },
      {
        id: "flood",
        icon: 2,
        label: "That Great Flood",
        description:
          "The errant begins to seem a fool’s one, when an old man on a porch, bent like driftwood, remembers the Chapel of Saint Wanas, once a refuge for the deprived of any cloth. But it is gone now, as is much of the neighborhood around it. Some 40 years ago, a great swell of earth and mud rose with the river's ire, burying streets and buildings and lives alike. The site was abandoned, abandoned to the river and the reeds.",
        effects: {
          updateCodexEntries: {
            "codex.nun": {
              add: ["codex.nun.neg"],
            },
          },
        },
      },
      {
        id: "secret",
        icon: 3,
        label: "Secrets Uprooted",
        description:
          "The stashed box is now in our hands. all that remains is to deliver it back to Mother Euphresme",
        effects: {
          updateCodexEntries: {
            "codex.nun": {
              add: ["codex.nun.neg"],
            },
          },
        },
      },
      {
        id: "completed",
        effects: {
          updateCodexEntries: {
            "codex.nun": {
              add: ["codex.nun.2"],
            },
          },
        },
      },
    ],
  });
}
