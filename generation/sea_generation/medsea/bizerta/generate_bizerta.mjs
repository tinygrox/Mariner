import { generatePort } from "../../../port_generation/port_generation.mjs";
import { market } from "../../../port_generation/helpers.mjs";
import { HAKIM, HAKIM_WARY } from "./hakim.mjs";
import { Galantha } from "../galantha.mjs";

export function generateBizerta() {
  generatePort({
    portRegions: ["medsea"],
    portId: "bizerta",
    label: "Bizerta",
    description:
      "On the Northernmost tip of Africa, lies a port founded by the Phoenicians. Where Carthage is dust on dust, Bizerte thrives and glitters.",
    recipeDescriptions: {
      docking:
        "Bizerta lies in the crest between a lake of Emerald and a sea of Saphire. Of it's many docks, harbors and marina's, we dock in the Vieux Port",
      exploreHH:
        "<From the Old Harbor, the many-coloured houses and the cool breeze of the water gives way white buildings and loud bazaars. If I apply my charm, I may find extrordinary peoples, places, or opportunities.>",
      exploreLH:
        "<From the Old Harbor, the many-coloured houses and the cool breeze of the water gives way white buildings and loud bazaars. If I stay open and receptive, I may find discover the peculiar sounds of the city, or be approached by a Raconteur.>",
    },
    barName: "Merset",
    wharf: {
      cargoOptions: [
        { id: "mariner.cargo.for.northsea.close", label: "North Sea" },
        { id: "mariner.cargo.for.medsea.local", label: "Med Sea" },
        { id: "mariner.cargo.for.medsea.local", label: "Med Sea" },
      ],
    },
    portElements: [
      HAKIM,
      HAKIM_WARY,
      Galantha,
      market({
        id: "naturalmarket",
        label: "<The House of the Green Crescent>",
        description: "<>",
        types: [],
        storyTrading: {
          story: {
            label: "<>",
            icon: "locksmithsdream2",
            description: "<>",
            aspects: {
              forge: 1,
            },
          },
          label: "<>",
          startdescription: "<>",
          description: "<>",
        },
      }),
    ],
  });

  // He's afraid, doesn't know if you can be trusted, really wants to know more about his friend's death.
  // So this recipe is supposed to be a catch-all.
  mod.setRecipe("recipes.conversations.medsea.bizerta", {
    id: "mariner.conversations.medsea.bizerta.hakim.wary",
    craftable: true,
    actionId: "talk",
    label: "<Too Wary To Talk>",
    startdescription: "<>",
    description: "<>",
    requirements: {
      "mariner.locations.medsea.bizerta.hakim.wary": 1,
    },
  });
}
