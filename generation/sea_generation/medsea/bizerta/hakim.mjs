import { SIGNALS } from "../../../generate_signal_aspects.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  repeatableConversation,
  uniqueConversation,
} from "../../../port_generation/helpers.mjs";
import { QUEST_ID, UPDATE_QUEST } from "../../../quest_generation.mjs";

export const HAKIM = {
  type: "npc",
  notDiscoverable: true,
  elementId: "hakim",
  label: "Hakim Tmar",
  description: "A student of philosophy, both natural and beyond.",
  deliveryOptions: {
    previousPackageLocations: [],
    knownLanguages: [],
    usedNames: [],
    usedInitials: [],
    gender: "male",
    whatToDoWithIt: [
      "<Leave it with the landlady, miss Al Trabelsi.",
      "<Tell him that despite personal issues, his paper is still due. I have sent him his reserach notes.>",
    ],
  },
  deliveryRiddles: [
    "Academic, Adept, Philosopher, Slacker",
    "Graduate in the Port between the Sea of Green and the Sea of Gold",
    "<He in search of a Fallen Comrad>",
    "<He has been found studying in Alexandria, Jerusalem, Paris, New York, but his home is the North-most tip of the Oldest continent>",
  ],
  storyTrading: {
    story: {
      label: "a story from student life. ",
      icon: "onthewhite",
      description:
        "<It was one of those nights where it all seemed possible, where the air was alight with jazz and the city light twinkled brighter then the stars. We saw the start of a revolution. Society could not control art, nor could Tradition control Art. In Harlem we found ourself among jazz mages and reformers, the writers, playwrights and musicians. The world was colourful, the world was bright, the drinks illigal but tasting all the sweeter. We debated with scholars, orated to poets and danced to all nights' end. Mortimer got an invitation to the Crossrow though I do not know if he ever attended.>",
      aspects: {
        lantern: 2,
        forge: 2,
        moth: 2,
        "mariner.storyaspect.occult": 1,
        "mariner.storyaspect.rousing": 1,
        "mariner.storyaspect.apollonian": 1,
      },
    },
    label: "<Trade stories with Hakim Tmar>",
    startdescription:
      '"Stories? I have collected a few in my 20s... Sometimes I think most I will have left from my education will be stories of an era instantly lost to history."',
    description:
      '<Hakim sighs, and his lips curl into a smile. "Allons-y then. To friends departend and times gone by. Once upon a time, while I was studying a summer in New York...">',
  },
  conversations: [
    repeatableConversation({
      id: "chest",
      element: "mariner.vaults.thebes.chest",
      label: "<Discuss the Warded Chest with Hakim>",
      startdescription: '"<What do you have for me?>"',
      description:
        '"I am not sure what I could do with this... I only dabble, and this is beyond mere dabblings. I have seen similar artifacts when i studied in Alexandria. I doubt you got permission to take it. Those glyphs are Hyksosan, and that image is clearly a version of the Elegiast. While he is not the unkindest hour, his power over wards are significant. I would take this to someone versed in spiritual matters, and confident in their abilities."',
    }),
    repeatableConversation({
      id: "hyksos",
      element: "mariner.vaults.damascus.glyphs.hidden",
      label: "Discuss the Unknown Glyphs with Hakim",
      startdescription:
        '"<You seek my help in a scholarly pursuit? I shall endeavor to supply.>"',
      description:
        'Hakim looks at the markings for only a few moments, before recoiling harshly and folding the paper with a decided hand. "I do know this script, from the time I spend studying the deeper mysteries in Alexandria. You have copied an Hyksosan ward there. Reading it itself can be dangerous, so I dare not look to close... But it seems like a threat, or possible an invitation."',
      effects: {
        "mariner.vaults.damascus.glyphs.hidden": -1,
        "mariner.vaults.damascus.glyphs": 1,
      },
    }),
    repeatableConversation({
      id: "songwinter",
      element: "mariner.song.winter",
      label: "<Discuss the Power of my Songs>",
      startdescription:
        '"<I never enrolled in the conservatory, but I can share what I learned in my studies.>"',
      description:
        '<"Winter may seem far here, but the desert nights are bitter cold. and in that bleak expanse of pale starlight, the dead can come to mind with crisp clarity. If you aim your music at the direction of Winter, the dead can be influenced. reanimated, parlayed, quieted, if paired with the appropriate Intent.">',
      effects: {},
    }),
    repeatableConversation({
      id: "songmoth",
      element: "mariner.song.moth",
      label: "<Discuss the Power of my Songs>",
      startdescription:
        '"<I never enrolled in the conservatory, but I can share what I learned in my studies."',
      description:
        '<"The songs of Moth? I hear prague I heard it expressed as the songs the wind plays through deep forest. but that is not the language the wind speaks here. Knotwingknot is a rare visitor here, and usually the Imago drives her winds past our windows. Perhaps talk to someone on the Continent.">',
      effects: {},
    }),
    repeatableConversation({
      id: "halfheart",
      element: "mariner.halfheart",
      label: "<Ask about my Halfheart>",
      startdescription: '"<A split soul? A common motif...>"',
      description:
        '"You can find the split soul in literature, folklore, philosophy. Plato himself thought that all humans in the world where halved beings, and the Egyptians recognised several parts of course. I wonder, with enough anatomical ans metaphysical knowledge, could a split soul be stitched together like the body can be? Would that have been what my friend had been pursuing?"',
      effects: {},
    }),
    repeatableConversation({
      id: "lackheart",
      element: "mariner.lackheart",
      label: "<Ask after Inner Darkness>",
      startdescription:
        '"<Emptyness in your life can spring from many sorrows.>"',
      description:
        '"Grief, the grind of life., a loss of purpose, a loss of self. I am watching the world burn, and I just hope that under the soot lies something better. I build mysekf up through education, travel and modern thought. But what certainty do I have is that what I gain is better then what I leave behind?"',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.student.1",
      element: "mariner.song.knock",
      label: "<Discuss with Hakim>",
      startdescription: '"<>"',
      description:
        "\"he was a friend. Once I would have considered him close as a brother. But these days I didn't even know he had returned to Bizerte, and I don't know where his pursuits have taken him since we last met. He studied Medicine then, in Alexandria, and we debated the Histories and the Lesser Dreams on young nights and old mornings.\"",
      effects: {},
    }),
    repeatableConversation({
      id: "quest.student.2",
      element: "mariner.song.knock",
      label: "<Discuss with Hakim>",
      startdescription: '"<>"',
      description:
        '"A secret fraternity of auto-what? No, never mind, do not tell me now.. I never had the stomach for the surgeon\'s theater like him. I can see how this would have drawn him in. He always had the tendency to burn bright with Illumination but be poor in Preservation, even of himself."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.stench.1",
      element: "mariner.song.knock",
      label: "<Discuss with Hakim>",
      startdescription: '"<>"',
      description:
        '"The goings on in a hospital he entered at a decade ago? It is a tenuous connection, some might even call it a leap. But until you are sure it is not related, I am glad you are pursuing it."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.bewitch.1",
      element: "mariner.song.knock",
      label: "<Discuss with Hakim>",
      startdescription: '"<>"',
      description:
        '"Talents such as yours are appealing to many… What you are willing to do with them is up to yourself and your god."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.bewitch.2",
      element: "mariner.song.knock",
      label: "<Discuss with Hakim>",
      startdescription: '"<>"',
      description:
        '"I have attempted to follow the news in Marseille from here. Plenty of french newspapers in the stalls. I heard of the irregularities, and I couldn\'t help but wonder if you had been involved."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.towers.1",
      element: "mariner.song.knock",
      label: "<Discuss with Hakim>",
      startdescription: '"<>"',
      description:
        '"When I first entered the Mansus, it was with him… three of us, holed up in our dorm. Scissors next to the bed. But I do not know much beyond the Silver Road and the dark Woods. I did not enjoy the urges I felt blooming there, and decided that the light of the Mansus was best admired from a safe distance."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.truth.1",
      element: "mariner.song.knock",
      label: "<Discuss with Hakim>",
      startdescription: '"<>"',
      description:
        '"Luxor! Marvelous city, marvelous history. I spend a gap year there, attempting to follow the Nile up to the point where it flows out of Myth and into Legend. Misguided, I know now, since I read the Book of the Centipede… But still, a marvelous experience with marvelous friends. We spent a week in Luxor, eating Freekah, seeing the Temple, and providing each other excuses not to leave civilization behind."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.sisterhood.1",
      element: "mariner.song.knock",
      label: "<Discuss with Hakim>",
      startdescription: '"<>"',
      description:
        '"I have been introduced to this mother Euphresme, I believe. I would wish for any visitor of the Queen of Cities to spend an evening with her. I cannot say I am too surprised to hear she operated a secret sect within her church, I doubt even a fraction of her secrets have been revealed so far. I do remember a young woman from Constaninople passing through here as a child. She traveled south, said she sought the edge of the map."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.caper.1",
      element: "mariner.song.knock",
      label: "<Discuss with Hakim>",
      startdescription: '"<>"',
      description:
        '"I have not kept up with the peerage in a while, but I do believe I have heard of this Hunyadi when I was in boeda-pesht. Not that I normally hang out with aristocracy, but they do like to slum… Some of the good old boys came down to join us in the demi-monde for a salon… though I believe their minds were more on the absinthe than on the politics. But I believe the name Hunyadi fell… as a lay-about, a rake, a lucky-one, a traitor."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.constelate.1",
      element: "mariner.song.knock",
      label: "<Discuss with Hakim>",
      startdescription: '"<>"',
      description:
        '"The House of Lethe? I must have missed that class… or that tome, depending on if this is history or Histories."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.imago.1",
      element: "mariner.song.knock",
      label: "<Discuss with Hakim>",
      startdescription: '"<>"',
      description:
        '"Matthias and Imago? I read one or two of the accounts, translated of course, but with the copied illustrations. A friend worked on them, and shared them with our Young Arab club meetings. I discussed them with Mortimer. He wished to understand Mattias, but I could not help being drawn to this fierce, fiery winged creature. The nature of humanity is dust, is the nature of Names light, or subtle fire?"',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.muse.1",
      element: "mariner.song.knock",
      label: "<Discuss with Hakim>",
      startdescription: '"<>"',
      description:
        '"I have not ventured much further north than Paris and Vienna, and I do not visit the clubs that dancers frequent very often. Cafes are more my style."',
      effects: {},
    }),
    //repeatableConversation({
    //  id: "vagabond.dedication",
    //  element: "mariner.ascension.vagabond.dedication",
    //  label: "<>",
    //  startdescription: '"<>"',
    //  description: '""',
    //  effects: {},
    //}),
    //repeatableConversation({
    //  id: "twins.dedication",
    //  element: "mariner.ascension.twins.dedication",
    //  label: "<>",
    //  startdescription: '"<>"',
    //  description: '""',
    //  effects: {},
    //}),
    // uniqueConversation({
    //   id: "showcasefile",
    //   element:
    // })
  ],
};

export const HAKIM_WARY = {
  type: "npc",
  notDiscoverable: true,
  elementId: "hakim.wary",
  label: "Hakim Tmar?",
  description: "A student of philosophy, both natural and beyond, I suppose.",
  conversations: [
    uniqueConversation({
      id: "showfindings",
      label: "<Show Findings>",
      startdescription: "<>",
      quest: QUEST_ID("frenchheartcult"),
      element: "mariner.vaults.bizerta_morgue.casefile",

      // Check that you can actually have that conversation.
      additionalRequirements: {
        [READ_QUEST_FLAG("frenchheartcult", "startedviatheprofessor")]: -1,
      },
      additionalProperties: {
        furthermore: [
          UPDATE_QUEST("frenchheartcult", "showedfindingstohakim"),
          {
            effects: {
              "mariner.locations.medsea.marseille.professor.away": 1,
            },
          },
          {
            aspects: { [SIGNALS.APPLY_DEFAULT_ROUTINE]: 1 },
          },
        ],
        linked: [],
      },
    }),
  ],
};
