import { generatePort } from "../../../port_generation/port_generation.mjs";
import { ALEXIA } from "./alexia.mjs";
import { generateFrenchGrailCultQuest } from "./french_grail_cult.mjs";
import { MFM } from "./manfrommarseille.mjs";
import { Galantha } from "../galantha.mjs";
import { generateBewitchQuestCard } from "./serenoquestcard.mjs";
import { PROFESSOR } from "./professor.mjs";
import { generateFrenchHeartCultQuest } from "./french_heart_cult.mjs";
import { generateNoonWaterQuest } from "./noonwater.mjs";
import { generateNoonWaterQuestCard } from "./noonwaterquestcard.mjs";
import { generateOzoneEvent, generateStormbreakEvent } from "./bewitch_events.mjs";

export function generateMarseille() {
  generatePort({
    portRegions: ["medsea"],
    portId: "marseille",
    label: "Marseille",
    description: "<The beating heart of the Riviera. They say this is the closest you can get to Noon in continental Europe.>",
    recipeDescriptions: {
      exploreHH:
        "<Under the Relentless Riviera Sun, we walk streets hazed by the fermented smells of the Mediterranean, The afternoon sometimes brings the relief of a thunderstorm down from the mountains, to wash away the sins of the day.>",
      exploreLH:
        "<Under the Relentless Riviera Sun, we walk streets hazed by the fermented smells of the Mediterranean, The afternoon sometimes brings the relief of a thunderstorm down from the mountains, to wash away the sins of the day.>",
    },
    barName: "L'Ancre",
    wharf: {
      cargoOptions: [
        { id: "mariner.cargo.for.northsea.close", label: "North Sea" },
        { id: "mariner.cargo.for.medsea.local", label: "Med Sea" },
        { id: "mariner.cargo.for.medsea.local", label: "Med Sea" },
      ],
    },
    portElements: [ALEXIA, PROFESSOR, MFM, Galantha],
  });

  generateFrenchGrailCultQuest();
  generateFrenchHeartCultQuest();
  generateBewitchQuestCard();
  generateOzoneEvent();
  generateStormbreakEvent();
  generateNoonWaterQuest();
  generateNoonWaterQuestCard();
}
