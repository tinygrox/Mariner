import {
    LOCAL_TO,
    specialWanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
    RESET_TIMER,
    TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";
import { QUEST_FLAG_ID } from "../../../global_flags_generation.mjs";
import { UPDATE_QUEST } from "../../../quest_generation.mjs";



export function generateOzoneEvent() {
    const { RECIPE_FILE, EVENT_START_ID } = specialWanderingEventScaffhold(
        "medsea.quest.bewitch.ozone"
    );
    mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "marseille"]);
    // the storm breaking in marseille
    mod.setRecipe(RECIPE_FILE, {
        id: EVENT_START_ID,
        label: `Ozone and Tinder`,
        tablereqs: {
            "mariner.quests.bewitch": 1,
            "mariner.influences.moth": 3,
            "mariner.influences.edge": 5,
            "mariner.influences.knock": 3,
        },
        grandReqs: {
            [READ_QUEST_FLAG("bewitch", "ozone")]: -1,
        },
        description:
            "The air is volatile tonight. The clouds rolling in are dark as black powder, and people’s moods are fickle like snapping flags. The whole city is waiting for the thunder roll and lightning crash, but for now, all that power remains in potentia.",
        furthermore:
            [UPDATE_QUEST("bewitch", "ozone"),
            {
                target: "~/exterior",
                effects: { "mariner.influences.moth": -3, "mariner.influences.edge": -5, "mariner.influences.knock": -3 },
            },]
    });

    // End: This quest is finished at the end of the Sunken City vault.
}

export function generateStormbreakEvent() {
    const { RECIPE_FILE, EVENT_START_ID } = specialWanderingEventScaffhold(
        "medsea.quest.bewitch.stormbreak"
    );
    mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "marseille"]);
    // the storm breaking in marseille
    mod.setRecipe(RECIPE_FILE, {
        id: EVENT_START_ID,
        label: `Storm Breaks`,
        tablereqs: { "mariner.weathermeta.windy": 1, "mariner.weathermeta.rainy": 1, "mariner.locations.medsea.marseille": 1 },
        grandReqs: {
            [READ_QUEST_FLAG("bewitch", "ozone")]: 1,
            [READ_QUEST_FLAG("bewitch", "stormbreaks")]: -1,
        },
        description:
            "Arguments break out, neighbors bicker as the mountain thunder fires like artillery, workers riot in the streets. The national guard, pushed back to their holds, threatens much violence and performs more still. And somewhere in all that confusion, no doubt one man is accomplishing all of his dealings. Whatever those might be, I hope he is satisfied.",
        effects: {
            "mariner.experiences.rattling": 1,
        },
        furthermore:
            UPDATE_QUEST("bewitch", "stormbreaks")
    })
}