import { randomlySwapToAnotherRoutine, swapToAnotherRoutine } from "../../../routine_generation.mjs";

export const PROFESSOR = {
  type: "npc",
  elementId: "professor",
  label: "<Professor>",
  description: "<>",
  notDiscoverable: true,
  defaultRoutine: "day",
  routines: [
    {
      id: "day",
      label: "<Diurnal Behaviour: Teaching>",
      description:
        "<These days, the Professor seems to be keeping himself busy with an agenda filled with courses to teach. He arrives early, teaches various topics from anatomy to advanced biology to an amphitheater full of students, then walks back to his home and stays in for the night.>",
      rate: 240,
      unavailable: true,
      update: swapToAnotherRoutine("rest"),
    },
    {
      id: "rest",
      label: "<Resting>",
      description:
        "<These days, the Professor seems to follow a lighter schedule, resting from the academic work. He teaches less, spends most of his day visiting acquaintances at cafes and boudoirs, and buying various supplies. He then spends most of his evenings writing, maybe to update his courses, maybe to reply his accumulating stack of mail. He still looks visibly tired.>",
      rate: 120,
      update: randomlySwapToAnotherRoutine({ nightowl: 75, day: 100 }),
    },
    {
      id: "nightowl",
      label: "<Night Owl>",
      description:
        "<We just spotted the Professor leaving his home, late at night, in the middle of one of his resting periods. Where could he be going?>",
      rate: 10,
      aspects: { modded_explore_allowed: 1, modded_talk_allowed: -1 },
      update: swapToAnotherRoutine("rest2"),
    },
    {
      id: "rest2",
      label: "<Resting>",
      description:
        "<These days, the Professor seems to follow a lighter schedule, resting from the academic work. He teaches less, spends most of his day visiting acquaintances at cafes and boudoirs, and buying various supplies. He then spends most of his evenings writing, maybe to update his courses, maybe to reply his accumulating stack of mail. He still looks visibly tired.>",
      rate: 120,
      update: randomlySwapToAnotherRoutine({ nightowl: 25, day: 100 }),
    },
  ],
  conversations: [],
};
