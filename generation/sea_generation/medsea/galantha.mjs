import { QUEST_FLAG_ID } from "../../global_flags_generation.mjs";

export const Galantha = {
  type: "npc",
  elementId: "galantha",
  label: "The Galantha, Roaming",
  description:
    "A tourist out of time, a conqueress surveying her lands, a wolf-amongst-sheep",
  aspects: { "mariner.galantha": 1 },
  storyTrading: {
    story: {
      label: "<story of the Galantha",
      icon: "onthewhite",
      description:
        "",
      aspects: {

        winter: 2,
        edge: 2,
        "mariner.storyaspect.cthonic": 1,
        "mariner.storyaspect.tragic": 1,
        "mariner.storyaspect.wise": 1,
      },
    },
    label: "Trade stories with the Galantha",
    startdescription: 'She blinks slowly, twice',
    description:
      '<>',
  },
  notDiscoverable: true,
};

mod.initializeAspectFile("aspects.mystery.character", ["mysteries", "character"]);
mod.setAspect("aspects.mystery.character", {
  id: "mariner.galantha",
  label: "Hidden Aspect demarking a card as Galantha",
  description:
    "<>",
  isHidden: true
});

mod.initializeRecipeFile("recipes.galantha", ["mysteries", "character"]);
mod.setRecipe("recipes.galantha", {
  id: "mariner.galantha.router",
  label: "An Encounter with the Galantha",
  startdescription:
    "\"So you found my again.\" The Galantha has almost managed to captured the essence of the modern in dress, but her voice remains an ancient thing \"Any reason to expect this time it will turn out differently?\" ",
  description:
    "\"What will it be?\" [place",
  warmup: 30,
  actionid: "talk",
  craftable: true,
  requirements: {
    "mariner.galantha": 1,
  },
  linked: [{ id: "mariner.galantha.fight", id: "mariner.galantha.bargain" }],
  slots: [
    {
      id: "intent",
      label: "Intent",
      description: "What kind of encounter do I want? [respond with a Halfheart to engage in a hight, or with the Lackheart to bargain]",
      required: {
        "mariner.lackheart": 1,
        "mariner.halfheart": 1,
      },
    },
  ],
});
mod.setRecipe("recipes.galantha", {
  id: "mariner.galantha.fight",
  label: "Fight with the Galantha",
  startdescription:
    "\"As you wish.\" The streets darken, the noises muffle.",
  description:
    "\"As you wish.\" The shadows sharpen, the wind runs cold.",
  warmup: 30,
  actionid: "talk",
  requirements: {
    "mariner.halfheart": 1,
  },
  linked: [{ id: "mariner.galantha.fight.success", id: "mariner.galantha.fight.failure" }],
});
mod.setRecipe("recipes.galantha", {
  id: "mariner.galantha.bargain",
  label: "Bargain with the Galantha",
  startdescription:
    "\"You are pleading for the Water? Fine, I can be convinced.\" She does not seem surprised, but her ancient eyes remain unreadable. \"You can trade me the location of the Abzu, the Well of the Letheians. I know it must be somewhere below Syria, Judea or Egypt from where the Lethians drew their water.\" ",
  description:
    "\"You are pleading for the Water? Fine, I can be convinced.\" She does not seem surprised, but her ancient eyes remain unreadable. \"You can trade me the location of the Abzu, the Well of the Letheians. I know it must be somewhere below Syria, Judea or Egypt from where the Lethians drew their water.\" ",
  warmup: 30,
  actionid: "talk",
  requirements: {
    "mariner.lackheart": 1,
  },
  linked: [{ id: "mariner.galantha.bargain.success", id: "mariner.galantha.bargain.failure" }],
});
mod.setRecipe("recipes.galantha", {
  id: "mariner.galantha.bargain.success",
  label: "A Painless Exchange",
  startdescription:
    "\"As simple as that.\"",
  description:
    "\"As simple as that.\"",
  warmup: 30,
  effects: { "mariner.noonwater": 1, "mariner.galantha": -1 }, //how to remove the galantha?
  furthermore: {
    [QUEST_FLAG_ID("galantha", "abzu")]: 1,
  },
  actionid: "talk",
  requirements: {
    "mariner.galantha": 1,
  },
});
mod.setRecipe("recipes.galantha", {
  id: "mariner.galantha.fight.success",
  maxexecutions: 1,
  label: "A Battle against an Immortal",
  startdescription:
    "We have bundled the Galantha through the harbor, and sent some among us to fetch the spare anchor from the hull. The person, the monster, our foe, redoubled their efforts to escape, and we must act decisively, and with grim determination, to keep a hold on her while she can be attached to the weight the chains are wrapped around her body. [Use Winter or Edge to navigate this challenge]",
  description:
    "We have bundled the Galantha through the harbor,  and sent some among us to fetch the spare anchor from the hull. The person, the monster, our foe, redoubled their efforts to escape, and we must act decisively, and with grim determination, to keep a hold on her while she can be attached to the weight the chains are wrapped around her body. [Use Winter or Edge to navigate this challenge]",
  warmup: 30,
  actionid: "talk",
  requirements: {
    "mariner.galantha": 1,
  },
  linked: [{ id: "mariner.galantha.fight.success2", id: "mariner.galantha.fight.failure" }],
});
mod.setRecipe("recipes.galantha", {
  id: "mariner.galantha.fight.success2",
  maxexecutions: 1,
  label: "Fight with the Galantha",
  startdescription:
    "Properly entangled, balled up in fury on the deck, we quickly set off,",
  description:
    "Properly entangled, balled up in fury on the deck, we quickly set off,",
  warmup: 30,
  actionid: "talk",
  requirements: {
    "mariner.galantha": 1,
  },
});
mod.setRecipe("recipes.galantha", {
  id: "mariner.galantha.fight.failure",
  maxexecutions: 1,
  label: "Slipped Away",
  startdescription:
    "The Galantha has slipped her bonds, and then slipped from our grasp completely",
  warmup: 30,
  actionid: "talk",
  requirements: {
    "mariner.galantha": 1,
  },
});
mod.setRecipe("recipes.galantha", {
  id: "mariner.galantha.bargain.failure",
  maxexecutions: 1,
  label: "No Deal",
  startdescription:
    "A pity",
  warmup: 30,
  actionid: "talk",
  requirements: {
    "mariner.galantha": 1,
  },
  effects: { "mariner.experiences.unraveling.standby": 1, }
});