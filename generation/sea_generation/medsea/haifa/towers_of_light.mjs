import { LOCAL_TO } from "../../../generation_helpers.mjs";
import { QUEST_FLAG_ID } from "../../../global_flags_generation.mjs";
import { UPDATE_QUEST, generateQuest } from "../../../quest_generation.mjs";

export function generateTowersOfLightQuest() {
  const RECIPE_FILE = mod.initializeRecipeFile("recipes.medsea.haifa.quest.towersoflight", ["locations", "medsea", "haifa"]);
  const QUEST_PREFIX = "mariner.medsea.haifa.towersoflight";

  generateQuest({
    id: "towersoflight",
    // Quest card informations
    label: "The Towers of Light",
    description: "",
    // Quest stages
    updates: [
      // Just met her
      {
        id: "start",
        label: "",
        description: "",
      },
      // Gave her the first story
      {
        id: "gavefirststory",
        label: "",
        description: "",
      },
      // Gave her the second story
      {
        id: "gavesecondstory",
        label: "",
        description: "",
      },
      // Gave her the third story
      {
        id: "gavethirdstory",
        label: "",
        description: "",
      },
      // Led her to the threshold and defended it successfully

      // Led her to the threshold and failed to defend it
    ],
  });

  // Beginning
  mod.setRecipes(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.start`,
    actionId: "talk",
    label: "A Glimpse In a Café",
    startdescription: "As I sit in dark shadow of a sunbaithed café, a young woman approaches me.",
    description: "",
    maxexecutions: 1,
    craftable: true,
    furthermore: [
      {
        effects: { "mariner.quests.towersoflight": 1 },
      },
      UPDATE_QUEST("towersoflight", "start"),
    ],
    requirements: {
      ...LOCAL_TO("medsea", "haifa", "zoha"),
    },
    unlockCodexEntries: ["codex.zoha"],
    warmup: 60,
  });
}
