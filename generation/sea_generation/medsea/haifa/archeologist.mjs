import {
  repeatableConversation,
  convertSourceConversation,
} from "../../../port_generation/helpers.mjs";
import {
  buildCodexEntryBlock,
  generateCodexEntry,
} from "../../../../generation/codex_generation/codex_generation.mjs";

export const ARCHEOLOGIST = {
  type: "npc",
  elementId: "archeologist",
  label: "Professor Miriam Sayegh",
  description:
    "The thoughtful, accomplished, dust-flecked, Archeology Chair of Haifa University.",
  deliveryOptions: {
    previousPackageLocations: [],
    knownLanguages: [],
    usedNames: [],
    usedInitials: [],
    gender: "male",
    whatToDoWithIt: ["<>"],
  },
  deliveryRiddles: [
    "<Our newest pen pall, seeking treasures in the dust>",
    "<Crow Pecking at the crumbs of the Past>",
    "The Waning Gibbous Moon in Haifa",
    "Socialite, academic and burgeoning occultist",
  ],
  storyTrading: {
    story: {
      label: "archeologiststory arabian fairytale",
      icon: "onthewhite",
      description: "<>",
      aspects: {
        winter: 2,
      },
    },
    label: "<>",
    startdescription: '"<>"',
    description: '<>"',
  },
  conversations: [
    repeatableConversation({
      id: "lackheart",
      element: "mariner.lackheart",
      label: "<Ask about my Lackheart>",
      startdescription: '"<A hole in your heart?>"',
      description:
        '"<I can relate with that. All things that you start with a shining passion can tarnish in time. Family, Career, Status. They go from the goals you chase to the chains weighing you down. I could envy you, how you can just drop everything and set off.>" She smiles and leans to the side, towards her window. "But luckily my office looks out over the sea. She can balance me, when my ambitions overtake me"',
    }),
    repeatableConversation({
      id: "halfheart",
      element: "mariner.halfheart",
      label: "<Ask about my Halfheart>",
      startdescription:
        '"<You want to ask about something called a Half-Heart? Let me check our codexes>"',
      description:
        '"<The term does enter the records once or or twice, in a poetic language of the Arabic Golden Age, or a sobriquet of characters at the edge of relevance of the Assyrian Dynasties. But neither of those are my specialities, so I can\'t say too much more.>"',
    }),
    repeatableConversation({
      id: "vagabonddedication",
      element: "mariner.ascension.vagabond.dedication",
      label: "<Talk to professor Sayegh about my Path>",
      startdescription: '"That is a ambition I can stand behind."',
      description:
        '"Pursuing hidden secrets, give voices to lost stories of the past. That is what brought me to Archeology originally, and now that the Secret side of the Histories is revealed, I feel that desire surge in my once more." She smiles and looks whistfully out the window."Sometimes this office I worked so hard for feels now like a trap."',
    }),
    repeatableConversation({
      id: "twinsdedication",
      element: "mariner.ascension.twins.dedication",
      label: "<Talk to professor Sayegh about my Path>",
      startdescription: '"<The Tearing and the Mending...>"',
      description:
        '"Once, the ailments of the Soul would have been considered medicine. Last year, I would have dismissed it as superstition. But now the Secret Histories have been revealed to me, and I am still sorting through former superstitions and certainties to see which to keep and which to discard. So... If you say your Heart needs Mending... I wish godspeed to you."',
    }),
    repeatableConversation({
      id: "song",
      element: "mariner.song",
      label: "<Discuss the Power of my Songs>",
      startdescription: '"<The Magic in Music?>"',
      description:
        '"<They are deeply intwined, in many cultures. But I do not have mcuh of an Orphian Arts. But I am learning about all the Invisible Arts, to look into what I otherwise neglected. I have learned of Libraries, who secure the knowledge that cannot remain in the wider world. the closest is to here is Yeshiva Tigris, but the Crossrow may contain more information on the topic.>"',
    }),
    repeatableConversation({
      id: "chest",
      element: "mariner.vaults.thebes.chest",
      label: "<Discuss the Warded Chest with Professor Sayegh>",
      startdescription: '"<A find in the Theban Branch?>"',
      description:
        '"I am not sure I can help with this. Until a few months ago, I would have said the only worry that came from taking this from an Egyptian tomb is the the anger of the locals. now... I am unsure how to determine the reality of a ward, or where to look to how to counteract it. This is more esotheric then my knowledge extends to now."',
    }),
    repeatableConversation({
      id: "quest.student.1",
      element: "mariner.song.knock",
      label: "<Discuss the Case of the Student Exsanguinate>",
      startdescription: '"<You are pursuing a line of research?>"',
      description:
        '"A student, you say? How horrible. A life cut short, so much promise wasted."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.stench.1",
      element: "mariner.song.knock",
      label: "<Discuss the Stench of Marseille>",
      startdescription: '"<You are pursuing a line of research?>"',
      description:
        '"That is some good Academic rigor displayed there… An unnatural pattern in the data, however benign, invites further investigation."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.bewitch.1",
      element: "mariner.song.knock",
      label: "<Discuss the Bewitchment of a City>",
      startdescription: '"<You are pursuing a line of research?>"',
      description:
        '"We all do things we are not proud of to get where we are. That\'s the nature of the beast I am afraid. In this world you devour, or are consumed."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.towers.1",
      element: "mariner.song.knock",
      label: "<Discus the Reverie of the Crumbling Towers>",
      startdescription: '"<You are pursuing a line of research?>"',
      description:
        '"A cult of mysticism? Traditionally I have taken my leave when the esoterica begins. But then again, I am now chasing a cult of people I suspect of erasing themselves from history, so my old ways might be foolish and outdated based on the new evidence."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.truth.1",
      element: "mariner.song.knock",
      label: "<Discuss the Recovery of Discarded Truth>",
      startdescription: '"<>"',
      description:
        '"A Coptic Church in a city in Egypt? It’s not quite like grains of sand in the desert, but I am sure you will not find all of them named and numbered in any tourist guide. I wish you luck."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.sisterhood.1",
      element: "mariner.song.knock",
      label: "<Discuss the Reunion of the Sisterhood>",
      startdescription: '"<You are pursuing a line of research?>"',
      description:
        '"Can you Imagine? Scattered. Told what you dedicated your life to is no longer your concern. To have to start over, flee, give up both the life you dedicated yourself to as well as whatever life you might have had before. I feel for these women. I hope they have moved on to bigger and better things… Perhaps some sought new opportunities in the New World, but if they are anything like me, they could never leave our history behind."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.caper.1",
      element: "mariner.song.knock",
      label: "<Discuss the Caper of the Copper Manse>",
      startdescription: '"<You are pursuing a line of research?>"',
      description:
        '"History may be written by the victors, but it is collected by bandits. Any distaste I might have for that practice is both profoundly warranted and deeply hypocritical. Claw your way into the system and do the best you can, better to be a generous thief than an empty handed saint."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.wist.1",
      element: "mariner.song.knock",
      label: "<Discuss the Path to Constellation>",
      startdescription: '"<You are pursuing a line of research?>"',
      description:
        '"I had to embrace the impossible if I did not want to embrace madness. People disappear from history all the time, but they rarely disappear so notably they burn a hole in the page. Whenever the eye closed, records stopped, pages crumbled, names vanished."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.wist.2",
      element: "mariner.song.knock",
      label: "<Discuss the Path to Constellation further>",
      startdescription: '"<You found a new line of inquiry?>"',
      description:
        '"Well! What you bring me is fascinating already. It has proven things to me, and it has caused me to doubt things I held as certain. You cannot imagine the excitement or horror of having to upend not only your worldview, but your academic frame of truth as well. My method for discerning certainty has proven inadequate, now I watch the world with eyes wide in wonder and terror."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.wist.3",
      element: "mariner.song.knock",
      label: "<Discuss the Path to Constellation>",
      startdescription: '"<You found a new line of inquiry?>"',
      description:
        '"The favors I am calling in, the deals I am making, I might be paying off for in decades to come, but they are all quite necessary. I have moved from books I never thought to read to books I was never able to read. But access I will gain, with bribes or lies if I have to. So no, we cannot say yet how the Letheians performed their mystic operations, but I will get there. I will get you what you need, as you will get me what I need."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.imago.1",
      element: "mariner.song.knock",
      label: "<Discuss the Exemplary Path>",
      startdescription: '"<You are pursuing a line of research?>"',
      description:
        '"I can do nothing but give thanks to the Letheians for having such a studious entry ritual. Much more useful to us then them dancing naked under the secret light of the Glutton\'s Moon."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.imago.2",
      element: "mariner.song.knock",
      label: "<Discuss the Exemplary Path further>",
      startdescription: '"<You found a new line of inquiry?>"',
      description:
        '"Your studiousness is infectious. I had not considered Matthias much as a person, just what his actions could tell us about his time and culture and context. But why? Why be a hunter?  Why go against the rules of your society? Why then discard your life\'s work for the love of a… of a… I am not even sure. A monster, a concept, a nightmare?"',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.muse.1",
      element: "mariner.song.knock",
      label: "<Discuss the Chase of the Missing Muse>",
      startdescription: '"<You are pursuing a line of research?>"',
      description:
        '"Perhaps she was never lost, but fled from a man who would have calcified her life as object instead of subject."',
      effects: {},
    }),
    // repeatableConversation({
    //   id: "<>",
    //   element: "mariner.song",
    //   label: "<>",
    //   startdescription: '"<>"',
    //   description: '"<>"',
    // }),
    // repeatableConversation({
    //   id: "<>",
    //   element: "<>",
    //   label: "<>",
    //   startdescription: '"<>"',
    //   description: '"<>"',
    // }),
    // repeatableConversation({
    //   id: "weather",
    //   element: "<>",
    //   label: "<>",
    //   startdescription: '"<>"',
    //   description: '"<>"',
    // }),
    repeatableConversation({
      id: "hyksos",
      element: "mariner.vaults.damascus.glyphs.hidden",
      label: "<Discuss the Unknown Glyphs with Professor Sayegh>",
      startdescription:
        '"Unknown Script in the Damascene Branch? Let us see if it is also unknown to me."',
      description:
        'Soon after, the table is a patchwork of books, reference sheets and scriptoria""',
    }),
  ],
};

mod.readRecipesFile("recipes.imago", ["mysteries", "imago"]);
mod.setRecipe("recipes.imago", {
  id: "mariner.archeologist.convert.aramaic.first",
  maxexecutions: 1,
  label: "Conversation and Education",
  startdescription: '"You found something at the Lethian Sites?"',
  description:
    '"I see...". Well I do not have the time to translate these, but thankfully, that is what teacher assistants are for. If you leave them with me for a few days, I will give you a translation.',
  warmup: 30,
  actionid: "talk",
  craftable: true,
  requirements: {
    "mariner.aramaic.untranslated": 1,
    "mariner.locations.medsea.haifa.archeologist": 1,
  },
  linked: [{ id: "mariner.archeologist.output.router" }],
});
mod.setRecipe("recipes.imago", {
  id: "mariner.archeologist.convert.aramaic.second",
  maxexecutions: 1,
  label: "Conversation and Education",
  startdescription: '"Another find? My assistant will be thrilled"',
  description:
    '"Appearantly his translation did the rounds amongst the faculty. It wcaused quite the stir. If this gains any more infamy, I will have to act as my own supprecion bureau. I have even seen some doodles of the illustrations between my students notes."',
  warmup: 30,
  actionid: "talk",
  craftable: true,
  requirements: {
    "mariner.aramaic.untranslated": 1,
    "mariner.locations.medsea.haifa.archeologist": 1,
  },
  linked: [{ id: "mariner.archeologist.output.router" }],
});
mod.setRecipe("recipes.imago", {
  id: "mariner.archeologist.convert.aramaic.third",
  maxexecutions: 1,
  label: "Conversation and Education",
  startdescription:
    '"Completed the set?" She smiles wryly. "I am scared and elated."',
  description:
    "\"\"She sighs.\"I get a new addition to our newly inaugurated 'Ocluded' Section. The Secrets within the other volumes were a bit too appealing. We want to avoid drawing the attention of the eyes of the greater authority to our university, so we restrict access and have a stern policy regarding scissors on campus. I'll show you the conviscated selection we have gathered as I set my T.A. to work.",
  warmup: 30,
  actionid: "talk",
  craftable: true,
  requirements: {
    "mariner.aramaic.untranslated": 1,
    "mariner.locations.medsea.haifa.archeologist": 1,
  },
  linked: [{ id: "mariner.archeologist.output.router" }],
});
mod.setRecipe("recipes.imago", {
  id: "mariner.archeologist.convert.aramaic.witness",
  maxexecutions: 1,
  label: "Conversation and Education",
  startdescription: '"One more? How did you manage that?"',
  description:
    '"A personal account from Matthias? An extraodinary source, for a multitude of reasons... I will make sure you ge a proper translation."',
  warmup: 30,
  actionid: "talk",
  craftable: true,
  requirements: {
    "mariner.matthiasamethystwitnessaramaic": 1,
    "mariner.locations.medsea.haifa.archeologist": 1,
  },
  linked: [{ id: "mariner.archeologist.output.router" }],
});
mod.setRecipe("recipes.imago", {
  id: "mariner.archeologist.convert.infinite",
  label: "Conversation and Education",
  startdescription: '"Another find? I will call up the regulars."',
  description: "",
  craftable: true,
  warmup: 30,
  actionid: "talk",
  requirements: {
    "mariner.aramaic.untranslated": 1,
    "mariner.locations.medsea.haifa.archeologist": 1,
  },
  linked: [{ id: "mariner.archeologist.output.router" }],
});
mod.setRecipe("recipes.imago", {
  id: "mariner.archeologist.output.router",
  warmup: 5,
  linked: [
    {
      id: "mariner.archeologist.output.transformation",
    },
    {
      id: "mariner.archeologist.output.loss",
    },
    {
      id: "mariner.archeologist.output.pursuit",
    },
    {
      id: "mariner.archeologist.output.witness",
    },
    {
      id: "mariner.archeologist.output.witness",
    },
  ],
});
mod.setRecipe("recipes.imago", {
  id: "mariner.archeologist.output.loss",
  maxexecutions: 1,
  label: "On Matthias and Imago: Loss Translated",
  description: `\"We discussed the translation, which caused the young man some blushing, I might add. But this is a univeristy, and we live in a modern world.\"`,
  warmup: 115,
  requirements: { "mariner.matthiasamethystlossaramaic": 1 },
  effects: {
    "mariner.matthiasamethystloss": 1,
    "mariner.matthiasamethystlossaramaic": -1,
  },
});
mod.setRecipe("recipes.imago", {
  id: "mariner.archeologist.output.transformation",
  maxexecutions: 1,
  label: "On Matthias and Imago: Transformation Translated",
  description: `\"These books have been cause for further study for me. Not just on the practices of the House of Lethe, but also on the House of the Sun.\"`,
  warmup: 115,
  requirements: { "mariner.matthiasamethysttransformationaramaic": 1 },
  effects: {
    "mariner.matthiasamethysttransformation": 1,
    "mariner.matthiasamethysttransformationaramaic": -1,
  },
});
mod.setRecipe("recipes.imago", {
  id: "mariner.archeologist.output.pursuit",
  maxexecutions: 1,
  label: "On Matthias and Imago: Pursuit Translated",
  description: `\"This issue caused quite a stir amongst the students. Late night coiffeuring it seems.\" her spoon clinks against her glass. \"I have not ventured the step yet. Perhaps once the semester is over...\"`,
  warmup: 115,
  requirements: { "mariner.matthiasamethystpursuitaramaic": 1 },
  effects: {
    "mariner.matthiasamethystpursuit": 1,
    "mariner.matthiasamethystpursuitaramaic": -1,
  },
});
mod.setRecipe("recipes.imago", {
  id: "mariner.archeologist.output.witness",
  maxexecutions: 1,
  label: "On Matthias and Imago: Witness Translated",
  description: `\"Well, sorry that took a while. I was determined to do this one myself, but it took a while to find the time. I wanted to make sure I made plenty of copies too, ofcourse. It appears to be a diary, or a set of personal notes. It begins with Mattias observing that he has been challenged by the Imago, and eagerly accepting this challenge. Then there is a lot of back and forth between them, with some fascinating implications for daily life in 10th century before christ in damascus... and a lot of dreamscape battles. Mattias found himself outmatched in the woods, so decided to track iamgo in the wake. He describes a route that she flies along the coast when the moon is in her first quarter, and the winds are rising. "In Reverie for past desolations..." Unsure what he means by that, but more practically, he things she can be caught then, on a vessel loading with enough Moth.\"`,
  warmup: 115,
  requirements: { "mariner.matthiasamethystwitnessaramaic": 1 },
  effects: {
    "mariner.matthiasamethystwitness": 1,
    "mariner.matthiasamethystwitnessaramaic": -1,
  },
});
