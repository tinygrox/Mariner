import { generatePort } from "../../../port_generation/port_generation.mjs";
import { market, smith } from "../../../port_generation/helpers.mjs";
import { theater, theaterSlots } from "../../../port_generation/helpers.mjs";
import { ARCHEOLOGIST } from "./archeologist.mjs";
import { ZOHA } from "./zoha.mjs";
import { Galantha } from "../galantha.mjs";
import { buildCodexEntryBlock, generateCodexEntry } from "../../../codex_generation/codex_generation.mjs";
import { SMITH_TYPES } from "../../../tools_generation/generate_tools.mjs";
import { generateTowersOfLightQuest } from "./towers_of_light.mjs";

export function generateHaifa() {
  generatePort({
    portRegions: ["medsea"],
    portId: "haifa",
    label: "Haïfa",
    description:
      "<Sloping down of tripple sacred mount Carmel is a city mosaiced in monotheistic faiths. A polyphony of worship wafts into the sky.>",
    recipeDescriptions: {
      docking: "<>",
      exploreHH:
        "<From the harbor of Haifa, every way to travel is a climb. But after a day of exploring, you can around to the sunset sea, and see all encounters the city gave you laid down below you.>",
      exploreLH:
        "<From the harbor of Haifa, every way to travel is a climb. But after a day of exploring, you can around to the sunset sea, and see all encounters the city gave you laid down below you.>",
    },
    barName: "Mirsa",
    wharf: {
      cargoOptions: [
        { id: "mariner.cargo.for.northsea.close", label: "North Sea" },
        { id: "mariner.cargo.for.medsea.local", label: "Med Sea" },
        { id: "mariner.cargo.for.medsea.local", label: "Med Sea" },
      ],
    },
    portElements: [
      ARCHEOLOGIST,
      ZOHA,
      Galantha,
      market({
        id: "ancientmarket",
        label: "<>",
        description: "<>",
        types: [],
        storyTrading: {
          story: {
            label: "<>",
            icon: "locksmithsdream2",
            description: "<>",
            aspects: {
              forge: 1,
            },
          },
          label: "<>",
          startdescription: "<>",
          description: "<>",
        },
      }),
      theater({
        id: "haifatheater",
        label: "<>",
        description: "<>",
        slots: theaterSlots(["mariner.trapping"]),
        storyTrading: {
          story: {
            label: "<>>",
            icon: "burningofunburntgod",
            description: "<>",
            aspects: {
              forge: 1,
            },
          },
          label: "<>",
          startdescription: "<>",
          description: "<>",
        },
      }),
      smith({
        id: "toolsmith",
        smithType: SMITH_TYPES.TOOLSMITH,
        label: "<Toolsmith>",
        description: "<>",
        notDiscoverable: true,
      }),
    ],
  });

  generateCodexEntry(
    "codex.houseoflethe",
    "[Med. Sea] House of Lethe",
    buildCodexEntryBlock(
      {
        "codex.miriam.questgiven": "secret1",
      },
      "1"
    ) +
      "\n\n\n" +
      buildCodexEntryBlock(
        {
          "codex.miriam.pursuitlosstransformation": "secret",
        },
        "2"
      ) +
      "\n\n\n" +
      buildCodexEntryBlock(
        {
          "codex.miriam.mattiaswitness": "secret",
        },
        "3"
      ) +
      "\n\n\n" +
      buildCodexEntryBlock(
        {
          "codex.miriam.pursuitlosstransformation": "secret",
        },
        "4"
      )
  );
}

generateTowersOfLightQuest();
