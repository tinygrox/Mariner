import {
  generateScoreSequenceMinigame,
  QUESTION_STAGE,
  SITUATION_STAGE,
  MINIGAME_STEP_EQUALS,
  HIGHEST_LORE_QUANTITY,
  WARNING_ABOVE_THRESHOLD,
} from "../../score_sequence_minigame_generation.mjs";
import { SIGNALS } from "../../generate_signal_aspects.mjs";

export function generateStruggimentoFinale() {
  generateScoreSequenceMinigame({
    id: "imago",
    startScore: 10,
    scoreStyle: { label: "<Distance from the Imago>", description: "<We must catch the Imago, like Matthias did before.>" },
    warnings: [WARNING_ABOVE_THRESHOLD(60, "Dangerously far, we risk losing the ship!")],
    defaultHelpSlot: {
      id: `help`,
      label: "Help",
      required: {
        tool: 1,
        "mariner.influence": 1,
        "mariner.crew": 1,
        "mariner.song": 1,
        "mariner.inspiration": 1,
        "mariner.tune": 1,
        "mariner.hearts.halfheart": 1,
        "mariner.hearts.lackheart": 1,
      },
      forbidden: {
        "mariner.crew.exhausted": 1,
        "mariner.tool.broken": 1,
        "mariner.lackheart.exhausted": 1,
        "mariner.halfheart.exhausted": 1,
      },
    },
    startRecipe: {
      label: "An Opening in the Waves",
      startdescription:
        "We slink through the gap, and we can now see the Blazing Galleon sailing the trembling airs between the clouds. The Imago now unfurls sails where once her she soared on her wings. We can spot her scars, bright as a twinned venus, but hotter still we feel the embers of her eyes.",
      jumpstart: true,
    },
    stages: [
      QUESTION_STAGE({
        id: "firstsighting",
        label: "<The First Sighting>",
        startdescription:
          "<Though she is only a mirage on the horizon, our gazes have locked. Even though the wind is steadily at our back, it still carries her voice to us. What she says is not quite an invitation, and not quite a warning, but carries a question. Do we know who we follow? do we know what we started? do we know who we hunt?>",
        requirements: MINIGAME_STEP_EQUALS(0),
        slots: [{
          id: `sighting`,
          label: "A Tale",
          description: "I must impress her with my knowledge, or impress into her that i know the hunters Thrill",
          required: {
            "mariner.booktalksabout.imago": 1,
            "mariner.story": 1,
            "mariner.thrill": 1
          },
        },],
        outcomes: [
          {
            id: "success",
            label: "We Hunt The Pursuer Pursued",
            startdescription:
              "We whisper of the ancient daliance that has bound her to these coasts. Of the Imago before the Forge, before Mattias. Few know what she did with the Drowned Witch, but we know it binds her to these coasts. She waits to hear our answer, waits long enough for us to see the facets of her eyes glimmer in the sun, before the Galleon finally begins to accelarate across the sky.",
            requirements: { "mariner.stories.imago": 1 },
            effects: {
              "mariner.minigames.scoresequence.imago.score": 20,
            },
          },
          {
            id: "loss",
            label: "We Hunt the Imago Who Was Pursued",
            startdescription:
              "We sing of obsession and struggle and pursuit. We laud the interest that blooms between two souls who find their even match. Chase and flight and sacrifice, and what can be lost and gained in change and flames. The wind carries back her laughter to us before her ship turns and it begins to sail away from us chasing the clouds. The Hunt begins.",
            requirements: { "mariner.booktalksabout.imago": 1 },
            effects: {
              "mariner.minigames.scoresequence.imago.score": 30,
            },
          },
          {
            id: "thrill",
            label: "We Hunt the Imago Who Was Pursued",
            startdescription:
              "We sing of obsession and struggle and pursuit. We laud the interest that blooms between two souls who find their even match. Chase and flight and sacrifice, and what can be lost and gained in change and flames. The wind carries back her laughter to us before her ship turns and it begins to sail away from us chasing the clouds. The Hunt begins.",
            requirements: { "mariner.thrill": 1 },
            effects: {
              "mariner.minigames.scoresequence.imago.score": 30,
            },
          },
          {
            id: "nill",
            label: "We Hunt our Prey",
            startdescription:
              "We call out our answer, but as the first words leave our lip, the Plate Galleon allready turns and slips away like a meteor through the sky. We hustle to mount a pursuit, but it is all we can do not to lose sight of her in the horizons glare.",
            effects: {
              "mariner.minigames.scoresequence.imago.score": 40,
            },
          },
        ],
      }),
      SITUATION_STAGE({
        id: "purplecoast",
        label: "<Chasing past a Purple-Shell Coast>",
        startdescription:
          "<We are in pursuit of the shimmering galley that the Imago is known to sail since she has relinquished her wings. It darts between the clouds weightlesly as we drag ourself forward on the surf before the rocky coast. It will take excellent sailing on the sea to keep chase with vessel made of starlight and industry. We must excell with the Rigors of Forge, the Tenacity of Edge or the Vigor of Heart.>",
        requirements: MINIGAME_STEP_EQUALS(1),
        defaultOutcome:
        {
          id: "failure",
          effects: {
            "mariner.minigames.scoresequence.imago.score": 10,
          },
          label: "<The Wind does not Catch>",
          startdescription:
            "<The wind does not favor us, as it is at the beck and call of its mistress. We cannot wrangle it, nor do can we get it to cooperate, so we struggle onwards, waddling like a sow. Many more performances like this an the Blazing ship shall once again dissolve into vapor on the wind.>",
        },

        availableActions: [
          {
            id: "edge",
            label: "<A Steady Clip: Use Edge>",
            requirements: HIGHEST_LORE_QUANTITY("edge", 7),
            outcomes: [
              {
                id: "success",
                effects: {
                  "mariner.minigames.scoresequence.imago.score": -7,
                  label: "<A Steady Clip: Use Edge>",
                  startdescription:
                    "<Soon we will leave the olive trees and the turquoise coves behind us, as our eyes are only focussed upon the stern that sails above us, rising and falling upon astral waves.>",

                },
              },
            ],
          },
          {
            id: "forge",
            label: "<A Steady Clip: Use Forge>",
            requirements: HIGHEST_LORE_QUANTITY("forge", 7),
            outcomes: [
              {
                id: "success",
                label: "<A Steady Clip: Use Forge>",
                startdescription:
                  "<Soon we will leave the olive trees and the turquoise coves behind us, as our eyes are only focussed upon the stern that sails above us, rising and falling upon astral waves.>",

                effects: {
                  "mariner.minigames.scoresequence.imago.score": -7,
                },
              },
            ],
          },
          {
            id: "heart",
            label: "<A Steady Clip: Use Heart>",
            requirements: HIGHEST_LORE_QUANTITY("heart", 7),
            outcomes: [
              {
                id: "success",
                label: "<A Steady Clip: Use Heart>",
                startdescription:
                  "<Soon we will leave the olive trees and the turquoise coves behind us, as our eyes are only focussed upon the stern that sails above us, rising and falling upon astral waves.>",

                effects: {
                  "mariner.minigames.scoresequence.imago.score": -7,
                },
              },
            ],
          },
        ],
      }),
      SITUATION_STAGE({
        id: "storm",
        label: "<Echoed Storm>",
        startdescription:
          "<A low roar and shrill shrieks are carried to us by the winds. On the vanishing coastline, the shimmer of flame spreads like spilled lamp-oil. We bear witness to the flicker of thousands souls being snuffed out, a hundred leagues away and a dreamt eternity ago. I can use the blaze to push forward but accept the lashing soul with giving Heart, or i can protect myself with solemn Winter.>",
        requirements: MINIGAME_STEP_EQUALS(2),
        defaultOutcome:
        {
          id: "failure",
          label: "<Echoed Misery>",
          startdescription:
            "<The echoed misery proves a distraction not only for me, but also for my crew. We do not sail the ship as well as she should. The once-culture on the coast confligrating in echoes of chaos and barbarism are reflected on our deck. The Imago can hold what ever vigil she holds in privacy, as we are too far to make out any detail.>",
          effects: {
            "mariner.minigames.scoresequence.imago.score": 10,

          },
          aspects: { [SIGNALS.EXHAUST_LH]: 1, [SIGNALS.EXHAUST_HH]: 1 },
        },

        availableActions: [
          {
            id: "winter",
            label: "<Remembered Blaze>",
            requirements: HIGHEST_LORE_QUANTITY("winter", 5),
            outcomes: [
              {
                id: "success",
                label: "<Remembered Blaze>",
                startdescription:
                  "<Next to us, the coast alights in a remembered blaze.The heat of the Forge sears memory, but the scars in the skin of world can act as a reminder. As we hustle to make headway on the ship, She and I both have our eyes set on the shoreline. But what she confronts consumes me, and my heart is kindled with recalled pain. I will not be able to access my Hearts until I find the time to rest them.>",

                effects: {
                  "mariner.minigames.scoresequence.imago.score": -2,
                },
              },
            ],
          },
          {
            id: "heart",
            label: "<Bleeding Sympathy>",
            requirements: HIGHEST_LORE_QUANTITY("heart", 5),
            outcomes: [
              {
                id: "success",
                label: "<Bleeding Sympathy>",
                startdescription:
                  "<I still my bleeding sympathy, as I watch a long-passed tragedy unfold. The Imago makes this piligrimage past inflicted scars, and I too can bear witness without giving of myself in echoed misery. Only the Elegiast remembers the names of all the voiceless dead, but I will hold a silent vigil on the ship, giving onto the dead what they are due, but keeping close what we mortals cannot share.>",

                effects: {
                  "mariner.minigames.scoresequence.imago.score": -7,
                },
                furthermore: [
                  {
                    target: "~/extant",
                    aspects: { [SIGNALS.EXHAUST_LH]: 1, [SIGNALS.EXHAUST_HH]: 1 },
                  },
                ],
              },
            ],
          },
        ],
      }),
      SITUATION_STAGE({
        id: "ash",
        label: "<Ashen Skies>",
        startdescription:
          "<The world starts to drain of of both color and light. Flakes of ash choke the sky and churn the seas, our sails grow slack with stains.The Violet Imago has summoned a memory of fire and change, if it is allowed to stain us it will rob us of command of the wind.If I apply myself with Lantern  keep our focus on our prey, or we can annoint our ship to stave of the influence with Grail.>",
        requirements: MINIGAME_STEP_EQUALS(3),
        defaultOutcome:
        {
          id: "failure",
          label: "<Smothered Sails>",
          startdescription:
            "<We attempt, but the acrid taste of ashes is is too much to to overcome. It heralded a change that alterned the worlds course immesurably. and now, it has also altered our course. It will take much effort to catch up with her, and our sails are now riddled with her influence.>",
          effects: {
            "mariner.minigames.scoresequence.imago.score": 10,
          },
          furthermore: [
            {
              target: "~/extant",
              aspects: { [SIGNALS.PERFORMANCE_CLEAR_WEATHER]: 1 },
            },
          ],
        },
        availableActions: [
          {
            id: "grail",
            label: "<Embrace the Stain>",
            requirements: HIGHEST_LORE_QUANTITY("grail", 5),
            outcomes: [
              {
                id: "success",
                label: "<Embrace the Stain>",
                startdescription:
                  "<We set all the sails we can, so she catches the wind even as it cracks and blackens and the influence of the Imago seeps in. Soon it shall be hers, and will not work with us as it used to. We can still catch the wind, if we put our backs in it. but I must rely on the crew now more then on my ship, until we can wash this influence of her.>",
                effects: {
                  "mariner.minigames.scoresequence.imago.score": -7,
                },
                furthermore: [
                  {
                    target: "~/extant",
                    aspects: { [SIGNALS.PERFORMANCE_CLEAR_WEATHER]: 1 },
                  },
                ],
              },
            ],
          },
          {
            id: "lantern",
            label: "<Bleach the Sails>",
            requirements: HIGHEST_LORE_QUANTITY("lantern", 5),
            outcomes: [
              {
                id: "success",
                label: "<Bleach>",
                startdescription:
                  "<I leave the chase to a skeleton crew, while I lower the sail with the most trusted hands to protect it from this ashen influence. We garner the power of what is bright and pure.>",
                effects: {
                  "mariner.minigames.scoresequence.imago.score": -2,
                },

              },
            ],
          },
        ],
      }),
      SITUATION_STAGE({
        id: "islands",
        label: "<Maneuvering through a Thousand Islands>",
        startdescription:
          "<The Blazing Ship dives down, almost coming level with us. It now weaves between the islands and islets of the archipellago, tempting us towards hidden reefs and dangerous eddies. It is both threat and temptation, for between the traps lie the quickest shortcuts in the routes. If i wish to push my crew and our adventage I can do so with Edge, or I can take the safer, steadier route with Heart.>",
        requirements: MINIGAME_STEP_EQUALS(4),
        defaultOutcome:
        {
          id: "failure",
          label: "<Mislead>",
          startdescription:
            "<We were mislead and tempted into a channel that was too narrow for us to travel. We did not manage to avoid the side, and we scrape our bow. The sinking feeling in our stomach is not just that we can hear the bubbling water in our hold, but also from the echoing laughter heard from the ship speeding away from us.>",
          effects: {
            "mariner.minigames.scoresequence.imago.score": 10,
          },
          furthermore: [
            {
              mutations: {
                target: "~/extant",
                filter: "mariner.ship",
                mutate: "mariner.ship.damage",
                level: 1,
                additive: true,
              },
            },
          ],
        },

        availableActions: [
          {
            id: "edge",
            label: "<Pursue with Hard work>",
            requirements: HIGHEST_LORE_QUANTITY("edge", 5),
            outcomes: [
              {
                id: "success",
                label: "<Pursue with Exhausting Ardor>",
                startdescription:
                  "<We press on, and when the pressure gets to high to stand we press on some more. We have succeeded at the risky ventures and shortcuts that Imago layed as traps for us. I may need to run the Kite with a skeleton crew for a while as my crew recovers.>",
                effects: {
                  "mariner.minigames.scoresequence.imago.score": -7,
                },
                furthermore: [
                  {
                    target: "~/extant",
                    mutations: {
                      "[mariner.crew] && [mariner.crew.exhausted]<1": {
                        mutate: "mariner.crew.exhausted",
                        level: 5,
                        limit: 3,
                      },
                    },
                  },
                ],
              },
            ],
          },
          {
            id: "heart",
            label: "<Pursue with Care>",
            requirements: HIGHEST_LORE_QUANTITY("heart", 5),
            outcomes: [
              {
                id: "success",
                label: "<With Skill and Care>",
                startdescription:
                  "<We sail with care, both for our surroundings and for eachother. when a trap is set, the worst thing you can do walk into it willingly, and risk life and limb.>",

                effects: {
                  "mariner.minigames.scoresequence.imago.score": -2,
                },
              },
            ],
          },
        ],
      }),
      SITUATION_STAGE({
        id: "secondchase",
        label: "<Chase across the Ladies Domain>",
        startdescription:
          "<The shimmering Galleon now dances above great waves instead of islands. There will be few obstacles in either of our way for a while, and the Imago seems to be gaining speed. It will take excellent sailing to accelarate across that was once first crossed by the phoenicians We must excell with the Rigors of Forge, the Tenacity of Edge or the Vigor of Heart.>",
        requirements: MINIGAME_STEP_EQUALS(5),
        defaultOutcome:
        {
          id: "failure",
          label: "<The Wind does not Catch>",
          startdescription:
            "<The wind does not favor us, as it is at the beck and call of its mistress. We cannot wrangle it, nor do can we get it to cooperate, so we struggle onwards, waddling like a sow. Many more performances like this an the Blazing ship shall once again dissolve into vapor on the wind.>",
          effects: {
            "mariner.minigames.scoresequence.imago.score": 10,
          },
        },
        availableActions: [
          {
            id: "edge",
            label: "<A Steady Clip: Use Edge>",
            requirements: HIGHEST_LORE_QUANTITY("edge", 7),
            outcomes: [
              {
                id: "success",
                label: "<A Steady Clip: Use Edge>",
                startdescription:
                  "<We find our rhythm on the rising and falling sea, and are now alone with the sea, the clouds, and the shimmering ship flying before use.>",
                effects: {
                  "mariner.minigames.scoresequence.imago.score": -7,
                },
              },
            ],
          },
          {
            id: "forge",
            label: "<A Steady Clip: Use Forge>",
            requirements: HIGHEST_LORE_QUANTITY("forge", 7),
            outcomes: [
              {
                id: "success",
                label: "<A Steady Clip: Use Forge>",
                startdescription:
                  "<We find our rhythm on the rising and falling sea, and are now alone with the sea, the clouds, and the shimmering ship flying before use.>",

                effects: {
                  "mariner.minigames.scoresequence.imago.score": -7,
                },
              },
            ],
          },
          {
            id: "heart",
            label: "<A Steady Clip: Use Heart>",
            requirements: HIGHEST_LORE_QUANTITY("heart", 7),
            outcomes: [
              {
                id: "success",
                label: "<A Steady Clip: Use Heart>",
                startdescription:
                  "<We find our rhythm on the rising and falling sea, and are now alone with the sea, the clouds, and the shimmering ship flying before use.>",
                effects: {
                  "mariner.minigames.scoresequence.imago.score": -7,
                },
              },
            ],
          },
        ],
      }),
      SITUATION_STAGE({
        id: "joyouschase",
        label: "<Joys of the Open Sea>",
        startdescription:
          "<Even the adrenaline rush of a chase can lose its edge as the hours carry us from novelty to normalcy. It is not yet boring, but there could again be joy in the open sea and the chase if we allow ourselves to indulge in the sun and the wind and the waves with indulgent Grail. I could also keep the reigns tight, and work our crew with a sneer of cold command of Winters power to close the gap to the Imago.>",
        requirements: MINIGAME_STEP_EQUALS(6),
        defaultOutcome:
        {
          id: "success",
          label: "<A Roiling Chaos>",
          startdescription:
            "<In the middle of of the Mediterranean, the surf from many coasts comes together in a tangle of waves and weather patterns. This day, we cannot find the rhytm nor the joy, nor the determination and pushed to and fro like a waddling sow. The Blazing Galleon, undetered, speeds ahead of us.>",
          effects: {
            "mariner.minigames.scoresequence.imago.score": 10,
          },
        },
        availableActions: [
          {
            id: "grail",
            label: "<Pursue Joy>",
            requirements: HIGHEST_LORE_QUANTITY("grail", 5),
            outcomes: [
              {
                id: "success",
                label: "<A Shanty on a Golden Afternoon>",
                startdescription:
                  "<There is beauty in the act of chasing, in the dance between the Kite and the Blazing Galleon. The rise and fall of the kite on the slow breathing of the sea, heated by the suns caress. I lead my crew in a shanty as they work, and keep my hungry eyes focussed on the Blazing Galeon.>",
                effects: {
                  "mariner.minigames.scoresequence.imago.score": -2,
                  "mariner.thrill": 1,
                },
                movements: {
                  "~/tabletop": "mariner.thrill",
                },
              },
            ],
          },
          {
            id: "winter",
            label: "<Pursue Focus and Speed",
            requirements: HIGHEST_LORE_QUANTITY("winter", 5),
            outcomes: [
              {
                id: "success",
                label: "<The Dead Quiet of Obsession>",
                startdescription:
                  "<We work in complete silence, carving through the waves like cutting a line through the sea. Grim determination is no substitute for joy, but it can be a replacement.>",
                effects: {
                  "mariner.minigames.scoresequence.imago.score": -7,
                },
              },
            ],
          },
        ],
      }),
      QUESTION_STAGE({
        id: "secondsighting",
        label: "<The Second Sighting>",
        startdescription:
          "<In the middle of the middle, when I least suspect another engagement, the Imago voices reaches the Kite again. So who chases me, she asks. adventurer, scoundrel, bard? If you are so in tune with the world and her stories, sing to me of my first dalliance. [In three songs I must describe her first paramour in every aspect]>",
        requirements: MINIGAME_STEP_EQUALS(7),
        slots: [{
          id: `first_slot`,
          label: "Song or Passion",
          description: "<The Imago's first Dalliance first aspect. Or, if I do not know what Aspect to serenade, i can impress her with a Thrill>",
          required: {
            "mariner.song": 1,
            "mariner.thrill": 1
          },
        },
        {
          id: `second_slot`,
          label: "Song or Passion",
          description: "<The Imago's first Dalliance first aspect. Or, if I do not know what Aspect to serenade, i can impress her with a Thrill>",
          required: {
            "mariner.song": 1,
            "mariner.thrill": 1
          },
        },
        {
          id: `third_slot`,
          label: "Song or Passion",
          description: "<The Imago's first Dalliance first aspect. Or, if I do not know what Aspect to serenade, i can impress her with a Thrill>",
          required: {
            "mariner.song": 1,
            "mariner.thrill": 1
          },
        },],
        outcomes: [
          {
            id: "success",
            label: "the Lady of Crossroads",
            startdescription:
              "The Imago first strayed from the Dappled King with the seaborn witches sister. Little is now known of the why and the how, only that, like many things in history, it did not last. Perhaps they pursuid a whimsy? Perhaps they shared a thirst. Perhaps they spend a season finding meaning in what later no longer satisfied.",
            requirements: { "mariner.song.moth": 1, "mariner.song.grail": 1, "mariner.song.heart": 1 },
            effects: {
              "mariner.minigames.scoresequence.imago.score": -5,
            },
          },
          {
            id: "hunter",
            label: "The Hunter between the Statuary",
            startdescription:
              "We sing of a quiet house of quiet men, and the hunter who arose from them. He dealt death to the deathless. But then he was challenges.Two wings in the darkness, the perfect price, the perfect trap.Their encounter is a dance and then then a dalliance. Together, they grow beyond the walls that emprison, the darkness that obscures, the skin that chaves. Into the fire they fly.",
            requirements: { "mariner.song.moth": 1, "mariner.song.grail": 1, "mariner.song.forge": 1 },
          },
          {
            id: "thrill",
            label: "The Hunter between the Statuary",
            startdescription:
              "We sing of a quiet house of quiet men, and the hunter who arose from them. He dealt death to the deathless. But then he was challenges.Two wings in the darkness, the perfect price, the perfect trap.Their encounter is a dance and then then a dalliance. Together, they grow beyond the walls that emprison, the darkness that obscures, the skin that chaves. Into the fire they fly.",
            requirements: { "mariner.thrill": 1 },
          },
          {
            id: "nill",
            label: "We Sing a Meaningless Ditty",
            startdescription: "Unsure how to respond, we respond with whatever comes to mind.",
            effects: {
              "mariner.minigames.scoresequence.imago.score": 10,
            },
          },
        ],
      }),
      SITUATION_STAGE({
        id: "gap",
        label: "<Sustaining through the Gap>",
        startdescription:
          "<As time wears on and hours get strung together like beads. Our biology brings up concerns more mundanes mystery and pursuit.The rumble we hear now is no distant thunder, no remembered eruptions, but our bodies demanding sustainance.>",
        requirements: MINIGAME_STEP_EQUALS(8),
        slots: [{
          id: `first_slot`,
          label: "Food or Passion",
          description: "<I must feed my crew... If I do not feed there bodies, I must feed their soul>",
          required: {
            "mariner.rations": 1,
            "mariner.thrill": 1
          },
        },],
        defaultOutcome:
        {
          id: "failure",
          label: "<Hunger gnaws>",
          description: "<Weakness overtakes the crew>",
          effects: {
            "mariner.minigames.scoresequence.imago.score": 10,
          },
          furthermore: [
            {
              target: "~/extant",
              mutations: {
                "[mariner.crew] && [mariner.crew.exhausted]<1": {
                  mutate: "mariner.crew.exhausted",
                  level: 5,
                  limit: 2,
                },
              },
            },
          ],
        },
        availableActions: [
          {
            id: "succes",
            label: "<Supply a Feast>",
            requirements: { "mariner.rations": 1 },
            outcomes: [
              {
                id: "success",
                label: "<Fuel for Obsession>",
                startdescription:
                  "<A ship at sea should always carry twice the amount of rations you expect for your journey, as the wisdom goes. Now we enjoy the fruit of that planning, allowing us to recover on this relatively uneventful part of the journey.>",

                effects: {
                  "mariner.minigames.scoresequence.imago.score": -5,
                },
              },
            ],
          },
          {
            id: "succes",
            label: "<Supply a Goal>",
            requirements: { "mariner.thrill": 1 },
            outcomes: [
              {
                id: "success",
                label: "<Obsession as Fuel>",
                startdescription:
                  "<Who love what they do every day will find it sustaining them into Eternity>",

                effects: {
                  "mariner.minigames.scoresequence.imago.score": -5,
                },
              },
            ],
          },
        ],
      }),
      SITUATION_STAGE({
        id: "counterwinds",
        label: "<The Counter Winds>",
        startdescription:
          "<We have passed the open sea, and can see a peninsula rising before us.The enchantments that the patricians once wove to bar Emperor-encumbant Elegabalus still linger. Though they failed to bar the Eastern Dawn, they will keep out lesser beings. Any summon under my command will be warded from the peninsula, unless I use my Lantern to guide them through. Else I use Edge and keep my speed up.>",
        requirements: MINIGAME_STEP_EQUALS(9),
        defaultOutcome:
        {
          id: "failure",
          label: "<Dispersed like Smoke>",
          startdescription:
            "<You rarely notice the magic of a routine until after it passes, and the Counter-Winds rock the boat and fray the espirit the corpse. No longer is our cooperation a ceaseless dance or a voiceless understanding. But we pass the barrier of the winds, further behind on the Imago.>",

          effects: {
            "mariner.minigames.scoresequence.imago.score": 10,
          },
          furthermore: [
            {
              target: "~/extant",
              effects: { "mariner.summon": -100 },
              aspects: { [SIGNALS.WOUND_HH]: 1 },
            },
          ],
        },
        availableActions: [
          {
            id: "lantern",
            label: "<Pursue a sustaining Focus>",
            requirements: HIGHEST_LORE_QUANTITY("lantern", 5),
            outcomes: [
              {
                id: "success",
                label: "<A Sustaining Focus>",
                startdescription:
                  "<The counter winds attempt to repell the spirits of my ship, but i will not not allow them to fade, even as the winds pull and push them to bleed and fray. But the light of the lantern does not allow the fading of colors, and I will not allow of the fading of any of my crew, true, real or metaphsysical.>",
                effects: {
                  "mariner.minigames.scoresequence.imago.score": -2,
                },
              },
            ],
          },
          {
            id: "edge",
            label: "<Pursue Speed with no Distractions>",
            requirements: HIGHEST_LORE_QUANTITY("edge", 5),
            outcomes: [
              {
                id: "success",
                label: "<Fading of Distractions>",
                startdescription:
                  "<As I let everything but the hunt fade from my mind, so do I let any spirits I had bound fade. Only the chase matters, the speed and the persistance. The rest is mist on the wind.>",

                effects: {
                  "mariner.minigames.scoresequence.imago.score": -7,
                },
                furthermore: [
                  {
                    target: "~/extant",
                    effects: { "mariner.summon": -100 },
                    aspects: { [SIGNALS.WOUND_HH]: 1 },
                  },
                ],
              },
            ],
          },
        ],
      }),
      SITUATION_STAGE({
        id: "scythes",
        label: "<Through the Strait of Scythes>",
        startdescription:
          "<No more monsters dwell here, at least they do not show in flesh or memory. But echoes of a whirlpool are still evoked in the rushing tides adn torrential currents. There are also many ships on this crossroads between east and west, and north and south. We will have to slip between the rushing currents and the rushing ships with the power of Knock or Moth.>",
        requirements: MINIGAME_STEP_EQUALS(10),
        defaultOutcome:
        {
          id: "failure",
          label: "<Blundering through the Strait>",
          startdescription: "<failing is never enjoyable, but failing with an audience is a particular horror.>",
          effects: {
            "mariner.minigames.scoresequence.imago.score": 10,
          },
          furthermore: [
            {
              target: "~/extant",
              mutations: {
                "[mariner.crew] && [mariner.crew.exhausted]<1": {
                  mutate: "mariner.crew.exhausted",
                  level: 5,
                  limit: 3,
                },
              },
            },
            {
              target: "~/extant",
              mutations: {
                filter: "mariner.ship",
                mutate: "mariner.ship.damage",
                level: 1,
                additive: true,
              },
            },
          ],
        },
        availableActions: [
          {
            id: "knock",
            label: "Pursue with Exhausting precision",
            requirements: HIGHEST_LORE_QUANTITY("knock", 5),
            outcomes: [
              {
                id: "success",
                label: "<Narrowly through the Strait>",
                startdescription:
                  "<We sail the strait on a knives-edge. careful of the currents and the rocks. We slip between ships like a thief in the night.>",

                effects: {
                  "mariner.minigames.scoresequence.imago.score": -5,
                },
                furthermore: [
                  {
                    target: "~/extant",
                    mutations: {
                      "[mariner.crew] && [mariner.crew.exhausted]<1": {
                        mutate: "mariner.crew.exhausted",
                        level: 5,
                        limit: 3,
                      },
                    },
                  },
                ],
              },
            ],
          },
          {
            id: "moth",
            label: "Pursue with Reckless Speed",
            requirements: HIGHEST_LORE_QUANTITY("moth", 5),
            outcomes: [
              {
                id: "success",
                label: "<Bluntly through the Strait>",
                startdescription:
                  "<We move foward, taking risks which we know will be harmful as well as profitable. We are hopeful this will not end the Kite, as we gain on our prey after the strait.>",
                effects: {
                  "mariner.minigames.scoresequence.imago.score": -7,
                },
                furthermore: [
                  {
                    mutations: {
                      target: "~/extant",
                      filter: "mariner.ship",
                      mutate: "mariner.ship.damage",
                      level: 1,
                      additive: true,
                    },
                  },
                ],
              },
            ],
          },
        ],
      }),
      SITUATION_STAGE({
        id: "thirdchase",
        label: "<Chasing up the River of Lyres>",
        startdescription:
          "<Imago's ship is now so low that her blade skims the water. It speeds into the mouth of a river. The wind turns with her, and we can rush after them catching the same wind. We must excell with the Rigors of Forge, the Tenacity of Edge or the Vigor of Heart.>",
        requirements: MINIGAME_STEP_EQUALS(11),
        defaultOutcome:
        {
          id: "success",
          label: "<The Wind does not Catch>",
          startdescription:
            "<The wind does not favor us, as it is at the beck and call of its mistress. We cannot wrangle it, nor do can we get it to cooperate, so we struggle onwards, waddling like a sow. Many more performances like this an the Blazing ship shall once again dissolve into vapor on the wind.>",

          effects: {
            "mariner.minigames.scoresequence.imago.score": 10,
          },
        },
        availableActions: [
          {
            id: "edge",
            label: "<A Steady Clip: Use Edge>",
            requirements: HIGHEST_LORE_QUANTITY("edge", 7),
            outcomes: [
              {
                id: "success",
                label: "<A Steady Clip: Use Edge>",
                startdescription:
                  "<We rush up the water, managing the vastly different rigors of river sailing. We keep pace with the Blazing Ship, which is now so close i can see the cindered canvas sails, the billowing smoke rising from the hold.>",
                effects: {
                  "mariner.minigames.scoresequence.imago.score": -7,
                },
              },
            ],
          },
          {
            id: "forge",
            label: "<A Steady Clip: Use Forge>",
            requirements: HIGHEST_LORE_QUANTITY("forge", 7),
            outcomes: [
              {
                id: "success",
                label: "<A Steady Clip: Use Forge>",
                startdescription:
                  "<We rush up the water, managing the vastly different rigors of river sailing. We keep pace with the Blazing Ship, which is now so close i can see the cindered canvas sails, the billowing smoke rising from the hold.>",

                effects: {
                  "mariner.minigames.scoresequence.imago.score": -7,
                },
              },
            ],
          },
          {
            id: "heart",
            label: "<A Steady Clip: Use Heart>",
            requirements: HIGHEST_LORE_QUANTITY("heart", 7),
            outcomes: [
              {
                id: "success",
                label: "<A Steady Clip: Use Heart>",
                startdescription:
                  "<We rush up the water, managing the vastly different rigors of river sailing. We keep pace with the Blazing Ship, which is now so close i can see the cindered canvas sails, the billowing smoke rising from the hold.>",
                effects: {
                  "mariner.minigames.scoresequence.imago.score": -7,
                },
              },
            ],
          },
        ],
      }),
      SITUATION_STAGE({
        id: "impossible",
        label: "<An Impossible Needle's Eye>",
        startdescription:
          "<The further we go, the narrower the river becomes. At this point we have sailed where no Brigantine should ever and the winds that have settled in our sails impossible snug. We can allready see the tunnel from which the river springs forth. We will need to fit the Kite where no ship is meant to fit, and it will take all the Knock we can muster to slip through that treshold.>",
        requirements: MINIGAME_STEP_EQUALS(12),
        defaultOutcome: {
          id: "failure",
          label: "<A Horrendous Crash>",
          startdescription:
            "<We rush up to the mountains edge, and there is simply no way the Kite could fit into the tunnels opening. Somehow, the harder the water rushes down, the harder we rush up it. We slam into the tunnelmouth, and instantly, whatever spell pulled us forward dissolves. The Kite starts to drift down, possibly in pieces, as we make our way back to the sea.>",
          aspects: { "mariner.endgame.crash": 1, }
        },
        availableActions: [
          {
            id: "knock",
            label: "<Passing Through>",
            requirements: HIGHEST_LORE_QUANTITY("knock", 7),
            outcomes: [
              {
                id: "success",
                label: "<Passing through the Eye of the Needle>",
                startdescription:
                  "<As the crew lowers the sails and brings in the line, we invoke the Mother-of-Ants, who pierces and the Beach-crow who opens the earth.The caverns entrance opens before us like a gaping beak and then an encircling darkness. The light behind quickly shrinks to a starlight needlepoint, and the darkness holds us for a moment.>",
                aspects: { "mariner.endgame.reach": 1, }
              },
            ],
          },
        ],
      }),
    ],
    endings: [
      {
        id: "lostship",
        requirements: { "mariner.minigames.score": 70 },
        label: "<Lost the ship>",
      },
      {
        id: "reach",
        requirements: { "mariner.endgame.reach": 1 },
        //link eventually
      },
      {
        id: "crash",
        requirements: { "mariner.endgame.crash": 1 },
        furthermore: [
          {
            target: "~/extant",
            mutations: {
              "[mariner.crew] && [mariner.crew.exhausted]<1": {
                mutate: "mariner.crew.exhausted",
                level: 5,
                limit: 3,
              },
            },
          },
          {
            target: "~/extant",
            mutations: {
              filter: "mariner.ship",
              mutate: "mariner.ship.damage",
              level: 1,
              additive: true,
            },
          },
        ],
      },
    ],
  });


}
