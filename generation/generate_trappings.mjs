import { LORE_ASPECTS } from "./generation_helpers.mjs";

const ifExists = (val, objToReturn) => (val ? objToReturn : {});

export function trappingAspects({ value, types, aspects }) {
  let loreTrappingAspects;
  let loreAspects;
  if (Array.isArray(aspects)) {
    loreTrappingAspects = Object.fromEntries(aspects.map((aspect) => [`mariner.trappings.${aspect}`, 1]));
    loreAspects = Object.fromEntries(aspects.map((aspect) => [aspect, 1]));
  } else {
    loreTrappingAspects = Object.fromEntries(
      Object.entries(aspects).map(([aspect, amount]) => [`mariner.trappings.${aspect}`, amount])
    );
    loreAspects = Object.fromEntries(Object.entries(aspects).map(([aspect, amount]) => [aspect, amount]));
  }

  return {
    "mariner.trapping": 1,
    ...ifExists(value, { "mariner.trapping.worth": value }),
    ...loreTrappingAspects,
    ...loreAspects,
    ...Object.fromEntries(types.map((type) => [`mariner.trappingtypes.${type}`, 1])),
  };
}

function generateBasicTrapping(loreAspect, trappingData) {
  const id = `mariner.trappings.${loreAspect}.1`;
  mod.setElement("trappings", {
    id,
    overlays: [
      `trappings/${loreAspect}/background`,
      ...new Array(trappingData.stages ?? 4).fill(0).map((_, index) => ({
        image: `trappings/${loreAspect}/quantity_${index + 1}`,
        expression: index <= 2 ? `[${id}] = ${index + 1}` : `[${id}] >= ${index + 1}`,
      })),
    ],
    label: trappingData.label,
    description: trappingData.description,
    aspects: {
      "mariner.trapping": 1,
      [`mariner.trappings.${loreAspect}`]: 1,
      [loreAspect]: 1,
    },
  });
}

function generateBasicTrappings() {
  const trappingsData = {
    knock: {
      label: "The Trappings of Knock Rites",
      description: "Locks and lockpicks. Venoms and hawthorne. A scalpel and a snakeskin.",
    },
    moth: {
      label: "The Trappings of Moth Rituals",
      description: "Dried flowers and potent leaves. Strange poppets and forgotten treasures. A set of childrens' teeth",
    },
    lantern: {
      label: "The Trappings of Lantern Communions",
      description: "Amber and polished mirrors. The whitest candle wax and hollowed eggshells. Origami stars.",
    },
    forge: {
      label: "The Trappings of Forge Praxis",
      description: "Shaved iron. A baked clay pot filled with ash. Nine sacred woods, to burn as fuel for the future.",
    },
    edge: {
      label: "The Trappings of Edge Practices",
      description: "Viridian war paint. Ritual knives. Spent bullets.",
    },
    winter: {
      label: "The Trappings of Winter Solemnities",
      description: "Graveyard earth. Polished bone. Idols of forgotten gods.",
    },
    heart: {
      label: "The Trappings of Heart Ceremonies",
      description: "Hand drums and pinecones. A stag's heart and a boar's skin. A string of pearls, pink and white.",
    },
    grail: {
      label: "The Trappings of Grail Sacraments",
      description: "Red berries and honey. A baby's caul and a mother's cord. Stained cups and scraped plates.",
    },
  };
  for (const loreAspect of LORE_ASPECTS) {
    mod.setHiddenAspect("aspects.trappings", {
      id: `mariner.trappings.${loreAspect}`,
    });
    generateBasicTrapping(loreAspect, trappingsData[loreAspect]);
  }
}

function generateTrappingTypes() {
  mod.initializeAspectFile("aspects.trappingtypes", ["trappings"]);
  mod.setAspect("aspects.trappingtypes", {
    id: "mariner.trappingtypes.readable",
    label: "Trapping Type: Records and Writings",
    description: "Script is the memory that will not die.",
  });
  mod.setAspect("aspects.trappingtypes", {
    id: "mariner.trappingtypes.artifact",
    label: "Trapping Type: Artifacts and Relics",
    description:
      "It is hard to pin down the point where something being old goes from a disadvantage to an advantage, but this has crossed that mark.",
  });
  mod.setAspect("aspects.trappingtypes", {
    id: "mariner.trappingtypes.crafts",
    label: "Trapping Type: Crafts And Finery",
    description: "The fine balance of form and function.",
  });
  mod.setAspect("aspects.trappingtypes", {
    id: "mariner.trappingtypes.jewelry",
    label: "Trapping Type: Gems and Jewelry",
    description: "Lustrous, lavish, luxurious.",
  });
  mod.setAspect("aspects.trappingtypes", {
    id: "mariner.trappingtypes.natural",
    label: "Trapping Type: Botanical and Anatomical",
    description: "The treasures that grow from soil or beneath the skin.",
  });
}

export function generateTrappings() {
  mod.initializeElementFile("trappings", ["trappings"]);
  mod.initializeAspectFile("aspects.trappings", ["trappings"]);
  mod.setAspect("aspects.trappings", {
    id: "mariner.trapping",
    label: "Trapping",
    description: "Those items, ingredients and implements that imbue a ritual with its quintessence.",
  });
  mod.setAspect("aspects.trappings", {
    id: "mariner.trapping.worth",
    label: "Worth",
    description: "This is valuable, if I can find who values it.",
  });

  generateTrappingTypes();
  generateBasicTrappings();
}
