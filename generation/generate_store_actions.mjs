import { optionSelectionRecipe } from "./option_selector.mjs";

export const simpleBuyItem = (
  itemId,
  slotLabel,
  slotDescription,
  notEnoughFundsLabel,
  notEnoughSpaceLabel,
  notEnoughDescription,
  label,
  description,
  linked,
  cost = 1
) => ({
  slot: { label: slotLabel, description: slotDescription },
  recipes: [
    {
      label: notEnoughSpaceLabel,
      startdescription: "<not enough space>",
      grandReqs: {
        "[~/extant:mariner.holduse] + max(0, [~/extant:mariner.instrument]-1)": "[~/extant:mariner.ship.holdsize]",
      },
    },
    {
      label: notEnoughFundsLabel,
      startdescription: notEnoughDescription,
      grandReqs: { "[~/exterior : funds]": -cost },
    },
    {
      label,
      startdescription: description,
      grandReqs: { "[~/exterior : funds]": cost },
      effects: { [itemId]: 1 },
      furthermore: [
        {
          target: "~/exterior",
          effects: { funds: -cost },
        },
      ],
      linked: (linked && [{ id: linked }]) || undefined,
    },
  ],
});

function generateStoreBuyResourceAction() {
  mod.initializeRecipeFile("recipes.buyresources", ["locations", "stores"]);
  mod.setRecipes(
    "recipes.buyresources",
    ...optionSelectionRecipe({
      id: "mariner.store.buyresources",
      craftable: true,
      actionId: "talk",
      grandReqs: {
        "mariner.store": 1,
        "mariner.location.hostile": -1,
      },
      label: "<Buy At the Supply Store>",
      startdescription:
        "Displayed prices:<br><br>-[1 <sprite name=funds>] Rations<br><br>-[1 <sprite name=funds>] Repair Kit<br><br>-[1 <sprite name=funds>] Medical Supplies<br><br><i>[Stop making choices to leave the store.]</i>",
      description: "<Alright then.>",
      options: [
        // Rations
        simpleBuyItem(
          "mariner.rations",
          "<Rations>",
          "<Exchange hard cash for hardtack>",
          "<Not Enough Funds To Buy Rations>",
          "<Not Enough Space in the Hold to buy Rations>",
          "<For longer voyages, I need to prepair for the ravages of the Third Torment.>",
          "<Buy Rations>",
          "<Hard bread, preserved fruits, salted fish, and enough spice to make them bearable.>",
          "mariner.store.buyresources"
        ),
        // Repair Kits
        simpleBuyItem(
          "mariner.repairkit",
          "<Repair Kit>",
          "<Exchange paper for canvas, and copper coin for iron nails.>",
          "<Not Enough Funds To Buy Repair Kits>",
          "<Not Enough Space in the Hold To Buy Repair Kits>",
          "<With enough supplies, superficial damage at sea can be solved before it become san issue.>", //this text goes in the slot.  is if not enough funds or space is in between.
          "<Buy Repair Kits>",
          "<We can stow enough to patch up leaks and tears.>", //no end description
          "mariner.store.buyresources"
        ),
        // Medical Supplies
        simpleBuyItem(
          "mariner.medicalsupplies",
          "<Medical Supplies>",
          "<Exchange securities for security>",
          "<Not Enough Funds To Buy Medical Supplies>",
          "<Not Enough Space in the Hold To Buy Medical Supplies>",
          "<Quick intervention can stench bleedings, set bones and prevent tragedies.>",
          "<Buy Medical Supplies>",
          "<Preparations for the likely incident, probable issues and for the rest, contingencies.>",
          "mariner.store.buyresources"
        ),
      ],
    })
  );
}

export function generateStoreActions() {
  generateStoreBuyResourceAction();
}
