/*
- starting recipe
- global additional text: appears at the top of the description during the stage warmup
- end conditions: recipe bodies
- starting score value
- stages:
    - stage description & label
    - outcomes
    - variants
        - description & label
        - outcomes
    

To build a Question stage, you use its outcomes and no variants
To build a Situation stage, you use its variants and a single default "no action used" outcome
*/

import { buildAdvancedRefinementBlock } from "./generation_helpers.mjs";

// WARNINGS HELPERS
export function WARNING_BELOW_THRESHOLD(min, minText) {
  return buildAdvancedRefinementBlock(
    [
      {
        aspect: "mariner.minigames.score",
        value: min + 1,
        text: "",
      },
    ],
    minText + "<br>"
  );
}

export function WARNING_ABOVE_THRESHOLD(max, maxText) {
  return buildAdvancedRefinementBlock([
    {
      aspect: "mariner.minigames.score",
      value: max,
      text: maxText + "<br>",
    },
  ]);
}

// STAGE HELPERS
export function QUESTION_STAGE({ id, label, startdescription, requirements, slots, outcomes }) {
  return { id, label, startdescription, requirements, slots, outcomes };
}

export function SITUATION_STAGE({ id, label, startdescription, requirements, slots, defaultOutcome, availableActions }) {
  return {
    id,
    label,
    startdescription,
    requirements,
    slots,
    outcomes: (defaultOutcome && [defaultOutcome]) || [],
    variants: availableActions,
  };
}

// CONDITIONS HELPERS
export function MINIGAME_STEP_EQUALS(number) {
  return { [`[mariner.minigames.step] = ${number}`]: 1 };
}

export function ELEMENT_IS_PRESENT(id) {
  return { [id]: 1 };
}

export function HIGHEST_LORE_QUANTITY(id, minimum = 1) {
  return {
    [`[${id}] >= max([lantern], max([forge], max([edge], max([winter], max([heart], max([grail], max([moth], [knock])))))))`]: 1,
    [id]: minimum,
  };
}

const DEFAULT_HELP_SLOT = {
  id: `help`,
  label: "Help",
  required: { tool: 1, "mariner.influence": 1, "mariner.crew": 1 },
  forbidden: { "mariner.crew.exhausted": 1, "mariner.tool.broken": 1 },
};

const SLOT_INSTANCE = (def, i) => ({ ...def, id: `${def.id}_${i}` });

export function generateMinigameFiles() {
  if (!mod.aspects["aspects.minigames.scoresequence"]) {
    mod.initializeAspectFile("aspects.minigames.scoresequence", ["minigames", "scoresequence"]);
    mod.setHiddenAspect("aspects.minigames.scoresequence", { id: "mariner.minigames.score" });
    mod.setHiddenAspect("aspects.minigames.scoresequence", { id: "mariner.minigames.step" });
  }

  if (!mod.recipes["recipes.minigames.scoresequence"]) {
    mod.initializeRecipeFile("recipes.minigames.scoresequence", ["minigames", "scoresequence"]);
    mod.setRecipe("recipes.minigames.scoresequence", {
      id: "mariner.minigames.scoresequence.holdhelp.hold",
      actionId: `mariner.minigames.scoresequence.holdhelp`,
      warmup: 40,
    });
  }

  if (!mod.verbs[`verbs.minigames.scoresequence`]) {
    mod.initializeVerbFile("verbs.minigames.scoresequence", ["minigames", "scoresequence"]);

    mod.setVerb("verbs.minigames.scoresequence", {
      id: `mariner.minigames.scoresequence.holdhelp`,
      multiple: true,
      spontaneous: true,
    });
  }
}

export function generateScoreSequenceMinigame({
  id,
  startRecipe = {},
  startScore = 10,
  scoreStyle,
  endings = [],
  warnings = [],
  stages = [],
  defaultWarmup = 10,
  postStageRecipe = {},
  defaultHelpSlot = DEFAULT_HELP_SLOT,
  expulsionFilter = {},
} = {}) {
  // Generate values global to all minigames unless they already exist
  generateMinigameFiles();

  // Generate the files specific to this minigame
  const RECIPE_FILE = `recipes.minigames.scoresequence.${id}`;
  const ELEMENT_FILE = `minigames.scoresequence.${id}`;
  const ASPECT_FILE = `aspects.minigames.scoresequence.${id}`;
  const PREFIX = `mariner.minigames.scoresequence.${id}`;

  mod.initializeRecipeFile(RECIPE_FILE, ["minigames", "scoresequence", id]);
  mod.initializeElementFile(ELEMENT_FILE, ["minigames", "scoresequence", id]);
  mod.initializeAspectFile(ASPECT_FILE, ["minigames", "scoresequence", id]);

  // The "score" is the quantity being evaluated, after each stage, to trigger certain end conditions
  mod.setElement(ELEMENT_FILE, { id: `${PREFIX}.score`, ...scoreStyle, aspects: { "mariner.minigames.score": 1 } });

  const generatedWarnings = warnings.length > 0 ? warnings.join("") : "";

  // spawn the starting score
  mod.setRecipe(RECIPE_FILE, {
    id: `${PREFIX}.start`,
    effects: { [`${PREFIX}.score`]: startScore },
    linked: [{ id: `${PREFIX}.pickstage` }],
    ...startRecipe,
  });

  mod.setRecipe(RECIPE_FILE, {
    id: `${PREFIX}.pickstage`,
    linked: stages.map((stage) => ({ id: `${PREFIX}.stages.${stage.id}.warmup` })),
  });

  mod.setRecipe(RECIPE_FILE, {
    ...postStageRecipe,
    id: `${PREFIX}.poststage`,
    effects: { "mariner.minigames.step": 1 },
    linked: [...endings.map((ending) => ({ id: `${PREFIX}.endings.${ending.id}` })), { id: `${PREFIX}.pickstage` }],
  });

  if (endings?.length === 0) console.log(`[WARN] score minigame "${id}" has no endings. Sounds fishy.`);
  for (const ending of endings) {
    mod.setRecipe(RECIPE_FILE, {
      ...ending,
      effects: { ...(ending.effects ?? {}), [`${PREFIX}.score`]: -100, "mariner.minigames.step": -100 },
      id: `${PREFIX}.endings.${ending.id}`,
    });
  }

  // Generate stages
  for (const stage of stages) {
    mod.setRecipe(RECIPE_FILE, {
      id: `${PREFIX}.stages.${stage.id}.warmup`,
      warmup: stage.warmup ?? defaultWarmup,
      slots: stage.slots ?? [
        SLOT_INSTANCE(defaultHelpSlot, 1),
        SLOT_INSTANCE(defaultHelpSlot, 2),
        SLOT_INSTANCE(defaultHelpSlot, 3),
      ],
      label: stage.label,
      startdescription: generatedWarnings + stage.startdescription,
      grandReqs: stage.requirements,
      // Stage variants
      alt: (stage.variants ?? []).map((variant) => ({
        id: `${PREFIX}.stages.${stage.id}.variants.${variant.id}.warmup`,
      })),
      // Stage outcomes
      linked: (stage.outcomes ?? []).map((outcome) => ({
        id: `${PREFIX}.stages.${stage.id}.outcomes.${outcome.id}`,
        chance: outcome.chance ?? 100,
      })),
    });

    // Generate stage variants & their outcomes
    for (const variant of stage.variants ?? []) {
      mod.setRecipe(RECIPE_FILE, {
        id: `${PREFIX}.stages.${stage.id}.variants.${variant.id}.warmup`,
        // TODO: don't just check for the presence and quantity, make sure this is the highest lore aspect in the list of usable lore aspects
        grandReqs: variant.requirements,
        warmup: variant.warmup ?? defaultWarmup,
        label: variant.label ?? "",
        startdescription: generatedWarnings + (variant.startdescription ?? ""),
        // Outcomes
        linked: (variant.outcomes ?? []).map((outcome) => ({
          id: `${PREFIX}.stages.${stage.id}.variants.${variant.id}.outcomes.${outcome.id}`,
          chance: outcome.chance ?? 100,
        })),
      });

      for (const outcome of variant.outcomes) {
        mod.setRecipe(RECIPE_FILE, {
          ...outcome,
          id: `${PREFIX}.stages.${stage.id}.variants.${variant.id}.outcomes.${outcome.id}`,
          warmup: outcome.warmup ?? defaultWarmup,
          linked: [{ id: `${PREFIX}.poststage` }],
          ...(outcome.doNotExpulseHelp
            ? {}
            : {
                inductions: [
                  {
                    id: "mariner.minigames.scoresequence.holdhelp.hold",
                    expulsion: {
                      filter: {
                        // tool: 1,
                        // "mariner.influence": 1,
                        // "mariner.crew": 1,
                        // "mariner.song": 1,
                        // "mariner.inspiration": 1,
                        // "mariner.heart": 1,
                        // "mariner.tune": 1,
                        "mariner.minigames.score": -1,
                        "mariner.minigames.step": -1,
                        ...expulsionFilter,
                      },
                      limit: 10,
                    },
                  },
                ],
              }),
        });
      }
    }

    // Generate stage outcomes
    for (const outcome of stage.outcomes) {
      mod.setRecipe(RECIPE_FILE, {
        ...outcome,
        id: `${PREFIX}.stages.${stage.id}.outcomes.${outcome.id}`,
        warmup: outcome.warmup ?? defaultWarmup,
        ...(outcome.doNotExpulseHelp
          ? {}
          : {
              inductions: [
                {
                  id: "mariner.minigames.scoresequence.holdhelp.hold",
                  expulsion: {
                    filter: { "mariner.minigames.score": -1, "mariner.minigames.step": -1 },
                    limit: 10,
                  },
                },
              ],
            }),
        linked: [{ id: `${PREFIX}.poststage` }],
      });
    }
  }
}
