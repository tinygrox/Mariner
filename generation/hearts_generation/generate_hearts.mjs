import { SIGNALS } from "../generate_signal_aspects.mjs";

export function generateHearts() {
  mod.initializeAspectFile("aspects.hearts", ["hearts"]);
  mod.setHiddenAspects(
    "aspects.hearts",
    { id: "mariner.heart" },
    { id: "mariner.hearts.halfheart" },
    { id: "mariner.hearts.lackheart" }
  );

  mod.initializeElementFile("composables.hearts", ["_composables"]);
  mod.setElements(
    "composables.hearts",
    // Definitions common to all hearts
    {
      id: "mariner.composables.heart",
      xtriggers: {
        [SIGNALS.USE_HEART]: [
          {
            morpheffect: "break",
            chance: "100 - ([~/source : mariner.wound] * 100)",
          },
          {
            morpheffect: "decay",
          },
          {
            morpheffect: "timespend",
            level: "([~/source : mariner.wound] * 60)",
            id: "mariner.heart",
          },
        ],
      },
      aspects: { "mariner.heart": 1 },
    },
    // Definitions common to all variations of the Halfheart
    {
      id: "mariner.composables.halfheart",
      $derives: "mariner.composables.heart",
      aspects: {
        heart: 2,
        forge: 2,
      },
      xtriggers: {
        // These signals are defined here and not common to all hearts, because they react to "HH-specific" signals. But the reaction is the same.
        [SIGNALS.EXHAUST_HH]: "mariner.lackheart.exhausted",
        [SIGNALS.WOUND_HH]: {
          morpheffect: "mutate",
          id: "mariner.wound",
          useCatalystQuantity: true,
        },
        [SIGNALS.ALLEVIATE_WOUND_HH]: [
          {
            morpheffect: "mutate",
            id: "mariner.wound",
            level: -1,
          },
          {
            morpheffect: "mutate",
            id: "mariner.groundedness",
            level: -100,
          },
        ],
      },
    },
    // Definitions common to all variations of the Lackheart
    {
      id: "mariner.composables.lackheart",
      $derives: "mariner.composables.heart",
      aspects: {
        winter: 2,
        knock: 2,
      },
      xtriggers: {
        [SIGNALS.EXHAUST_LH]: "mariner.lackheart.exhausted",
        [SIGNALS.WOUND_LH]: {
          morpheffect: "mutate",
          id: "mariner.wound",
          useCatalystQuantity: true,
        },
        [SIGNALS.ALLEVIATE_WOUND_LH]: [
          {
            morpheffect: "mutate",
            id: "mariner.wound",
            level: -1,
          },
          {
            morpheffect: "mutate",
            id: "mariner.groundedness",
            level: -100,
          },
        ],
      },
    }
  );

  mod.initializeElementFile("hearts", ["hearts"]);
  mod.setElements(
    "hearts",
    // Halfheart variations
    {
      id: "mariner.halfheart",
      $derives: "mariner.composables.halfheart",
      label: "My Half-Heart",
      description:
        "My passions, my drive, my charm, my thrumming blood, my clever fingers. If I present this to the world, I may be able to bend things to my will. [Use this while Exploring, Talking or Performing, and you may find or discuss things of interest to you]",
      aspects: {
        "mariner.hearts.halfheart": 1,
      },
      xtriggers: {
        [SIGNALS.PICK_OPTION_1]: {
          morpheffect: "setmutation",
          id: "mariner.pickedoption1",
          level: 1,
        },
        [SIGNALS.PICK_OPTION_2]: {
          morpheffect: "setmutation",
          id: "mariner.pickedoption2",
          level: 1,
        },
        [SIGNALS.PICK_OPTION_3]: {
          morpheffect: "setmutation",
          id: "mariner.pickedoption3",
          level: 1,
        },
      },
    },
    {
      id: "mariner.halfheart.exhausted",
      $derives: "mariner.composables.halfheart",
      icon: "mariner.halfheart",
      label: "My Half-Heart [exhausted]",
      description: "My passions, my drive, my charm, my thrumming blood, my clever fingers. Each need a rest.",
      lifetime: 60,
      decayTo: "mariner.halfheart",
      resaturate: true,
    },
    // Lackheart variations
    {
      id: "mariner.lackheart",
      $derives: "mariner.composables.lackheart",
      label: "My Lack-Heart",
      description:
        "A wound in the soul, a raw nerve, a longing. If I leave this exposed, the universe will try to fill it. [Use this while Exploring, Talking or Performing and you may find Tunes, Stories or other Inspirations]",
      aspects: {
        "mariner.hearts.lackheart": 1,
      },
    },
    {
      id: "mariner.lackheart.exhausted",
      $derives: "mariner.composables.lackheart",
      icon: "mariner.lackheart",
      label: "My Lack-Heart [exhausted]",
      description: "A wound in the soul, a raw nerve, a longing. I could not bear to expose it now.",
      decayTo: "mariner.lackheart",
      resaturate: true,
    }
  );
}
