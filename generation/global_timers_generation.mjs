const ASPECTS_FILE = "aspects.global_timers";

export function TIMER_ID(id) {
  const timerId = `mariner.globaltimers.${id}`;
  if (!mod.aspects[ASPECTS_FILE]) mod.initializeAspectFile(ASPECTS_FILE, ["timers"]);
  if (!mod.getAspect(ASPECTS_FILE, timerId)) mod.setHiddenAspect(ASPECTS_FILE, { id: timerId });
  return timerId;
}

/**
 * Builds the root/aspect path required by grandReq properties to check a timer's value
 * @param {number} id the timer id
 * @returns string
 */
export function READ_TIMER(id) {
  return `root/${TIMER_ID(id)}`;
}

export function TIMER_ELAPSED(id, cyclesAmount) {
  return {
    "root/mariner.cycle": `Abs([${READ_TIMER(id)}]) + ${cyclesAmount}`,
  };
}

export function TIMER_LESS_THAN(id, cyclesAmount) {
  return {
    "root/mariner.cycle": `-(Abs([${READ_TIMER(id)}]) + ${cyclesAmount})`,
  };
}

export function TIMER_BETWEEN(id, start, end) {
  return {
    [`[root/mariner.cycle] >= (Abs([${READ_TIMER(id)}]) + ${start}) && [root/mariner.cycle] < (Abs([${READ_TIMER(
      id
    )}]) + ${end})`]: 1,
  };
}

export function RESET_TIMER(id) {
  return { [TIMER_ID(id)]: `[root/mariner.cycle] - [${READ_TIMER(id)}]` };
}
