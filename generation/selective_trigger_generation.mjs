import { SelectiveOutcomeGenerator } from "./selective_outcome_generation.mjs";




export class SelectiveTriggerGenerator extends SelectiveOutcomeGenerator {
    TRIGGER_ASPECT = null;

    constructor({ filePath, prefix, aspectsFile, recipesFile, elementsFile, decksFile }) {
        super({ filePath, prefix, aspectsFile, recipesFile, elementsFile, decksFile })
        this.TRIGGER_ASPECT = `mariner.${prefix}.triggeraspect`;
    }

    generateTechnicalAspects() {
        super.generateTechnicalAspects();
        this.mod.setHiddenAspect(this.ASPECTS_FILE, {
            id: this.TRIGGER_ASPECT
        })
    }

    generateOutcomeElement(outcome) {
        this.mod.setElement(this.ELEMENTS_FILE, {
            id: this.outcomeCardId(outcome.id),
            aspects: {
                [this.CARD_ASPECT]: 1,
                ...outcome.aspects
            },

            // xtriggers: {
            //     [this.TRIGGER_ASPECT]: [
            //       {
            //         morpheffect: "mutate",
            //         id: "mariner.locations.northsea.copenhagen"
            //       }
            //     ]
            //   },
        })
    }

    generatePostFilteringExecution() {
        // this.mod.setRecipe(this.RECIPES_FILE, {
        //     id: `mariner.${this.prefix}.outcomeselection.finishedfiltering`,
        //     requirements: { [this.CARD_ASPECT]: -2 },
        //     linked: [
        //         ...this.outcomes.map(({ id, recipeId }) => ({
        //             id: `mariner.${this.prefix}.outcomeselection.selectandroute.${id}`,
        //             requirements: { [`mariner.${this.prefix}.${id}.outcomecard`]: 1 },
        //             effects: { [this.CARD_ASPECT]: -10 },
        //             linked: [{ id: recipeId }, { id: `mariner.${this.prefix}.outcomeselection.novalidoutcome` }]
        //         })),
        //         { id: `mariner.${this.prefix}.outcomeselection.novalidoutcome` }
        //     ]
        // })
    }
}