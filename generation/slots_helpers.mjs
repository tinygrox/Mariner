import deepmerge from "deepmerge";

const AUTHORIZED_HELP_PER_VERB = {
  "mariner.sail": {
    required: {
      "mariner.crew": 1,
      tool: 1,
      "mariner.inspiration.nature": 1,
      "mariner.lackheart": 1,
      "mariner.halfheart": 1,
    },
    forbidden: {
      "mariner.crew.traits.landlubber": 1,
      "mariner.crew.exhausted": 1,
      "mariner.tool.broken": 1,
    },
  },
  "mariner.sing": {
    required: {
      "mariner.crew": 1, //talk to gen
      "mariner.song": 1,
      "mariner.instrument": 1,
      "mariner.inspiration.melody": 1,
      "mariner.tune": 1,
    },
    forbidden: {
      "mariner.crew.traits.rhythmless": 1,
      "mariner.crew.exhausted": 1,
    },
  },
  talk: {
    required: {
      "mariner.crew": 1, //talk to gen
      "mariner.song": 1,
      "mariner.tune": 1,
      "mariner.story": 1,
      "mariner.lackheart": 1,
      "mariner.halfheart": 1,
    },
    forbidden: {
      "mariner.crew.traits.mute": 1,
      "mariner.crew.exhausted": 1,
    },
  },
  explore: {
    required: {
      "mariner.crew": 1,
      tool: 1,
      "mariner.inspiration.nature": 1,
    },
    forbidden: {
      "mariner.crew.traits.craven": 1,
      "mariner.crew.exhausted": 1,
      "mariner.tool.broken": 1,
    },
  },
  "mariner.navigate": {
    required: {
      "mariner.crew": 1, //talk to gen
      "mariner.song": 1,
      "mariner.story": 1,
      "mariner.inspiration.melody": 1,
      "mariner.tune": 1,
      "mariner.lackheart": 1,
      "mariner.halfheart": 1,
    },
    forbidden: {
      "mariner.crew.traits.singleminded": 1,
      "mariner.crew.exhausted": 1,
      "mariner.tool.broken": 1,
    },
  },
};

export function SHIP_MANOEUVERING_SLOTS(howMany = 3, slotSpec = {}) {
  let slot = {
    ...slotSpec,
    actionId: "mariner.sail",
    forbidden: deepmerge({ "mariner.crew.traits.fingerless": 1 }, slotSpec.forbidden ?? {}),
    context: deepmerge({ challengeHelp: true, doNotAuthorizeHelpItem: true }, slotSpec.context ?? {}),
  };
  return CHALLENGE_SLOTS(howMany, "mariner.sail", slot);
}

export function SEA_EVENT_SLOTS(howMany = 3, slotSpec = {}) {
  let slot = {
    ...slotSpec,
    context: deepmerge(slotSpec.context ?? {}, { doNotAuthorizeHelpItem: true }),
  };
  return CHALLENGE_SLOTS(howMany, "mariner.sail", slot);
}

export function CHALLENGE_SLOT(slotSpec) {
  let slot = {
    ...slotSpec,
    required: deepmerge({ funds: 1 }, slotSpec.required ?? {}),
    context: deepmerge({ challengeHelp: true }, slotSpec.context ?? {}),
  };
  return SLOT(slot);
}

export function CHALLENGE_SLOTS(howMany, actionId, slotSpec) {
  let slots = new Array(howMany);
  for (let i = 0; i < howMany; i++) {
    slots[i] = CHALLENGE_SLOT({ ...slotSpec, actionId, id: (slotSpec?.id ?? actionId) + (i + 1) });
  }
  return slots;
}

export function SLOTS(howMany, { actionId, id, label, description, required, forbidden, context }) {
  let slots = new Array(howMany);
  for (let i = 0; i < howMany; i++) {
    slots[i] = SLOT({ actionId, id: (id ?? actionId) + (i + 1), label, description, required, forbidden, context });
  }
  return slots;
}

/**
 *
 * @param {*} actionId
 * @param {*} id
 * @param {*} label
 * @param {*} description
 * @param {*} required
 * @param {*} forbidden
 * @param {*} context
 * @returns
 *
 * portAction: this slot will be part of a port action
 * challengeHelp: this slot is part of a challenge
 * doNotAuthorizeHelpItem: this slot doesn't allow help items to be used
 * shipManoeuver: this slot is part of an action related to manoeuvering the ship or sailing in general
 * pickingTeam: this slot is part of an action to pick a select team of infiltration
 */
export function SLOT({ actionId, id, label, description, required, forbidden, context }) {
  const { portAction, challengeHelp, doNotAuthorizeHelpItem, shipManoeuver, pickingTeam } = context;
  let slot = { actionId, id: id ?? actionId, label, description, required, forbidden };

  if (!label && challengeHelp) slot.label = "Help";
  if (!description && challengeHelp) slot.description = "Which implements are available for my approach.";

  if (challengeHelp) {
    if (!AUTHORIZED_HELP_PER_VERB[actionId])
      throw new Error(
        `SLOT was defined with the context flag "help" to true, but the actionId "${actionId}" is not a verb with predefined required and forbidden aspects!`
      );
    slot = {
      ...slot,
      required: deepmerge(slot.required ?? {}, AUTHORIZED_HELP_PER_VERB[actionId].required),
      forbidden: deepmerge(slot.forbidden ?? {}, AUTHORIZED_HELP_PER_VERB[actionId].forbidden),
    };
    if (doNotAuthorizeHelpItem) slot.forbidden["mariner.vaults.help"] = 1;
    else slot.required["mariner.vaults.help"] = 1;
  }

  if (portAction) slot.forbidden["mariner.crew.traits.unsociable"] = 1;
  if (shipManoeuver) slot.forbidden["mariner.crew.traits.fingerless"] = 1;

  return slot;
}
