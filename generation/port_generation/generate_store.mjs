import { SIGNALS } from "../generate_signal_aspects.mjs";
import { ME_LINK } from "../generation_helpers.mjs";
import { computeHostileSlots } from "./port_element_generation.mjs";

export function generateStore({ portRegion, portId, notDiscoverable, exploreHHDeckId, storeName }) {
  const elementCompleteId = `mariner.locations.${portRegion}.${portId}.store`;
  const hostileElementCompleteId = `mariner.locations.${portRegion}.${portId}.store.hostile`;
  const elementCompleteAwayId = "mariner.nostorebaratsea";

  const DISCOVERED_ASPECT = `mariner.discoveredstores.${portRegion}.${portId}`;
  const DISCOVER_STORE_RECIPE = `mariner.discoverstore.${portRegion}.${portId}`;
  const DISPLAY_STORE_ASPECT = `mariner.displaylocalstore.${portRegion}.${portId}`;

  const label = storeName;
  const description = "<General Store>";

  const localElement = {
    id: elementCompleteId,
    $derives: ["mariner.composables.store", "mariner.composables.canbecomehostile"],
    label,
    description,
    icon: `locations/stores/mariner.locations.${portRegion}.store`,
    xtriggers: {
      "mariner.location.makehostile": hostileElementCompleteId,
      [SIGNALS.DISCOVER_LOCAL_ELEMENT]: ME_LINK(DISCOVER_STORE_RECIPE),
    },
  };

  mod.setElement(`locations.${portRegion}.${portId}`, localElement);
  mod.setElement(`locations.${portRegion}.${portId}`, {
    ...localElement,
    $derives: ["mariner.composables.store.base", "mariner.composables.hostile"],
    id: hostileElementCompleteId,
    label: label + " [hostile]",
    slots: computeHostileSlots("talk"),
    xtriggers: {
      "mariner.location.makenothostile": elementCompleteId,
    },
  });

  if (!notDiscoverable) mod.getDeck(`decks.locations.${portRegion}`, exploreHHDeckId).spec.push(elementCompleteId);

  mod.setHiddenAspect(`aspects.discoveredstores.${portRegion}`, {
    id: DISCOVERED_ASPECT,
  });
  mod.setHiddenAspect(`aspects.displaylocalstore.${portRegion}`, {
    id: DISPLAY_STORE_ASPECT,
  });

  mod.addToXtriggers("genericstores", "mariner.nostoreatsea", {
    [DISPLAY_STORE_ASPECT]: elementCompleteId,
  });

  mod.setRecipe(`recipes.discoverstores.${portRegion}`, {
    id: DISCOVER_STORE_RECIPE,
    label: "Discovered The Local Store.",
    description: "<found local supply store>",
    requirements: { [elementCompleteId]: 1 },
    effects: { [elementCompleteId]: -1 },
    furthermore: [
      // Add the discovered bar aspect on the generic card
      {
        target: "~/exterior",
        mutations: {
          "mariner.nostorehere": { [DISCOVERED_ASPECT]: 1 },
        },
      },
      // Turn the generic card into its "at sea" counterpart
      { target: "~/exterior", decays: ["mariner.nostorehere"] },
      // Use the xtriggers defined on it to turn it into the newly discovered local bar
      {
        target: "~/exterior",
        aspects: { [DISPLAY_STORE_ASPECT]: 1 },
      },
    ],
    linked: [{ id: "mariner.addgroundedness" }],
  });
}
