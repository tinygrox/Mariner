import { SIGNALS } from "../generate_signal_aspects.mjs";
import { ME_LINK, greyscaledOverlay, intentionSlot } from "../generation_helpers.mjs";
import { generateDeliveryPackage } from "../packages_generation/generate_package.mjs";
import { ROUTINE_ID, generateRoutineState } from "../routine_generation.mjs";
import { SLOTS } from "../slots_helpers.mjs";
import { generateStoryTrading } from "../stories_generation/trading_helper.mjs";
import { STORY_TRADE_SLOT } from "./helpers.mjs";

const ifExists = (val, objToReturn) => (val ? objToReturn : {});

export function computeHostileSlots(actionId) {
  return SLOTS(3, { actionId, context: { challengeHelp: true, doNotAuthorizeHelpItem: true, portAction: true } });
}

const DISCOVERY_RECIPE_ID = (portRegion, portId, elementId) => `mariner.explore.${portRegion}.${portId}.discover.${elementId}`;

function generatePortNpc({
  portRegion,
  portId,
  elementId,
  icon,
  overlays,
  distantOverlays,
  label,
  dynamicLabel,
  description,
  discoveryLabel,
  discoveryDescription,
  localAspects,
  awayAspects,
  slots,
  deliveryOptions,
  deliveryRiddles,
  conversations,
  canBeHostile,
  storyTrading,
  overlayId,
  defaultRoutine,
  routines,
}) {
  const destinationAspectId = `mariner.locations.${portRegion}.${portId}.destination`;
  const elementCompleteId = `mariner.locations.${portRegion}.${portId}.${elementId}`;
  const hostileElementCompleteId = `mariner.locations.${portRegion}.${portId}.${elementId}.hostile`;
  const elementCompleteAwayId = elementCompleteId + ".away";
  const recipientId = `mariner.recipient.${portRegion}.${portId}.${deliveryOptions?.sameRecipientAs ?? elementId}`;
  const isDeliveryRecipient = deliveryRiddles || (deliveryOptions && deliveryOptions.sameRecipientAs);

  mod.setHiddenAspect("aspects.deliveryrecipients", { id: recipientId });

  const localElement = {
    id: elementCompleteId,
    $derives: [
      isDeliveryRecipient ? "mariner.composables.recipientnpc" : "mariner.composables.npc",
      ...(canBeHostile ? ["mariner.composables.canbecomehostile"] : []),
    ],
    xexts: {
      $background: `locations/${portRegion}/${portId}/${overlayId ?? elementId}`,
      $frame: `locations/${portRegion}/${portId}/${portId}_frame`,
    },
    overlays,
    label,
    dynamicLabel,
    description,
    icon,
    decayTo: elementCompleteAwayId,
    aspects: {
      ...ifExists(isDeliveryRecipient, { [recipientId]: 1 }),
      ...localAspects,
    },
    slots,
    xtriggers: {
      ...ifExists(canBeHostile, {
        "mariner.location.makehostile": hostileElementCompleteId,
      }),
      ...ifExists(defaultRoutine, {
        [SIGNALS.APPLY_DEFAULT_ROUTINE]: [
          {
            id: ROUTINE_ID(elementId, defaultRoutine),
            morpheffect: "setmutation",
            level: 1,
          },
        ],
      }),
      ...ifExists(discoveryLabel ?? discoveryDescription, {
        [SIGNALS.DISCOVER_LOCAL_ELEMENT]: ME_LINK(DISCOVERY_RECIPE_ID(portRegion, portId, elementId)),
      }),
    },
  };

  mod.elements[`locations.${portRegion}.${portId}`].push(
    ...[
      localElement,
      {
        id: elementCompleteAwayId,
        overlays: distantOverlays ?? [
          greyscaledOverlay(`locations/${portRegion}/${portId}/${overlayId ?? elementId}`),
          `locations/${portRegion}/${portId}/${portId}_frame`,
        ],
        label: `${label} [away]`,
        icon,
        description,
        decayTo: elementCompleteId,
        aspects: {
          [destinationAspectId]: 1,
          ...awayAspects,
        },
      },
    ]
  );

  if (canBeHostile) {
    mod.elements[`locations.${portRegion}.${portId}`].push({
      ...localElement,
      $derives: [
        isDeliveryRecipient ? "mariner.composables.recipientnpc" : "mariner.composables.npc",
        "mariner.composables.npc.hostile",
      ],
      id: hostileElementCompleteId,
      icon: elementCompleteId,
      label: label + " [hard to meet]",
      aspects: localAspects,
      xtriggers: {
        "mariner.location.makenothostile": elementCompleteId,
      },
    });
  }
  const isPrimaryRecipient = deliveryRiddles && (!deliveryOptions || !deliveryOptions.sameRecipientAs);
  if (isPrimaryRecipient) {
    // This aspect is on the generated packages, not on the recipient itself. Yeah, this is confusing, I know.
    const packageRecipientId = `mariner.packagerecipient.${portRegion}.${portId}.${elementId}`;
    mod.setHiddenAspect("aspects.packagerecipients", {
      id: packageRecipientId,
      label: `Has to be delivered to ${label}`,
    });
    mod.setRecipe("recipes.deliveries", {
      id: `mariner.deliverpackage.${portRegion}.${elementId}`,
      label: "Deliver Package",
      actionId: "talk",
      startdescription:
        "Package delivered. If I express interest in information wtih my Lack-Heart, I can learn new secrets of the sea, if I imply interest in material goods with my Half-Heart, I will be paid for my services monetarily.",
      craftable: false,
      warmup: 30,
      effects: {
        "mariner.package": -1,
      },
      requirements: {
        [packageRecipientId]: 1,
        [recipientId]: 1,
      },
      slots: [
        {
          id: "intent",
          label: "Intent",
          description: "What kind of reward do I want?",
          required: {
            "mariner.lackheart": 1,
            "mariner.halfheart": 1,
          },
        },
      ],
      linked: [
        { id: `mariner.deliverpackage.${portRegion}.${elementId}.askforfunds` },
        { id: `mariner.deliverpackage.${portRegion}.${elementId}.askforintel` },
      ],
    });
    mod.setRecipe("recipes.deliveries", {
      id: `mariner.deliverpackage.${portRegion}.${elementId}.askforfunds`,
      label: "Deliver Package",
      actionId: "talk",
      description: "Package delivered in exchange for funds.",
      craftable: false,
      warmup: 0,
      effects: { funds: 3 },
      requirements: { "mariner.halfheart": 1 },
    });
    mod.setRecipe("recipes.deliveries", {
      id: `mariner.deliverpackage.${portRegion}.${elementId}.askforintel`,
      label: "Deliver Package",
      actionId: "talk",
      description: "Package Delivered in exchange of intel.",
      craftable: false,
      warmup: 0,
      effects: { "mariner.scrapofinformation": 1 },
    });
    mod.addFirstToLinked(
      "recipes.deliveries",
      "mariner.deliverpackage.start",
      `mariner.deliverpackage.${portRegion}.${elementId}`
    );

    for (let i = 1; i <= 10; i++)
      generateDeliveryPackage({
        portRegion,
        portId,
        recipientId: elementId,
        packageId: i,
        ...deliveryOptions,
        deliveryRiddles: deliveryRiddles ?? ["<test riddle>"],
      });
  }

  // Generate conversations
  if (conversations) {
    for (const convObject of conversations) {
      const npcId = convObject.useRecipientId ? recipientId : elementCompleteId;
      mod.setRecipe(`recipes.conversations.${portRegion}.${portId}`, {
        id: `mariner.conversations.${portRegion}.${portId}.${elementId}.${convObject.id}`,
        craftable: true,
        actionId: "talk",
        maxexecutions: convObject.maxexecutions,
        warmup: 60,
        label: convObject.label,
        startdescription: convObject.startdescription,
        grandReqs: {
          ...convObject.requirements,
          [npcId]: 1,
          ...(canBeHostile ? { "mariner.location.hostile": -1 } : {}),
        },
        description: convObject.description,
        effects: convObject.effects,
        ...(convObject.additionalProperties ?? {}),
      });
    }

    if (!mod.recipes["recipes.generalhints"]) mod.initializeRecipeFile("recipes.generalhints");
    for (const type of ["location", "song", "topic", "story", "story.source", "mystery"])
      mod.setRecipe("recipes.generalhints", {
        id: `mariner.conversations.nothingtosayabout.${type}`,
        hintOnly: true,
        actionId: "talk",
        requirements: {
          "mariner.npc": 1,
          [`mariner.${type}`]: 1,
        },
        label: "Nothing To Say About This",
        startdescription: "This one doesn't seem to have anything to say about that.",
      });
    mod.setRecipe("recipes.generalhints", {
      id: `mariner.conversations.nothingtosayabout.desire`,
      hintOnly: true,
      actionId: "talk",
      requirements: {
        "mariner.npc": 1,
        desire: 1,
      },
      label: "Nothing To Say About This",
      startdescription: "This one doesn't seem to have anything to say about that.",
    });
  }

  if (storyTrading) {
    generateStoryTrading({
      id: `${portId}.${elementId}`,
      path: ["stories", "trading", portRegion, portId],
      story: storyTrading.story,
      requirements: { [recipientId]: 1 },
      label: storyTrading.label,
      startdescription: storyTrading.startdescription,
      description: storyTrading.description,
    });
  }

  if (routines) {
    for (const routine of routines) {
      generateRoutineState(elementId, routine, routines);
    }
  }
}

function generatePortLocation({
  portRegion,
  portId,
  elementId,
  icon,
  label,
  dynamicLabel,
  description,
  discoveryLabel,
  discoveryDescription,
  localAspects,
  awayAspects,
  slots,
  canBeHostile,
  storyTrading,
  overlayId,
}) {
  const destinationAspectId = `mariner.locations.${portRegion}.${portId}.destination`;
  const elementCompleteId = `mariner.locations.${portRegion}.${portId}.${elementId}`;
  const hostileElementCompleteId = `mariner.locations.${portRegion}.${portId}.${elementId}.hostile`;
  const elementCompleteAwayId = elementCompleteId + ".away";

  const localElement = {
    id: elementCompleteId,
    overlays: [
      `locations/${portRegion}/${portId}/${overlayId ?? elementId}`,
      `locations/${portRegion}/${portId}/${portId}_frame`,
    ],
    label,
    dynamicLabel,
    description,
    icon,
    decayTo: elementCompleteAwayId,
    aspects: {
      ...(localAspects?.["mariner.theater"] ? {} : { modded_explore_allowed: 1 }),
      "mariner.local": 1,
      ...ifExists(canBeHostile, { "mariner.location.nothostile": 1 }),
      ...localAspects,
    },
    unique: true,
    slots: slots ?? [],
    xtriggers: {
      ...ifExists(canBeHostile, {
        "mariner.location.makehostile": hostileElementCompleteId,
      }),
      ...ifExists(discoveryLabel ?? discoveryDescription, {
        [SIGNALS.DISCOVER_LOCAL_ELEMENT]: ME_LINK(DISCOVERY_RECIPE_ID(portRegion, portId, elementId)),
      }),
    },
  };

  mod.elements[`locations.${portRegion}.${portId}`].push(
    ...[
      localElement,
      {
        id: elementCompleteAwayId,
        overlays: [
          greyscaledOverlay(`locations/${portRegion}/${portId}/${overlayId ?? elementId}`),
          `locations/${portRegion}/${portId}/${portId}_frame`,
        ],
        label: `${label} [away]`,
        icon,
        description,
        decayTo: elementCompleteId,
        aspects: {
          [destinationAspectId]: 1,
          "mariner.location": 1,
          ...awayAspects,
        },
        unique: true,
      },
    ]
  );

  if (canBeHostile) {
    mod.elements[`locations.${portRegion}.${portId}`].push({
      ...localElement,
      id: hostileElementCompleteId,
      label: label + " [hostile]",
      slots: computeHostileSlots("explore"),
      aspects: {
        modded_explore_allowed: 1,
        "mariner.local": 1,
        "mariner.location.hostile": 1,
        "mariner.location": 1,
        ...localAspects,
      },
      xtriggers: {
        "mariner.location.makenothostile": elementCompleteId,
      },
    });
  }

  if (storyTrading) {
    generateStoryTrading({
      id: `${portId}.${elementId}`,
      path: ["stories", "trading", portRegion, portId],
      story: storyTrading.story,
      requirements: { [elementCompleteId]: 1 },
      label: storyTrading.label,
      startdescription: storyTrading.startdescription,
      description: storyTrading.description,
    });
  }
}

export function generatePortElement({
  portRegion,
  portId,
  elementId,
  type,
  overlays,
  distantOverlays,
  icon,
  label,
  dynamicLabel,
  description,
  notDiscoverable,
  discoveryLabel,
  discoveryDescription,
  localAspects,
  awayAspects,
  slots,
  exploreHHDeckId,
  deliveryOptions,
  deliveryRiddles,
  conversations,
  canBeHostile,
  storyTrading,
  overlayId,
  defaultRoutine,
  routines,
}) {
  const elementCompleteId = `mariner.locations.${portRegion}.${portId}.${elementId}`;

  if (type === "npc") {
    if (canBeHostile === undefined) canBeHostile = true;
    generatePortNpc({
      portRegion,
      portId,
      elementId,
      icon,
      overlays,
      distantOverlays,
      label,
      dynamicLabel,
      description,
      discoveryLabel,
      discoveryDescription,
      localAspects,
      awayAspects,
      slots,
      deliveryOptions,
      deliveryRiddles,
      conversations,
      canBeHostile,
      storyTrading,
      overlayId,
      defaultRoutine,
      routines,
    });
  } else
    generatePortLocation({
      portRegion,
      portId,
      elementId,
      icon,
      label,
      dynamicLabel,
      description,
      discoveryLabel,
      discoveryDescription,
      localAspects,
      awayAspects,
      slots,
      canBeHostile,
      storyTrading,
      overlayId,
    });

  // Add the local element to the right port discovery deck
  if (!notDiscoverable) mod.getDeck(`decks.locations.${portRegion}`, exploreHHDeckId).spec.push(elementCompleteId);

  if (discoveryLabel || discoveryDescription) {
    mod.setRecipe(`recipes.discover.${portRegion}`, {
      id: DISCOVERY_RECIPE_ID(portRegion, portId, elementId),
      label: discoveryLabel,
      startdescription: discoveryDescription,
    });
  }
}
