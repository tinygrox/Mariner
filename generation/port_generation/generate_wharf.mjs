import { SIGNALS } from "../generate_signal_aspects.mjs";
import { generateWharfBuyCargoAction } from "../generate_wharf_actions.mjs";
import { ME_LINK } from "../generation_helpers.mjs";

export function generateWharf({ portRegion, portId, notDiscoverable, exploreHHDeckId, wharf }) {
  const elementCompleteId = `mariner.locations.${portRegion}.${portId}.wharf`;
  const hostileElementCompleteId = `mariner.locations.${portRegion}.${portId}.wharf.hostile`;

  const DISCOVERED_ASPECT = `mariner.discoveredwharves.${portRegion}.${portId}`;
  const DISCOVER_WHARF_RECIPE = `mariner.discoverwharf.${portRegion}.${portId}`;
  const DISPLAY_WHARF_ASPECT = `mariner.displaylocalwharf.${portRegion}.${portId}`;

  const localElement = {
    id: elementCompleteId,
    $derives: ["mariner.composables.wharf.available", "mariner.composables.canbecomehostile"],
    icon: `locations/wharves/mariner.locations.${portRegion}.wharf`,
    aspects: {
      modded_talk_allowed: 1,
      "mariner.location.nothostile": 1,
    },
    xtriggers: {
      "mariner.location.makehostile": hostileElementCompleteId,
      [SIGNALS.DISCOVER_LOCAL_ELEMENT]: ME_LINK(DISCOVER_WHARF_RECIPE),
    },
  };

  mod.setElement(`locations.${portRegion}.${portId}`, localElement);

  mod.setElement(`locations.${portRegion}.${portId}`, {
    ...localElement,
    id: hostileElementCompleteId,
    $derives: ["mariner.composables.wharf", "mariner.composables.hostile"],
    aspects: {},
    label: mod.getElement("composables.locations", "mariner.composables.wharf").label + " [hostile]",
    xtriggers: {
      "mariner.location.makenothostile": elementCompleteId,
    },
  });

  if (!notDiscoverable) mod.getDeck(`decks.locations.${portRegion}`, exploreHHDeckId).spec.push(elementCompleteId);
  // Done until here

  mod.setHiddenAspect(`aspects.discoveredwharves.${portRegion}`, {
    id: DISCOVERED_ASPECT,
  });
  mod.setHiddenAspect(`aspects.displaylocalwharf.${portRegion}`, {
    id: DISPLAY_WHARF_ASPECT,
  });

  mod.addToXtriggers("genericwharves", "mariner.nowharfatsea", {
    [DISPLAY_WHARF_ASPECT]: elementCompleteId,
  });

  mod.setRecipe(`recipes.discoverwharves.${portRegion}`, {
    id: DISCOVER_WHARF_RECIPE,
    label: "Discovered the Local Wharf",
    description: "I have found the street that smells like oil and wood chips.",
    requirements: { [elementCompleteId]: 1 },
    effects: { [elementCompleteId]: -1 },
    furthermore: [
      // Add the discovered wharf aspect on the generic card
      {
        target: "~/exterior",
        mutations: {
          "mariner.nowharfhere": { [DISCOVERED_ASPECT]: 1 },
        },
      },
      // Turn the generic card into its "at sea" counterpart
      { target: "~/exterior", decays: ["mariner.nowharfhere"] },
      // Use the xtriggers defined on it to turn it into the newly discovered local wharf
      {
        target: "~/exterior",
        aspects: { [DISPLAY_WHARF_ASPECT]: 1 },
      },
    ],
    linked: [{ id: "mariner.addgroundedness" }],
  });

  generateWharfBuyCargoAction(
    portRegion,
    portId,
    wharf.cargoOptions ?? [
      { id: "mariner.cargo.for.northsea", label: "North Sea" },
      { id: "mariner.cargo.for.medsea", label: "Med Sea" },
      { id: "mariner.cargo.for.medsea", label: "Med Sea" },
    ]
  );
}
