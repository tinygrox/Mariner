import { SIGNALS } from "../../generate_signal_aspects.mjs";
import { ME_RECIPE } from "../../generation_helpers.mjs";
import { seaMesh } from "../../sea_generation/generate_sailing_routes.mjs";

let AUTOINCREMENT = 1;
let AUTOINCREMENT_ASPECT = "mariner.ais.agents.id";

const recipeForAgent = (id) => [
  {
    morpheffect: "break",
    chance: `(${NOT_AGENT_OF_CURRENT_CONTROLLER}) * 100`,
  },
  ME_RECIPE(id),
];

const effectForAgent = (effect) => [
  {
    morpheffect: "break",
    chance: `(${NOT_AGENT_OF_CURRENT_CONTROLLER}) * 100`,
  },
  effect,
];

export function generatePortAILogic(portRegion, portId) {
  // Docked to
  mod.setAspect("aspects.agents.locations", {
    id: `mariner.agentdata.dockedto.${portRegion}.${portId}`,
    icon: "mariner.docked",
    label: `Docked to: ${portRegion} ${portId}`,
    description: `Docked to: ${portRegion} ${portId}`,
    xtriggers: {
      [SIGNALS.AGENT_UNDOCK]: recipeForAgent(`mariner.agentactions.undockfrom.${portRegion}.${portId}`),
      [SIGNALS.AGENT_PRESSURE_PORT]: recipeForAgent(`mariner.agentactions.applypressureto.${portRegion}.${portId}`),
    },
  });

  // Undocking
  mod.setRecipe("recipes.agents.locations", {
    id: `mariner.agentactions.undockfrom.${portRegion}.${portId}`,
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: `${AGENT_OF_CURRENT_CONTROLLER} && [mariner.agentdata.dockedto.${portRegion}.${portId}]`,
            mutate: `mariner.agentdata.dockedto.${portRegion}.${portId}`,
            level: 0,
            additive: false,
          },
        ],
      },
    ],
  });

  // Apply pressure
  mod.setRecipe("recipes.agents.locations", {
    id: `mariner.agentactions.applypressureto.${portRegion}.${portId}`,
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: `[mariner.locations.${portRegion}.${portId}.uqg]`,
            mutate: `mariner.reputation.heat`,
            level: 3,
            additive: true,
          },
        ],
      },
    ],
  });

  // Sailing Towards
  mod.setAspect("aspects.agents.locations", {
    id: `mariner.agentdata.sailingtowards.${portRegion}.${portId}`,
    icon: "mariner.destination",
    label: `Sailing towards: ${portRegion} ${portId}`,
    description: `Sailing towards: ${portRegion} ${portId}`,
    xtriggers: {
      [SIGNALS.AGENT_DOCK]: recipeForAgent(`mariner.agentactions.dockto.${portRegion}.${portId}`),
    },
  });

  // Docking
  mod.setRecipe("recipes.agents.locations", {
    id: `mariner.agentactions.dockto.${portRegion}.${portId}`,
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: `${AGENT_OF_CURRENT_CONTROLLER} && [mariner.agentdata.sailingtowards.${portRegion}.${portId}]`,
            mutate: `mariner.agentdata.dockedto.${portRegion}.${portId}`,
            level: 1,
            additive: false,
          },
          {
            filter: `${AGENT_OF_CURRENT_CONTROLLER} && [mariner.agentdata.sailingtowards.${portRegion}.${portId}]`,
            mutate: `mariner.agentdata.sailingtowards.${portRegion}.${portId}`,
            level: 0,
            additive: false,
          },
        ],
      },
    ],
  });

  mod.setRecipe("recipes.agents.locations", {
    id: `mariner.agentactions.pickdestination.${portRegion}.${portId}`,
    grandReqs: {
      [`[~/exterior : { ${AGENT_OF_CURRENT_CONTROLLER} } : mariner.agentdata.dockedto.${portRegion}.${portId}]`]: -1,
    },
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: `${AGENT_OF_CURRENT_CONTROLLER}`,
            mutate: `mariner.agentdata.sailingtowards.${portRegion}.${portId}`,
            level: 1,
            additive: false,
          },
        ],
      },
    ],
  });
}

export function generateSeaAILogic(portRegion) {
  mod.setAspect("aspects.agents.locations", {
    id: `mariner.agentdata.insea.${portRegion}`,
    label: `In sea ${portRegion}`,
    description: `In sea ${portRegion}`,
    xtriggers: {
      [SIGNALS.AGENT_PICK_DESTINATION]: recipeForAgent(`mariner.agentactions.pickdestination.${portRegion}`),
      [SIGNALS.AGENT_LEAVE_SEA]: recipeForAgent(`mariner.agentactions.leavesea.${portRegion}`),
      // When this signal is emitted, it means we're arriving in a sea. A new sea. All existing insea aspects should destroy themselves.
      [SIGNALS.AGENT_ARRIVE_IN_SEA]: effectForAgent({
        morpheffect: "setmutation",
        id: `mariner.agentdata.insea.${portRegion}`,
        level: 0,
      }),
    },
  });

  // Pick in-sea destination
  mod.setRecipe("recipes.agents.locations", {
    id: `mariner.agentactions.pickdestination.${portRegion}`,
    linked: [
      {
        id: `mariner.agentactions.pickdestination.${portRegion}.*`,
        randomPick: true,
      },
    ],
  });

  // Set sail towards neighboring sea
  // Origin recipe to pick a sea at random
  mod.setRecipe("recipes.agents.locations", {
    id: `mariner.agentactions.leavesea.${portRegion}`,
    linked: [
      {
        id: `mariner.agentactions.sailtowardssea.${portRegion}.to.*`,
        randomPick: true,
      },
    ],
  });

  for (const destinationRegion of Object.keys(seaMesh[portRegion])) {
    // Set sail
    mod.setRecipe("recipes.agents.locations", {
      id: `mariner.agentactions.sailtowardssea.${portRegion}.to.${destinationRegion}`,
      furthermore: [
        {
          target: "~/exterior",
          mutations: [
            {
              filter: `${AGENT_OF_CURRENT_CONTROLLER}`,
              mutate: `mariner.agentdata.sailingtowardssea.${destinationRegion}`,
              level: 1,
              additive: false,
            },
          ],
        },
      ],
    });
  }

  // Arrive in sea aspects and logic
  mod.setAspect("aspects.agents.locations", {
    id: `mariner.agentdata.sailingtowardssea.${portRegion}`,
    label: `Sailing to sea ${portRegion}`,
    description: `Sailing to sea ${portRegion}`,
    xtriggers: {
      [SIGNALS.AGENT_ARRIVE_IN_SEA]: recipeForAgent(`mariner.agentactions.arriveinsea.${portRegion}`),
    },
  });
  mod.setRecipe("recipes.agents.locations", {
    id: `mariner.agentactions.arriveinsea.${portRegion}`,
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: AGENT_OF_CURRENT_CONTROLLER,
            mutate: `mariner.agentdata.insea.${portRegion}`,
            level: 1,
            additive: false,
          },
          {
            filter: AGENT_OF_CURRENT_CONTROLLER,
            mutate: `mariner.agentdata.sailingtowardssea.${portRegion}`,
            level: 0,
            additive: false,
          },
        ],
      },
    ],
  });
}

function generateAgent({ agentId, label, description, aspects, btreeId }) {
  mod.setElement("agents", {
    id: `mariner.agents.${agentId}.controller`,
    $derives: ["mariner.composables.agentcontroller"],
    label,
    description,
    aspects: { ...aspects, [AUTOINCREMENT_ASPECT]: AUTOINCREMENT },
  });

  mod.setElement("agents", {
    id: `mariner.agents.${agentId}`,
    label,
    description,
    aspects: {
      ...aspects,
      "mariner.agent": 1,
      [AUTOINCREMENT_ASPECT]: AUTOINCREMENT,
    },
    xtriggers: {
      [SIGNALS.AGENT_INVESTIGATE]: ME_RECIPE("mariner.agentactions.default.investigate"),
    },
  });

  mod.setRecipe("recipes.agents.default", {
    id: "mariner.agentactions.default.investigate",
    furthermore: [
      {
        target: "~/exterior",
        aspects: {
          [SIGNALS.AGENT_PRESSURE_PORT]: 1,
        },
      },
    ],
  });

  mod.setRecipe(`recipes.ais.${btreeId}`, {
    id: `mariner.spawnagents.${agentId}`,
    actionId: `ais.${btreeId}`,
    furthermore: [
      {
        effects: { [`mariner.agents.${agentId}.controller`]: 1 },
      },
      {
        target: "~/tabletop",
        effects: { [`mariner.agents.${agentId}`]: 1 },
      },
    ],
    linked: [{ id: `mariner.ais.${btreeId}.resetsequences` }],
  });

  AUTOINCREMENT++;
}

export function generateHardcodedAgents() {
  generateAgent({
    agentId: "ship.theathenian",
    label: "The Athenian",
    description: "<This is the Athenian, a hunter ship.>",
    btreeId: "defaultshipbtree",
  });
}

export const AGENT_OF_CURRENT_CONTROLLER = `[~/token : ${AUTOINCREMENT_ASPECT}] = [root/${AUTOINCREMENT_ASPECT}]`;
export const NOT_AGENT_OF_CURRENT_CONTROLLER = `[~/token : ${AUTOINCREMENT_ASPECT}] != [root/${AUTOINCREMENT_ASPECT}]`;

function apiProxy(actions) {
  actions = actions ?? [
    SIGNALS.AGENT_UNDOCK,
    SIGNALS.AGENT_DOCK,
    SIGNALS.AGENT_PICK_DESTINATION,
    SIGNALS.AGENT_LEAVE_SEA,
    SIGNALS.AGENT_ARRIVE_IN_SEA,
    SIGNALS.AGENT_INVESTIGATE,
  ];

  for (const a of actions) {
    mod.setRecipe("recipes.controllers.proxycalls", {
      id: `mariner.controllers.proxycalls.${a.substring(15)}`,
      furthermore: [
        {
          rootAdd: {
            [AUTOINCREMENT_ASPECT]: `-[root/${AUTOINCREMENT_ASPECT}]`,
          },
        },
        {
          rootAdd: {
            [AUTOINCREMENT_ASPECT]: `${AUTOINCREMENT_ASPECT}`,
          },
        },
        { target: "~/exterior", aspects: { [a]: 1 } },
      ],
    });
  }

  return Object.fromEntries(actions.map((a) => [a, ME_RECIPE(`mariner.controllers.proxycalls.${a.substring(15)}`)]));
}

export function generateAgents() {
  mod.initializeAspectFile("aspects.agents", ["ais"]);
  mod.initializeElementFile("agents", ["ais"]);
  mod.initializeRecipeFile("recipes.agents.default", ["ais"]);
  mod.initializeRecipeFile("recipes.controllers.proxycalls", ["ais"]);
  mod.initializeElementFile("composables.agents", ["_composables"]);

  mod.setAspect("aspects.agents", {
    id: "mariner.ais.agents.id",
    label: "Agent ID",
  });
  mod.setAspect("aspects.agents", {
    id: "mariner.agentcontroller",
    label: "AI Agent Controller",
  });
  mod.setAspect("aspects.agents", { id: "mariner.agent", label: "AI Agent" });

  // Generate controller composable
  // This card won't actually have the xtriggers on it, the
  mod.setElement("composables.agents", {
    id: "mariner.composables.agentcontroller",
    aspects: { "mariner.agentcontroller": 1 },
    xtriggers: apiProxy(),
  });

  generateHardcodedAgents();

  mod.readRecipesFile("recipes.firstarrival", ["sailing"]);
  const injectionRecipe = mod.getRecipe("recipes.firstarrival", "mariner.firstnavigation");
  injectionRecipe.rootAdd = {
    ...(injectionRecipe.rootAdd ?? {}),
    [AUTOINCREMENT_ASPECT]: AUTOINCREMENT,
  };
}
