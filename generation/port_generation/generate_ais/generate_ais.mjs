import { generateAgents } from "./agents.mjs";
import { BehaviourTree } from "./behaviour_trees/behaviour_tree.mjs";
import {
  Selector,
  Sequence,
  TravelToLocalPortSequence,
} from "./behaviour_trees/flow_nodes.mjs";
import {
  ArriveInSea,
  DockTask,
  EndMissionOrderTask,
  InvestigateTask,
  NO_ACTION,
  PickDestinationTask,
  ReceiveMissionOrderTask,
  SailTowardsNeighboringSea,
  Task,
  UndockTask,
  WaitTask,
} from "./behaviour_trees/task_nodes.mjs";

export const missionOrderId = (id) => `mariner.ais.missionorders.${id}`;

function generateMissionOrders() {
  mod.initializeAspectFile("aspects.missionorders", ["ais"]);
  mod.initializeElementFile("missionorders", ["ais"]);

  mod.setAspect("aspects.missionorders", {
    id: "mariner.ais.missionorder",
    label: "AI Agent Mission Order",
  });

  const defineMissionOrder = (id) => {
    mod.setElement("missionorders", {
      id: missionOrderId(id),
      label: `Mission order: ${id}`,
      description: `Mission order: ${id}`,
      aspects: { "mariner.ais.missionorder": 1 },
    });
  };

  defineMissionOrder("patrolcurrentregion");
  defineMissionOrder("changesea");
}

/*
How our AIs work:
- they're spawned as a token, running a looping graph.
- the graph models a behaviour tree
- the tasks (leaves of the tree) act onto the controller
- the controller is a card sitting in the token. It reacts to specific signals to xtrigger recipes, doing things on the board.

The actions do things and when they manipulate an agent represented by one or more cards, they act onto the controller, and the controller "puppeeters" the actual content 
of the table. That way, we can reuse the same spawned behaviour trees multiple times but acting on a different agents.

After reaching a task, it goes back to the root and reevaluates the entire path again, unless a Sequence is marked as "exclusive". Outside of exclusive, Tasks must never leave
the agent in a state that would break them.

Sequences use a dedicated element they increment to know which branch to now run. They're built with an "end" recipe, that resets the counter. A succeeding task will link to the root, a failing task will return to the "end" of Sequence, which will clear its tokens before linking back to the root.

Where are the informations about a ship stored? It is stored onto the ship card. Why? Because we need it there to display the right visuals and label, and the btree can run generic code written as "mutate the agent with the same controller id value as the one sitting in the token"
*/
export function generateAISystems() {
  generateMissionOrders();

  const defaultShipBtree = new BehaviourTree({
    id: "defaultshipbtree",
    defaultWarmup: 2,
    root: new Selector({
      id: "selectorderbehaviour",
      label:
        "Pick the mission order, or execute the associated behaviour if found",
      children: [
        new ReceiveMissionOrderTask(),
        // Sequence is:
        // - leave
        // - pick a destination, or not
        // - wait a while
        // - after a bit, "dock" to the new port if any
        new Sequence({
          id: "patrolcurrentregion",
          label: "Patrol current region Sequence",
          grandReqs: { [missionOrderId("patrolcurrentregion")]: 1 },
          children: [
            new TravelToLocalPortSequence(),
            // Wait a bit ("investigate")
            new WaitTask({ id: "waitinvestigate", warmup: 20 }),
            // Investigate the port
            new InvestigateTask(),
            new EndMissionOrderTask(),
          ],
        }),
        // Sequence is:
        // - leave
        // - pick a sea
        // - wait a while
        // - arrive
        new Sequence({
          id: "traveltorandomregion",
          label: "Travel to random region Sequence",
          grandReqs: { [missionOrderId("changesea")]: 1 },
          children: [
            // Undock if docked
            new UndockTask(),
            // Pick a neighboring sea
            new SailTowardsNeighboringSea(),
            // Wait a bit (test warmup, should be longer in practice)
            new WaitTask({ warmup: 200 }),
            // arrive
            new ArriveInSea(),
            // revise the order of mission
            new EndMissionOrderTask(),
          ],
        }),
      ],
    }),
  });
  defaultShipBtree.build();

  generateAgents();
}
