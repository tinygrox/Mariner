import { SIGNALS } from "../generate_signal_aspects.mjs";
import { ME_LINK } from "../generation_helpers.mjs";
import { computeHostileSlots } from "./port_element_generation.mjs";

export function generateBar({ portRegion, portId, notDiscoverable, exploreHHDeckId, barName }) {
  const elementCompleteId = `mariner.locations.${portRegion}.${portId}.bar`;
  const hostileElementCompleteId = `mariner.locations.${portRegion}.${portId}.bar.hostile`;
  const elementCompleteAwayId = "mariner.nobaratsea";

  const DISCOVERED_ASPECT = `mariner.discoveredbars.${portRegion}.${portId}`;
  const DISCOVER_BAR_RECIPE = `mariner.discoverbar.${portRegion}.${portId}`;
  const DISPLAY_BAR_ASPECT = `mariner.displaylocalbar.${portRegion}.${portId}`;

  const label = barName;
  const description = "Different town, same crowd. Different language, same stories.";

  const localElement = {
    id: elementCompleteId,
    $derives: ["mariner.composables.bar", "mariner.composables.canbecomehostile"],
    label,
    description,
    icon: `locations/bars/mariner.locations.${portRegion}.bar`,
    xtriggers: {
      "mariner.location.makehostile": hostileElementCompleteId,
      [SIGNALS.DISCOVER_LOCAL_ELEMENT]: ME_LINK(DISCOVER_BAR_RECIPE),
    },
  };

  mod.setElement(`locations.${portRegion}.${portId}`, localElement);

  mod.setElement(`locations.${portRegion}.${portId}`, {
    ...localElement,
    $derives: ["mariner.composables.bar.base", "mariner.composables.hostile"],
    id: hostileElementCompleteId,
    label: label + " [hostile]",
    slots: computeHostileSlots("talk"),
    xtriggers: {
      "mariner.location.makenothostile": elementCompleteId,
    },
  });

  if (!notDiscoverable) mod.getDeck(`decks.locations.${portRegion}`, exploreHHDeckId).spec.push(elementCompleteId);

  mod.setHiddenAspect(`aspects.discoveredbars.${portRegion}`, {
    id: DISCOVERED_ASPECT,
  });
  mod.setHiddenAspect(`aspects.displaylocalbar.${portRegion}`, {
    id: DISPLAY_BAR_ASPECT,
  });

  mod.addToXtriggers("genericbars", "mariner.nobaratsea", {
    [DISPLAY_BAR_ASPECT]: elementCompleteId,
  });

  mod.setRecipe(`recipes.discoverbars.${portRegion}`, {
    id: DISCOVER_BAR_RECIPE,
    label: "Discovered The Sailor's Bar.",
    description: "I have found the bars where sailors meet when the longing for travel pulls at them oncemore.",
    requirements: { [elementCompleteId]: 1 },
    effects: { [elementCompleteId]: -1 },
    furthermore: [
      // Add the discovered bar aspect on the generic card
      {
        target: "~/exterior",
        mutations: {
          "mariner.nobarhere": { [DISCOVERED_ASPECT]: 1 },
        },
      },
      // Turn the generic card into its "at sea" counterpart
      { target: "~/exterior", decays: ["mariner.nobarhere"] },
      // Use the xtriggers defined on it to turn it into the newly discovered local bar
      {
        target: "~/exterior",
        aspects: { [DISPLAY_BAR_ASPECT]: 1 },
      },
    ],
    linked: [{ id: "mariner.addgroundedness" }],
  });
}
