const DEFAULT_SEASONS_EVENTS_IN_VAULTS = [
  {
    id: "suspicion",
    startingRecipe: "mariner.tide.suspicion.pre",
    requiredElement: "seasonsuspicion",
  },
];
const DEFAULT_SEASONS_IN_VAULTS = ["seasonsuspicion"];

const computeEventId = (event) => {
  if (!event.startsWith("events.")) return event;
  const eventCompleteIdParts = event.split(".");
  return eventCompleteIdParts[eventCompleteIdParts.length - 1];
};

// "events" is an array of {spawnRecipe, startingRecipe, id, requiredElement, requirements}
// if spawnRecipe is provided, don't create one. Otherwise, define it. If eventId is provided, you can deduce all the other parts, assuming it was generated
// following the conventions set by the vaultbuilder
export function defineCustomTideForVault(vaultId, layerId, events, pace) {
  const computeEventElements = (events) =>
    events.map((eventInformations) => {
      if (eventInformations.requiredElement)
        return eventInformations.requiredElement;
      if (eventInformations.id)
        return `mariner.events.vaults.${vaultId}.events.${computeEventId(
          eventInformations.id
        )}`;
      throw new Error(
        "Not enough event informations to deduce event element id"
      );
    });

  const TIDE_ID = `${vaultId}.layer.${layerId}`;
  const RECIPES_FILE = `recipes.tide.${TIDE_ID}`;
  mod.initializeRecipeFile(RECIPES_FILE, ["tide", "tides"]);

  mod.setDeck("decks.tides", {
    id: `mariner.decks.tide.${TIDE_ID}`,
    spec: [...computeEventElements(events), ...DEFAULT_SEASONS_IN_VAULTS],
    resetonexhaustion: true,
    shuffleAfterDraw: true,
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `mariner.tide.pickcustomtide.${TIDE_ID}.pre`,
    extantreqs: {
      [`mariner.aspects.vaults.${TIDE_ID}`]: 1,
    },
    warmup: pace ?? 120,
    label: "The Tide Comes In",
    description: "Time and Tide wait for no man.",
    linked: [{ id: `mariner.tide.${TIDE_ID}` }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `mariner.tide.${TIDE_ID}`,
    deckeffects: { [`mariner.decks.tide.${TIDE_ID}`]: 1 },
    linked: [{ id: "mariner.tide.postevent" }], // will get stuff prepended to it right below
  });

  for (let eventInformations of [
    ...events,
    ...DEFAULT_SEASONS_EVENTS_IN_VAULTS,
  ]) {
    if (eventInformations.spawnRecipe) {
      mod.addFirstToLinked(
        RECIPES_FILE,
        `mariner.tide.${TIDE_ID}`,
        eventInformations.spawnRecipe
      );
      continue;
    }
    let eventRequirements = eventInformations.requirements;
    if (!eventRequirements) {
      const requiredElement =
        eventInformations.requiredElement ??
        `mariner.events.vaults.${vaultId}.events.${computeEventId(
          eventInformations.id
        )}`;
      eventRequirements = {
        requirements: { [requiredElement]: 1 },
        effects: { [requiredElement]: -1 },
      };
    }
    mod.setRecipe(RECIPES_FILE, {
      id: `mariner.tide.${TIDE_ID}.spawnevent.${computeEventId(
        eventInformations.id
      )}`,
      ...eventRequirements,
      inductions: [
        {
          id:
            eventInformations.startingRecipe ??
            `mariner.vaults.${vaultId}.${eventInformations.id}`,
        },
      ],
      linked: [{ id: "mariner.tide.postevent" }],
    });
    mod.addFirstToLinked(
      RECIPES_FILE,
      `mariner.tide.${TIDE_ID}`,
      `mariner.tide.${TIDE_ID}.spawnevent.${computeEventId(
        eventInformations.id
      )}`
    );
  }
}
