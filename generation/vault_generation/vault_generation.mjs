import * as m from "../malleary.mjs";
import { generateDestinationReputationData } from "../port_generation/notoriety_generation.mjs";
import {
  exists,
  greyscaledOverlay,
  ME_LINK,
  ME_RECIPE,
  negativeValuesObject,
  SET_ADVENTURE_TRACKLIST,
} from "../generation_helpers.mjs";
import { generateDocksideWarehouseVault } from "./dockside_warehouse.mjs";
import { generateEkstersNestVault } from "./northsea/eksters_nest/eksters_nest.mjs";
import { generateSalvageMissionVault } from "./salvage_mission/salvage_mission.mjs";
import { generateSunkencityVault } from "./northsea/sunken_city/sunken_city.mjs";
import { generateRescueMissionVault } from "./rescue_mission/rescue_mission.mjs";
import { generateEddangerForestVault } from "./northsea/eddanger_forest/eddanger_forest.mjs";
import { SIGNALS } from "../generate_signal_aspects.mjs";
import { generateThebesVault } from "./mediterraneansea/thebes/thebes.mjs";
import { generateDamascusVault } from "./mediterraneansea/damascus/damascus.mjs";
import { generateAleppoVault } from "./mediterraneansea/aleppo/aleppo.mjs";
import { generateGaliciaVault } from "./mediterraneansea/galicia/galicia.mjs";
import { generateBizertaMorgueVault } from "./mediterraneansea/bizerta_morgue/bizerta_morgue.mjs";
import { generateGardenVault } from "./mediterraneansea/garden/garden.mjs";
import { probabilityChallengeTemplates } from "../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS, SLOT, SLOTS } from "../slots_helpers.mjs";

export function thresholdReq(lore, amount = 4) {
  return { recipe: { requirements: { [lore]: amount } }, suffix: lore };
}

export function thresholdReqs(lores, amount = 4) {
  return lores.map((lore) => thresholdReq(lore, amount));
}

export function generateVaultLocationElements({
  portRegion,
  portId,
  id,
  generic,
  authorizeRetreat,
  awayAspects,
  arrivalEffects,
  arrivalLinked,
  arrivalAlt,
  label,
  description,
  local,
  notorietyLabel,
  notorietyLabels,
  notDiscoverable,
  recipeContentOnLeave,
}) {
  /*
    This id is the root used to:
     - build other "distant" card ids
     - notoriety stuff
    It is used to have a different id per generation, compared to the potential local id for the vault (all versions of the docksidewarehouse use the same local task card that acts as the "docked" version of the location)
    */
  const portCompleteIdInSea = `mariner.locations.${portRegion}.${portId}`;
  /*
    For non generic vaults this id will be identical to the portCompleteIdInSea because they only exist in one location.
    Generic vaults provide an id that makes all different distant iterations of the same generic vault give the same local card
    */
  const portCompleteIdLocal = local?.id ?? portCompleteIdInSea;
  // Ids for away and locked states
  const portCompleteAwayId = portCompleteIdInSea + ".away";
  const portCompleteLockedId = portCompleteIdInSea + ".locked";

  const uniquenessGroupAspectId = `mariner.locations.${portRegion}.${portId}.uqg`;
  mod.setHiddenAspect("aspects.seas.uniquenessgroups", {
    id: uniquenessGroupAspectId,
  });

  const updateCosmeticRepRecipeId = generateDestinationReputationData({
    id: portId,
    levels: notorietyLabels,
    isVault: true,
  });

  // Add the flipcard of the port to the sea elements
  if (!mod.elements[`locations.${portRegion}`]) mod.initializeElementFile(`locations.${portRegion}`, ["locations", portRegion]);

  mod.setElements(
    `locations.${portRegion}`,
    {
      id: portCompleteIdLocal,
      $derives: ["mariner.composables.reputabledestination"],
      xexts: {
        $background: `locations/${portRegion}/${portId}`,
        $frame: "",
      },
      label: local?.label ?? `${label} [docked]`,
      description: local?.description ?? "However deep we'll delve or far we go, this is our way back to the open sea.",
      aspects: {
        "mariner.port": 1,
        "mariner.docked": 1,
        "mariner.vault": 1,
        "mariner.invault": 1,
        ...(!authorizeRetreat ? { "mariner.noretreatvault": 1 } : {}),
        [`mariner.locations.${portRegion}.sailingthere`]: 1,
        ...(local?.aspects ?? {}),
      },
      uniquenessgroup: uniquenessGroupAspectId,
      xtriggers: {
        [SIGNALS.LEAVE_PORT]: [m.transformXTrigger(portCompleteAwayId)],
        [SIGNALS.LOCK_PORT]: [m.transformXTrigger(portCompleteLockedId)],
        [SIGNALS.UPDATE_DISPLAYED_REPUTATION]: ME_RECIPE(updateCosmeticRepRecipeId),
      },
    },
    {
      id: portCompleteAwayId,
      $derives: ["mariner.composables.reputabledestination"],
      xexts: {
        $background: `locations/${portRegion}/${portId}`,
        $frame: "",
      },
      label,
      description,
      aspects: {
        "mariner.destination": 1,
        "mariner.vault": 1,
        ...(!authorizeRetreat ? { "mariner.noretreatvault": 1, "mariner.destroyonleave": 1 } : {}),
        [`mariner.locations.${portRegion}.sailingthere`]: 1,
        ...awayAspects,
      },
      xtriggers: {
        [SIGNALS.ARRIVE_TO_DESTINATION]: [
          m.transformXTrigger(portCompleteIdLocal),
          ME_LINK("mariner.sailing.arriving.handlelonging"),
        ],
        [SIGNALS.LOCK_PORT]: [m.transformXTrigger(portCompleteLockedId)],
        [SIGNALS.UPDATE_DISPLAYED_REPUTATION]: ME_RECIPE(updateCosmeticRepRecipeId),
      },
      uniquenessgroup: uniquenessGroupAspectId,
    },
    {
      id: portCompleteLockedId,
      $derives: ["mariner.composables.reputabledestination"],
      xexts: {
        $background: `locations/${portRegion}/${portId}`,
        $frame: "",
      },
      decayTo: portCompleteAwayId,
      label,
      description,
      aspects: {
        "mariner.destination.locked": 1,
        "mariner.vault": 1,
        ...(!authorizeRetreat ? { "mariner.noretreatvault": 1 } : {}),
        [`mariner.locations.${portRegion}.presentthere`]: 1,
      },
      uniquenessgroup: uniquenessGroupAspectId,
      xtriggers: {
        [`mariner.signal.arriveinsea.${portRegion}`]: [m.transformXTrigger(portCompleteAwayId)],
        [SIGNALS.UPDATE_DISPLAYED_REPUTATION]: ME_RECIPE(updateCosmeticRepRecipeId),
      },
    }
  );

  // Add port to its sea deck
  mod.getDeck("decks.seas", `mariner.decks.locations.${portRegion}.alldestinations`).spec.push(portCompleteIdLocal);

  if (!notDiscoverable)
    mod.getDeck("decks.seas", `mariner.decks.locations.${portRegion}.discoverable`).spec.push(portCompleteAwayId);

  // Create the dockto recipe. TODO: fix, Not really clean because each generic vault instance will generate it again
  const locationId = id ? id : `${portRegion}.${portId}`;
  mod.setRecipe(`recipes.dockto.${portRegion}`, {
    id: `mariner.dockto.${portRegion}.${portId}`,
    label: `Docking at ${label}`,
    startdescription: "Docking!",
    requirements: {
      [portCompleteIdInSea]: 1,
    },
    furthermore: arrivalEffects,
    ...exists(arrivalAlt, { alt: arrivalAlt }),
    ...exists(arrivalLinked, { linked: arrivalLinked }),
    ...SET_ADVENTURE_TRACKLIST,
  });
  // mod.addToLinked(
  //   "recipes.sailing.arriving",
  //   "mariner.sailing.arriving.docking",
  //   `mariner.dockto.${portRegion}.${portId}`
  // );

  recipeContentOnLeave = recipeContentOnLeave ?? {};
  recipeContentOnLeave.deckShuffles = [
    ...(recipeContentOnLeave.deckShuffles ?? []),
    ...(mod.deckIdsToShuffleOnLeave[portId] ?? []),
  ];
  recipeContentOnLeave.linked = [...(recipeContentOnLeave.linked ?? []), { id: "mariner.sailing.leaving.leaveport" }];
  mod.setRecipe(`recipes.sailing.leaving.${portRegion}`, {
    id: `mariner.leaving.${locationId}`,
    tablereqs: {
      [portCompleteIdInSea]: 1,
    },
    ...recipeContentOnLeave,
  });
  mod.addFirstToLinked("recipes.sailing.leaving", "mariner.sailing.leaving.start", `mariner.leaving.${locationId}`);
}

export function generateProbabilityChallenge(file, prefix, actionId, template, successLinked, failureLinked, howManySlots) {
  // Generate 1 base recipe, 3 alt recipes, per aspect
  let altLinks = [];
  for (const aspect of Object.keys(template.aspects)) {
    altLinks.push({ id: `${prefix}.challenge.${aspect}.high` });
  }
  for (const aspect of Object.keys(template.aspects)) {
    altLinks.push({ id: `${prefix}.challenge.${aspect}.med` });
  }
  for (const aspect of Object.keys(template.aspects)) {
    altLinks.push({ id: `${prefix}.challenge.${aspect}.low` });
  }

  mod.setRecipe(file, {
    id: `${prefix}.challenge`,
    actionId,
    warmup: 15,
    label: template.base.label,
    startdescription: template.base.description,
    ...template.base,
    alt: altLinks,
    linked: failureLinked,
    slots: CHALLENGE_SLOTS(howManySlots ?? 1, actionId),
  });

  // For each aspect, for each tier (low med high)
  for (const aspect of Object.keys(template.aspects)) {
    const chances = { low: 30, med: 70, high: 90 };
    const requirements = { low: 2, med: 5, high: 10 };
    for (const tier of ["high", "med", "low"]) {
      mod.setRecipe(file, {
        id: `${prefix}.challenge.${aspect}.${tier}`,
        actionId,
        label: template.aspects[aspect][tier].label,
        startdescription: template.aspects[aspect][tier].description,
        requirements: { [aspect]: requirements[tier] },
        linked: [{ chance: chances[tier], id: successLinked }, ...failureLinked],
      });
    }
  }
}

export function generateLayerRecipe({ recipesFile, task, activityAspects, exclusionAspects }) {
  // Exclusivity hint
  if (exclusionAspects) {
    mod.setRecipe(recipesFile, {
      ...task,
      actionId: task.actionId,
      hintonly: true,
      id: `${task.id}.hintexclusiveattention`,
      extantreqs: {
        ...task.extantreqs,
        ...negativeValuesObject(exclusionAspects),
      },
      label: task.label,
      startdescription:
        "I cannot do this task, as some other task focuses my attention completely, or because this task requires my undivided attention.",
      linked: [],
      alt: [],
    });
  }

  // Pre.task, used mainly to instantly create some elements (like exclusivity aspects)
  mod.setRecipe(recipesFile, {
    actionId: task.actionId,
    craftable: task.craftable ?? true,
    ...task,
    warmup: 0,
    id: `${task.id}.pre`,
    furthermore: [],
    extantreqs: { ...task.extantreqs, ...exclusionAspects },
    effects: { ...activityAspects },
    linked: [{ id: task.id }],
  });
  // Task
  mod.setRecipe(recipesFile, {
    actionId: task.actionId,
    warmup: task.warmup ?? 15,
    ...task,
    craftable: false,
    aspects: {
      ...task.aspects,
    },
  });
}

export function generateProbabilityChallengeTask({
  recipesFile,
  actionId,
  task,
  success,
  failure,
  template,
  bypass,
  activityAspects,
  exclusionAspects,
}) {
  let linkedToChallenge = [{ id: `mariner.challenge.${template.id}` }];

  // Possible bypass recipe
  if (bypass) {
    mod.setRecipe(recipesFile, {
      id: `${task.id}.bypass`,
      actionId,
      ...bypass,
      linked: [{ id: `${task.id}.success.expulsewasted` }],
    });
    linkedToChallenge.unshift({ id: `${task.id}.bypass` });
  }

  // Exclusivity hint
  if (exclusionAspects) {
    mod.setRecipe(recipesFile, {
      ...task,
      actionId,
      hintonly: true,
      id: `${task.id}.hintexclusiveattention`,
      extantreqs: {
        ...task.extantreqs,
        ...negativeValuesObject(exclusionAspects),
      },
      label: task.label,
      startdescription:
        "I cannot do this task, as some other task focuses my attention completely, or because this task requires my undivided attention.",
      linked: [],
      alt: [],
    });
  }

  // Pre.task, used mainly to instantly create some elements (like exclusivity aspects)
  mod.setRecipe(recipesFile, {
    actionId,
    craftable: task.craftable ?? true,
    ...task,
    warmup: 0,
    id: `${task.id}.pre`,
    extantreqs: { ...task.extantreqs, ...exclusionAspects },
    effects: { ...task.effects, ...activityAspects },
    linked: [{ id: task.id }],
  });
  // Task
  mod.setRecipe(recipesFile, {
    actionId,
    warmup: task.warmup ?? 15,
    ...task,
    craftable: false,
    requirements: undefined,
    addCallbacks: {
      FAIL_CHALLENGE: `${task.id}.failure`,
      SUCCEED_CHALLENGE: `${task.id}.success.expulsewasted`,
    },
    aspects: {
      ...task.aspects,
      [SIGNALS.USE_HEART]: 1,
    },
    linked: linkedToChallenge,
  });

  mod.setRecipe(recipesFile, {
    id: `${task.id}.success.expulsewasted`,
    actionId,
    linked: [{ id: `${task.id}.success` }, { id: `${task.id}.failure` }],
  });
  // Success
  mod.setRecipe(recipesFile, {
    id: `${task.id}.success`,
    actionId,
    label: task.label,
    ...success,
    effects: { ...success.effects, ...negativeValuesObject(activityAspects) },
    aspects: { [SIGNALS.USE_TOOL]: 1 },
  });
  // Failure
  mod.setRecipe(recipesFile, {
    id: `${task.id}.failure`,
    actionId,
    label: task.label,
    ...failure,
    effects: { ...failure.effects, ...negativeValuesObject(activityAspects) },
    aspects: { [SIGNALS.MISUSE_TOOL]: 1 },
  });
  // Do not generate dedicated stuff anymore, we rely on global challenges + callbacks now
  // generateProbabilityChallenge(
  //   recipesFile,
  //   task.id,
  //   actionId,
  //   template,
  //   `${task.id}.success.expulsewasted`,
  //   [{ id: `${task.id}.failure` }]
  // );
}

export function generateTresholdChallengeTask({
  recipesFile,
  actionId,
  task,
  slots,
  success,
  successEnd,
  failure,
  bypass,
  activityAspects,
  exclusionAspects,
}) {
  let linkedToEnd = [{ id: `${task.id}.success.expulsewasted` }, { id: `${task.id}.failure` }];

  if (bypass) {
    mod.setRecipe(recipesFile, {
      id: `${task.id}.bypass`,
      actionId,
      ...bypass,
      linked: [{ id: `${task.id}.success.expulsewasted.bypass` }],
    });
    linkedToEnd.unshift({ id: `${task.id}.bypass` });
    mod.setRecipe(recipesFile, {
      id: `${task.id}.success.expulsewasted.bypass`,
      actionId,
      //   alt: [
      //     {
      //       id: "mariner.vaults.destroywasted",
      //       additional: true,
      //       expulsion: {
      //         limit: 50,
      //         filter: {
      //           "mariner.vaults.help": 1,
      //         },
      //       },
      //     },
      //   ],
      linked: [{ id: `${task.id}.success` }],
    });
  }

  const successRecipeLink = (task, successRecipe, suffix) => {
    mod.setRecipe(recipesFile, {
      id: `${task.id}.success.expulsewasted${suffix ? "." + suffix : ""}`,
      actionId,
      // alt: [{
      //     id: "mariner.vaults.destroywasted", additional: true, expulsion: {
      //         "limit": 50,
      //         "filter": {
      //             "mariner.vaults.help": 1
      //         }
      //     }
      // }],
      linked: [{ id: `${task.id}.success` }],
      ...successRecipe,
    });
    linkedToEnd = linkedToEnd.filter((l) => l.id !== `${task.id}.success.expulsewasted`);
    linkedToEnd.unshift({
      id: `${task.id}.success.expulsewasted${suffix ? "." + suffix : ""}`,
    });
  };

  if (Array.isArray(success)) {
    for (const successRecipe of success) successRecipeLink(task, successRecipe.recipe, successRecipe.suffix);
  } else {
    successRecipeLink(task, success);
  }

  // Exclusivity hint
  mod.setRecipe(recipesFile, {
    ...task,
    actionId,
    hintonly: true,
    id: `${task.id}.hintexclusiveattention`,
    extantreqs: {
      ...task.extantreqs,
      ...negativeValuesObject(exclusionAspects),
    },
    label: task.label,
    startdescription:
      "I cannot do this task, as some other task focuses my attention completely, or because this task requires my undivided attention.",
    linked: [],
    alt: [],
  });

  mod.setRecipe(recipesFile, {
    actionId,
    craftable: true,
    warmup: 30,
    slots: slots ?? [
      {
        id: "event_help",
        label: "Help",
        description: "Which implements are available for my approach.",
        required: {
          "mariner.song": 1,
          "mariner.crew": 1,
          "mariner.inspiration.nature": 1,
          "mariner.vaults.help": 1,
          "mariner.tune": 1,
          tool: 1,
        },
      },
    ],
    ...task,
    extantreqs: { ...task.extantreqs, ...exclusionAspects },
    effects: { ...task.effects, ...activityAspects },
    aspects: {
      ...task.aspects,
      [SIGNALS.USE_HEART]: 1,
    },
    linked: linkedToEnd,
  });

  mod.setRecipe(recipesFile, {
    id: `${task.id}.success`,
    actionId,
    label: task.label,
    ...successEnd,
    effects: {
      ...successEnd.effects,
      ...negativeValuesObject(activityAspects),
    },
    aspects: { [SIGNALS.USE_TOOL]: 1, ...successEnd.aspects },
  });
  mod.setRecipe(recipesFile, {
    id: `${task.id}.failure`,
    actionId,
    label: task.label,
    // alt: [
    //   {
    //     id: "mariner.vaults.destroywasted",
    //     additional: true,
    //     expulsion: {
    //       limit: 50,
    //       filter: {
    //         "mariner.vaults.help": 1,
    //       },
    //     },
    //   },
    // ],
    ...failure,
    effects: { ...failure.effects, ...negativeValuesObject(activityAspects) },
    aspects: { [SIGNALS.MISUSE_TOOL]: 1, ...failure.aspects },
  });
}

// NOT USED ANYWHERE. BROKEN BECAUSE OF HOW REP NOW WORKS, PROBABLY. MIGHT NOT HAVE EVER WORKED CORRECTLY.
export function generateDynamicRiskExplorationTask({
  vaultId,
  taskCompleteId,
  taskId,
  label,
  description,
  exhaustedLabel,
  exhaustedDescription,
  tasks,
  events,
  activityAspects,
  exclusionAspects,
  next,
  generic,
  portRegion,
}) {
  const startRecipeId = `mariner.vaults.${vaultId}.${taskId}.task`;
  const drawTaskRecipeId = `mariner.vaults.${vaultId}.${taskId}.drawtask`;
  const drawEventRecipeId = `mariner.vaults.${vaultId}.${taskId}.drawevent`;
  const reputationId = `mariner.reputation.${generic ? "vaults" : portRegion}.${vaultId}`;

  // Exclusivity hint
  mod.setRecipe(`recipes.vaults.${vaultId}`, {
    label,
    actionId: "explore",
    hintonly: true,
    id: `${startRecipeId}.hintexclusiveattention`,
    requirements: { [taskCompleteId]: 1 },
    extantreqs: { ...negativeValuesObject(exclusionAspects) },
    startdescription:
      "I cannot do this task, as some other task focuses my attention completely, or because this task requires my undivided attention.",
    linked: [],
    alt: [],
  });

  // Starting recipe
  mod.setRecipe(`recipes.vaults.${vaultId}`, {
    id: startRecipeId,
    actionId: "explore",
    craftable: true,
    warmup: 30,
    label: label,
    startdescription: description,
    requirements: { [taskCompleteId]: 1 },
    extantreqs: { ...exclusionAspects },
    effects: { ...activityAspects },
    linked: [
      ...(events
        ? [
            {
              id: drawEventRecipeId + ".highestrisk",
              requirements: { [reputationId]: 4 },
              linked: [{ id: drawEventRecipeId, chance: 90 }, { id: drawTaskRecipeId }],
            },
            {
              id: drawEventRecipeId + ".highrisk",
              requirements: { [reputationId]: 3 },
              linked: [{ id: drawEventRecipeId, chance: 70 }, { id: drawTaskRecipeId }],
            },
            {
              id: drawEventRecipeId + ".medrisk",
              requirements: { [reputationId]: 2 },
              linked: [{ id: drawEventRecipeId, chance: 50 }, { id: drawTaskRecipeId }],
            },
            {
              id: drawEventRecipeId + ".lowrisk",
              requirements: { [reputationId]: 1 },
              linked: [{ id: drawEventRecipeId, chance: 25 }, { id: drawTaskRecipeId }],
            },
            {
              id: drawEventRecipeId + ".lowestrisk",
              linked: [{ id: drawEventRecipeId, chance: 10 }, { id: drawTaskRecipeId }],
            },
          ]
        : []),
      { id: drawTaskRecipeId },
    ],
  });

  if (!mod.decks[`decks.vaults.${vaultId}`]) mod.initializeDeckFile(`decks.vaults.${vaultId}`, ["vaults", vaultId]);

  // Draw a task
  generateTasksDrawForExplorationTask(vaultId, tasks, taskId, activityAspects, next, exhaustedLabel, exhaustedDescription);

  // Draw an event
  if (events) {
    generateEventsDrawForExplorationTask(events, vaultId, drawEventRecipeId, taskId, label, activityAspects);
  }
}

export function generateExplorationTask({
  vaultId,
  taskCompleteId,
  eventProbability,
  taskId,
  label,
  description,
  warmup,
  exhaustedLabel,
  exhaustedDescription,
  tasks,
  events,
  activityAspects,
  exclusionAspects,
  next,
  preRecipe,
  recipe,
  additionalPreLinked,
  resetOnExhaustion,
  shuffleAfterDraw,
}) {
  const startRecipeId = `mariner.vaults.${vaultId}.${taskId}.task`;
  const drawTaskRecipeId = `mariner.vaults.${vaultId}.${taskId}.drawtask`;
  const drawEventRecipeId = `mariner.vaults.${vaultId}.${taskId}.drawevent`;

  // Exclusivity hint
  mod.setRecipe(`recipes.vaults.${vaultId}`, {
    label,
    actionId: "explore",
    hintonly: true,
    id: `${startRecipeId}.hintexclusiveattention`,
    requirements: { [taskCompleteId]: 1 },
    extantreqs: { ...negativeValuesObject(exclusionAspects) },
    startdescription:
      "I cannot do this task, as some other task focuses my attention completely, or because this task requires my undivided attention.",
    linked: [],
    alt: [],
  });

  // Starting recipe
  mod.setRecipe(`recipes.vaults.${vaultId}`, {
    ...preRecipe,
    id: `${startRecipeId}.pre`,
    actionId: "explore",
    craftable: true,
    label: label,
    startdescription: description,
    requirements: { [taskCompleteId]: 1, ...preRecipe?.requirements },
    extantreqs: { ...exclusionAspects, ...preRecipe?.extantreqs },
    effects: { ...activityAspects, ...preRecipe?.effects },
    linked: [...(additionalPreLinked ?? []), { id: startRecipeId }],
  });

  mod.setRecipe(`recipes.vaults.${vaultId}`, {
    id: startRecipeId,
    warmup: warmup ?? 30,
    linked: [
      ...(events && events.length > 0 ? [{ id: drawEventRecipeId, chance: eventProbability ?? 20 }] : []),
      { id: drawTaskRecipeId },
    ],
    ...recipe,
  });

  if (!mod.decks[`decks.vaults.${vaultId}`]) mod.initializeDeckFile(`decks.vaults.${vaultId}`, ["vaults", vaultId]);

  // Draw a task
  generateTasksDrawForExplorationTask(
    vaultId,
    tasks,
    taskId,
    activityAspects,
    next,
    exhaustedLabel,
    exhaustedDescription,
    resetOnExhaustion,
    shuffleAfterDraw
  );

  // Draw an event
  if (events !== undefined && events.length > 0) {
    generateEventsDrawForExplorationTask(events, vaultId, drawEventRecipeId, taskId, label, activityAspects);
  }
}

function generateTasksDrawForExplorationTask(
  vaultId,
  tasks,
  taskId,
  activityAspects,
  next,
  exhaustedLabel,
  exhaustedDescription,
  resetOnExhaustion,
  shuffleAfterDraw
) {
  const tasksDeckId = `mariner.decks.vaults.${vaultId}.explore.${taskId}.tasks`;
  const exhaustedTasksRecipeId = `mariner.decks.vaults.${vaultId}.explore.${taskId}.exhausted`;

  mod.setDeck(`decks.vaults.${vaultId}`, {
    id: tasksDeckId,
    label: "Potential Possibilities",
    description: "Places, things and people to be explored or exploited.",
    spec: tasks,
    resetOnExhaustion,
    defaultcard: "mariner.emptydeck",
  });

  if (!mod.deckIdsToShuffleOnLeave[vaultId]) mod.deckIdsToShuffleOnLeave[vaultId] = [];
  mod.deckIdsToShuffleOnLeave[vaultId].push(tasksDeckId);

  mod.setRecipe(`recipes.vaults.${vaultId}`, {
    id: `mariner.vaults.${vaultId}.${taskId}.drawtask`,
    actionId: "explore",
    deckeffects: { [tasksDeckId]: 1 },
    effects: { ...negativeValuesObject(activityAspects) },
    linked: [{ id: exhaustedTasksRecipeId }, ...(next ?? [])],
  });
  mod.setRecipe(`recipes.vaults.${vaultId}`, {
    id: exhaustedTasksRecipeId,
    actionId: "explore",
    requirements: { "mariner.emptydeck": 1 },
    effects: { "mariner.emptydeck": -1 },
    label: exhaustedLabel,
    description: exhaustedDescription,
  });
}

function generateEventsDrawForExplorationTask(events, vaultId, drawEventRecipeId, taskId, label, activityAspects) {
  const eventsDeckId = `mariner.decks.vaults.${vaultId}.explore.${taskId}.events`;
  const computeEventId = (event) => {
    const eventCompleteIdParts = event.split(".");
    return eventCompleteIdParts[eventCompleteIdParts.length - 1];
  };
  const eventDummyElements = [];
  for (let event of events) {
    const eventId = computeEventId(event);

    if (!mod.elements[`events.vaults.${vaultId}`]) mod.initializeElementFile(`events.vaults.${vaultId}`, ["vaults", vaultId]);
    mod.setElement(`events.vaults.${vaultId}`, {
      id: `mariner.events.vaults.${vaultId}.events.${eventId}`,
      aspects: { "mariner.vaults.event": 1 },
    });
    eventDummyElements.push(`mariner.events.vaults.${vaultId}.events.${eventId}`);
  }

  mod.setDeck(`decks.vaults.${vaultId}`, {
    id: eventsDeckId,
    label: "Exploration Deck",
    spec: eventDummyElements,
    resetonexhaustion: true,
  });

  if (!mod.deckIdsToShuffleOnLeave[vaultId]) mod.deckIdsToShuffleOnLeave[vaultId] = [];
  mod.deckIdsToShuffleOnLeave[vaultId].push(eventsDeckId);

  mod.setRecipe(`recipes.vaults.${vaultId}`, {
    id: drawEventRecipeId,
    actionId: "explore",
    deckeffects: { [eventsDeckId]: 1 },
    linked: events
      .map((e) => computeEventId(e))
      .map((id) => ({
        id: `mariner.vaults.${vaultId}.${taskId}.drawevent.${id}`,
      })),
  });

  for (const event of events) {
    const dummyEventElementId = `mariner.events.vaults.${vaultId}.events.${computeEventId(event)}`;
    mod.setRecipe(`recipes.vaults.${vaultId}`, {
      id: `mariner.vaults.${vaultId}.${taskId}.drawevent.${computeEventId(event)}`,
      label: label,
      requirements: { [dummyEventElementId]: 1 },
      effects: {
        [dummyEventElementId]: -1,
        ...negativeValuesObject(activityAspects),
      },
      inductions: [{ id: event }],
    });
  }
}

export function generateGlobalProbabilityChallenge(challengeId, template, howManySlots) {
  mod.initializeRecipeFile(`challenges.${challengeId}`, ["challenges"]);
  // Generate 1 base recipe, 3 alt recipes, per aspect
  let altLinks = [];
  for (const aspect of Object.keys(template.aspects)) {
    altLinks.push({ id: `mariner.challenge.${challengeId}.${aspect}.high` });
  }
  for (const aspect of Object.keys(template.aspects)) {
    altLinks.push({ id: `mariner.challenge.${challengeId}.${aspect}.med` });
  }
  for (const aspect of Object.keys(template.aspects)) {
    altLinks.push({ id: `mariner.challenge.${challengeId}.${aspect}.low` });
  }

  mod.setRecipe(`challenges.${challengeId}`, {
    id: `mariner.challenge.${challengeId}`,
    warmup: 15,
    label: template.base.label,
    startdescription: template.base.description,
    ...template.base,
    alt: altLinks,
    linked: [{ useCallback: "FAIL_CHALLENGE" }],
    slots: ["explore", "talk", "mariner.navigate", "mariner.sail", "mariner.sing"]
      .map((actionId) => CHALLENGE_SLOTS(howManySlots ?? 1, actionId))
      .flat(),
  });

  // For each aspect, for each tier (low med high)
  for (const aspect of Object.keys(template.aspects)) {
    const chances = { low: 30, med: 70, high: 90 };
    const requirements = { low: 2, med: 5, high: 10 };
    for (const tier of ["high", "med", "low"]) {
      mod.setRecipe(`challenges.${challengeId}`, {
        id: `mariner.challenge.${challengeId}.${aspect}.${tier}`,
        label: template.aspects[aspect][tier].label,
        startdescription: template.aspects[aspect][tier].description,
        requirements: { [aspect]: requirements[tier] },
        linked: [{ chance: chances[tier], useCallback: "SUCCEED_CHALLENGE" }, { useCallback: "FAIL_CHALLENGE" }],
      });
    }
  }
}

export function generateVaultsContent() {
  mod.initializeDeckFile("decks.vaults", ["vaults"]);
  mod.initializeAspectFile("aspects.vaults", ["vaults"]);

  // We generate global challenges using callbacks. Vaults and most other things will now link to the right one and setup the proper callbacks beforehand.

  for (const [challengeId, template] of Object.entries(probabilityChallengeTemplates)) {
    generateGlobalProbabilityChallenge(challengeId.toLowerCase(), template);
  }

  // Generic vaults
  generateDocksideWarehouseVault();
  generateSalvageMissionVault();
  generateRescueMissionVault();

  // North sea
  generateEkstersNestVault();
  generateSunkencityVault();
  generateEddangerForestVault();

  // Med sea
  generateThebesVault();
  generateDamascusVault();
  generateAleppoVault();
  generateBizertaMorgueVault();
  generateGaliciaVault();
  generateGardenVault();
}
