import { generateBend } from "./tasks/bend.mjs";
import { generateBranch } from "./tasks/branch.mjs";
import { generateReeds } from "./tasks/reeds.mjs";
import { generateCity } from "./tasks/city.mjs";
import { generateGauntletLayer } from "./tasks/fields.mjs";
import { vaultPrefix, ASPECTS_FILE, RECIPES_FILE } from "./thebes.mjs";

// TASKS
export function generateReedsLayer(vb) {
  vb.defineLayer("1");

  mod.setAspect(ASPECTS_FILE, { id: `mariner.flag.stone.draw` });
  mod.setAspect(ASPECTS_FILE, { id: `mariner.flag.wood.draw` });

  vb.defineHelpElement({
    id: "stone",
    label: "<Indication of the Branch: Stone>",
    unique: true,
    description:
      "<In the Branches neighborhood. The city was cobbled with white marble, some of those crumbs remain. North of the Theban branch, the white cypresses rose. These stones share the right faded glory, at least when I remove the muck. >",
  });
  vb.defineHelpElement({
    id: "wood",
    label: "<Indication of the Branch: Wood>",
    unique: true,
    description:
      "<Here the city was cobbled with white marble, some of those crumbs remain. North of the Theban branch, the white cypresses rose, The mummified woods I found here have the right grain, and still smell faintly of cedar.>",
  });
  mod.setElement(vb.ELEMENTS_FILE, {
    id: vb.vp("chest"),
    label: "<A Lacquered Chest: Occluded>",
    aspects: { "mariner.navigate_allowed": 1, "mariner.topic": 1 },
    unique: true,
    description:
      "<Inside a waxed cloth casing, we find a chest of oiled cypress wood. Around the wood are wrapped tight leather bands, and on those bands are written symbols in a hooked script unfamiliar to me. The wards on this treasure hold strong, which is why it survived the waters. But it also means I better take it to an expert before attempting to open it.>",
  });
  mod.setElement(vb.ELEMENTS_FILE, {
    id: vb.vp("chest.revealed"),
    label: "<A Lacquered Chest>",
    aspects: { "mariner.navigate_allowed": 1, "mariner.topic": 1 },
    unique: true,
    description:
      "<Inside a waxed cloth casing, we find a chest of oiled cypress wood. Around the wood are wrapped tight leather bands, and on those bands are written symbols in a hooked script unfamiliar to me. The wards on this treasure hold strong, which is why it survived the waters. Advice was rendered on the proper incantations, but the implements I have to provide myself.>",
    slots: [
      {
        id: "tool",
        label: "Tool",
        description: "A tool to pry and pilfer",
        actionId: "mariner.navigate",
        required: {
          tool: 1,
        },
      },
      {
        id: "trappings",
        label: "Trapping",
        description: "The trappings that unseam and unlatch",
        actionId: "mariner.navigate",
        required: {
          "mariner.trapping": 1,
        },
      },
      {
        id: "expert",
        label: "An Expert",
        description:
          "This requires expert hands to not leave the fingers curse-stained. In this case, I will be the assistant to the Expert I implore.",
        actionId: "mariner.navigate",
        required: {
          "mariner.npc": 1,
          "mariner.local": 1,
        },
      },
    ],
  });
  vb.defineExplorationTask({
    taskId: "riverbanks",
    locationLabel: "Luxor River Banks",
    locationDescription:
      "The Nile, just like the Histories, sometimes shifts and strays. The boardwalks of Thebes where the Lethian House was situated are now the shallow depths besides the temple-filled corridors of the City of Kings. I have gotten directions from Miriam, but with the shifting river and the changing town, finding the exact spot of the House will be an effort.",
    tasks: ["reeds", "bend", "city"],
    events: ["events.others", "events.scales", "events.colossus"],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `mariner.reveal.chest.hint`,
    requirements: {
      [vb.vp("chest")]: 1,
    },
    hintonly: true,
    actionid: "mariner.navigate",
    label: "Unseal The Warded Chest?",
    startdescription:
      "<This box is sealed, with nails, with wrappings, and with a embossed script. Not even the water dared to disturb it's sanctity, and so for me to attempt it unassisted would be foolish.>",
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `mariner.unlatch.chest.hint.nonothing`,
    requirements: {
      [vb.vp("chest.revealed")]: 1,
      "mariner.tool.forge": -1,
      "mariner.trappings.knock": -1,
      "mariner.locations.medsea.constantinople.nun": -1,
    },
    warmup: 30,
    label: "Unseal The Warded Chest?",
    startdescription:
      "<This box is sealed, with nails, with wrappings, and with a embossed script. I will need the right tool to pry open its latches, and the right trappings to unseam its seals.>",
    hintonly: true,
    actionid: "mariner.navigate",
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `mariner.unlatch.chest.hint.notoolnoperson`,
    requirements: {
      [vb.vp("chest.revealed")]: 1,
      "mariner.tool.forge": -1,
      "mariner.trappings.knock": 1,
      "mariner.locations.medsea.constantinople.nun": -1,
    },
    warmup: 30,
    label: "Unseal The Warded Box?",
    startdescription:
      "<This box is sealed, with nails, with wrappings, and with a embossed script. I have found the trappings to evoke the rites of opening, but I still need to tools to pry the >",
    hintonly: true,
    actionid: "mariner.navigate",
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `mariner.unlatch.chest.hint.notrappingnoperson`,
    requirements: {
      [vb.vp("chest.revealed")]: 1,
      "mariner.tool.forge": 1,
      "mariner.trappings.knock": -1,
      "mariner.locations.medsea.constantinople.nun": -1,
    },
    warmup: 30,
    label: "Unseal The Warded Box?",
    startdescription:
      "<This box is sealed, with nails, with wrappings, and with a embossed script. I have found the tool I needed, and the right trappings to unseam its seals.>",
    hintonly: true,
    actionid: "mariner.navigate",
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `mariner.unlatch.chest.hint.nopersonnotool`,
    requirements: {
      [vb.vp("chest.revealed")]: 1,
      "mariner.tool.forge": -1,
      "mariner.trappings.knock": 1,
      "mariner.locations.medsea.constantinople.nun": -1,
    },
    warmup: 30,
    label: "Unseal The Warded Box?",
    startdescription:
      "<This box is sealed, with nails, with wrappings, and with a embossed script. I have been warned that the wards are strong, and I should not attempt it without being under Mother Euphresme's Guidance.>",
    hintonly: true,
    actionid: "mariner.navigate",
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `mariner.unlatch.chest.hint.notool`,
    requirements: {
      [vb.vp("chest.revealed")]: 1,
      "mariner.tool.forge": -1,
      "mariner.trappings.knock": 1,
      "mariner.locations.medsea.constantinople.nun": 1,
    },
    warmup: 30,
    label: "Unseal The Warded Box?",
    startdescription:
      "<This box is sealed, with nails, with wrappings, and with a embossed script. I have found the trappings to evoke the rites of opening, but I still need to tools to pry the >",
    hintonly: true,
    actionid: "mariner.navigate",
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `mariner.unlatch.chest.hint.notrapping`,
    requirements: {
      [vb.vp("chest.revealed")]: 1,
      "mariner.tool.forge": 1,
      "mariner.trappings.knock": -1,
      "mariner.locations.medsea.constantinople.nun": 1,
    },
    warmup: 30,
    label: "Unseal The Warded Box?",
    startdescription:
      "<This box is sealed, with nails, with wrappings, and with a embossed script. I have found the tool I needed, and the right trappings to unseam its seals.>",
    hintonly: true,
    actionid: "mariner.navigate",
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `mariner.unlatch.chest.hint.noperson`,
    requirements: {
      [vb.vp("chest.revealed")]: 1,
      "mariner.tool.forge": 1,
      "mariner.trappings.knock": 1,
      "mariner.locations.medsea.constantinople.nun": -1,
    },
    warmup: 30,
    label: "Unseal The Warded Box?",
    startdescription:
      "<This box is sealed, with nails, with wrappings, and with a embossed script. I have been warned that the wards are strong, and I should not attempt it without being under Mother Euphresme's Guidance.>",
    hintonly: true,
    actionid: "mariner.navigate",
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `mariner.unlatch.chest`,
    requirements: {
      [vb.vp("chest.revealed")]: 1,
      "mariner.tool.forge": 1,
      "mariner.trappings.knock": 1,
      "mariner.locations.medsea.constantinople.nun": 1,
    },
    warmup: 30,
    actionid: "mariner.navigate",
    label: "Unseal The Warded Box",
    startdescription:
      "I have layed down my tools on oiled parchment, as Euphresme sanctified the space with tooth and venom. Her eyes glitter as her lips curl into the first incantations. I stand ready to assist.",
    description:
      "<Some parts of the ritual feel like a holy sacrament, other parts like a harrowing ceaserian surgery or a tense dismantling of a device. Euphresme is High Priestess, Head Nurse, engineer. Most items are extracted, all except for the book. But she keeps open the lid while I hastefully make a copy.>",
    effects: {
      "mariner.matthiasamethystpursuitaramaic": 1,
      "mariner.trappings.lantern.1": 2,
      "mariner.trappings.winter.1": 1,
      "mariner.tool.moth": 1,
    },
  });

  generateBend(vb);
  generateBranch(vb);
  generateReeds(vb);
  generateCity(vb);
  generateGauntletLayer(vb);
}
