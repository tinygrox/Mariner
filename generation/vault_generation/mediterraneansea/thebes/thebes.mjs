import { VaultBuilder } from "../../vaultbuilder.mjs";
import { generateReedsLayer } from "./luxor_reeds.mjs";
import { generateReedEvents } from "./events.mjs";

export const ELEMENTS_FILE = "vaults.thebes";
export const RECIPES_FILE = "recipes.vaults.thebes";
export const ASPECTS_FILE = "aspects.vaults.thebes";

export const vaultPrefix = (end) => `mariner.vaults.thebes.${end}`;

export function generateThebesVault() {
  const vb = new VaultBuilder("thebes", "medsea");
  generateReedsLayer(vb);
  generateReedEvents(vb);
  vb.computeCustomTideIfRequired();
}
