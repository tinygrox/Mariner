import { vaultPrefix, ASPECTS_FILE, RECIPES_FILE } from "../galicia.mjs";
import { probabilityChallengeTemplates } from "../../../../probability_challenge_templates.mjs";
import { SPAWN_MISFORTUNE_CURSE } from "../../../../curses_generation/curses/misfortune.mjs";
import { QUEST_FLAG_ID } from "../../../../global_flags_generation.mjs";
import { UPDATE_QUEST } from "../../../../quest_generation.mjs";
import { CHALLENGE_SLOTS } from "../../../../slots_helpers.mjs";

export function generateGalantha(vb) {
  vb.defineTask({
    taskId: "galantha",
    label: "The Galantha",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description: '"I was wondering when you would come, let\'s have it then. Claim the restitution you seek."',
    actions: {
      talk: {
        challenge: {
          task: {
            label: "Bargain with the Galantha",
            startdescription:
              '"You are pleading for the Water? Fine, I can be convinced." She does not seem surprised, but her ancient eyes remain unreadable. "You can trade me the location of the Abzu, the Well of the Letheians. I know it must be somewhere below Syria, Judea or Egypt from where the Lethians drew their water." ',
          },
          success: {
            label: "A Painless Exchange",
            description:
              '"As simple as that." She retreats to the cave, and trepidatious as beaten hounds, we follow. Will she disappear beneath the earth as easily as she disappeared from the ship? But she quickly reemerges from the earth, carrying her gifts wrapped in white linen.',
            effects: { "mariner.noonwater": 1, [vb.vp("galantha")]: -1 },
            rootSet: {
              [QUEST_FLAG_ID("galantha", "abzu")]: 1,
            },
          },
          failure: {
            label: "A Painless Exchange",
            description:
              '"As simple as that." She retreats to the cave, and trepidatious as beaten hounds, we follow. Will she disappear beneath the earth as easily as she disappeared from the ship? But she quickly reemerges from the earth, carrying her gifts wrapped in white linen.',
            effects: { "mariner.noonwater": 1, [vb.vp("galantha")]: -1 },
            rootSet: {
              [QUEST_FLAG_ID("galantha", "abzu")]: 1,
            },
          },
          template: probabilityChallengeTemplates.COMMUNE,
        },
        slots: CHALLENGE_SLOTS(3, "talk"),
      },
      explore: {
        challenge: {
          task: {
            label: "Fight With the Galantha",
            startdescription: '"As you wish." The shadows sharpen, the wind runs cold.',
          },
          success: {
            label: "Fight with the Galantha",
            description:
              "While she fights with cunning and cold determined strength and at her gaze all things warm and vibrant wilt and falter, we still prevail. Like many buffers stopping an avalanche, we envelope her and stop her movements. Ragged Ones cannot be ended before their time, but Sereno, cruel in his wroth, shared with us a technique to rid ourselves off them for a while. We must overwhelm her before we can put it to the test, with our dogged Heart or indomitable Edge. [Use Heart or Edge to navigate this challenge]",
            effects: { "mariner.noonwater": 1, [vb.vp("galantha")]: -1 },
            linked: [{ id: vb.vp("succes_step1") }],
          },
          failure: {
            label: "Inevitable",
            description:
              'She sighs, as she steps barefooted between our crumbled forms. "They never learn, a millennium might go by… Well, time to see if the whole world has stayed as constant. Mourn your dead, I will depart for Portus Cale and explore the Mare Internum. Pray you don’t meet me unprepared again."',
            effects: { [vb.vp("galantha")]: -1 },
            rootSet: {
              [QUEST_FLAG_ID("galantha", "roaming")]: 1,
            },
          },
          template: probabilityChallengeTemplates.STRATEGIZE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("succes_step1"),
    label: "Fight with The Galantha",
    startdescription:
      "We have bundled the Galantha to our ship, and sent some among us to fetch the spare anchor from the hull. The person, the monster, our foe, redoubled their efforts to escape, and we must act decisively, and with grim determination, to keep a hold on her while she can be attached to the weight the chains are wrapped around her body. [Use Winter or Edge to navigate this challenge]",
    craftable: false,
    slots: CHALLENGE_SLOTS(3, "explore"),
    warmup: 30,
    linked: [{ id: vb.vp("succes_step2_winter") }, { id: vb.vp("succes_step2_edge") }, { id: vb.vp("failure_step2") }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("failure_step2"),
    label: "Slipped Away",
    startdescription: "The Galantha has slipped her bonds, and then slipped from our grasp completely.",
    craftable: false,
    rootSet: {
      [QUEST_FLAG_ID("galantha", "roaming")]: 1,
    },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("succes_step2_winter"),
    label: "Fight with the Galantha winter",
    requirements: { winter: 5 },
    craftable: false,
    linked: [{ id: vb.vp("succes_step2") }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("succes_step2_edge"),
    label: "Fight with the Galantha",
    requirements: { edge: 5 },
    craftable: false,
    linked: [{ id: vb.vp("succes_step2") }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("succes_step2"),
    label: "Chill Retreating",
    startdescription:
      "Properly entangled, balled up in fury on the deck, we quickly set off, back down the river, out to the open sea. Her fighting strength leaves her there, her tongue stills. She simply stares, eyes dark with anger.",
    craftable: false,
    effects: { "mariner.galantha.captured": 1 },
    warmup: 30,
  });
}
