import { vaultPrefix, ASPECTS_FILE, RECIPES_FILE } from "../galicia.mjs";
import { probabilityChallengeTemplates } from "../../../../probability_challenge_templates.mjs";
import { SPAWN_MISFORTUNE_CURSE } from "../../../../curses_generation/curses/misfortune.mjs";
import { CHALLENGE_SLOTS, SHIP_MANOEUVERING_SLOTS } from "../../../../slots_helpers.mjs";

export function generateRiver(vb) {
  mod.setAspect(ASPECTS_FILE, { id: `mariner.flag.galantha.prepared` });

  vb.defineTask({
    taskId: "river",
    label: "The Winding River",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "Some rivers thunder, other trickle. This lazy, contented flow through the galician lowlands does not pose much danger effort to traverse in itself. But it is a active thoroughfare, and we must navigate it up stream, and on the winds whims.",
    actions: {
      "mariner.sail": {
        challenge: {
          hint: {
            label: "Manage the Traffic",
            startdescription: "To travel along a river, a ship may not be required, but in our case, it is expected.",
            requirements: { "mariner.ship": -1 },
          },
          task: {
            label: "Manage the Traffic",
            startdescription:
              "As the pulsing vein through the region, the river is well traveled with both goods and people. The Kite must make her way through barges, fisherman vessels, leisure yachts and tiny rowboats. making our way through here withoug accident will require both our focus and our command.",
            requirements: { "mariner.ship": 1 },
          },
          success: {
            label: "Onwards and Upwards",
            description: "We traverse upward the river, and watch the lowland turn into highland, and hills.",
            linked: [{ id: vb.vp("galantha_pre") }],
          },
          failure: {
            label: "Collison",
            description:
              "An unwise turn, a misjudged angle, and now the kite is damaged and a local vessel is cursing me in three languages. We have to pay them a princely sum to get them to leave, and we have to decide if we press onwards with the damage kite, or limp back to a wharf.",
            linked: [{ id: vb.vp("galantha_pre") }],
            mutations: [
              {
                filter: "mariner.ship",
                mutate: "mariner.ship.damage",
                level: 1,
                additive: true,
              },
            ],
          },
          template: probabilityChallengeTemplates.OVERCOME,
        },
        slots: [
          ...SHIP_MANOEUVERING_SLOTS(),
          {
            id: "mariner.ship.kite",
            label: "Vessel",
            actionid: "maririner.sail",
            description: "I need my ship to traverse the river",
            required: { "mariner.ship.kite": 1 },
          },
          {
            id: "offering",
            actionid: "explore",
            label: "Offering",
            description:
              "I could lighten the load of the Kite [you can offer the river those things that fill the kites Hold or Cabins.",
            required: { "mariner.cabinuse": 1, "mariner.holduse": 1 },
          },
        ],
      },
      talk: {
        challenge: {
          task: {
            label: "Manage the Locals",
            startdescription:
              "Towns and homesteads are snuggled onto the rivers curves, suckling of it's wealth and enduring it's temperamental flooding. If an ancient Long has been active in this area, perhaps the locals will know about it, or where to find her.",
          },
          success: {
            label: '"La Muller de Escopia"',
            description:
              'On the banks of the river, the locals all admit to have seen a pale woman: "La Muller de Escopia", the woman of river foam. She always walls bare-feeted, trailing the rivers edge, but her accent is local, and she always pays in silver coins. opinions are divided wether those who recieved the coins are blessed or cursed now, but all agree that the foothills where she lives are best to be avoided.',
            effects: { [vb.vp("intel")]: 1 },
            furthermore: [
              {
                rootAdd: {
                  "mariner.flag.galantha.prepared": 1,
                },
              },
            ],
          },
          failure: {
            label: "Mistrust and Confusion",
            description:
              "Either we cannot make our intentions clear, or they refuse to speak on this topic. We spot several signs around us, warding against evil or the unknown, but no one wishes to explain their fear. We must continue our way wearily, no wiser then we were before.",
            effects: { notoriety: 1 },
          },
          template: probabilityChallengeTemplates.HONEYED_TONGUE,
        },
        slots: CHALLENGE_SLOTS(3, "talk"),
      },
    },
  });
  mod.getElement(vb.ELEMENTS_FILE, vb.vp("river")).aspects["modded_explore_allowed"] = 1;

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("galantha_pre"),
    label: "<Galantha Pre Router>",
    craftable: false,
    linked: [{ id: vb.vp("galantha_prepped") }, { id: vb.vp("galantha_unprepped") }],
    slots: [
      {
        id: "ahead",
        label: "What am I getting into?",
        description: "Am I aware of trouble ahead?",
        greedy: true,
        required: {
          [vb.vp("intel")]: 1,
        },
      },
    ],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("galantha_prepped"),
    requirements: { [vb.vp("intel")]: 1 },
    label: "Into her Domain",
    startdescription:
      "Some boundaries are not demarkated, but it is important to know when they are are crossed. entering the lair of a long could be considered such one. As soon as we cross that final bend, we calm our travel speed, and proceed carefully .There, where the hills resemble mountains, we see her, waiting on a rock, eyes on the river below.",
    craftable: false,
    warmup: 30,
    effects: { [vb.vp("galantha")]: 1 },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("galantha_unprepped"),
    label: "Midnight at Noon",
    startdescription:
      "while we travel through the iberian countryside, as suddenly the world is swallowed by darkness. wolf-snarl fear strikes our heart, but before we can orient ourself for another sightless fight, the shadow passes, and we stand blinking under the Iberian Sun. But now another stands in our mids, and the ship creaks ominously in the river.  ",
    craftable: false,
    warmup: 30,
    effects: { [vb.vp("galantha")]: 1 },
    mutations: [
      {
        filter: "mariner.ship",
        mutate: "mariner.ship.damage",
        level: 1,
        additive: true,
      },
    ],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("river_sink_hint"),
    requirements: { [vb.vp("river")]: 1 },
    actionid: "explore",
    label: "Feed the River",
    startdescription:
      "These days, the river is turgid and mud-rich, but its waters were once said to run cold and deep. The Well that fed it with Noonwater did not outlast the roman empire, but the chasms and channels are still there, under the river bed. Things offered to the river sometimes sink to sunless, foreign shores.",
    hintonly: true,
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("river_sink_hold"),
    requirements: { [vb.vp("river")]: 1, "mariner.holduse": 1 },
    actionid: "explore",
    label: "Feed the River",
    startdescription:
      "These days, the river is turgid and mud-rich, but its waters were once said to run cold and deep. The Well that fed it with Noonwater did not outlast the roman empire, but the chasms and channels are still there, under the river bed. Things offered to the river sometimes sink to sunless, foreign shores.",
    craftable: true,
    warmup: 30,
    effects: { "mariner.holduse": -1 },
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("river_sink_cabin"),
    requirements: { [vb.vp("river")]: 1, "mariner.cabinuse": 1 },
    actionid: "explore",
    label: "Feed the River",
    startdescription:
      "These days, the river is turgid and mud-rich, but its waters were once said to run cold and deep. The Well that fed it with Noonwater did not outlast the roman empire, but the chasms and channels are still there, under the river bed. Things offered to the river sometimes sink to sunless, foreign shores.",
    craftable: true,
    warmup: 30,
    linked: [vb.vp("river_sink_cabin_galantha"), vb.vp("river_sink_cabin_nogalantha")],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("river_sink_cabin_galantha"),
    requirements: { [vb.vp("river")]: 1, "mariner.galantha.captured": 1 },
    actionid: "explore",
    label: "Sink Galantha",
    startdescription:
      'Sereno has warned me that not much can stop a Ragged Long. "Curse, plead or pray, they will not go before there time. You can, however, mightily inconvenience them." One such way is to bind them and to sink them, and I entrust the Galantha to slip into the chasms below the river. Those eyes don’t leave me even when she is tossed overboard, and as she slips beneath the swaying river. Long after she has disappeared out of sight, we feel eyes peering at the ship from below.',
    craftable: false,
    warmup: 30,
    effects: { "mariner.galantha.captured": -1, "mariner.experiences.dreadful": 1 },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("river_sink_cabin_nogalantha"),
    actionid: "explore",
    label: "Into Oblivion",
    startdescription:
      "One knock, one push, one splash on one moonless night, and one soul is never darkens the decks of the Kite again. Dawn will bring a fallout.",
    craftable: false,
    warmup: 30,
    effects: { "mariner.cabinuse": -1, "mariner.experiences.dreadful": 1 },
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: "mariner.crew",
            id: "mariner.crew.exhausted",
            level: 1,
            additive: true,
          },
          {
            filter: "mariner.crew",
            id: "mariner.crew.longing",
            level: 1,
            additive: true,
          },
        ],
      },
    ],
  });
}
