import { generateGalantha } from "./tasks/galantha.mjs";
import { generateRiver } from "./tasks/river.mjs";
import { vaultPrefix, ELEMENTS_FILE, ASPECTS_FILE, RECIPES_FILE } from "./galicia.mjs";
import { trappingAspects } from "../../../generate_trappings.mjs";

// TASKS
export function generateLimiaLayer(vb) {
    vb.defineLayer("1");

    //items you take with you

    //help items
    vb.defineHelpElement({
        id: "intel",
        label: "<Intel on the Galantha>",
        unique: true,
        description:
            "<We know where she roamed, and where she has retreated too. We will not be surpsied when we encounter her>",
        aspects: { "op": 1 }
    });

    mod.setElement(ELEMENTS_FILE, {
        id: "mariner.galantha.captured",
        label: `Galantha-Shackled`,
        description: "We have the Galantha bound, but the chains won't hold forever.",
    })

    vb.defineExplorationTask({
        taskId: "limia",
        locationLabel: "A River in Galicia",
        locationDescription:
            "In the North of Portugal, a lethargic river monds into the temperamental Ocean. Sereno has indicated certainty that the Long we seek can be found up river, in the defunct homeland of the Ordo Limiae.",
        tasks: ["river"],
        events: [],
    });

    generateGalantha(vb);
    generateRiver(vb);
}