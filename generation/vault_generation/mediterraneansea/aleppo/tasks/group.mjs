import { LORE_ASPECTS, buildRefinementBlock } from "../../../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS } from "../../../../slots_helpers.mjs";

export function generateMazeGroup(vb) {
  vb.defineTask({
    taskId: "group",
    label: "Group of Students",
    icon: "tasks/mariner.tasks.concentrate.vertdegris",
    description:
      "Here in a corner, sitting on comfy benches and armchairs, a group of people is vividly discussing some kind of parable. As they converse, spots of light keep drifting accross their faces, scintillating leaves in the golden afternoon of a sunflooded wood.",
    actions: {
      talk: {
        hint: {
          label: "Enforce the Atmosphere?",
          startdescription:
            "The group is focused on debating some kind of parable. They could actually be talking in riddles on some esoteric matter, but I couldn't know. I can follow the logic pattern of the conversation though, and I can feel the strong tension filling the atmosphere around them. Maybe I could use that to my advantage.",
          requirements: { "mariner.song": -1 },
        },
        challenge: {
          task: {
            label: "Enforce the Atmosphere",
            startdescription: `The group is focused on debating some kind of parable. They could actually be talking in riddles on some esoteric matter, but I couldn't know. I can follow the logic pattern of the conversation though, and I can feel the strong tension filling the atmosphere around them. Maybe I could use that to my advantage.\n${buildRefinementBlock(
              {
                lantern:
                  '<i>"If I may, I think we should consider how this specific part of the story can act as a superb metaphor of the epiphany of the soul, flooding us with light…"</i>',
                forge:
                  '<i>"I could be wrong, but to me, this moral has to be related to the previously mentioned principle of perpetual renewal…"</i>',
                edge: '<i>"Pardon me if my contribution contradicts your line of thinking, but couldn\'t their relationship also describe how the concept of struggle, as a way in itself…"</i>',
                winter:
                  '<i>"Well, that is a striking thought. I think we should spend a minute absorbing this idea, as well as what the silence between sentences themselves brings..."</i>',
                heart: '<i>""</i>',
                grail: '<i>""</i>',
                moth: '<i>""</i>',
                knock: '<i>""</i>',
              }
            )}`,
          },
          success: {
            label: "A Fruitful Discussion",
            description:
              "My contributions to the conversation seem to have struck them. To each of them, they reply with even more counterarguments, feeding into the debated story - that I cleverly redirected towards the aspects of it I wanted to bring to light the most. I can feel its influence rising all around us, gently guiding my feet.",
            linked: LORE_ASPECTS.map((aspect) => ({
              id: vb.vp(`enforceinfluence.${aspect}`),
              requirements: { [`mariner.song.${aspect}`]: 1 },
              effects: {
                [`mariner.influences.${aspect}`]: `[mariner.song.${aspect}]`,
              },
            })),
          },
          failure: {
            label: "Deceit and Confusion",
            description:
              "Did I misinterpret the sense of the conversation? Did I confuse them? Did they notice I wasn't following them on the same level? They didn't move, but something shifted in the air. I am no longer welcome to participate. Time to move on, it seems.",
            effects: { [vb.vp("group")]: -1 },
          },
          template: probabilityChallengeTemplates.SILVER_TONGUE,
        },
        slots: [
          ...CHALLENGE_SLOTS(3, "talk"),
          {
            actionId: "talk",
            id: "song",
            label: "Direction",
            description: "Which direction do I want to push the conversation toward?",
            required: { "mariner.song": 1 },
          },
        ],
      },
    },
  });
}
