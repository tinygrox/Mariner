import { TEAM_SIZE, negativeValuesObject } from "../../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../probability_challenge_templates.mjs";

export function generateStreetLayer(vb) {
  vb.defineLayer("street");

  const crewSlot = (i) => ({
    id: `crew_${i}`,
    actionId: "talk",
    label: "Selected To Infiltrate",
    description: "This one is participating in infiltration.",
    required: { "mariner.crew": 1 },
    forbidden: { "mariner.crew.traits.individualistic": 1 },
  });

  vb.defineTask({
    taskId: "corner",
    label: "Adjoining Lane",
    description:
      "Here, I will pick who comes to infiltrate the location with me, and who doesn't. The larger the crew, the more hands to search the site, but the harder it will be to blend in. They will surely figure us out far quicker if an entire crew barges in, no matter the excuse.",
    slots: [crewSlot(1), crewSlot(2), crewSlot(3), crewSlot(4), crewSlot(5), crewSlot(6), crewSlot(7), crewSlot(8)],
    actions: {
      talk: {
        recipe: [
          {
            id: "pickcrew",
            warmup: 10,
            label: "Picking Who Comes",
            startdescription: 'These people will come with me.<br><br><i>"If you\'re not picked, wait for us here."</i>',
            description: "I have made my choice, and these are the people that will follow me into the morgue.",
            furthermore: [
              {
                target: "~/exterior",
                mutations: [
                  {
                    filter: `[mariner.crew] && [${vb.vp("selected")}]`,
                    mutate: vb.vp("selected"),
                    level: 0,
                    additive: false,
                  },
                ],
              },
              {
                mutations: [
                  {
                    filter: `[mariner.crew]`,
                    mutate: vb.vp("selected"),
                    level: 1,
                    additive: false,
                  },
                ],
              },
              {
                effects: {
                  ...negativeValuesObject({ [vb.currentActiveLayerAspectId()]: 1 }),
                },
              },
            ],
          },
        ],
      },
    },
  });

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("holdnonselected"),
    actionId: "mariner.haltable.holdnonselected",
    label: "Waiting For Us",
    startdescription: "These are the people I didn't pick for this job. They'll join us again as soon as we depart.",
    description: '</i>"Let\'s leave before they start to really search the area and find us."</i>',
    warmup: 60,
    linked: [{ id: "mariner.vaults.bizerta_morgue.holdnonselected" }],
  });

  const HOLD_NON_SELECTED = {
    inductions: [
      {
        id: "mariner.vaults.bizerta_morgue.holdnonselected",
        expulsion: {
          filter: {
            "mariner.crew": 1,
            [vb.vp("selected")]: -1,
          },
        },
      },
    ],
  };

  const DELETE_SELECTION_MARKS = {
    target: "~/visible",
    mutations: [
      {
        filter: `[mariner.crew] && [${vb.vp("selected")}]`,
        mutate: vb.vp("selected"),
        level: 0,
        additive: false,
      },
    ],
  };

  vb.defineTask({
    taskId: "doorman",
    label: "Morgue Security Doorman",
    description:
      "Standing between us and the building is a sleepy guard. Mid-height, bored to death, and visibly not moving any time soon. Some show of authority could probably make him let us in, at least for a while, until he realises his mistake. Alternatively, we could just dispose of him in a more brutal manner.",
    actions: {
      talk: {
        challenge: {
          task: {
            label: "Improvise An Imperious Need",
            startdescription:
              "Stern faces, imposing presence, mellow voice, apparent grief; any method will do. He needs to understand: we absolutely must get in.",
          },
          success: {
            label: "We're In",
            description:
              "The Doorman didn't make much of a fuss. If anything, he was probably happy to finally be of help to someone. We're in. Time to explore this place, as discreetly as we can, before they realise that something is up.",
            furthermore: [
              { effects: { [vb.vp("doorman")]: -1, [vb.vp("corridors")]: 1, [vb.vp("threatofdiscovery")]: 1 } },
              { aspects: { [vb.vp("reducetime")]: TEAM_SIZE(vb.vp("selected"), 10) } },
            ],
            inductions: [HOLD_NON_SELECTED, DELETE_SELECTION_MARKS],
            purge: { [vb.currentLayerAspectId()]: 5 },
          },
          failure: {
            label: "Wavering Conviction",
            description:
              "We bothered him enough that he let us in, but deep down, he can feel that something is not right. It might take a short while, but he will surely end up calling someone for confirmation, and then, we'll be unmasked.",
            furthermore: [
              { effects: { [vb.vp("doorman")]: -1, [vb.vp("corridors")]: 1, [vb.vp("threatofdiscovery")]: 1 } },
              { aspects: { [vb.vp("reducetime")]: `[${vb.vp("selected")}] + 2` } },
            ],
            inductions: [HOLD_NON_SELECTED, DELETE_SELECTION_MARKS],
            effects: { [vb.vp("doorman")]: -1 },
            purge: { [vb.currentLayerAspectId()]: 5 },
          },
          template: probabilityChallengeTemplates.FORKED_TONGUE,
        },
      },
      explore: {
        challenge: {
          task: {
            label: "Force Our Way In",
            startdescription:
              "Violence is so often an option. This one man could easily be disposed off and moved to a more discreet location close by. This would then allow us to barge into the place with our full crew, but this will arise suspicion, and someone's bound to find him pretty soon.",
          },
          success: {
            label: "Efficient Disposal",
            description:
              "We apply a short and efficient burst of violence, and an instant later, the guard is laying down in an alleyway, a few paces away. It looks like no one inside heard it happen.",
            furthermore: [
              { effects: { [vb.vp("doorman")]: -1, [vb.vp("corridors")]: 1, [vb.vp("threatofdiscovery")]: 1 } },
              { aspects: { [vb.vp("reducetime")]: 6 } },
              DELETE_SELECTION_MARKS,
            ],
            purge: { [vb.currentLayerAspectId()]: 5 },
          },
          failure: {
            label: "Confused Struggle",
            description:
              "We finally managed to shut him up, but the sounds of the struggle probably raised a few eyebrows inside. Once we're in, we will need to search the place as quickly as we can, for questions will arise and our cover will be blown in a matter of minutes.",
            furthermore: [
              { effects: { [vb.vp("doorman")]: -1, [vb.vp("corridors")]: 1, [vb.vp("threatofdiscovery")]: 1 } },
              { aspects: { [vb.vp("reducetime")]: 8 } },
              DELETE_SELECTION_MARKS,
            ],
            purge: { [vb.currentLayerAspectId()]: 5 },
          },
          template: probabilityChallengeTemplates.OVERCOME,
        },
      },
    },
  });
}
