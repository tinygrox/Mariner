import { SIGNALS } from "../../../generate_signal_aspects.mjs";
import { negativeValuesObject } from "../../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../probability_challenge_templates.mjs";

export function generateMorgueLayer(vb) {
  vb.defineLayer("morgue");

  vb.defineNonLocalElement({
    id: "body",
    label: "Student Exsanguinate: Body",
    description: "<>",
  });

  vb.defineNonLocalElement({
    id: "casefile",
    label: "Student Exsanguinate: Case File",
  });

  vb.defineHelpElement({
    id: "authoritytoken",
    label: "<Authority Token>",
  });

  vb.defineExplorationTask({
    taskId: "corridors",
    locationLabel: "The Morgue",
    locationDescription:
      "What looked like a small building on the outside reveals itself as a confusing intrication of small offices, corridors and closets. We will have to blend in, act like we're expected somewhere here, and search each room until we find what we came for.",
    label: "Infiltrating The Morgue",
    description:
      "Under the unassuming eyes of a few assistants, clerks and lawmen, we pace around and carefully map the building out.",
    exhaustedLabel: "Mapped Out",
    exhaustedDescription: "Such a small place can only contain so many office desks and cabinets to find.",
    tasks: ["desk1", "desk2", "desk3", "desk4", "desk5", "morticianassistant"],
  });

  mod.setVerb(`verbs.vaults.${vb.vaultId}`, { id: "mariner.haltable.searchdesk", multiple: true });

  const deskTask = (id, taskLabel, taskDescription, label, startdescription, description, reward) => {
    mod.setRecipe(vb.RECIPES_FILE, {
      id: vb.vp(`searchdesk.${id}`),
      actionId: "mariner.searchdesk",
      warmup: 100,
      label: "Rummaging",
      startdescription: "My men are rummaging through the office space. This will take some time.",
      description: description,
      ...(reward ? { furthermore: { effects: { [reward]: 1 } } } : {}),
      effects: { [vb.vp(`desk${id}`)]: -1 },
    });

    const LAUNCH_SEARCH = {
      id: vb.vp(`searchdesk.${id}`),
      expulsion: {
        filter: {
          tool: 1,
          "mariner.song": 1,
          "mariner.crew": 1,
          "mariner.inspiration.nature": 1,
          "mariner.lackheart": 1,
          "mariner.halfheart": 1,
          [vb.vp(`desk${id}`)]: 1,
          [vb.currentActiveLayerAspectId()]: 1,
        },
      },
    };

    vb.defineTask({
      taskId: `desk${id}`,
      label: taskLabel,
      description: taskDescription,
      actions: {
        explore: {
          hint: {
            label: "Search the Desk?",
            startdescription: "I need to commit at least one person to this task.",
            requirements: { "mariner.crew": -1 },
          },
          challenge: {
            task: {
              requirements: { "mariner.crew": 1 },
              label: label,
              startdescription: startdescription,
            },
            success: {
              label: label,
              description: description,
              inductions: [LAUNCH_SEARCH],
            },
            failure: {
              label: label,
              description: description,
              furthermore: [{ target: "~/exterior", aspects: { [vb.vp("reducetime")]: 1 } }],
              inductions: [LAUNCH_SEARCH],
            },
            template: probabilityChallengeTemplates.OPERATE,
          },
        },
      },
    });
  };

  deskTask(
    "1",
    "Director's Office",
    "Pristine desk, a few notes and files, lots of marine paintings on the wall. The room feels very impersonal, almost unused, despite little plaques on the desk, indicating the director has been managing this morgue for two decades.",
    "Searching the Director's Office",
    "The dusty desk indicates the director hasn't done actual work here in a long time. We sneak into the room and start searching for any useful bit of information.",
    "As we search through the desk, a single file, hidden under a stack of administrative papers held by a heavily decorated paperweight, catches our attention. It is a blank page, with a few lines typewritten on it: <i>'Shut this case as quickly as possible. Hand it out to your mortician immediately, this is an immediate priority. The population is growing restless, put this one to rest as swiftly as you can.'</i> Could it be discussing our case?" // "shut this one down real quick. Details don't matter"
  );
  deskTask(
    "2",
    "Mortician's Office",
    "Freshly painted white walls, devoid of any decoration. And in the air, a discrete fragrance of pinecone. Heavy curtains add to the feeling of entering a solemn shrine.",
    "Searching the Mortician's Office",
    "We enter the room in a religious silence, wary of disturbing <i>anything</i>, and begin to search without muttering a single word.",
    "In one of the drawers, we find a few objects of a strange nature. As we look at them, the nature of the room starts making sense: its owner clearly is a practitioner of the night arts. Next to these, a personal journal seems to mention our case: <i>'I know I've been asked to close this one quickly, but I just cannot waste more time chasing every single weird case. Dr. Vasseur can surely handle this one for me.'</i>"
  ); // "a note is there from one of the pathologists, saying I know you've been asked to close this one quickly, but I just need some more time to finish this one"
  deskTask(
    "3",
    "Clerk's Office",
    "What looks like half the morgue's archive occupies the entire right wall, transforming the already cramped anonymous room into little more than a long closet. Facing it, an old metal desk, extended by cheap plywood side tables. And everywhere, boxes, folders and stamps, contributing to the claustrophobic feeling of the place.",
    "Rummaging Through the Clerk's Office",
    "The clerk isn't here. On the desk, a handmade sign, written in a childish cursive way, reads 'Sofia'. Next to it, a wooden toy train, used as an improvised paperweight, pins a short note: 'if you don't find me here, I am probably in the second archive room'. We skim through the papers, stack by stack.",
    "What looked like a dusty office reveals itself to be the beating heart of this building. Evidently, most of the work the director 'accomplishes' is done by this young clerk. Date stamps and pre-signed blank pages: it only takes a few minutes to produce a decently composed authority document. With that token of authority, we are assured to access any part of the building without a second glance.",
    vb.vp("authoritytoken")
  ); // "a collection of stamps the clerk uses to make most of the word of the director in his place because he's often absent. Easy to forge a letter with these"
  deskTask(
    "4",
    "Pathologist' Office",
    'The name on the door is "Dr. Vasseur". Inside, a tidy desk takes most of the space. On the wall, reproductions of expensive paintings dress the space. Golden pens, pairs of fancy shoes next to the door, fake classical paintings: the den of a true narcissist.',
    "",
    "One would have a hard time guessing this room is part of a morgue. We search through the few papers and drawers proving this place is actually an office, as quickly as we can.",
    "As anyone could have predicted, the room is almost devoid of any actual case file. In the bin, we find a crumpled paper, with a few inscriptions on it. One person wrote: <i>'Take care of this for me, will you? I have important business to attend to.'</i> Underneath it, an angry reply says: <i>'Screw you Jérémie, I already have tons of cases you've already discharged on my hands! This is the last case I will take care of for you... Try to remember we're supposed to do the same job for once!'</i>" // "I don't have time to take care of this weird case, you'll just handle it. Make it quick, the director wants it done already."
  );
  deskTask(
    "5",
    "Pathologist' Office",
    'The name on the door is "Dr. Mejri". The place looks like a cramped mess of files, paper stacks, notes. A single desk, barely visible underneath the papers, faces a whiteboard covered in sketches.',
    "Searching the Pathologist' Office",
    "Moving through the stacks of cases, one can feel how each folder is in its right place, following a somewhat obscure - and partially irrational plan. We check each folder, one by one, trying to remember its original place, dismantling and rebuilding this fragile construction, bit by bit.",
    "On top of the main stack, on the desk, we find our treasure: the autopsy document of this dreadful case. Written and then scribbled over several times, it looks like even the pathologist still isn't sure of what happened to this person.",
    vb.vp("casefile")
  );

  vb.defineTask({
    taskId: `morticianassistant`,
    label: "Mortician's Assistant",
    description: "<>",
    actions: {
      talk: {
        hint: {
          grandReqs: {
            [`![${vb.vp("casefile")}] || ![${vb.vp("authoritytoken")}]`]: 1,
          },
        },
        challenge: {
          task: {
            label: "<Gain Access to the Body>",
            startdescription: "<>",
            requirements: { [vb.vp("casefile")]: 1, [vb.vp("authoritytoken")]: 1 },
          },
          success: {
            label: "<Got Access>",
            description: "<>",
            effects: { [vb.vp("freezer")]: 1, [vb.vp("morticianassistant")]: -1 },
          },
          failure: {
            label: "<>",
            description: "<>",
            furthermore: [{ target: "~/exterior", aspects: { [vb.vp("reducetime")]: 1 } }],
          },
          template: probabilityChallengeTemplates.SILVER_TONGUE,
        },
      },
    },
  });

  vb.defineTask({
    taskId: `freezer`,
    label: "<Freezer and Body on Slate>",
    description: "<>",
    actions: {
      explore: {
        recipe: [
          {
            id: "getbody",
            actionId: "explore", // TODO: auto inject that
            warmup: 10,
            label: "<Bag the Body>",
            startdescription: "<>",
            requirements: {
              [vb.vp("body")]: 1,
            },
            furthermore: [
              {
                target: "~/exterior",
                mutations: [
                  {
                    filter: "mariner.locations.medsea.bizerta.uqg",
                    mutate: "mariner.reputation",
                    level: 3,
                    additive: false,
                  },
                  {
                    filter: "mariner.locations.medsea.marseille.uqg",
                    mutate: "mariner.reputation",
                    level: 3,
                    additive: false,
                  },
                ],
              },
              {
                target: "~/exterior",
                aspects: { [SIGNALS.UPDATE_DISPLAYED_REPUTATION]: 1 },
              },
              {
                effects: {
                  [vb.vp("body")]: 1,
                  [vb.vp("freezer")]: 1,
                  ...negativeValuesObject({ [vb.currentActiveLayerAspectId()]: 1 }),
                },
              },
            ],
          },
        ],
      },
    },
  });
}
