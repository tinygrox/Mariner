import { CREW_TYPES } from "../../../crew_generation/generate_crewmen.mjs";
import { vaultPrefix, ASPECTS_FILE, RECIPES_FILE, ELEMENTS_FILE } from "./damascus.mjs";

export function generateKickOut(vb) {
  mod.setHiddenAspect(ASPECTS_FILE, {
    id: vb.vp("removecrewmember"),
  });
  mod.setHiddenAspect(ASPECTS_FILE, {
    id: vb.vp("freecrewmember"),
  });
  mod.setAspect(ASPECTS_FILE, {
    id: vb.vp("removed"),
    label: "Kicked Out",
    description: "They have been removed of the premises.",
  });
  // Basic
  const RemovedId = vb.vp("removed_basic");
  const freeId = `mariner.crew.basic`;
  mod.setElement(ELEMENTS_FILE, {
    id: vb.vp("removed_basic"),
    icon: "generic_p",
    label: "Removed Crewmember",
    description: "They will wait for me outside, but their role in this caper is over.",
    aspects: {
      [vb.vp("removed")]: 1,
      "mariner.local": 1,
    },
    xtriggers: {
      [vb.vp("freecrewmember")]: `mariner.crew.basic`,
    },
  });
  mod.addToXtriggers("crewmen", freeId, {
    [vb.vp("removecrewmember")]: RemovedId,
  });

  // Specialists
  for (const crewmemberType of CREW_TYPES) {
    const RemovedId = vb.vp(`removed.${crewmemberType}`);
    const freeId = `mariner.crew.specialist.${crewmemberType}`;
    mod.setElement(ELEMENTS_FILE, {
      id: RemovedId,
      icon: "generic_p",
      label: "removed Crewmember",
      description:
        "Someone spotted us. This one wasn't as lucky as the others.",
      aspects: {
        [vb.vp("removed")]: 1,
        "mariner.local": 1,
      },
      xtriggers: {
        [vb.vp("freecrewmember")]: freeId,
      },
    });
    mod.addToXtriggers("crewmen", freeId, {
      [vb.vp("removecrewmember")]:
        RemovedId,
    });
  }
}