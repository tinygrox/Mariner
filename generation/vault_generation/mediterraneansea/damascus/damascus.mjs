import { VaultBuilder } from "../../vaultbuilder.mjs";
import { generateMuseumLayer } from "./museum.mjs";
import { generateMuseumEvents } from "./events.mjs";
import { generateCollectionLayer } from "./collection/collection.mjs";

export const ELEMENTS_FILE = "vaults.damascus";
export const RECIPES_FILE = "recipes.vaults.damascus";
export const ASPECTS_FILE = "aspects.vaults.damascus";

export const vaultPrefix = (end) => `mariner.vaults.damascus.${end}`;

export function generateDamascusVault() {
  const vb = new VaultBuilder("damascus", "medsea");
  generateMuseumLayer(vb);
  generateMuseumEvents(vb);
  vb.computeCustomTideIfRequired();
  generateCollectionLayer(vb);
}
