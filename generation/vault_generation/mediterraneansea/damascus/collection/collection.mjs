import { generateDesk } from "./tasks/desk.mjs";
import { generateLastBed } from "./tasks/lastbed.mjs";
import { generatePinned } from "./tasks/pinned.mjs";
import { generateWeaponRack } from "./tasks/weaponrack.mjs";
import { trappingAspects } from "../../../../generate_trappings.mjs"

// TASKS
export function generateCollectionLayer(vb) {
    vb.defineLayer("2");

    //items you take with you

    mod.setElement(vb.ELEMENTS_FILE, {
        id: "mariner.tools.shears",
        label: "Alakapurine Shears, Pre-Used",
        description:
            "<Used to shed what is no longer of use.>",
        aspects: trappingAspects({
            value: 5,
            types: ["natural"],
            aspects: { moth: 4 },
        }),
    });

    mod.setElement(vb.ELEMENTS_FILE, {
        id: "mariner.trappings.implements",
        label: "<Implements of Mattias>",
        description:
            "<More reviled then the arts of ending are the arts of agony. these tools are proud as wolf fangs>",
        aspects: trappingAspects({
            value: 5,
            types: ["crafts", "artifact"],
            aspects: { edge: 3, winter: 3 },
        }),
    });

    mod.setElement(vb.ELEMENTS_FILE, {
        id: "mariner.diaphenous.sheets",
        label: "Diaphenous Sheet",
        description:
            "<nearly all of the scales have flakes off,, and what is left a delicate sheet, sheer as a whisper, real as remorse>",
        aspects: trappingAspects({
            value: 5,
            types: ["natural"],
            aspects: { moth: 3, winter: 2 },
        }),
    });

    mod.setElement(vb.ELEMENTS_FILE, {
        id: "mariner.vaults.damascus.diaphanous.sheets",
        label: "<>",
        description: "<>",
        aspects: trappingAspects({
            value: 5,
            types: ["natural"],
            aspects: { moth: 3, knock: 2 },
        }),
    });

    vb.defineHelpElement({
        id: "echoes",
        icon: "mariner.vaults.rescuemission.distance",
        label: "s",
        description:
            "",
        aspects: { moth: 2, winter: 2 },
    });

    vb.defineExplorationTask({
        taskId: "collection",
        locationLabel: "<Pinned Collection>",
        locationDescription:
            "In a hidden place, carved between the gaps if the bricks, Matthias has carved himself a space to keep his trophies and his tools. With neither doors nor windows, it is dark as a tomb, but unseen, papery things rustle at our movements.",
        tasks: ["desk", "lastbed", "pinned", "weaponrack"],
        events: [],
    });

    generateDesk(vb)
    generateLastBed(vb)
    generatePinned(vb)
    generateWeaponRack(vb)
}