import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS } from "../../../../../slots_helpers.mjs";

export function generatePinned(vb) {
  vb.defineTask({
    taskId: "pinned",
    label: "The Pinned Collection",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "Along the walls, gossamer wings hang, impaled by eroded copper nails.A few are still pulled taught, but most hang like shredded rags. When we move they catch air currents in desperate dances, as if still attempting to tear free.",
    actions: {
      explore: {
        challenge: {
          task: {
            label: "Picking at Insect Wings",
            startdescription:
              "These remnants of Mattias' quarries are fragile, not meant to be maintained in the Wake. Upon disruption, they might dissolve, but with carefull handling and the proper tools, we could could fold one of the wings unto our care.",
          },
          success: {
            label: "Trophy from a Trophy Hoard",
            description:
              "I walk through the gallery of limpid bandages, each rising at my breath. With, care I select a pair that seems well enough to travel. They vibrate under my exhale, still pinned at each tip. I control my breath,, and watch each scale catch the light like flashing knives. I bend, and fold, and fold again. What remains is small enough to fit in a pack.",
            effects: { "mariner.diaphenous.sheets": 1, [vb.vp("pinned")]: -1 }, //diaphenous sheet does not show up
          },
          failure: {
            label: "Fragile as Broken Trust",
            description:
              "We cannot find the right vibration, and touch tears through the sheets like parting morning mists. With each attempts our hands shake more, and in the end nothing is left but the last dispersing vapors, but those severed wings beat still within my skull.",
            effects: {
              "mariner.experiences.vivid.standby": 1,
              [vb.vp("pinned")]: -1,
            },
          },
          template: probabilityChallengeTemplates.OPERATE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });
}
