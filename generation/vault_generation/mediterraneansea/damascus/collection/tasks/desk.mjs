import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";
import { SPAWN_MISFORTUNE_CURSE } from "../../../../../curses_generation/curses/misfortune.mjs";
import { CHALLENGE_SLOTS } from "../../../../../slots_helpers.mjs";

export function generateDesk(vb) {
  vb.defineTask({
    taskId: "desk",
    label: "Still Desk",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "Mattias, while a hunter, killer and scoundrel, was still a Letheian, who take their scholarly pursuits serious by nature. Here Matthias must have read, paced, sketched, risen in elation, collapsed in despair.",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Puzzle At Ages Past",
            startdescription:
              "In a room without moisture or airflow, impermeable to bug and beast, paper, vellum and papyrus are maintained with greater stability then in many an archive, they are however, neither sorted nor labeled, but left as if the owner might anytime return. I must now riffle through the notes, pages, books and identify what is of interest... unfortunately they are written in a scala of languages, all of which dead and none of which I have mastered.",
          },
          success: {
            label: "A Pattern in Madness",
            description:
              "I study the papers... not what just the symbols, but the quality of the paper, how they are spread on the desks, the inks used. I determine what are notes and drafts, and what is, it seems, a nearly completed work, carefully kept. I gather up all the a",
            effects: { "mariner.matthiasamethystwitnessaramaic": 1 },
          },
          failure: {
            label: "Confounded and Confined",
            description:
              "At long last, I realize that there are too many layers of obfuscation between me and what i seek. The mess of the desk, the languages, the time, and the fine layer of dust wafting in my face. A sense of hopeless overcomes me in this lightless, airless space. Why am I trespassing in a place i cannot understand?.",
          },
          template: probabilityChallengeTemplates.STUDY,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
      explore: {
        challenge: {
          task: {
            label: "Rummage Through the Scraps of History",
            startdescription:
              "Between shriveled parchment and writing implements, lie other miscellanea and eclectica. But Mattias was a hunter and a trapper, and we cannot be sure if some wards or snares are not still placed on this desk. I will act as if I am sure there are many, and find out what is save to take.",
          },
          success: {
            label: "Pluck a Paper-Weight",
            description:
              "With understanding, feeling and a not not to be neglected amount of luck, I evaluate which objects to take and hwich to leave. I end up with a smattering of objects that museums would pay a ransom for, will be fuel to the flame of my occult pursuits.",
            linked: [{ id: "mariner.drawreward.ancient" }],
          },
          failure: {
            label: "Ancient Hex",
            description:
              "I gather all that I can, but soon I will discover I was a fool. What I thought to be an innocuous objects, carried with it an ancient curse. Is this Mattias work? Or Perhaps a Moth Name attempting to counteract him. I will never know, but an ancient malaise is clings to my shadow, and it will bloom once we are back in sunlight once again",
            alt: [SPAWN_MISFORTUNE_CURSE],
            linked: [{ id: "mariner.drawreward.ancient" }],
          },
          template: probabilityChallengeTemplates.STUDY,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });
}
