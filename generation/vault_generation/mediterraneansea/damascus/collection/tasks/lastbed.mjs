import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS } from "../../../../../slots_helpers.mjs";

export function generateLastBed(vb) {
  vb.defineTask({
    taskId: "lastbed",
    label: "The Last Bed",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "When their involvement had gotten Mattias excommunicated from the House of Lethe, and got the Imago banished from the Woods, both collected what they needed before their final Voyage. Perhaps this is where Matthias spend his final moments in the Flesh.",
    actions: {
      "mariner.sing": {
        challenge: {
          task: {
            label: "What Lingers?",
            startdescription:
              "This is spot is the most sacrosanct of Mattias inner sanctum. Few places are as personal as the bed you feel safe in, and few things are as intense as what Mattias accomplished here. Echoes of that act remain... We may wish to see if we could harmonize, to see what we can learn about the pair.",
          },
          success: {
            label: "Pursuit, Loss, Transformation",
            description:
              "These Echoes are of carnal things. The hunter, the prey, the chase, the turnabout. Hatred, longing, obsession, desire, connection. The chase was over when the quarry no longer fled, and the hunter no longer tracked. Parlay became dalliance became passion. And then a final flight, with wings clipped and flesh shed. Together now, to Higher and Brighter places.",
            effects: { [vb.vp("echoes")]: 1 },
          },
          failure: {
            label: "Taken To Far",
            description:
              "I can align my tempo to the echoes, but they tier me from my time and place. This room is a hot coffin, with outside my former brethren, congregating on my banishment. The only way out is inwards, but her betrayed siblings and cousins lie in wait below those lightless boughs. I feel the the cold steel, and I feel the hot cut. only once i hear the snapping of scissors echoing of tree boughs, do I manage to free myself of Mattias. ",
            effects: { [vb.vp("echoes")]: 1, "mariner.experiences.unraveling.standby": 1 },
          },
          template: probabilityChallengeTemplates.COMMUNE,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.sing"),
      },
      explore: {
        challenge: {
          task: {
            label: "What Remains?",
            startdescription:
              "if I search the bedding of Mattias, I may find out something he left behind. Few things can be taken into the Mansus after all, and fewer things can be taken into death.",
          },
          success: {
            label: "The Last of Mattias",
            description:
              "between the reeds, we find the remnants of Mattias and Imago final act in the wake. Locks of shorn hair, crumpled membranes cut at the root, and one and half ounces of shriveled flesh. Next to them, the implement of the sacrifice.",
            effects: { "mariner.tools.shears": 1 },
          },
          failure: {
            label: "The Last Caress",
            description:
              "I rummage behind the pillows, dig my fingers between the reeds, until a sharp pain causes me to recoil. I have found what I was looking for, but its blade ever sharp, took a knick out of my fingers. It is not much of an injury, but my finger now aches for seperation. What may be lost....",
            effects: { "mariner.tools.shears": 1, "mariner.experiences.pain.standby": 1 },
          },
          template: probabilityChallengeTemplates.PERCEIVE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });
}
