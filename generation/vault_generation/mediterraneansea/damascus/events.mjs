import { INJURE_CREWMEMBER, LOCAL_REPUTATION, requireAndDestroy } from "../../../generation_helpers.mjs";
import { generateTresholdChallengeTask } from "../../vault_generation.mjs";
import { vaultPrefix, ASPECTS_FILE, RECIPES_FILE } from "./damascus.mjs";
export function generateMuseumEvents(vb) {
  mod.setRecipe(RECIPES_FILE, {
    id: vaultPrefix("events.guards"),
    craftable: false,
    warmup: 1,
    linked: [{ id: vaultPrefix("events.eyes") }, { id: vaultPrefix("events.sweep") }, { id: vaultPrefix("events.emptying") }],
  });

  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.xxxx",
    task: {
      id: vaultPrefix("events.eyes"),
      grandReqs: { [LOCAL_REPUTATION]: -2 },
      craftable: false,
      label: "<Watchful Eyes>",
      startdescription:
        "<The guards of this tomb of treasures have materialized in the corners of the room, wielding an air of authority and eyes of Argus. you can approach this event with Moth and Winter>",
    },
    success: [
      { recipe: { requirements: { moth: 4 } }, suffix: "moth" },
      { recipe: { requirements: { winter: 4 } }, suffix: "winter" },
    ],
    successEnd: {
      label: "<Glazed by Boredom>",
      description:
        "<When the crowds move like a flock of sheep or flight of starlings, it’s easy to lose yourself in the pattern, instead of seeing the individuals. The focus of my watchers has been broken, and they move on to the rest of their route.>",
    },
    failure: {
      label: "<Identified>",
      description:
        "<Security materializes behind one of my men as a shadow, and politely, but insistently, direct them towards the exit.>",
      effects: { "mariner.notoriety": 1 },
      aspects: { [vb.vp("removecrewmember")]: 1 },
      slots: [
        {
          id: "victim",
          label: "Crewmember",
          description: "One of our own.",
          greedy: true,
          required: { "mariner.crew": 1 },
        },
      ],
    },
  });

  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.xxxx",
    task: {
      id: vaultPrefix("events.throngs"),
      craftable: false,
      label: "<Throngs of Guests>",
      startdescription:
        "<The Masses enter, and with them their bodies, noises and demands. This tide poses an possibility of distraction for those on my staff who are concentrating, or specialists I have attracted to my cause. I can aid in this with Knife-Bright Lantern or Candle Sharp Edge.>",
    },
    success: [
      { recipe: { requirements: { lantern: 4 } }, suffix: "lantern" },
      { recipe: { requirements: { edge: 4 } }, suffix: "edge" },
    ],
    successEnd: {
      label: "<Hold the Line>",
      description:
        "<I intervene, intercept, interject, and keep make sure the focus of those around me is not broken for am moment.>",
    },
    failure: {
      label: "<Cracks>",
      description:
        "<With bumps elbows and disharmoneous chatter. with stepping on foots and asking for attention, attention slips away where we need it to stay taught.>",
      effects: { "mariner.notoriety": 1 },
      slots: [
        {
          id: "curator",
          label: "Curator",
          description: "their attention is being frayed.",
          greedy: true,
          required: vb.vp({ curator: 1 }),
        },
      ],
      aspects: { "mariner.flubbed": 1 },
    },
  });

  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.xxxx",
    task: {
      id: vaultPrefix("events.sweep"),
      grandReqs: { [LOCAL_REPUTATION]: -4 },
      craftable: false,
      label: "<Swooping in>",
      startdescription:
        "<More guards have been drawn here, like gulls to garbage. They stride through the crowds, two by two, conspicuous but polite. They look each person in the eyes… It will take a lot of Moths' evasiveness to avoid them now, or a stern stoicism like Winter.>",
    },
    success: [
      { recipe: { requirements: { moth: 5 } }, suffix: "moth" },
      { recipe: { requirements: { winter: 5 } }, suffix: "winter" },
    ],
    successEnd: {
      label: "<Passed Over>",
      description:
        "<Me and my crew are still as statues, inconspicuous as dust. We are passed by like furniture, as the guards filter out of the room for now.  But they may come back unless they apprehend someone to give the blame to.>",
    },
    failure: {
      label: "<Swept Away>",
      description:
        "<Some of my crew have been irregular or undesirable, the same in the eyes of many authorities, and are taken out of the buildings. Since a museum is closer to a vault than a prison, they will not have capacity to detain them, but access can be denied. My crew will wait for me outside.>",
      effects: { "mariner.notoriety": 1 },
      aspects: { [vb.vp("removecrewmember")]: 2 },
      slots: [
        {
          id: "victim",
          label: "Crewmember",
          description: "One of our own.",
          greedy: true,
          required: { "mariner.crew": 1 },
        },
        {
          id: "victim2",
          label: "Crewmember",
          description: "One of our own.",
          greedy: true,
          required: { "mariner.crew": 1 },
        },
      ],
    },
  });

  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.xxxx",
    task: {
      id: vaultPrefix("events.emptying"),
      grandReqs: { [LOCAL_REPUTATION]: 4 },
      craftable: false,
      label: "<Emptying the Museum>",
      startdescription:
        "<Polite announcements have turned into shouts in several languages. This ward is closed until further notice. Everyone please clear the building calmly but immediately. It will be very hard to remain. We must find some forgotten corner out of sight and slink away with the powers of Knock or Moth.>",
    },
    success: [
      { recipe: { requirements: { knock: 4 } }, suffix: "knock" },
      { recipe: { requirements: { moth: 4 } }, suffix: "moth" },
    ],
    successEnd: {
      label: "<Stowed Away>",
      description:
        "<We laid low, and now the room is empty, of guards and of guests. But This will not remain so long, especially once we take action again.>",
      effects: { [vb.vp("empty")]: 1, [vb.vp("crowds")]: -1 },
    },
    failure: {
      label: "<Flushed Out>",
      description:
        "<The further we move, the less a person and more a crowd. We flush out into the street where we pool in a confused cacophony,before slowly dispersing. I reconvene with my men, but we will have to wait until this story clears before we attempt to enter again.>",
      purge: { "mariner.aspects.vaults.damascus.layer.1": 10 },
    },
  });
}
