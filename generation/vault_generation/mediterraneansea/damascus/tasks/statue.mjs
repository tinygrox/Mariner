import { probabilityChallengeTemplates } from "../../../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS } from "../../../../slots_helpers.mjs";

export function generateStatue(vb) {
  vb.defineTask({
    taskId: "statue",
    label: "The Avian Statue",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "Ivory inlay over coarse sandstone, this statue is winged and beaked, yet still appears more man than bird. The House of Lethe was not known as an religious organization in Damascus., and the plague mentions the statue was commissioned by a member called Matthias.",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Commune with the White Heron",
            startdescription:
              "The House of Lethe was not known as a religious organization, nor does this coincide with any of the gods canonical. The plaque besides the statue discourages further speculation… But I could open up my heart, and see what I can find in those empty bird skull eyes.",
          },
          success: {
            label: "Recognition",
            description:
              "I recognise the bird who sang, the flutanist who carved the singing bones, mourning dove and the keeper of the names. But to know is to be known. While he will have kept my name tucked in his feathers irregardless, now his gaze is fixed upon me. I shiver in dusty stone-heat.",
            effects: { [vb.vp("bird")]: 1 },
          },
          failure: {
            label: "Grim Certainty",
            description:
              "I cannot connect with this entity, but I am left with the absolute certainty that once the time is there, he will know me.",
            effects: { "mariner.experiences.rattling.standby": 1 },
          },
          template: probabilityChallengeTemplates.COMMUNE,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
      explore: {
        challenge: {
          task: {
            label: "Puzzle Out the Function of the White Heron",
            startdescription:
              "The House of Lethe was not known as a religious organization, nor does this coincide with any of the gods canonical. The plaque besides the statue discourages further speculation… but some information is around, in the plagues and the objects. Perhaps we can puzzle through the gaps of history and censure, and define its function.",
          },
          success: {
            label: "Truth in Form",
            description:
              "There are elements on this statue that do not match its design. Keyhole shaped gaps between the ivory inlay. The beak, angled to the left, as are it's taloned fingers. What is it pointing at? Would what it was indicating still be there? Was the statue even placed back where it was?",
            effects: { [vb.vp("guidance")]: 1 },
          },
          failure: {
            label: "Form for Form's Sake",
            description:
              "Perhaps this is just art for art's sake, or more banal still, religious. As ever with an item that archeologists are unsure of its use, the plague describes its function as ritualistic. This is about all I will learn here today.",
            effects: { [vb.vp("statue")]: -1 },
          },
          template: probabilityChallengeTemplates.PERCEIVE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });
}
