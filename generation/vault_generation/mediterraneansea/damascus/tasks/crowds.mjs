import { probabilityChallengeTemplates } from "../../../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS } from "../../../../slots_helpers.mjs";

export function generateCrowds(vb) {
  vb.defineTask({
    taskId: "crowds",
    label: "The Many-faced Crowd",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "The curators, the security guards, the teeming public. Each carries its own opportunities but each also has its attention on the art, or anything untowards that might happen.",
    actions: {
      talk: {
        challenge: {
          hint: {
            label: "Tarnished Reputation",
            startdescription:
              "I cannot engage a curator, when they are busy alerting the guards about my presence. I may have to take a more direct approach, or try another time.",
            requirements: { "mariner.notoriety": 4 },
          },
          task: {
            label: "Engage a Curator",
            startdescription:
              "If I show myself engaging and bright, I could see if I can attract one of the Curators who works at the museum. They will have access to certain things the public does not, and may be interested in sharing with an enthusiastic dilettante.",
          },
          success: {
            label: "A Professorial So-and-So",
            description:
              "The professor finds our presence engaging, and our questions flattering. They have taken a moment to help us, but their time is limited, and their interest can wane.",
            effects: { [vb.vp("curator")]: 1 },
          },
          failure: {
            label: "Hasty Help",
            description:
              "Someone came to lend aid to the questions of the public, but they would clearly rather be elsewhere. I cannot engage them for long. I can attempt to make use of them, but I have to hope we are not interrupted.",
            furthermore: [
              {
                effects: vb.vp({ curator: 1 }),
              },
              {
                aspects: { "mariner.flubbed": 1 },
              },
            ],
          },
          template: probabilityChallengeTemplates.PLEAD,
        },
        slots: [
          ...CHALLENGE_SLOTS(3, "talk"),
          {
            id: "influence",
            actionid: "talk",
            label: "Additional Charms",
            description: "I could attract a curator just like I attract any bright member of my crew.",
            required: { "mariner.influence": 1 },
          },
        ],
      },
      explore: {
        challenge: {
          task: {
            label: "Create a Distraction",
            startdescription:
              "I could have one of my crewmembers create a distraction in the crowd. They will assuredly be removed from the premises, but it could assure a period of uninterrupted time for us to perform other operations.",
          },
          success: {
            label: "Muffled Arguments",
            description:
              "From just far enough away, I can hear a hubbub start and slowly build in intensity. It keeps the guards engaged, and forms a crowd that keeps the room from filling up too much. I have a break to accomplish what I need.",
            effects: { [vb.vp("distraction")]: 1 },
          },
          failure: {
            label: "A Commotion and a Conclusion",
            description:
              "I can hear the commotion started by my crewmember, and I can hear it escalate quicker than planned. Soon I can hear the sound of the guards, and the sound of my friend dragged off the premises. I also see the guards craning the crowd, looking who might have been associated with the display of disorder.",
            effects: { [vb.vp("distraction")]: 1 },
            alt: [{ id: "mariner.vaults.damascus.events.sweep", additional: true }],
          },
          template: probabilityChallengeTemplates.TAMPER,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
      "mariner.sing": {
        challenge: {
          task: {
            label: "Work the Crowd",
            startdescription:
              "There is enough of a crowd here to work the Orphean Arts. If I need any enhancement to accomplish what I need, I can gather them from the crowds.",
          },
          success: {
            label: "Rising like Dust",
            description:
              "Between the cabinets and the the crowds, power laces itself around like specks, invisible but in sunlight. people will take note, but hopefully, they will not notice.",
            effects: { "mariner.influences.lantern": 1 },
          },
          failure: {
            label: "All Kinds of Attention",
            description:
              "All eyes are on me for a moment, and there is nervous, uncertain applaise. But all eyes includes the eyes of the guards...",
            effects: { "mariner.notoriety": 1, "mariner.influences.lantern": 1 },
            alt: [{ id: "mariner.vaults.damascus.events.eyes", additional: true }],
          },
          template: probabilityChallengeTemplates.COMMUNE,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.sing"),
      },
    },
  });
}
