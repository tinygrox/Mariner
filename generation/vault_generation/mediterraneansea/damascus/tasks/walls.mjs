import { vaultPrefix, ASPECTS_FILE, RECIPES_FILE } from "../damascus.mjs";
import { probabilityChallengeTemplates } from "../../../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS } from "../../../../slots_helpers.mjs";

export function generateWalls(vb) {
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("transcribing.pre"),
    label: "<Transcription Begins>",
    warmup: 5,
    startdescription: "Hand to paper, the process of transcribing the page has begun.",
    actionId: "mariner.transcribing",
    effects: { [vb.vp("progress")]: 1 },
    requirements: { "mariner.crew": 1 },
    craftable: false,
    linked: [{ id: vb.vp("transcribing.going") }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("transcribing.going"),
    warmup: 25,
    label: "<Transcription in Progress>",
    startdescription: "The scratching of paper, the squinting of eyes, the rolling of sweat.",
    effects: { [vb.vp("progress")]: 1 },
    linked: [{ id: vb.vp("transcribing.end") }, { id: vb.vp("transcribing.going") }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("transcribing.end"),
    requirements: { [vb.vp("progress")]: 6 },
    warmup: 10,
    label: "<The Final Line>",
    startdescription: "The final line, the final word, the final symbol and the final sigh of relief.",
    linked: [
      {
        id: "reward1",
        requirements: { [vb.vp("pages1")]: 1 },
        effects: {
          [vb.vp("page1_totake")]: 1,
          [vb.vp("pages1")]: -1,
          [vb.vp("progress")]: -6,
        },
      },
      {
        id: "reward2",
        requirements: { [vb.vp("pages2")]: 1 },
        effects: {
          [vb.vp("page2_totake")]: 1,
          [vb.vp("pages2")]: -1,
          [vb.vp("progress")]: -6,
        },
      },
      {
        id: "reward3",
        requirements: { [vb.vp("pages3")]: 1 },
        effects: {
          [vb.vp("page3_totake")]: 1,
          [vb.vp("pages3")]: -1,
          [vb.vp("progress")]: -6,
        },
      },
      {
        id: "reward4",
        requirements: { [vb.vp("pages4")]: 1 },
        effects: {
          [vb.vp("page4_totake")]: 1,
          [vb.vp("pages4")]: -1,
          [vb.vp("progress")]: -6,
        },
      },
    ],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("hint.all"),
    label: "<The Writing on the Walls: More?>",
    startdescription:
      "Might there be more to do with these old stones? I can see vague traces of glyphs on the edges of the bricks. It is hard to tell, and harder to investigate with the eyes of the public and guards upon me. If there is something for me to do, I could not do it now.",
    requirements: {
      [vb.vp("guidance")]: -1,
      [vb.vp("empty")]: -1,
      [vb.vp("walls")]: 1,
    },
    actionId: "explore",
    hintonly: true,
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("hint.guidance"),
    label: "The Writing on the Walls: Too Crowded",
    startdescription:
      "The bird statue points to the englyphed section of the walls, but I cannot recognise the script. I will not find out unless I am able to get my hands on the walls itself… And that is unlikely during opening hours. This mystery will have to wait for the right opportunity when the room is cleared or night has fallen.",
    requirements: {
      [vb.vp("guidance")]: 1,
      [vb.vp("empty")]: -1,
      [vb.vp("walls")]: 1,
    },
    actionId: "explore",
    hintonly: true,
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("stepthrough"),
    craftable: false,
    label: "Secret Threshold",
    startdescription:
      "We trace the hooked glyphs with our fingers, focussing on the cracked between the bricks. Closer and closer our focus grows, and larger and larger seems the vacuous darks between the bricks, until it seems larger than me, larger than us, until it is the dark that swallows us.",
    requirements: {
      [vb.vp("guidance")]: 1,
      [vb.vp("empty")]: 1,
    },
    actionId: "explore",
  });

  vb.defineTask({
    taskId: "walls",
    label: "The Dissected Walls",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "The walls of the Damascene branch have been dissected and then reassembled here, brick by brick, with commendable attention to detail, but perhaps not a lot of decorum. The greatest treasure of the house is displayed here, an original volume of a book  from the house of Lethe.",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "The Writing on the Wall",
            startdescription:
              "In four displays the pages have been spread out, and pressed to the glass, the aramaic letters dappled pattern on the finer grain of vellum. Three displays are open, attracting spectators like flies, but the fourth is hidden behind curtains of black velvet. Curate, contain, censure. If I wish to learn the story contained, each page needs to be copied. Let’s see what we are working with.",
          },
          success: {
            label: "A Message from History",
            description:
              "Over a millennium ago, some sage set up countless nights to pen down this story, to gain access to an organization, thrice chained. Now its pages are displayed here, unnamed, untranslated, with a quarter of the work hidden from the public’s eye.",
            effects: {
              [vb.vp("pages1")]: 1,
              [vb.vp("pages2")]: 1,
              [vb.vp("pages3")]: 1,
              [vb.vp("pages4")]: 1,
            },
          },
          failure: {
            label: "A Message from History",
            description:
              "Over a millennium ago, some sage set up countless nights to pen down this story, to gain access to an organization, thrice chained. Now its pages are displayed here, unnamed, untranslated, with a quarter of the work hidden from the public’s eye.",
            effects: {
              [vb.vp("pages1")]: 1,
              [vb.vp("pages2")]: 1,
              [vb.vp("pages3")]: 1,
              [vb.vp("pages4")]: 1,
            },
          },
          template: probabilityChallengeTemplates.STUDY,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
      explore: {
        challenge: {
          task: {
            requirements: { [vb.vp("guidance")]: 1, [vb.vp("empty")]: 1 },
            grandReqs: {
              "[~/extant:mariner.weathermeta.misty]": 1,
            },
            label: "The Writing on the Walls",
            startdescription:
              "In the muted light coming from the fog-choked moon, we discover script etched into the bricks of the House. The walls were rebuilt here with such care… could the secrets of Matthias have been brought with it? There are the faint edges of glyphs visible on the edges of bricks. Where will this pattern lead me? ",
          },
          success: {
            label: "<Trace the Glyphs>",
            description:
              "<We follow the glyphs, more with our fingers then our sighs. with dusky charcoal, the glyphs shadow is caught on paper. So far, their meaning allude us, even the name of the language or script impossible to guage>",
            effects: { [vb.vp("glyphs")]: 1 },
            linked: [{ id: vb.vp("stepthrough") }],
          },
          failure: {
            label: "That Hooked Snare",
            description:
              "We follow the lined glyphs, but lose ourselves in its curves, hooks and angles. We follow it, and follow it, and then light begins to glimmer in the corner of our eyes, and excited shouts. We lost ourselves in the snare until we where found by guards, and something of us remains there as we are taken away. As long as I cannot guard myself against Hyksian Snares.",
            effects: { "mariner.notoriety": 1 },
            alt: [
              {
                id: "mariner.vaults.damascus.events.emptying",
                additional: true,
              },
            ],
          },
          template: probabilityChallengeTemplates.SNEAK_IN,
        },
        slots: CHALLENGE_SLOTS(5, "explore"),
      },
    },
  });

  vb.defineTask({
    taskId: "pages1",
    label: "<First Spread of Matthias and the Imago: Loss>",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "<Priceless historical artifact, but without speaking the language, just irregular pattern on a diaphanous sheet.>",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "The Dissected Walls: Transcription",
            startdescription:
              "The pages pinned to this wall hold a story I wish to know, but they are written in aramaic, and not readily published in full. But with a lot of patience, and some skilled eyes and fingers, we could copy every letter, and keep each intention intact.",
          },
          success: {
            label: "Hidden World Proliferating",
            description:
              "The work has begun. The careful carving on the paper, the blind calligraphy. Now, these are just shapes and loops, a locked treasure, now multiplying. As this work continues, we should safeguard it against interruption.",
            alt: [
              {
                id: vb.vp("transcribing.pre"),
                additional: true,
                expulsion: {
                  limit: 2,
                  filter: {
                    [vb.vp("pages1")]: 1,
                    "mariner.crew": 1,
                    "mariner.halfheart": 1,
                  },
                },
                effects: { [vb.vp("progress")]: 2 },
              },
            ],
          },
          failure: {
            label: "Blind Scratchings, Erratic Lines",
            description:
              "The process is not fast, nor flawless. We are not specialists at this, but we cannot know which mistake might be catastrophic. So we will work slowly, meticulously, and when we are unsure, we will do it again.",
            alt: [
              {
                id: vb.vp("transcribing.pre"),
                additional: true,
                expulsion: {
                  limit: 2,
                  filter: {
                    [vb.vp("pages1")]: 1,
                    "mariner.crew": 1,
                    "mariner.halfheart": 1,
                  },
                },
              },
            ],
            effects: { "mariner.notoriety": 1 },
          },
          template: probabilityChallengeTemplates.STUDY,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
      explore: {
        hint: {
          label: "The Writing on the Wall: Larceny",
          startdescription:
            "If I throw all caution to the wind, I could take this page, from behind the velvet curtains, if everyone looked elsewhere. This would be risky, and have lasting impact on my reputation.",
          requirements: { op: -1 },
        },
        challenge: {
          task: {
            label: "The Writing on the Wall: Larceny",
            startdescription:
              "With the coast seemingly clear, I can make my move. But even if people do not notice it right away, they will notice it gone later. This would give me a lasting reputation throughout the Empire.",
            requirements: { op: 1 },
          },
          success: {
            label: "Stowed Away",
            description:
              "one swift rip and the first act of the Matthias and Imago disappears in my jacket. Nobody saw it was me, but all see the empty frame on the wall and my description will be recognised by the authorities.",
            effects: { "mariner.notoriety": 2, [vb.vp("page1_totake")]: 1 },
          },
          failure: {
            label: "Instant Alarm",
            description:
              "As soon as my fingers clutch the fingers, a guards wandering eye catches mine. Instantly, shouts of alarm ring through the building.",
            effects: { "mariner.notoriety": 4, [vb.vp("page1_totake")]: 1 },
            alt: [
              {
                id: "mariner.vaults.damascus.events.emptying",
                additional: true,
              },
            ],
          },
          template: probabilityChallengeTemplates.AVOID,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });

  vb.defineTask({
    taskId: "pages2",
    label: "<Second Spread of Matthias and the Imago: Loss>",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "<Priceless historical artifact, but without speaking the language, just irregular pattern on a diaphanous sheet.>",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "The Writing on the Wall: Transcription",
            startdescription:
              "The pages pinned to this wall hold a story I wish to know, but they are written in aramaic, and not readily published in full. But with a lot of patience, and some skilled eyes and fingers, we could copy every letter, and keep each intention intact.",
          },
          success: {
            label: "Hidden World Proliferating",
            description:
              "The work has begun. The careful carving on the paper, the blind calligraphy. Now, these are just shapes and loops, a locked treasure, now multiplying. As this work continues, we should safeguard it against interruption.",
            alt: [
              {
                id: vb.vp("transcribing.pre"),
                additional: true,
                expulsion: {
                  limit: 2,
                  filter: {
                    [vb.vp("pages2")]: 1,
                    "mariner.crew": 1,
                    "mariner.halfheart": 1,
                  },
                },
                effects: { [vb.vp("progress")]: 2 },
              },
            ],
          },
          failure: {
            label: "Blind Scratchings, Erratic Lines",
            description:
              "The process is not fast, nor flawless. We are not specialists at this, but we cannot know which mistake might be catastrophic. So we will work slowly, meticulously, and when we are unsure, we will do it again.",
            alt: [
              {
                id: vb.vp("transcribing.pre"),
                additional: true,
                expulsion: {
                  limit: 2,
                  filter: {
                    [vb.vp("pages2")]: 1,
                    "mariner.crew": 1,
                    "mariner.halfheart": 1,
                  },
                },
              },
            ],
            effects: { "mariner.notoriety": 1 },
          },
          template: probabilityChallengeTemplates.STUDY,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
      explore: {
        hint: {
          label: "The Writing on the Wall: Larceny",
          startdescription:
            "If I throw all caution to the wind, I could take this page, from behind the velvet curtains, if everyone looked elsewhere. This would be risky, and have lasting impact on my reputation.",
          requirements: { op: 1 },
        },
        challenge: {
          task: {
            label: "The Writing on the Wall: Larceny",
            startdescription:
              "With the coast seemingly clear, I can make my move. But even if people do not notice it right away, they will notice it gone later. This would give me a lasting reputation throughout the Empire.",
          },
          success: {
            label: "Stowed Away",
            description:
              "one swift rip and the second act of the Matthias and Imago disappears in my jacket. Nobody saw it was me, but all see the empty frame on the wall and my description will be recognised by the authorities.",
            effects: { [vb.vp("page2_totake")]: 1 },
          },
          failure: {
            label: "Instant Alarm",
            description:
              "As soon as my fingers clutch the fingers, a guards wandering eye catches mine. Instantly, shouts of alarm ring through the building.",
            effects: { "mariner.notoriety": 4, [vb.vp("page2_totake")]: 1 },
            alt: [
              {
                id: "mariner.vaults.damascus.events.emptying",
                additional: true,
              },
            ],
          },
          template: probabilityChallengeTemplates.AVOID,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });

  vb.defineTask({
    taskId: "pages3",
    label: "<Third Spread of Matthias and the Imago: Loss>",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "<Priceless historical artifact, but without speaking the language, just irregular pattern on a diaphanous sheet.>",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "The Writing on the Wall: Transcription",
            startdescription:
              "The pages pinned to this wall hold a story I wish to know, but they are written in aramaic, and not readily published in full. But with a lot of patience, and some skilled eyes and fingers, we could copy every letter, and keep each intention intact.",
          },
          success: {
            label: "Hidden World Proliferating",
            description:
              "The work has begun. The careful carving on the paper, the blind calligraphy. Now, these are just shapes and loops, a locked treasure, now multiplying. As this work continues, we should safeguard it against interruption.",
            alt: [
              {
                id: vb.vp("transcribing.pre"),
                additional: true,
                expulsion: {
                  limit: 2,
                  filter: {
                    [vb.vp("pages3")]: 1,
                    "mariner.crew": 1,
                    "mariner.halfheart": 1,
                  },
                },
                effects: { [vb.vp("progress")]: 2 },
              },
            ],
          },
          failure: {
            label: "Blind Scratchings, Erratic Lines",
            description:
              "The process is not fast, nor flawless. We are not specialists at this, but we cannot know which mistake might be catastrophic. So we will work slowly, meticulously, and when we are unsure, we will do it again.",
            alt: [
              {
                id: vb.vp("transcribing.pre"),
                additional: true,
                expulsion: {
                  limit: 2,
                  filter: {
                    [vb.vp("pages3")]: 1,
                    "mariner.crew": 1,
                    "mariner.halfheart": 1,
                  },
                },
              },
            ],
            effects: { "mariner.notoriety": 1 },
          },
          template: probabilityChallengeTemplates.STUDY,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
      explore: {
        hint: {
          label: "The Writing on the Wall: Larceny",
          startdescription:
            "If I throw all caution to the wind, I could take this page, from behind the velvet curtains, if everyone looked elsewhere. This would be risky, and have lasting impact on my reputation.",
          requirements: { op: -1 },
        },
        challenge: {
          task: {
            label: "The Writing on the Wall: Larceny",
            startdescription:
              "With the coast seemingly clear, I can make my move. But even if people do not notice it right away, they will notice it gone later. This would give me a lasting reputation throughout the Empire.",
            requirements: { op: 1 },
          },
          success: {
            label: "Stowed Away",
            description:
              "one swift rip and the third act of the Matthias and Imago disappears in my jacket. Nobody saw it was me, but all see the empty frame on the wall and my description will be recognised by the authorities.",
            effects: { "mariner.notoriety": 2, [vb.vp("page3_totake")]: 1 },
          },
          failure: {
            label: "Instant Alarm",
            description:
              "As soon as my fingers clutch the fingers, a guards wandering eye catches mine. Instantly, shouts of alarm ring through the building.",
            effects: { "mariner.notoriety": 4, [vb.vp("page3_totake")]: 1 },
            alt: [
              {
                id: vb.vp("transcribing.pre"),
                additional: true,
                expulsion: {
                  limit: 2,
                  filter: { [vb.vp("pages3")]: 1, "mariner.crew": 1 },
                },
              },
              {
                id: "mariner.vaults.damascus.events.emptying",
                additional: true,
              }, //alt link to recipe that causes some lasting reputation damage.
            ],
          },
          template: probabilityChallengeTemplates.AVOID,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });

  vb.defineTask({
    taskId: "pages4",
    label: "<The Final Spread of Matthias and the Imago: Loss>",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "<The Final Section of the book has been deemed too dangerous for public viewing, even when they are written in a language dead to the vulgar masses.>",
    actions: {
      "mariner.navigate": {
        hint: {
          label: "The Writing on the Wall: Unveiling",
          startdescription:
            "I cannot access the final pages of Matthias… Not until I convince a curator to part the velvet curtains for me. Before the copying can begin, I must take a curator there.",
          requirements: { [vb.vp("curator")]: -1 },
        },
        challenge: {
          task: {
            label: "The Writing on the Wall: Transcription",
            startdescription:
              "The pages pinned to this wall hold a story I wish to know, but they are written in aramaic, and not readily published in full. But with a lot of patience, and some skilled eyes and fingers, we could copy every letter, and keep each intention intact.",
          },
          requirements: { [vb.vp("curator")]: 1 },
          success: {
            label: "Hidden World Proliferating",
            description:
              "The work has begun. The careful carving on the paper, the blind calligraphy. Now, these are just shapes and loops, a locked treasure, now multiplying. As this work continues, we should safeguard it against interruption.",
            alt: [
              {
                id: vb.vp("transcribing.pre"),
                additional: true,
                expulsion: {
                  limit: 2,
                  filter: {
                    [vb.vp("pages4")]: 1,
                    "mariner.crew": 1,
                    "mariner.halfheart": 1,
                  },
                },
                effects: { [vb.vp("progress")]: 2 },
              },
            ],
          },
          failure: {
            label: "Blind Scratchings, Erratic Lines",
            description:
              "The process is not fast, nor flawless. We are not specialists at this, but we cannot know which mistake might be catastrophic. So we will work slowly, meticulously, and when we are unsure, we will do it again.",
            alt: [
              {
                id: vb.vp("transcribing.pre"),
                additional: true,
                expulsion: {
                  limit: 2,
                  filter: {
                    [vb.vp("pages4")]: 1,
                    "mariner.crew": 1,
                    "mariner.halfheart": 1,
                  },
                },
              },
            ],
            effects: { "mariner.notoriety": 1 },
          },
          template: probabilityChallengeTemplates.STUDY,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
      talk: {
        hint: {
          label: "The Writing on the Wall: Unveiling",
          startdescription:
            "I cannot access the final pages of Matthias… Not until I convince a curator to part the velvet curtains for me.",
          requirements: { [vb.vp("curator")]: -1 },
        },
        challenge: {
          task: {
            label: "The Writing on the Wall: Unveiling",
            startdescription:
              "I have my curator curious, but unconvinced. The consensus on a censor is sensible, they might offer.  It seems I will never get the time to copy the whole piece, so I will have to extract every drop of the story from them, as they try to lecture instead of regale.",
            requirements: { [vb.vp("curator")]: 1 },
          },
          success: {
            label: "Beyond Ink",
            description:
              "I do not copy, I take notes. They are natural orators, and with pointed questions, I guide the lecture into the shape of a narrative flow. The longer they talk, the more they get into it, and more than a few other guests stick around to watch and listen. As they reach their thrilling conclusion, a small applaus can be heard. I parted ways with the curator, as we both got what we wanted.",
            effects: { [vb.vp("page4_totake")]: 1 },
            alt: [
              {
                id: "mariner.vaults.damascus.events.throngs",
                additional: true,
              }, //alt link to recipe that causes some lasting reputation damage.]
            ],
          },
          failure: {
            label: "Left Unattended",
            description:
              "The curtains part, and the  lecture begins… but soon falters. Perhaps the curator suddenly gleaned our intentions may not be purely academic. The scholar reassesses and reconsiders, and suddenly scholarly pursuits are exchanged for self preservation. He backs away with an excuse, and I am left to copy… But I am sure a guard may arrive soon.",
            effects: { [vb.vp("curator")]: -1 },
            alt: [
              {
                id: vb.vp("transcribing.pre"),
                additional: true,
                additional: true,
                expulsion: {
                  limit: 2,
                  filter: { [vb.vp("pages4")]: 1, "mariner.crew": 1 },
                },
              },
              {
                id: "mariner.vaults.damascus.events.eyes",
                additional: true,
              }, //alt link to recipe that causes some lasting reputation damage.
            ],
          },
          template: probabilityChallengeTemplates.HONEYED_TONGUE,
        },
        slots: CHALLENGE_SLOTS(3, "talk"),
      },
      explore: {
        hint: {
          label: "The Writing on the Wall: Larceny",
          startdescription:
            "If I throw all caution to the wind, I could take this page, from behind the velvet curtains, if everyone looked elsewhere. This would be risky, and have lasting impact on my reputation.",
          requirements: { op: -1 },
        },
        challenge: {
          task: {
            label: "The Writing on the Wall: Larceny",
            startdescription:
              "With the coast seemingly clear, I can make my move. But even if people do not notice it right away, they will notice it gone later. This would give me a lasting reputation throughout the Empire.",
          },
          requirements: { op: 1 },
          success: {
            label: "Stowed Away",
            description:
              "Four precise slices, and a whisper of velvet, and the last act of the Matthias and Imago disappears in my jacket. Nobody noticed, but they will eventually, and my description will be recognised by the authorities.",
            effects: { "mariner.notoriety": 2, [vb.vp("page4_totake")]: 1 },
          },
          failure: {
            label: "Instant Alarm",
            description:
              "As soon as my fingers clutch the fingers, a guards wandering eye catches mine. Instantly, shouts of alarm ring through the building.",
            effects: { "mariner.notoriety": 4, [vb.vp("page4_totake")]: 1 },
            alt: [
              {
                id: "mariner.vaults.damascus.events.emptying",
                additional: true,
              }, //alt link to recipe that causes some lasting reputation damage.
            ],
          },
          template: probabilityChallengeTemplates.AVOID,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });
}
