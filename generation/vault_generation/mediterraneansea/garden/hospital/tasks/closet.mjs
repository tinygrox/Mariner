import { CHALLENGE_SLOTS } from "../../../../../slots_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";

export function generateCloset(vb) {
  vb.defineTask({
    taskId: "closet",
    label: "A Supply Closet",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description: "An unassuming door centrally placed between patients wards and operating rooms.",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Observe The Supply Closet",
            startdescription: "",
          },
          success: {
            label: "",
            description: "",
            effects: { [vb.vp("closet")]: -1, [vb.vp("patientward")]: 1 },
          },
          failure: {
            label: "",
            description: "",
            effects: { [vb.vp("closet")]: -1 },
          },
          template: probabilityChallengeTemplates.STUDY,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
      explore: {
        challenge: {
          task: {
            label: "Rummage through the Supply Closet",
            startdescription: "",
          },
          success: {
            label: "",
            description: "",
            effects: {
              [vb.vp("closet")]: -1,
              [vb.vp("disguise.staff")]: 1,
              [vb.vp("disguise.patient")]: 1,
            },
          },
          failure: {
            label: "",
            description: "",
            effects: {
              [vb.vp("closet")]: -1,
              [vb.vp("disguise.patient")]: -1,
              notoriety: 1,
            },
          },
          template: probabilityChallengeTemplates.OPERATE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });
}
