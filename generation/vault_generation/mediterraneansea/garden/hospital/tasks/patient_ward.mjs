import { CHALLENGE_SLOTS } from "../../../../../slots_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";
import { CALL_FUNCTION } from "../../../../../generate_function_calls.mjs";
import { RECIPES_FILE } from "../../garden.mjs";

export function generatePatientWard(vb) {
  vb.defineTask({
    taskId: "patientward",
    label: "The Patient Ward",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description: "",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Investigate the Patient Ward",
            startdescription: "",
          },
          success: {
            label: "",
            description: "",
            linked: [{ id: vb.vp("following") }, { id: vb.vp("observing") }],
          },
          failure: {
            label: "",
            description: "",
            effects: { [vb.vp("patientwards")]: -1 },
          },
          template: probabilityChallengeTemplates.STUDY,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
      explore: {
        challenge: {
          task: {
            label: "Rummage through the Patient Ward",
            startdescription: "",
          },
          success: {
            label: "",
            description: "",
            effects: {
              [vb.vp("disguise.patient")]: 1,
              [vb.vp("patientwards")]: -1,
            },
            furthermore: CALL_FUNCTION("drawreward.jumble"),
          },
          failure: {
            label: "",
            description: "",
            effects: {
              [vb.vp("disguise.patient")]: 1,
              notoriety: 1,
              [vb.vp("patientwards")]: -1,
            },
          },
          template: probabilityChallengeTemplates.AVOID,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("following"),
    requirements: { [vb.vp("hospital.annexd")]: 1 },
    warmup: 10,
    label: "<Following the Orderlies to the Annex D>",
    startdescription: "",
    //effects: {}, the effects that lead you to the next layer
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("observing"),
    warmup: 10,
    label: "<Observing what goes on at the Patient Wards>",
    startdescription: "",
    effects: {
      [vb.vp("insight.patients")]: 1,
      [vb.vp("patientwards")]: -1,
    },
  });
}
