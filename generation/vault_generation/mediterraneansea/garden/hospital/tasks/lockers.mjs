import { CHALLENGE_SLOTS } from "../../../../../slots_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";

export function generateLockers(vb) {
  vb.defineTask({
    taskId: "lockers",
    label: "The Lockerroom",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description: "",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Observe the Lockers",
            startdescription: "",
          },
          success: {
            label: "",
            description: "",
            effects: { [vb.vp("lockers")]: -1, [vb.vp("operatingroom")]: 1 },
          },
          failure: {
            label: "",
            description: "",
            effects: { [vb.vp("lockers")]: -1 },
          },
          template: probabilityChallengeTemplates.STUDY,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
      explore: {
        challenge: {
          task: {
            label: "Rummage through the Lockers",
            startdescription: "",
          },
          success: {
            label: "",
            description: "",
            effects: {
              [vb.vp("disguise.doctor")]: 1,
              [vb.vp("disguise.doctor")]: 1,
              [vb.vp("lockers")]: -1,
            },
          },
          failure: {
            label: "",
            description: "",
            effects: { [vb.vp("lockers")]: -1, [vb.vp("disguise.doctor")]: 1 },
          },
          template: probabilityChallengeTemplates.OPERATE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });
}
