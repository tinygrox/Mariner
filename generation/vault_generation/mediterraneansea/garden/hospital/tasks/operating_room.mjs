import { CHALLENGE_SLOTS } from "../../../../../slots_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";

export function generateOperatingRoom(vb) {
  vb.defineTask({
    taskId: "operatingroom",
    label: "Operating Room",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description: "",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Observe the operating Room",
            startdescription: "",
          },
          success: {
            label: "",
            description: "",
            effects: {
              [vb.vp("insight.operation")]: 1,
              [vb.vp("operatingroom")]: -1,
            },
          },
          failure: {
            label: "",
            description: "",
            effects: { notoriety: 1, [vb.vp("operatingroom")]: -1 },
          },
          template: probabilityChallengeTemplates.STUDY,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
      explore: {
        challenge: {
          task: {
            label: "Rummage through the Operating Room",
            startdescription: "",
          },
          success: {
            label: "",
            description: "",
            effects: {
              "mariner.medicalsupplies": 1,
              [vb.vp("operatingroom")]: -1,
            },
          },
          failure: {
            label: "",
            description: "",
            effects: {
              "mariner.medicalsupplies": 1,
              notoriety: 1,
              [vb.vp("operatingroom")]: -1,
            },
          },
          template: probabilityChallengeTemplates.OPERATE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });
}
