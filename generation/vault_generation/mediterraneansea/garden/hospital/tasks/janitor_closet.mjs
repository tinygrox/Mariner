import { CHALLENGE_SLOTS } from "../../../../../slots_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";
export function generateJanitorCloset(vb) {
  vb.defineTask({
    taskId: "janitorcloset",
    label: "The Janitor Closet",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "Away form sunlight and the pristine halls that visitors roam, there sits a door where cleaning supplies are kept.",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Observe the Janitors Closet",
            startdescription:
              "If we study the Janitors closet, and follow him on his rounds, we could follow them to a locker rooms.",
          },
          success: {
            label: "Janus Shadow",
            description:
              "Thankfully, this janitor does not have eyes at the back of it's head. We follow his route to the locker rooms.",
            effects: { [vb.vp("janitorcloset")]: -1, [vb.vp("lockers")]: 1 },
          },
          failure: {
            label: "Knotted Concourse.",
            description:
              "We tried to walk in the janitor's shadow, but lost him in the knot of hallways of the hospital. We will have to find our own way forward.",
            effects: { [vb.vp("janitorcloset")]: -1 },
          },
          template: probabilityChallengeTemplates.AVOID,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
      explore: {
        challenge: {
          task: {
            label: "Rummage through the Janitor Closet",
            startdescription:
              "What is kept in the Janitors closet? Supplies sure, but we can hope that one thing that turns barriers into passages...",
          },
          success: {
            label: "Unglorious Tools of Unlauded Trade",
            description: "The garments of obscurity, the ring of initiations, the tools of use.",
            effects: { [vb.vp("keyring")]: 1, [vb.vp("disguise.staff")]: 1, [vb.vp("janitorcloset")]: -1 },
          },
          failure: {
            label: "Caught With our Hand on the Contraband.",
            description:
              "Someone walked through the halls behind us, silent-slippered like a cat, and screeched an alarm when they saw us with our hands on the janitors stock. We make our way out with the spoils, but the Hospital now know there are intruders around.",
            effects: {
              [vb.vp("keyring")]: 1,
              [vb.vp("janitorcloset")]: -1,
              [vb.vp("disguise.staff")]: 1,
              notoriety: 2,
            },
          },
          template: probabilityChallengeTemplates.OPERATE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });
}
