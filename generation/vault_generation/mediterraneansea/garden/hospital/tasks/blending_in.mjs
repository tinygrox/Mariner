import { SIGNALS } from "../../../../../generate_signal_aspects.mjs";

export function generateBlendIn(vb) {
  mod.setAspect(vb.ASPECTS_FILE, {
    id: "mariner.hospital.disguise",
    label: "A Disguise",
    description:
      "Take on another skin, to change how I am percieved and where I am permitted.",
  });

  vb.defineHelpElement({
    id: vb.vp("disguise.staff"),
    label: "Disguise: Staff",
    description:
      "Orderlies, nurses, cleaning crew. all the unlauded, invisible jobs that makes this institution shine.",
    [SIGNALS.UNDRESS]: [
      {
        morpheffect: "setmutation",
        id: vb.vp("disguise.staff"),
        level: 0,
      },
      {
        morpheffect: "spawn",
        id: vb.vp("disguise.staff"),
        level: 1,
      },
    ],
  });
  vb.defineHelpElement({
    id: vb.vp("disguise.patient"),
    label: "Disguise: Patient",
    description:
      "How many patients feel honored that the whole operation of the hospital operates just for them?",
    [SIGNALS.UNDRESS]: [
      {
        morpheffect: "setmutation",
        id: vb.vp("disguise.patient"),
        level: 0,
      },
      {
        morpheffect: "spawn",
        id: vb.vp("disguise.patient"),
        level: 1,
      },
    ],
  });
  vb.defineHelpElement({
    id: vb.vp("disguise.doctor"),
    label: "Disguise: Doctor",
    description:
      "A borrowed air of authority. When you arrive doors open, when you speak, people listen. Are the doctors here just as worthy of this honor as you?",
    xtriggers: {
      [SIGNALS.UNDRESS]: [
        {
          morpheffect: "setmutation",
          id: vb.vp("disguise.doctor"),
          level: 0,
        },
        {
          morpheffect: "spawn",
          id: vb.vp("disguise.doctor"),
          level: 2,
        },
      ],
    },
  });

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("don.disguise"),
    label: "Don a Disguise",
    startdescription:
      "I can disguise one of my crewmembers or myself. the more of my crew is disguised, the less attention we attract. Different disguises may gain further access in different spaces.",
    description:
      "The first Art of changing ones skin, most accepted in society, though not when used to claim the skin of someone else.",
    warmup: 30,
    actionid: "mariner.navigate",
    craftable: true,
    requirements: {
      "mariner.disguise": 1,
      "mariner.crew": 1,
    },
    linked: [{ id: vb.vp("don.disguise.router") }],
  });

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("don.disguise.router"),
    linked: [
      {
        id: vb.vp("don.disguise.staff"),
      },
      {
        id: vb.vp("don.disguise.patient"),
      },
      {
        id: vb.vp("don.disguise.doctors"),
      },
      { id: vb.vp("don.disguise.failed") },
    ],
  });
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("don.disguise.staff"),
    label: "<Don adisguise: Staff>",
    description: `<Tough, sturdy fabric for a dirty, menial jobs. Slate grey color for those jobs that should just desappear between the concrete and the cement.>`,
    warmup: 25,
    requirements: { [vb.vp("disguise.staff")]: 1 },
    effects: {
      [vb.vp("disguise.staff")]: -1,
    },
    aspects: { [SIGNALS.UNDRESS]: 1 },
    furthermore: [
      {
        mutations: [
          {
            filter: "mariner.crew",
            mutate: `mariner.disguise.aspect.staff`,
            level: 1,
          },
        ],
      },
    ],
  });
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("don.disguise.patient"),
    label: "<Don a disguise: Patient>",
    description: `<White robes, but not those of the acolyte. bare arms and legs, but no hint of sensuality.>`,
    warmup: 25,
    requirements: { [vb.vp("disguise.patient")]: 1 },
    effects: {
      [vb.vp("disguise.patient")]: -1,
    },
    aspects: { [SIGNALS.UNDRESS]: 1 },
    furthermore: [
      {
        mutations: [
          {
            filter: "mariner.crew",
            mutate: `mariner.disguise.aspect.patient`,
            level: 1,
          },
        ],
      },
    ],
  });
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("don.disguise.doctor"),
    label: "<Don a disguise: Doctor>",
    description: `<Not two generations prior, all doctors still wore grave black, but here in the shining Saint Respice, all doctors are swaddled in purest white.>`,
    warmup: 25,
    requirements: { [vb.vp("disguise.doctor")]: 1 },
    effects: {
      [vb.vp("disguise.doctor")]: -1,
    },
    aspects: { [SIGNALS.UNDRESS]: 1 },
    furthermore: [
      {
        mutations: [
          {
            filter: "mariner.crew",
            mutate: `mariner.disguise.aspect.staff`,
            level: 1,
          },
        ],
      },
    ],
  });
}
