import { generateBlendIn } from "./tasks/blending_in.mjs";
import { generateBureaucracy } from "./tasks/bureaucracy.mjs";
import { generateCloset } from "./tasks/closet.mjs";
import { generateJanitorCloset } from "./tasks/janitor_closet.mjs";
import { generateLockers } from "./tasks/lockers.mjs";
import { generateOperatingRoom } from "./tasks/operating_room.mjs";
import { generatePatientWard } from "./tasks/patient_ward.mjs";
import { vaultPrefix } from "../garden.mjs";

// TASKS
export function generateHospitalLayer(vb) {
  vb.defineLayer("1");

  //items you take with you

  //help items

  vb.defineHelpElement({
    id: "hospital.annexd",
    label: "<Annex D... But where?>",
    description:
      "<We know that Annex D is where we want to go, but not where annex D is. But that will be where ansqwers are.>",
    unique: true,
  });
  //keys
  vb.defineHelpElement({
    id: "keyring",
    label: "<The Janitors Keyring>",
    description:
      "In this place, the janitors key-ring might as well be handed down by the Mother of Ants herself.",
    aspects: { knock: 4 },
    unique: true,
  });
  //insight: or
  vb.defineHelpElement({
    id: "insight.operation",
    label: "Peculiarity of Treatment",
    description:
      "Those wheeled into the operation room are either the creme de la creme of society, or have a large doting family following the gurney.",
    unique: true,
  });
  //insight 2: patient movement
  vb.defineHelpElement({
    id: "insight.patients",
    label: "An Inbalance in Behavior",
    description:
      "When patients who seem to be of lower means are getting sicker, they get wheeled away by orderlies, never to return, never to appear on any of the O.R. boards.",
    unique: true,
  });
  //insight 3: paperwork
  vb.defineHelpElement({
    id: "insight.paperwork",
    label: "A Pattern in Paper",
    description:
      'with an astounding regularity patients are moved to "Annex D", and like clockwork, they are put as "released" a week later.',
    unique: true,
  });

  vb.defineExplorationTask({
    taskId: "hospital",
    locationLabel: "Saint Respice Hospital",
    locationDescription:
      "The tiles glissen and the shingles shine, each staff polished and pressed and pleasant. Rest, Regularity, Cleanlyness is carved above the door.  This hospital lives by the tennets of the Lady-of-the-Lamp with the same fervor as the martyr-monks followed the legacy of their Respitius on the monestary that once stood on these grounds.",
    tasks: [
      "closet",
      "operatingroom",
      "lockers",
      "patientward",
      "janitorcloset",
    ],
    //events: [],
    preRecipe: {
      purge: {
        [vb.currentLayerAspectId()]: 5,
      },
    },
    resetOnExhaustion: true,
  });

  generateCloset(vb);
  generateBureaucracy(vb);
  generateBlendIn(vb);
  generateLockers(vb);
  generateJanitorCloset(vb);
  generateOperatingRoom(vb);
  generatePatientWard(vb);
}
