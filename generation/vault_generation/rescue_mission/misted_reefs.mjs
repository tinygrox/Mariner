import { INJURE_CREWMEMBER, requireAndDestroy, SINK_SHIP } from "../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS, SHIP_MANOEUVERING_SLOTS } from "../../slots_helpers.mjs";
import { SHIP_NAME } from "../ship_wrecks_names.mjs";
import { generateTresholdChallengeTask } from "../vault_generation.mjs";
import { RECIPES_FILE, vaultPrefix } from "./rescue_mission.mjs";

// EVENTS
export function generateMistedReefsEvents(vb) {
  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.forge",
    task: {
      id: vaultPrefix("events.windrises"),
      craftable: false,
      label: "The Wind Rises",
      startdescription:
        "The flag stands straight horizontal, and a constant wind tempts the kite to turn its way. We need to tame this weather with Forge, or survive it with Heart.",
    },
    success: [
      { recipe: { requirements: { forge: 4 } }, suffix: "forge" },
      { recipe: { requirements: { heart: 4 } }, suffix: "heart" },
    ],
    successEnd: {
      label: "Strong Tailwind",
      description:
        "We leash the wind, and let it serve us instead. With this we will arrive at our destination sooner than expected.",
    },
    failure: {
      label: "Blown off Course",
      startdescription: "We fight against the wind, but at some point we have to bend, and give ourselves.",
      warmup: 30,
      linked: [
        {
          id: vb.vp("events.fairweather"),
          chance: 33,
        },
        {
          id: vb.vp("events.windrises"),
          chance: 50,
        },
        {
          id: vb.vp("events.marinemists"),
        },
      ],
    },
  });

  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.heart",
    task: {
      id: vaultPrefix("events.fairweather"),
      craftable: false,
      label: "Fair Weather",
      startdescription:
        "The sea is a mercurial mistress, but rarely, she grants opportunity. The weathervane turns in our favor today. If we seize this opportunity, we may reap the benefits. Forge for the will to act or let our Heart beat in tune with the world.",
    },
    success: [
      { recipe: { requirements: { forge: 4 } }, suffix: "forge" },
      { recipe: { requirements: { heart: 4 } }, suffix: "heart" },
    ],
    successEnd: {
      label: "Flawless Sailing",
      description:
        "The weather is crisp, the wind swells our sails. We travel twice as fast this day, and enjoy the journey fourfold.",
      effects: { "mariner.thrill": 1 },
    },
    failure: {
      label: "Merely Good",
      description:
        "Opportunity slips away, but the weather stays true. We travel at the expected rate, and whistle while we work. A journey to enjoy.",
    },
  });

  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.knock",
    task: {
      id: vaultPrefix("events.marinemists"),
      craftable: false,
      label: "Marine Mists",
      startdescription:
        "Today the marine layer is thick as pea soup, and the feeble, ailing sun is not enough to cut through it. We can pierce it with Lantern or intuit the route with Knock.",
    },
    success: [
      { recipe: { requirements: { lantern: 4 } }, suffix: "lantern" },
      { recipe: { requirements: { knock: 4 } }, suffix: "knock" },
    ],
    successEnd: {
      label: "Navigate Through",
      description:
        "Guided by maps, and tools and what we can see with our eyes and beyond, we have found our way out of the mistbank without harm.",
    },
    failure: {
      label: "A Scrape on the Bow",
      description:
        "A horrid groan, and our worries were proven true. We had guessed our location incorrectly, or the sandbanks shifted further than we thought. Now we are oriented again, though our ship's hull bleeds.",
      mutations: [
        {
          filter: "mariner.ship",
          mutate: "mariner.ship.damage",
          level: 1,
          additive: true,
        },
      ],
      alt: [SINK_SHIP],
    },
  });
}

// TASKS
export function generateMistedReefsLayer(vb) {
  vb.defineLayer("2");

  vb.defineHelpElement({
    id: "distance1",
    icon: "mariner.vaults.rescuemission.distance",
    label: "Past the Uncharted Waters",
    description: "A distance travelled, a leg traversed. [I have passed the Uncharted Waters.]",
    aspects: { forge: 2 },
  });

  vb.defineHelpElement({
    id: "distance2",
    icon: "mariner.vaults.rescuemission.distance",
    label: "Past the Hidden Currents",
    description: "A distance travelled, a leg traversed. [I have passed the Hidden Currents.]",
    aspects: { heart: 2 },
  });

  vb.defineExplorationTask({
    taskId: "mistedreefs",
    locationLabel: "Misted Reefs",
    icon: "mariner.vaults.rescuemission.mistedreefs",
    locationDescription: `It is no wonder the ${SHIP_NAME} went down here. The fractured rocks peek from the water like broken teeth, but only when they are not covered by a suffocating sea fog.`,
    tasks: ["unchartedwaters", "hiddencurrents", "traveltime"],
    events: ["events.fairweather", "events.windrises", "events.marinemists"],
    next: [{ id: "mariner.applyshipnameid" }],
  });

  vb.defineTask({
    taskId: "unchartedwaters",
    label: "Uncharted Waters",
    icon: "tasks/mariner.tasks.research.blue",
    description:
      "These are the Waters that no company cared to map, where the sun doesn’t care to shine and where the shallows often shift. ",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Chart the Uncharted",
            startdescription: `Waters that no company cared to map, where the sun doesn’t care to shine and shallows shift often. And here I have to find the unhallowed brine where the ${SHIP_NAME} found its demise.`,
          },
          success: {
            label: "We Know The Way",
            description:
              "By our knowledge of the hidden stars above or what we learned from the locals back in town, we do not lose our way.",
            effects: vb.vp({ distance1: 1 }),
          },
          failure: {
            label: "Lose our Way",
            startdescription:
              "What I thought was a lighthouse turns out to be a parked automobile. We are not where we should go, and must try again. But first, we drift into eventful waters",
            warmup: 30,
            linked: [
              {
                id: vb.vp("events.fairweather"),
                chance: 33,
              },
              {
                id: vb.vp("events.windrises"),
                chance: 50,
              },
              {
                id: vb.vp("events.marinemists"),
              },
            ],
          },
          template: probabilityChallengeTemplates.PERCEIVE,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
    },
  });

  vb.defineTask({
    taskId: "hiddencurrents",
    label: "Hidden Currents",
    icon: "tasks/mariner.tasks.waves.pink",
    description:
      "Among these many fractured islands in this fractal coast, the currents are unpredictable and strange. Our path leads us to a narrow passage way filled with churning water and gleaming rocks.",
    actions: {
      "mariner.sail": {
        hint: {
          label: "Avoid the Rapids",
          startdescription:
            "There is a navigable channel, but it is narrow... I know the Kite can traverse it, if my sailors follow my commands.",
          requirements: { "mariner.ship": -1 },
        },
        challenge: {
          task: {
            label: "Avoid the Rapids",
            startdescription:
              "There is a navigable channel, but it is narrow, and like Scylla and Charybdis, there are dangers on either side. My crew needs to act precise, and ordered, and disciplined. We can make it through, if they follow my command to the inch. ",
            requirements: { "mariner.ship": 1 },
          },
          success: {
            label: "With Calculated Precision",
            description:
              "With movements precise as a marshall's measurements, we navigate the channel. Orders flow down the chain of command, and together we are pulled out of the danger. ",
            effects: vb.vp({ distance2: 1 }),
          },
          failure: {
            label: "Mayhem and Misfortune",
            description:
              "It did not happen right away, but order slips to chaos. Before I know it, orders are opposed, a sail flaps aimlessly in the wind, and my ship crashes upon the rocks.",
            mutations: [
              {
                filter: "mariner.ship",
                mutate: "mariner.ship.damage",
                level: 1,
                additive: true,
              },
            ],
          },
          template: probabilityChallengeTemplates.COMMAND,
        },
        slots: [
          ...SHIP_MANOEUVERING_SLOTS(),
          {
            id: "shipslot",
            label: "My Ship",
            description: "To travel the sea, I require my vessel",
            required: { "mariner.ship": 1 },
          },
        ],
      },
    },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("hintneither"),
    label: "The Final Leg?",
    startdescription:
      "There are steps on a journey that one must take, before a destination is reached. I know I must pass through uncharted waters, and brave the rapid torrents between the isles. If I haven't yet, I am not yet at my destination.",
    requirements: vb.vp({ distance1: -1, distance2: -1, traveltime: 1 }),
    actionId: "mariner.sail",
    hintonly: true,
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("hintuncharted"),
    label: "The Final Leg?",
    startdescription:
      "There are steps on a journey that one must take, before a destination is reached. I have passed throug the uncharted waters, but not yet braved the rapids.",
    requirements: vb.vp({ distance1: -1, distance2: 1, traveltime: 1 }),
    actionId: "mariner.sail",
    hintonly: true,
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("hintrapids"),
    label: "The Final Leg?",
    startdescription:
      "There are steps on a journey that one must take, before a destination is reached. I have braved the rapids, but not yet passed the uncharted waters.",
    requirements: vb.vp({ distance1: 1, distance2: -1, traveltime: 1 }),
    actionId: "mariner.sail",
    hintonly: true,
  });

  vb.defineTask({
    taskId: "traveltime",
    label: "The Distance",
    icon: "tasks/mariner.tasks.path.vertdegris",
    description: "There are steps on a journey that must be taken, before a destination is reached.",
    actions: {
      "mariner.sail": {
        challenge: {
          task: {
            label: "The Final Leg",
            startdescription:
              "I have found the path, I am past the challenges. Now just to sail to the supposed site of the wreck, and choose a spot to dive.",
            requirements: vb.vp({ distance1: 1, distance2: 1 }),
          },
          success: {
            label: "The Vacinity",
            description:
              "We have arrived, as much as one can when the precise destination is only an estimation. Now follows the gamble which all our preparation has lead towards.",
            effects: vb.vp({ fractalcoast: 1, distance1: -1, distance2: -1 }),
          },
          failure: {
            label: "Twisted 'Round",
            description:
              "The winds blow unfavorably, and my crew sails unresponsibly, and before we know, we are further behind than we are ahead. I will have to take a part of the journey again. ",
            effects: vb.vp({ distance1: -1, distance2: -1 }),
          },
          template: probabilityChallengeTemplates.OVERTAKE,
        },
        slots: [
          ...CHALLENGE_SLOTS(5, "mariner.sail"),
          {
            id: "shipslot",
            label: "My Ship",
            description: "To travel the sea, I require my vessel",
            required: { "mariner.ship": 1 },
          },
        ],
      },
    },
  });

  vb.defineTask({
    taskId: "fractalcoast",
    label: "A Fractal Coast",
    icon: "tasks/mariner.tasks.shoreline.yellow",
    description:
      "A distress signal was sent, somewhere from this fractured fractal coast. Can I find where it was send from before there is no more reason to find it?",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Decide The Spot",
            startdescription:
              "Now we either gather the hints and clues we have gathered so far, or we must act on our own intellect, and find the spot the survivors remain.",
          },
          success: {
            label: "A Decision Made",
            description: "We have tallied the facts, and discarded what we view as fiction. ",
            effects: vb.vp({
              locationright: 1,
            }), //help items used should be purged
          },
          failure: {
            label: "A Decision Made ",
            description: "We have tallied the facts, and discarded what we view as fiction.",
            effects: vb.vp({}), // help item used should be purged
          },
          template: probabilityChallengeTemplates.DESIGN,
        },
        slots: CHALLENGE_SLOTS(6, "mariner.navigate"),
      },
    },
  });

  vb.defineTask({
    taskId: "locationright",
    label: "An Unassuming Island",
    icon: "tasks/mariner.tasks.observe.green",
    description: "One island among the many, which our triangulations have divined as the location of the survivors.",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Scour the Shoreline",
            startdescription:
              "Now we have identified the limit, we must look for a place to land, and the best place from which to approach the survivors.",
          },
          success: {
            label: "Footprints and Broken Branches",
            description:
              "We have found the traces on the shore that point to sentient life, scrapping for survival. If we follow this trail, we might have a shot at finding them. ",
            effects: vb.vp({ locationright: -1, shore: 1 }),
            purge: { [vb.currentLayerAspectId()]: 10 },
          },
          failure: {
            label: "No Sign of Life",
            description:
              "It takes us the better part of the day before we spot anything, and a the mindnumming sameness of staring at the muted shores under the slate sky has made my half-heart shiver and my lack-heart itch. I must find some stimulation",
            effects: { "mariner.wanderlust": 1 },
          },
          template: probabilityChallengeTemplates.STUDY,
        },
        slots: [...CHALLENGE_SLOTS(3, "mariner.navigate")],
      },
    },
  });

  vb.defineTask({
    taskId: "locationwrong",
    label: "An Unassuming Island",
    icon: "tasks/mariner.tasks.observe.green",
    description: "One island among the many, which our triangulations have divined as the location of the survivors.",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Scour the Shoreline",
            startdescription:
              "Now we have identified the limit, we must look for a place to land, and the best place from which to approach the survivors.",
          },
          success: {
            label: "Reconsider our Calculations",
            description:
              "We prepare for a long day, and decide to go over our calculations one more time. Then one crewmember spots an error in our triangulations, and with the inevitability of an iceberg we realize we are at the wrong island. Back to the drawing board.",
            effects: vb.vp({ locationwrong: -1, fractalcoast: 1 }),
          },
          failure: {
            label: "A Fruitless Day",
            description:
              "All day we peer at the shore, and are nerves are fraying at the end. At the end we have to admit our calculations might not be as sure as we assumed. We have to go back to the drawing board.",
            effects: { "mariner.wanderlust": 1, [vb.vp("fractalcoast")]: 1 },
          },
          template: probabilityChallengeTemplates.STUDY,
        },
        slots: [...CHALLENGE_SLOTS(3, "mariner.navigate")],
      },
    },
  });
}
