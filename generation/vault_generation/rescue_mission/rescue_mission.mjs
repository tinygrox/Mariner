import { generateHarborEvents, generateHarborLayer } from "./harbor.mjs";
import {
  generateMistedReefsEvents,
  generateMistedReefsLayer,
} from "./misted_reefs.mjs";
import { generateIslandLayer } from "./island.mjs";
import { VaultBuilder } from "../vaultbuilder.mjs";

export const ELEMENTS_FILE = "vaults.rescuemission";
export const RECIPES_FILE = "recipes.vaults.rescuemission";
export const ASPECTS_FILE = "aspects.vaults.rescuemission";

export const vaultPrefix = (end) => `mariner.vaults.rescuemission.${end}`;

export function generateRescueMissionVault() {
  const vb = new VaultBuilder("rescuemission", null, true);

  generateHarborEvents(vb);
  generateHarborLayer(vb);

  generateMistedReefsEvents(vb);
  generateMistedReefsLayer(vb);

  generateIslandLayer(vb);
}
