import { ENUM_LABELS } from "../../../generate_enums.mjs";
import { LOCAL_REPUTATION, LORE_ASPECTS, SPAWN_SUSPICION, requireAndDestroy } from "../../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../probability_challenge_templates.mjs";
import { UPDATE_QUEST } from "../../../quest_generation.mjs";
import { CHALLENGE_SLOTS } from "../../../slots_helpers.mjs";
import { generateProbabilityChallengeTask } from "../../vault_generation.mjs";
import { ELEMENTS_FILE, RECIPES_FILE, ASPECTS_FILE } from "./eksters_nest.mjs";

export function generateAuntie(vb) {
  // AUNTIE
  mod.setHiddenAspect(ASPECTS_FILE, {
    id: vb.vp("auntie.trigger.changestate"),
  });
  mod.setHiddenAspect(ASPECTS_FILE, { id: vb.vp("auntie.trigger.awaken") });
  mod.setHiddenAspect(ASPECTS_FILE, { id: vb.vp("auntie") });
  mod.setElement(ELEMENTS_FILE, {
    id: vb.vp("auntie.asleep"),
    icon: "tasks/mariner.tasks.magpie.asleep.goldenyellow",
    label: "Auntie Ekster [Asleep]",
    description:
      "A middle-aged woman with magpie-hair lies in a puddle of cushions, coats and shawls. From a thousand pockets blink a million little treasures. Her breathing is softer than a whisper. Should I just take what I want? Or do I wish to negotiate with Auntie Ekster.",
    aspects: {
      "mariner.local": 1,
      "mariner.aspects.vaults.ekstersnest.layer.3": 1,
      modded_explore_allowed: 1,
      modded_talk_allowed: 1,
      [vb.vp("auntie")]: 1,
    },
    xtriggers: {
      [vb.vp("auntie.trigger.changestate")]: vb.vp("auntie.awake"),
      [vb.vp("auntie.trigger.awaken")]: vb.vp("auntie.awake"),
    },
    slots: CHALLENGE_SLOTS(3, "explore"),
  });
  mod.setElement(ELEMENTS_FILE, {
    id: vb.vp("auntie.awake"),
    icon: "tasks/mariner.tasks.magpie.awake.goldenyellow",
    label: "Auntie Ekster [Awake]",
    description:
      "Peering at me from beneath a giant overcoat is a bright-eyed woman with magpie-hair. From a thousand pockets blink a million little treasures. Her fingers are never still, her mouth is never smiling. Should I attempt to negotiate with her? Or do I wish to lull her into a more comfortable state.",
    aspects: {
      "mariner.local": 1,
      "mariner.aspects.vaults.ekstersnest.layer.3": 1,
      "mariner.sing_allowed": 1,
      modded_talk_allowed: 1,
      [vb.vp("auntie")]: 1,
    },
    xtriggers: {
      [vb.vp("auntie.trigger.changestate")]: vb.vp("auntie.asleep"),
    },
    slots: [
      ...CHALLENGE_SLOTS(3, "mariner.sing"),
      {
        id: "desire",
        label: "Bargaining Trapping",
        description: "I could bargain for a Trade with Auntie Ekster. But do I have what she desires?",
        required: {
          "mariner.trapping": 1,
        },
      },
    ],
  });

  // Generate Auntie
  // She's spawned asleep, and if the rep is high enough, awoken by a trigger
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("updateauntiestate"),
    grandReqs: { [LOCAL_REPUTATION]: 2, [vb.vp("auntie.asleep")]: 1 },
    aspects: { [vb.vp("auntie.trigger.awaken")]: 1 },
  });

  // Auntie (asleep)
  generateProbabilityChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "explore",
    activityAspects: { "mariner.aspects.vaults.ekstersnest.activelayer.3": 1 },
    bypass: requireAndDestroy(vb.vp("birdeyedaid")),
    task: {
      id: vb.vp("auntie.steal.task"),
      label: "Steal the Ring out of Auntie's Hoard?",
      startdescription:
        "Somewhere in the folds of her clothing, The Ladies’ stolen ring must be kept. She is sleeping so soundly... We could just run our hands through her pockets like wandering spiders until we find what we need. If she wakes up however, we might lose more than finger.",
      requirements: { [vb.vp("auntie.asleep")]: 1 },
    },
    success: {
      label: "Fingers Found a Ring",
      description:
        "Light as a breeze, we found what we need without disturbing as much as a single lock of her black and white hair. I close my fist around the ring. We are ready to leave.",
      furthermore: [
        UPDATE_QUEST("amsterdamsewers", "ringstolen"),
        {
          effects: { [vb.vp("auntie.asleep")]: -1, [vb.vp("ring")]: 1 },
        },
      ],
    },
    failure: {
      label: "The Dragon Wakes",
      description:
        "Before we even laid a finger on her, her hands snapped like a vice around our wrists. Fierce blue eyes blink furiously and she bears her teeth like a fox.",
      effects: { "mariner.notoriety": 1 },
      aspects: { [vb.vp("auntie.trigger.changestate")]: 1 },
      inductions: [SPAWN_SUSPICION],
    },
    template: probabilityChallengeTemplates.AVOID,
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("auntie.wakeup.task"),
    requirements: { [vb.vp("auntie.asleep")]: 1 },
    craftable: true,
    actionId: "talk",
    warmup: 1,
    label: "Wake up Auntie Ekster?",
    startdescription:
      "If I have faith in my powers of negotiation, I might be better off waking Auntie up. Her desires are strange however and fickle. I must be sure I have the thing that interests her today.",
    description:
      "Carefully, we break the Haze-induced slumber. She wakes slowly, as if emerging from great depths. But as her eyes go from muddy to clear to crystal, she bears her teeth like a wolf.",
    aspects: { [vb.vp("auntie.trigger.changestate")]: 1 },
  });

  // Auntie (awake)
  mod.setRecipe("recipes.vaults.ekstersnest", {
    actionId: "talk",
    hintonly: true,
    id: "mariner.vaults.ekstersnest.auntie.awake.hint",
    label: "Curious Eyes",
    startdescription:
      "Auntie always desires something. Her patrons have made her so. If I offer her the thing she wants she may grant me what I need",
    requirements: {
      [vb.vp("auntie.awake")]: 1,
      "mariner.trapping": -1,
    },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: "auntie.bargain.checkdesire",
    craftable: true,
    actionId: "talk",
    label: "Negotiate for the Ring?",
    startdescription:
      "Auntie Ekster is far on the path of the Beachcrow, and no longer allowed to expend coins of any kind. She mostly spends her time stealing from peoples' dreams now. Yet she must still make specific sacrifices for the Beachcombers treasure hoard.",
    requirements: { [vb.vp("auntie.awake")]: 1, "mariner.trapping": 1 },
    warmup: 5,
    linked: [
      {
        id: vb.vp(`auntie.bargain.checkdesire.*`),
      },
      { id: "auntie.bargain.checkdesire.failure" },
    ],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: "auntie.bargain.checkdesire.failure",
    label: "No Interest",
    startdescription: "She shoves my offering off the table with her foot, and turns away. The message is clear.",
    effects: { "mariner.notoriety": 1 },
    inductions: [SPAWN_SUSPICION],
  });

  for (const aspect of LORE_ASPECTS) {
    mod.setRecipe(RECIPES_FILE, {
      id: vb.vp(`auntie.bargain.checkdesire.${aspect}`),
      grandReqs: {
        [`[mariner.auntieekstersdesire]=[root/${ENUM_LABELS.principles[aspect]}]`]: 1,
        [`mariner.trappings.${aspect}`]: 1,
      },
      furthermore: [
        UPDATE_QUEST("amsterdamsewers", "ringcollected"),
        {
          effects: {
            "mariner.trapping": -1,
            [vb.vp("ring")]: 1,
            [vb.vp("auntie.awake")]: -1,
          },
        },
      ],
      label: "To Own and to Possess",
      description:
        "She clicks her tongue excitedly, and indicates for me to put it down next to a gilded shovel and a wad of hair. From somewhere on her person, a little shiny band appears. She will not give it to me. But I can take it from her hand.",
    });
  }

  // Put her to sleep
  generateProbabilityChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.sing",
    activityAspects: { "mariner.aspects.vaults.ekstersnest.activelayer.3": 1 },
    task: {
      id: vb.vp("auntie.puttosleep.task"),
      label: "Lull Auntie Ekster to sleep?",
      startdescription:
        "Auntie Ekster has marinated in the Sweetapple Haze for a long time. She is resistant to it, but not immune. If I employ the right words to the right rhythm with the right intentions, I could break down her defences.",
      requirements: { [vb.vp("auntie.awake")]: 1 },
    },
    success: {
      label: "Intoxicating Lullaby",
      description:
        "It doesn’t take much. My words simply guide the mind to the state the body is already mariner.grabpassengersonarrival. Around me, all the scattered crowd is hush, even breath gone shallow. My crew remove the plugs from his ear.",
      aspects: { [vb.vp("auntie.trigger.changestate")]: 1 },
    },
    failure: {
      label: "Sandman Rejected",
      description:
        "Klaas Vaak, that local Mask of the Vagabond could not load her eyes with desert-sand this time. Auntie Ekster furrows her brow and purses her lips. She knows what I just attempted, and she is not amused.",
      effects: { "mariner.notoriety": 1 },
      inductions: [SPAWN_SUSPICION],
    },
    template: probabilityChallengeTemplates.TAMPER,
  });
}
