import { INJURE_CREWMEMBER, requireAndDestroy } from "../../../../generation_helpers.mjs";
import { generateTresholdChallengeTask } from "../../../vault_generation.mjs";
import { RECIPES_FILE, vaultPrefix } from "../eksters_nest.mjs";

// EVENTS
function generateFoulVapors() {
  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.winter",
    task: {
      id: vaultPrefix("events.foulvapors"),
      craftable: false,
      label: "Foul Vapors",
      startdescription:
        "An unhealthy miasma has collected here. It will take a hale physique or detached determination to get through. [Approach this challenge with Heart or Winter.]",
    },
    success: [
      { recipe: { requirements: { heart: 4 } }, suffix: "heart" },
      { recipe: { requirements: { winter: 4 } }, suffix: "winter" },
    ],
    successEnd: {
      label: "Push Through",
      description:
        "Our eyes may have watered and our throats may have heaved, but we found our way to fresh air again. Sort of. As we blink away the effects, we notice strange symbols scrawled on the walls.",
      inductions: [{ id: vaultPrefix("events.collectclue") }],
    },
    failure: {
      label: "Coughing Like Hags",
      description: "It's in our nose, our eyes, our mouths. It slithers its way down our gullet and boils our guts.",
      inductions: [INJURE_CREWMEMBER, { id: vaultPrefix("events.collectclue") }],
    },
  });
}

function generateFlashFlood() {
  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.forge",
    task: {
      id: vaultPrefix("events.flashflood"),
      craftable: false,
      label: "Flash Flood",
      startdescription:
        "There is little warning, but then a wave shakes the floor and the water surges to the ceiling and it is all we can do to stay standing. As the water lowers, we spot a clue to the Bazaar enterence we had missed before. [Approach this challenge with Forge or Grail.]",
    },
    success: [
      { recipe: { requirements: { forge: 4 } }, suffix: "forge" },
      { recipe: { requirements: { grail: 4 } }, suffix: "grail" },
    ],
    successEnd: {
      label: "Part the water",
      description: "Like a stone in the surf, the water passes around us.",
      alt: [{ id: vaultPrefix("events.collectclue"), additional: true }],
    },
    failure: {
      label: "Lost!",
      description:
        "Trampled by the waves, we were left with the other detritus. Where are we now? We will have to start our mapping again.",
      purge: { [vaultPrefix("clue")]: 1 },
    },
  });
}

function generateSmugglingCrew() {
  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.moth",
    task: {
      id: vaultPrefix("events.smugglingcrew"),
      craftable: false,
      label: "Smuggling Crew",
      startdescription:
        "We turn a corner, and come eye to eye with a clandestine crew on a caper. How do we explain our presence here? How do we ease the tensions? [Approach this challenge with Grail or Moth or spend a funds to buy their silence.]",
    },
    bypass: requireAndDestroy("funds"),
    slots: [
      {
        id: "cash",
        label: "Help or Cash",
        description: "Cash to make em go away, or whatever it takes to overcome this challenge",
        required: {
          funds: 1,
          "mariner.crew": 1,
          "mariner.song": 1,
          tool: 1,
          "mariner.instrument": 1,
          "mariner.inspiration.nature": 1,
        },
      },
    ],
    success: [
      { recipe: { requirements: { grail: 4 } }, suffix: "grail" },
      { recipe: { requirements: { moth: 4 } }, suffix: "moth" },
    ],
    successEnd: {
      label: "Smoothed Over",
      description:
        '"We never saw you, you never saw us, goedendag". There is a scoundrels\' code that is universal. I suspect that extends to the Magpie-Nest beyond the Bazaar. They tip their hats to us, and tell us to watch the celing closely here. Soon, we see why...',
      alt: [{ id: vaultPrefix("events.collectclue"), additional: true }],
    },
    failure: {
      label: "Unpleasant Partings",
      description:
        "We parted, but not on good terms. I could hear them crowing in guttural gangster-dutch as they departed. I suspect the Nest will hear of the strangers exploring the sewers. Luckily, we saw them scan the ceiling, and with a little searching, we find a clue to the entrance of the Bazar.",
      effects: { "mariner.notoriety": 1 },
      alt: [{ id: vaultPrefix("events.collectclue"), additional: true }],
    },
  });
}

export function generateSewersEvents() {
  mod.setRecipe(RECIPES_FILE, {
    id: vaultPrefix("events.collectclue"),
    actionId: "mariner.event.knock",
    label: "Clue Uncovered",
    description: "I have found part of the Labyrinths' Riddle, and I am one step closer to its solution.",
    effects: { [vaultPrefix("clue")]: 1 },
  });
  generateFoulVapors();
  generateFlashFlood();
  generateSmugglingCrew();
}
