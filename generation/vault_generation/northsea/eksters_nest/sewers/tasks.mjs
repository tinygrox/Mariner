import { RECIPES_FILE, ASPECTS_FILE, vaultPrefix } from "../eksters_nest.mjs";
import { generateLabyrinthsEnd } from "./tasks/labyrinthsend.mjs";
import { generateOthersUnderground } from "./tasks/othersunderground.mjs";
import { generateRiddlesOnTheWall } from "./tasks/riddlesonthewall.mjs";
import { generateHiddenStash } from "./tasks/hiddenstash.mjs";
import { VaultBuilder } from "../../../vaultbuilder.mjs";

// TASKS
export function generateSewersLayer(vb) {
  vb.defineLayer("1");

  vb.defineHelpElement({
    id: "clue",
    label: "Clue",
    icon: vb.vp("clue"),
    description:
      "A piece of poetry, a scratched symbol. Determining the password into the Bazar requires skill, knowledge and ingenuity.",
  });

  vb.defineHelpElement({
    id: "silverflorin",
    label: "Silver Florin",
    description: '"Money opens all doors" is not just an aphorism here.',
    icon: "cash",
  });

  vb.defineExplorationTask({
    taskId: "sewerlabyrinth",
    locationLabel: "Sewer Labyrinth",
    locationDescription:
      "The network of canals and waterways stretches as much below ground as above. Hidden somewhere here is a secret market selling illicit goods. From there operates a gang of adept-thieves who specialise in owning what has been dispossessed.",
    tasks: ["riddlesonthewalls", "othersunderground", "hiddenstash"],
    label: "Wander the Sewers To Find The Entrance",
    description:
      "We wander these lightless tunnels and stinking canals. In the gloom and the murk and the muck, it is hard to tell one corner from another. I do not know what we will find ahead of us, and I am unsure if I can find what we left behind.",
    preRecipe: {
      purge: {
        [vb.currentLayerAspectId()]: 5,
      },
    },
    additionalPreLinked: [
      {
        id: vb.vp("foundlabyrinthsend"),
      },
    ],
    slots: [
      {
        actionId: "explore",
        required: { [vb.vp("clue")]: 1 },
        id: "clueslot1",
        label: "Clue to the Riddle",
        description:
          "A map split into parts. the more clues I find, the more likely I am to reach the labyrinth's conclusion.",
      },
      {
        actionId: "explore",
        required: { [vb.vp("clue")]: 1 },
        id: "clueslot2",
        label: "Clue to the Riddle",
        description:
          "A map split into parts. the more clues I find, the more likely I am to reach the labyrinth's conclusion.",
      },
      {
        actionId: "explore",
        required: { [vb.vp("clue")]: 1 },
        id: "clueslot3",
        label: "Clue to the Riddle",
        description:
          "A map split into parts. the more clues I find, the more likely I am to reach the labyrinth's conclusion.",
      },
      {
        actionId: "explore",
        required: { [vb.vp("clue")]: 1 },
        id: "clueslot4",
        label: "Clue to the Riddle",
        description:
          "A map split into parts. the more clues I find, the more likely I am to reach the labyrinth's conclusion.",
      },
      {
        actionId: "explore",
        required: { [vb.vp("clue")]: 1 },
        id: "clueslot5",
        label: "Clue to the Riddle",
        description:
          "A map split into parts. the more clues I find, the more likely I am to reach the labyrinth's conclusion.",
      },
      {
        actionId: "explore",
        required: { [vb.vp("clue")]: 1 },
        id: "clueslot6",
        label: "Clue to the Riddle",
        description:
          "A map split into parts. the more clues I find, the more likely I am to reach the labyrinth's conclusion.",
      },
    ],
    events: ["events.foulvapors", "events.flashflood", "events.smugglingcrew"],
    resetOnExhaustion: true,
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("foundlabyrinthsend"),
    label: "Found the Entrance",
    startdescription:
      "We finally found an entrance, after discarding the wheat from the chaff in the clues we found. The entrance is well hidden, and if i wander of, I may lose track of its location again. But if I wish to press forwards... I still have to get past the Bouncer of the Bazaar.",
    warmup: 10,
    requirements: {
      [vb.vp("clue")]: 6,
    },
    effects: {
      [vb.vp("clue")]: -3,
      [vb.vp("labyrinthsend")]: 1,
      [vb.currentActiveLayerAspectId()]: -1,
    },
  });

  generateRiddlesOnTheWall(vb);
  generateOthersUnderground(vb);
  generateLabyrinthsEnd(vb);
  generateHiddenStash(vb);
  mod.setAspect(ASPECTS_FILE, { id: `mariner.flag.smuggler.draw` });
}
