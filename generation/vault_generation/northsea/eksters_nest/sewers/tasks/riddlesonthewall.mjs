import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";

export function generateRiddlesOnTheWall(vb) {
  vb.defineTask({
    taskId: "riddlesonthewalls",
    label: "Riddles on the Walls",
    icon: "tasks/mariner.tasks.concentrate.vertdegris",
    description:
      "To ensure the wrong people can’t find the bazaar, the entrance is hidden. To ensure the right people can enter it, they left clues to find your way in.",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Crack the Code",
            startdescription:
              "Maybe this strange sign is a secret riddle to help you locate the bazaar. Maybe it's just a strange set of scratches.",
          },
          success: {
            label: "An Answer?",
            description: "I have concocted an answer to the riddle. It rings true in my mind. I have faith in my abilities.",
            effects: vb.vp({ clue: 2, riddlesonthewalls: -1 }),
          },
          failure: {
            label: "A Flurry of Folly",
            description:
              "My head connects the dots in many ways, but none of them make sense. All answers I contort from these clues are laughable, and I fear my giggles may turn to sobs.",
            effects: {
              "mariner.experiences.vivid.standby": 1,
              [vb.vp("riddlesonthewalls")]: -1,
            },
          },
          template: probabilityChallengeTemplates.DESIGN,
        },
      },
    },
  });
}
