import { INJURE_CREWMEMBER, LOCAL_REPUTATION } from "../../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../probability_challenge_templates.mjs";
import { generateTresholdChallengeTask, thresholdReqs } from "../../vault_generation.mjs";
import { generateAuntie } from "./auntie.mjs";
import { RECIPES_FILE } from "./eksters_nest.mjs";

export function generateEksternestEvents(vb) {
  // HAZE TOKEN
  mod.setRecipe(RECIPES_FILE, {
    actionId: "mariner.haltable.haze",
    id: vb.vp("token.haze.appears"),
    label: "The Haze Accumulates",
    startdescription: "The rare herb hangs in the air. its sweetness could overcome one of us at any moment.",
    description:
      "An imprint of a hand on their cheek or a face soaked in liquid. We do what we must to wake the fallen up. Quickly.",
    warmup: 120,
    linked: [{ id: vb.vp("token.haze.windup") }],
  });
  mod.setRecipe(RECIPES_FILE, {
    actionId: "mariner.haltable.haze",
    id: vb.vp("token.haze.windup"),
    label: "Cinnamon and Cinnabar",
    startdescription: "The haze coalesces: The air tastes red. The air tastes of wonder. The air tastes of more.",
    description:
      "An imprint of a hand on their cheek or a face sooked in liquid. We do what we must to wake the fallen up. Quickly.",
    warmup: 60,
    linked: [{ id: vb.vp("token.haze.waredoff") }, { id: vb.vp("token.haze.attack") }],
  });
  mod.setRecipe(RECIPES_FILE, {
    actionId: "mariner.haltable.haze",
    id: vb.vp("token.haze.waredoff"),
    label: "Clear and Cleansing",
    startdescription: "Like by the piercing rays of dawn, the fog of our mind is lifted before it can obscure our judgements.",
    extantreqs: {
      [vb.vp("crystaltoneward")]: 1,
    },
    description:
      "An imprint of a hand on their cheek or a face sooked in liquid. We do what we must to wake the fallen up. Quickly.",
    warmup: 30,
    linked: [{ id: vb.vp("token.haze.windup") }],
  });
  mod.setRecipe(RECIPES_FILE, {
    actionId: "mariner.haltable.haze",
    id: vb.vp("token.haze.attack"),
    label: "A Mind Overcome",
    startdescription: "The haze has grown too potent. One of mine will slip away from us to a more pleasant place.",
    description:
      "An imprint of a hand on their cheek or a face sooked in liquid. We do what we must to wake the fallen up. Quickly.",
    warmup: 30,
    linked: [{ id: vb.vp("token.haze.windup") }],
    slots: [
      {
        id: "victim",
        label: "Mind Aflutter",
        description: "This one is on the floor, eyes on infinity. If we carry them out quickly, they might be saved.",
        greedy: true,
        required: { "mariner.crew": 1 },
      },
    ],
  });

  // WAVES OF DELIRIUM
  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.lantern",
    task: {
      id: vb.vp("events.wavesofdelirium"),
      craftable: false,
      label: "Waves of Delirium",
      startdescription:
        "Some infernal vortex in the air has blown a stronger draught of the Sweetapple Haze our way. The world shimmers, and a golden light starts to drip from the walls. I must protect my faculties, to not let a part of my sanity slip that may never return. [Approach this challenge with Lantern or Winter.]",
    },
    success: thresholdReqs(["lantern", "winter"]),
    successEnd: {
      label: "Visions Abate",
      description:
        "The air no longer flickers like broken glass, and the floor no longer roils like sand. I am returned to a world of crimson drapes and poppy-smoke.",
    },
    failure: {
      label: "Rising like Smoke",
      description:
        "My mind expands, and portions detach, pecked away. I am a treasure hoard of experiences, exposed to be plucked for the choicest parts.",
      effects: { "mariner.experiences.vivid.standby": 1 },
    },
  });

  // FLOCK OF GANGSTERS
  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.grail",
    task: {
      id: vb.vp("events.flockofgangsters"),
      craftable: false,
      label: "A Flock of Gangsters",
      startdescription:
        "One moment all is sleepy and still in the Nest, the next there is a fluttering of coats and a flash of knives. The Birds are feeling territorial. There is no escape here. We have to placate or intimidate them. [You can approach this problem with Winter or Grail.]",
    },
    success: thresholdReqs(["grail", "winter"]),
    successEnd: {
      label: "The Return to Rest",
      description:
        "Slowly, warely, they all slink back to their roost, sink back to their pillows on the floor. They dare not approach us now. But if my reputation grows, they may be emboldened further.",
    },
    failure: {
      warmup: 15,
      label: "The Eyes Of Unimpressed",
      description:
        "Our airs did not impress them, or else they were too far gone to notice. We are shown to the door, and some even go as far as approaching us, studying our fighting capacity.",
      effects: { "mariner.notoriety": 1 },
      inductions: [{ id: vb.vp("events.battleintheden") }],
    },
  });

  // BATTLE IN THE DEN
  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.edge",
    task: {
      id: vb.vp("events.battleintheden"),
      craftable: false,
      grandReqs: { [LOCAL_REPUTATION]: 3 },
      label: "A Battle in the Den",
      startdescription:
        "At once the air turns from languid to nightmarish. The gangster's eyes are wide and feverish. This place is not made for it, but soon the din of battle will overpower the shuffling of the beaded curtains. [Approach this challenge with Edge or Forge.]",
    },
    success: thresholdReqs(["edge", "forge"]),
    successEnd: {
      label: "Eyes Dun and Bleeding",
      description:
        "Quiet returns to the Eksternest, as the bodies of the dying lie between the bodies of the dreaming, both staring up at the ceiling unseeing.",
    },
    failure: {
      label: "It is Done",
      description:
        "It is over, I could not obtain what I needed by stealth or subtlety. And now even through force I have failed. I am escorted out of the nest. I must return another time, when I am better prepared.",
      linked: [{ id: vb.vp("thrownout") }],
      inductions: [INJURE_CREWMEMBER],
    },
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("thrownout"),
    effects: vb.vp({ sunlessbazaar: 1 }),
    purge: {
      [vb.vp("nest")]: 1,
      "mariner.aspects.vaults.ekstersnest.layer.3": 5,
    },
    haltverb: { "mariner.haltable.*": 10 },
  });
}

export function generateEksternestLayer(vb) {
  // Layer  elements
  vb.defineLayer("3");

  // Items
  vb.defineHelpElement({
    id: "birdeyedaid",
    label: "Bird-eyed Aid",
    description:
      "A pocket was singled out to me, on Aunties grey overcoat. If I catch her unaware, I could pilfer it quick and easy.",
  });

  vb.definePassiveHelpElement({
    id: "crystaltoneward",
    label: "A Crystal-Tone Ward",
    description: "There is only one thing influencing the mind of my crew, and that is their captain.",
    lifetime: 600,
  });

  // Tasks
  vb.defineExplorationTask({
    taskId: "nest",
    locationLabel: "The Eksternest",
    locationDescription:
      "Loaded with pillows and beaded curtains, this is a cozy hole in the ground in which to languish with your treasures. The air has a shiver from more than just the narcotic smoke. This place is beneath the gaze of the Hours.",
    tasks: ["littlebirds", "haze", "auntie.asleep"],
    events: ["events.wavesofdelirium", "events.flockofgangsters"],
    next: [{ id: vb.vp("updateauntiestate") }],
  });

  // LITTLE BIRDS
  vb.defineTask({
    taskId: "littlebirds",
    label: "Auntie Little Birds",
    icon: "tasks/mariner.tasks.crowd.vertdegris",
    description:
      "She calls them her little sparrows, or swallows, or doves. The little Birds are wide-eyed boys and girls, Auntie has a place for the misplaced and the overlooked. They congregate here, bringing little trinkets, and then fade into pillows, staring silently.",
    actions: {
      talk: {
        challenge: {
          task: {
            label: "Tame the Birds",
            startdescription:
              "These strange little creatures will know where Auntie has hidden the trinket I seek. But they are as wily as the hour they follow, with hungry eyes and clever minds. I will have to trick them.",
          },
          success: {
            label: "A Little Game",
            description:
              "I challenge them to puzzles, trickier each time. They delight in beating me, slowly waking up from the hazy stupor. When their eyes shine bright and their cheeks dimple, I ask my final question. A single pointed finger is the answer.",
            effects: vb.vp({ birdeyedaid: 1 }),
          },
          failure: {
            label: "Wary Eyes",
            description:
              "I get no response. They just stare at me, eyes large and empty. Then one shifts away from the flock and whispers something in Auntie's Ear.",
            effects: { "mariner.notoriety": 1 }, // TODO: wake up Auntie if she's asleep ? Another token to create?...
            furthermore: [
              {
                target: "~/exterior",
                aspects: {
                  [vb.vp("auntie.trigger.awaken")]: 1,
                },
              },
            ],
          },
          template: probabilityChallengeTemplates.FORKED_TONGUE,
        },
      },
    },
  });

  // HAZE TASK
  vb.defineTask({
    taskId: "haze",
    label: "The Sweetapple Haze",
    icon: "tasks/mariner.tasks.haze.darkred",
    description:
      "The only requirements for what is thrown on the pyres here, is that the ingredients are rare, and the sensations they bring even rarer. This peculiar blend is known to include flowers from the Rending Mountains, and an effusion found on hands of certain dreamers.",
    actions: {
      "mariner.sing": {
        challenge: {
          task: {
            label: "Ward Our Mind",
            startdescription:
              "Like mist creeping through streets and alleys, this smoke finds its way into passages of our brains. My Lack-Heart is bottomless, but my friends’ mind will be filled with fog soon.",
          },
          success: {
            label: "Clarify our Purpose",
            description:
              "Whispered, half-sung, I weave a protection around us made of rhythm and intention. The Haze will not take them today. Clear like a tuning fork, my voice cuts through the mental fog.",
            effects: { [vb.vp("crystaltoneward")]: 1 },
          },
          failure: {
            label: "Lost to the Haze",
            description:
              "As I sing, I breathe more deeply. As I breathe deeper, the Haze enters me more further. And so, my mind expands. Colors intensify, and the crow-shadow darkness becomes more stark. After a while, I noticed I have stopped singing.",
            effects: { "mariner.experiences.sophorific.standby": 1 },
          },
          template: probabilityChallengeTemplates.WARD,
        },
      },
    },
  });

  // AUNTIE
  vb.defineNonLocalElement({
    id: "ring",
    icon: "mariner.vaults.ekstersnest.ring",
    label: "A Misplaced Ring",
    description: "Someone very much wants this back.",
    unique: true,
    aspects: { "mariner.topic": 1 },
  });
  generateAuntie(vb);
}
