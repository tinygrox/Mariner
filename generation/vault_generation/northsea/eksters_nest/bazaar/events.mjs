import { SIGNALS } from "../../../../generate_signal_aspects.mjs";
import { UPGRADE_REPUTATION } from "../../../../port_generation/notoriety_generation.mjs";
import { generateTresholdChallengeTask } from "../../../vault_generation.mjs";
import { RECIPES_FILE, vaultPrefix } from "../eksters_nest.mjs";

// EVENTS TODO: it's been fixed, add it to the events or something
function generateUpgradeAmsterdamReputationEvent() {
  mod.setRecipe(RECIPES_FILE, {
    id: vaultPrefix("events.upgradeamsterdamreputation"),
    actionId: "mariner.upgradeamsterdamreputation",
    warmup: 10,
    label: "Rumours in Amsterdam",
    startdescription:
      "Gossip rises up from the sewer grates like odious airs, and just like with the smell, everyone finds it distasteful, but nobody can resist to comment.",
    description:
      "Tales of me, true or false, steadily are spread through the valiant city of the compassionate, Amsterdam.",
    slots: [
      {
        id: "reputation",
        greedy: true,
        label: "Infamy",
        description: "The reputation I gather here, true or false.",
        required: { "mariner.locations.northsea.amsterdam.uqg": 1 },
      },
    ],
    furthermore: UPGRADE_REPUTATION("northsea", "amsterdam"),
  });
}

function generateUncomfortableAcquaintance() {
  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.winter",
    task: {
      id: vaultPrefix("events.uncomfortableacquaintance"),
      craftable: false,
      label: "An Uncomfortable Acquaintance",
      startdescription:
        "A face in the crowd has lit up with recognition. With a raised hand they elbow their way towards me. Whatever we once went through, this person is not discrete. They could harm our covert status, what is left of it. [you can solve this issue with Moth or Winter]",
    },
    success: [
      { recipe: { requirements: { moth: 4 } }, suffix: "moth" },
      { recipe: { requirements: { winter: 4 } }, suffix: "winter" },
    ],
    successEnd: {
      label: "Brushed Off",
      description:
        "I turn my shoulder and disappear into the crowd, while our crew intercepts. This boisterous acquaintance is redirected, and our reputation is untarnished.",
    },
    failure: {
      label: "A conversation in echoes",
      description:
        "The bazaar is silenced by the booming voice of my acquaintance. They knew they remembered my face. They still remember that business in Kuala Lumpur, wasn’t that a lark. They want to know what I am up to these days, still the same schemes?",
      effects: { "mariner.notoriety": 1 },
    },
  });
}

function generateBazaarBrawl() {
  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.edge",
    task: {
      id: vaultPrefix("events.bazaarbrawl"),
      craftable: false,
      label: "Bazaar Brawl",
      startdescription:
        "All commerce is warfare and all warfare occurs at the edge. Therefore there is always a tension in the air, and the room is ready to boil over. When this occurs, it's time to square up or get out. [Approach this challenge with Edge or Moth.]",
    },
    success: [
      { recipe: { requirements: { moth: 4 } }, suffix: "moth" },
      { recipe: { requirements: { edge: 4 } }, suffix: "edge" },
    ],
    successEnd: {
      label: "Broken Free",
      description:
        "By hook and by crook, by knee and by elbow, we manage to slip out of the brawl. We leave uninterrupted, but we may leave some broken noses behind.",
    },
    failure: {
      label: "Battered and Bruised",
      description:
        "The crowd moves like a wave, like a single furious organism with a dozen legs and hundred limbs. I get trampled beneath the throngs, and it takes a while for me to crawl out.",
      effects: { "mariner.experiences.injury.standby": 1 },
    },
  });
}

export function generateSunlessBazaarEvents() {
  generateUncomfortableAcquaintance();
  generateBazaarBrawl();
  generateUpgradeAmsterdamReputationEvent();
}
