import { SIGNALS } from "../../../../../generate_signal_aspects.mjs";
import { requireAndDestroy } from "../../../../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";

export function generateReputationMerchant(vb) {
  vb.defineTask({
    taskId: "reputationmerchant",
    label: "The Gossipbrokers",
    icon: "tasks/mariner.tasks.gossip.darkred",
    description:
      "Most stalls deal in valuables or finance, but the gossipbrokers fare is Reputation. More precious than either, especially since it is so precariously hard to restore.",
    slots: [{ actionId: "talk", id: "funds", label: "Funds", required: { funds: 1 } }],
    actions: {
      talk: {
        challenge: {
          bypass: requireAndDestroy("funds"),
          task: {
            label: "Damage Control",
            startdescription:
              "If I have blundered, I could enlist the gossipbrokers to mitigate some of the damage to my name. The Gossipbrokers have sway in many places, and they could put Auntie Ekster and her gang more at ease...",
          },
          success: {
            label: "Proverbial Polish",
            description:
              "The Brokers know my reputation, but have no worries. The Brokers will see what they can do. The Brokers will smooth over some ruffled feathers, and ease the path for me down the road.",
            effects: {
              [vb.vp("reputationmerchant")]: -1,
            },
            furthermore: [
              {
                target: "~/exterior",
                mutations: [
                  {
                    filter: "mariner.locations.northsea.ekstersnest.uqg",
                    mutate: "mariner.reputation",
                    level: -1,
                    additive: true,
                  },
                  {
                    filter: "mariner.locations.northsea.ekstersnest.uqg",
                    mutate: "mariner.reputation.heat",
                    level: 0,
                    additive: false,
                  },
                ],
              },
              {
                target: "~/exterior",
                aspects: { [SIGNALS.UPDATE_DISPLAYED_REPUTATION]: 1 },
              },
            ],
          },
          failure: {
            label: "No Deal",
            description:
              "The Brokers have found me wanting. The Brokers have found me unpleasant. The Brokers will warn their good friend, Auntie Ekster, and will tell her all about us.",
            effects: {
              [vb.vp("reputationmerchant")]: -1,
            },
          },
          template: probabilityChallengeTemplates.PLEAD,
        },
      },
    },
  });
}
