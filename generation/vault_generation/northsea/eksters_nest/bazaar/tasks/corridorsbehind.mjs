import { LOCAL_REPUTATION } from "../../../../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";

export function generateCorridorsBehind(vb) {
  vb.defineTask({
    isExclusive: true,
    taskId: "corridorsbehind",
    label: "Corridors Behind",
    icon: "tasks/mariner.tasks.delve.darkred",
    description:
      "An unassuming curtain leads to the Eksternest, the hideout-cum-shrine-cum-drugden from which Auntie Ekster operates. Even from outside the sweet air makes my eyes water and my mind thrum.",
    actions: {
      explore: {
        hint: {
          label: "Nest Closed",
          startdescription:
            "Even through her blissful haze, Auntie Ekster has noticed the stir we have caused in the Bazaar. The entrance to the Eksternest has been closed of, and will remain closed, until I return another time.",
          grandReqs: { [LOCAL_REPUTATION]: 3 },
        },
        challenge: {
          task: {
            label: "The Velvet Maze",
            startdescription:
              "Behind one curtain, another. Behind that curtain, the next. Soon all sense of direction is lost as we are entombed in cloth from all sides. Each breath is heavy and poppy-laced. But still... deeper we must delve.",
            grandReqs: { [LOCAL_REPUTATION]: -3 },
          },
          success: {
            label: "Into the Den of Greed",
            description:
              "When the final ochre rug is displaced, we have found ourselves in our destination. The sweet smoke is like syrup on our tongue. Bodies have arranged themselves in piles of stupefied bliss. We must be careful if we are not to join them.",
            effects: {
              [vb.vp("corridorsbehind")]: -1,
              [vb.vp("nest")]: 1,
            },
            purge: { [vb.currentLayerAspectId()]: 5 },
            inductions: [{ id: vb.vp("token.haze.appears") }],
          },
          failure: {
            label: "Turned around",
            description:
              "Stumbling and coughing, we push aside the final curtain, and drop to the floor. When our eyes clear, we find ourself staring up at the gas-lamps of the bazaar, and the amused eyes its denizens.",
            effects: {
              "mariner.experiences.vivid.standby": 1,
            },
          },
          template: probabilityChallengeTemplates.DELVE,
        },
      },
    },
  });
}
