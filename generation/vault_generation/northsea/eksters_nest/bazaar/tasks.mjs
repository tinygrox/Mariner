import { generateCorridorsBehind } from "./tasks/corridorsbehind.mjs";
import { generateDesireMerchant } from "./tasks/desiremerchant.mjs";
import { generateIntelMerchant } from "./tasks/intelmerchant.mjs";
import { generateLittleBird } from "./tasks/littlebird.mjs";
import { generateReputationMerchant } from "./tasks/reputationmerchant.mjs";
import { generateShapeDisappearing } from "./tasks/shapedisappearing.mjs";
import { generateTrappingMerchant } from "./tasks/trappingmerchant.mjs";

// ACTIONS
export function generateSunlessBazaarLayer(vb) {
  vb.defineLayer("2");

  // Tasks
  vb.defineExplorationTask({
    taskId: "sunlessbazaar",
    locationLabel: "Sunless Bazaar",
    locationDescription:
      "A sprawling cluster of tents, stalls and bivouacs. Here filters down what cannot be sold by daylight above. It is said that somewhere in this network of tunnels and market-canals a gang of adepts operate from here, who are favored by two Hours of Grail.",
    warmup: 15,
    tasks: [
      "littlebird",
      "shapedisappearing",
      "reputationmerchant",
      "desiremerchant",
      "intelmerchant",
      "trappingmerchant",
    ],
    events: ["events.uncomfortableacquaintance", "events.bazaarbrawl"],
    preRecipe: {
      purge: {
        [vb.currentLayerAspectId()]: 5,
      },
    },
    resetOnExhaustion: true,
  });

  generateLittleBird(vb);
  generateShapeDisappearing(vb);
  generateReputationMerchant(vb);
  generateDesireMerchant(vb);
  generateIntelMerchant(vb);
  generateTrappingMerchant(vb);
  generateCorridorsBehind(vb);
}
