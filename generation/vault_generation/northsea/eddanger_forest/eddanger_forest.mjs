import { generateWallLayer } from "./forest_wall.mjs";
import { generateGladeLayer } from "./innermost_glade.mjs";
import { VaultBuilder } from "../../vaultbuilder.mjs";
import { generateRisenTokens } from "./risens.mjs";
import { generateEyesOfTheCourtToken } from "./eyesofthecourt.mjs";
import { generateGauntletLayer } from "./gauntlet.mjs";

export const vaultPrefix = (end) => `mariner.vaults.eddangerforest.${end}`;
export const RECIPES_FILE = "recipes.vaults.eddangerforest";
export const ASPECTS_FILE = "aspects.vaults.eddangerforest";

export function generateEddangerForestVault() {
  const vb = new VaultBuilder("eddangerforest", "northsea", false);

  generateWallLayer(vb);
  generateEyesOfTheCourtToken(vb);
  generateGauntletLayer(vb);
  generateGladeLayer(vb);
  generateRisenTokens(vb);
}
