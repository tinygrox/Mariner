import { SPAWN_PARCHED_THROAT_CURSE } from "../../../curses_generation/curses/parched_throat.mjs";
import { INJURE_CREWMEMBER, requireAndDestroy } from "../../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../probability_challenge_templates.mjs";
import { RECIPES_FILE, vaultPrefix } from "./eddanger_forest.mjs";
import { trappingAspects } from "../../../generate_trappings.mjs";
import { CHALLENGE_SLOTS } from "../../../slots_helpers.mjs";

// EVENTS

// TASKS
export function generateGladeLayer(vb) {
  vb.defineLayer("2");

  vb.definePassiveHelpElement({
    id: "insight",
    label: "Botanical Insight",
    unique: true,
    description:
      "Considerations of resources and relays. Of bodies and of stems and trunks and vines. And of where to snip and where to prune, if it comes to that. ",
  });

  vb.defineHelpElement({
    id: "favor",
    label: "Whimsical Blessings",
    description: "My actions seem to have pleased the Yew. But favor never last for one so fickle. I should act on it quickly.",
    lifetime: "120",
  });

  mod.setElement(vb.ELEMENTS_FILE, {
    id: "mariner.vaults.eddangerforest.fruit",
    label: "The Merry-Red Berries",
    description:
      "Such delight is contained in their form, their shade, their flesh... But each contains shriveled black heart, lethal as venom.",
    aspects: trappingAspects({
      value: 5,
      types: ["natural"],
      aspects: { grail: 5 },
    }),
  });
  mod.setElement(vb.ELEMENTS_FILE, {
    id: "mariner.vaults.eddangerforest.flower",
    label: "Corpse-Bloom Cutting",
    description:
      "Life from death is as natural as death after life. As long as we provide the flowers of soil and meat, it will remain in bloom.",
    aspects: trappingAspects({
      value: 2,
      types: ["natural"],
      aspects: { winter: 5 },
    }),
  });
  mod.setElement(vb.ELEMENTS_FILE, {
    id: "mariner.vaults.eddangerforest.treasure",
    label: "Manyfold Knot",
    unique: true,
    icon: "toolsecrethistoriesb",
    description:
      "An item sacred to a certain religious sect. I know they venerated the Ring-Yew, among others. In between the many contortions of the iron bands, I can see a heart. Is it trapping it, or protecting it?",
    aspects: trappingAspects({
      value: 5,
      types: ["artifact"],
      aspects: { heart: 1 },
    }),
  });

  vb.defineExplorationTask({
    taskId: "glade",
    locationLabel: "The Innermost Glade",
    locationDescription:
      "After the crowded forest, always rustling and chirping with things unseen, this glade is completely quiet. In its center stands the matriarch of the woods, and the forest floor around is carpeted with flowers. ",
    tasks: ["nooks", "corpses", "fruits"],
  });

  vb.defineTask({
    taskId: "nooks",
    label: "The Taking Nooks",
    icon: "tasks/mariner.tasks.delve.forge",
    description:
      "The Old Yew at the center of the glade is surrounded by the supplicant dead. Their arms still stretch out, with rotting fingers trapped between the many pleads and folds of her bark.",
    actions: {
      explore: {
        hint: {
          label: "Probing Ministrations",
          startdescription:
            "Examing the folds up close, we find the folds edges spongy and giving. We could invade them with clever fingers, which may inspire the tree to open up further to the touch.",
          requirements: { [vb.vp("favor")]: -1 },
        },
        challenge: {
          bypass: requireAndDestroy(vb.vp("favor")),
          task: {
            label: "Probing Ministrations",
            startdescription:
              "Examing the folds up close, we find the folds edges spongy and giving. We could invade them with clever fingers, which may inspire the tree to open up further to the touch. The greater fear is of our hand not being released after the deed is done. Only the Malachite Favor could ensure our safety.",
          },
          success: {
            label: "Dowry Granted",
            description:
              "The bark is hard and sturdy, but the innards of are pliable and soft. We move quickly, securely, lovingly. We remove from inside those treasures which were entrusted there, and behind our fingertips, the hollow folds in towards a satisfied knot.",
            linked: [{ id: vb.vp("yew_rewards_draw") }],
            grandReqs: {
              "root/mariner.flag.heart.draw": -1,
            },
            furthermore: [
              {
                rootAdd: {
                  "mariner.flag.heart.draw": 1,
                },
              },
            ],
          },
          failure: {
            label: "A Handfasting",
            description:
              "One of our aids' hands slips in, but as soon as it is enveloped, the tree grip closes on it like a vise. They claw at the bark and whimper, but the Yew will not let go of her new conquest",
            inductions: [INJURE_CREWMEMBER],
          },
          template: probabilityChallengeTemplates.DELVE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
      "mariner.sing": {
        challenge: {
          task: {
            label: "Vie for Favor",
            startdescription:
              "Each of the hollows is clamped tight around the putrid digits of this previous applicant. If we can put the Malachite in one of her good moods, our fate may yet be different.",
          },
          success: {
            label: "Sylvan Serenade",
            description:
              "As I sing I find the words. The Ring Yew does not dwell on the loss, but she is familiar with Longing. A soft creaking shiver travels through the tree, and one of the Nooks is opened, just a little bit.",
            effects: { [vb.vp("favor")]: 1 },
          },
          failure: {
            label: "Unsung",
            description:
              "I have declared myself, and was rejected. The word could have been perfect and the tune correct, but the Malachite is a creature of whimsy, and this time, our tone did not please.",
            alt: [SPAWN_PARCHED_THROAT_CURSE],
          },
          template: probabilityChallengeTemplates.PLEAD,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.sing"),
      },
    },
  });

  vb.defineTask({
    taskId: "corpses",
    label: "The Blooming Corpses",
    icon: "tasks/mariner.tasks.summon.petroleblue",
    description:
      "In the glade, especially at the base of the Heart Tree, the forest floor is littered with corpses. Adventurers, revelers, lost souls, all now forever languishing in the Honey-Tree's court, flower-crowned and rotten.",
    actions: {
      explore: {
        challenge: {
          task: {
            label: "Desecrate the Corpses",
            startdescription:
              "Many bodies are strewn here, entangled by vines with flowers sprouting from their death-stiff jaws. From their rotting clothing and decaying bags we can take the valuables they can no longer appreciate.",
          },
          success: {
            label: "Left Loot",
            description:
              "Our pockets are now lined with whatever the nearest corpse carried with it into the woods. But as soon as we turn away, the robbed body is dragged into animation, vines clinging to bones like rippling muscles.",
            linked: [{ id: "mariner.drawreward.jumble.2" }],
            inductions: [{ id: vb.vp("risen.loop") }],
          },
          failure: {
            label: "Snapped Away",
            description:
              "As our hand reaches into the pocket, we jostle the dead flesh. Immediately the vines around it snap to life like coiling snakes. It is dragged against the corpse, which strangles it with one vine-cabled hand while lifting itself up with the other.",
            inductions: [INJURE_CREWMEMBER, { id: vb.vp("risen.loop") }],
          },
          template: probabilityChallengeTemplates.AVOID,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
      "mariner.sing": {
        challenge: {
          task: {
            label: "Sing-Collect the Corpse-Bloom",
            startdescription:
              "From the crown of each corpse, a single flower has sprouted. While I do not yet know the who or why, I am sure something as extraordinary as this will be valuable to someone.",
          },
          success: {
            label: "Snip the Stem",
            description:
              "Verse by verse, we cut the flower off by the stem. It bleeds yellow resin, and we see the eyes of the corpse begin to flutter. We retreat quickly before its decayed fingers can grab hold.",
            effects: { "mariner.vaults.eddangerforest.flower": 1 },
            inductions: [{ id: vb.vp("risen.loop") }],
          },
          failure: {
            label: "Tug of the Vine Strings",
            description:
              "The corpse awakes as soon as the flower's stem is snipped, as does the vegetation that pierces its limbs and the creatures that found shelter in it's hollow chest. It does not speak but drones, and it's jaws barely open to release the swarming bees that incapacitate the foolish culprit.",
            inductions: [INJURE_CREWMEMBER, { id: vb.vp("risen.loop") }],
          },
          template: probabilityChallengeTemplates.AVOID,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.sing"),
      },
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Study the Corpses",
            startdescription:
              "Life following death is part of natures cycle. Nutrients flow, energy is transformed, and life is renewed. Though, it is rarely the same life that was lost. Something growing on dead flesh is not unexpected, but there is something peculiar to the poses of these bodies, and the way the vines are wrapping them.",
          },
          success: {
            label: "Nature without Divisions",
            description:
              "Here before us,we see all kingdoms of life united from death. Fungi creep over dead flesh, roots embbeded in there cylia, with beetles bustling in between. All of Life is a circle under the Ring Yew. Even Hunter and Prey, Plant and Parasite are cousins under her care. I see where the roots have dug into the flesh, and where rot has left the tissue soft like tearing cloth.",
            effects: { [vb.vp("insight")]: 1 },
          },
          failure: {
            label: "Too Aware",
            description:
              "While I we had noticed the plants covering the bodies before, we now become aware the creeping and crawling court that has taken residence, scratching beneath the skin. We can allready feel them too, our flesh attuned to our future. The fate of all those who will be burried in soft soil.",
            effects: { "mariner.experiences.unsettling.standby": 1 },
          },
          template: probabilityChallengeTemplates.STRATEGIZE,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
    },
  });

  vb.defineTask({
    taskId: "fruits",
    label: "The Berries in the Boughs",
    icon: "tasks/mariner.tasks.treasure.green",
    description:
      "Between blue needles of the Mother Yew, bright red treasures blink. The fruits from a sacred yew have many miraculous properties asigned to them in myth and story, beside their profane toxicity.",
    actions: {
      explore: {
        challenge: {
          task: {
            label: "Gather the Fruits",
            startdescription:
              "The berries hang low in the bows, high enough to challenge your reach, low enough to yield to your grasp. Their flesh is soft and delicate, and we should be careful not to tear them. That is a profound offence on hallowed grounds, for which the penance requires you to swallow the fatal seeds.",
          },
          success: {
            label: "The Berries Bound",
            description: "We gather the fruits in a white handkerchief and bind it to our belt, careful not to crush them.",
            effects: {
              [vb.vp("fruits")]: -1,
              "mariner.vaults.eddangerforest.fruit": 1,
            },
          },
          failure: {
            label: "Penance Payed",
            description:
              "One of the berries is dropped, and crushed under foot. As the aroma of mulled wine and humus fills the air, everything else becomes dead quiet. Not even the grass dares to russtle as the malefactor drops upon their trembling knees and kiss the ground where the berry dropped. Their last breath is a sigh, and they shall not move until time and the Wood-Things make them.",
            effects: {
              [vb.vp("fruits")]: -1,
              "mariner.vaults.eddangerforest.fruit": 1,
              "mariner.experiences.dreadful.standby": 1,
            },
            inductions: [INJURE_CREWMEMBER],
          },
          template: probabilityChallengeTemplates.OPERATE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("yew_rewards_draw"),
    craftable: false,
    linked: [
      {
        id: "draw_treasure",
        maxexecutions: 1,
        effects: { "mariner.vaults.eddangerforest.treasure": 1 },
      },
      {
        id: "mariner.drawreward.precious",
      },
    ],
  });
}
