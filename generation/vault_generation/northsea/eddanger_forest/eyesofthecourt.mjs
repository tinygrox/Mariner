import { LOCAL_REPUTATION } from "../../../generation_helpers.mjs";

export function generateEyesOfTheCourtToken(vb) {
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("eoc.start"),
    actionId: "mariner.haltable.eoc",
    grandReqs: { [LOCAL_REPUTATION]: 1 },
    linked: [{ id: vb.vp("eoc.loop") }],
  });
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("eoc.loop"),
    warmup: 30,
    label: "The Eyes of the Forest",
    startdescription:
      "As we pierce the borders of the woods, we get the creeping feeling that our presence has been noticed. ",
    linked: [
      { id: vb.vp("eoc.decideaction.4") },
      { id: vb.vp("eoc.decideaction.3") },
      { id: vb.vp("eoc.decideaction.2") },
      { id: vb.vp("eoc.loop") },
    ],
  });

  // Decision recipes layer. Each tier has 30% chance to trigger its maximum threat, otherwise it links to the tier-1 decision-making recipe. Whatever happens, loop back.
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("eoc.decideaction.2"),
    grandReqs: { [LOCAL_REPUTATION]: 2 },
    linked: [
      { id: vb.vp("eoc.breaktool.pre"), chance: 30 },
      { id: vb.vp("eoc.loop") },
    ],
  });
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("eoc.decideaction.3"),
    grandReqs: { [LOCAL_REPUTATION]: 3 },
    linked: [
      { id: vb.vp("eoc.exhaustcrew.pre"), chance: 30 },
      { id: vb.vp("eoc.decideaction.2") },
    ],
  });
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("eoc.decideaction.4"),
    grandReqs: { [LOCAL_REPUTATION]: 4 },
    linked: [
      { id: vb.vp("eoc.killcrew.pre"), chance: 30 },
      { id: vb.vp("eoc.decideaction.3") },
    ],
  });

  // Threats requirements layer. The threat may be escalated multiple times if a planned threat can't be triggered.
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("eoc.breaktool.pre"),
    linked: [
      { id: vb.vp("eoc.breaktool") },
      { id: vb.vp("eoc.exhaustcrew.pre") },
    ],
  });
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("eoc.exhaustcrew.pre"),
    linked: [
      { id: vb.vp("eoc.exhaustcrew") },
      { id: vb.vp("eoc.killcrew.pre") },
    ],
  });
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("eoc.killcrew.pre"),
    linked: [
      { id: vb.vp("eoc.killcrew") },
      { id: vb.vp("eoc.killcrew.gameover") },
    ],
  });

  // Threats.
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("eoc.breaktool"),
    grandReqs: {
      "[~/exterior : {[mariner.tool.broken]=0} : tool]": 1,
    },
    label: "Gremlin Tampering",
    startdescription:
      "This wild things that form the Honey Queens' court harbor a deep recentment for artifice, and one of my tools just bore the brunt of that ire. ",
    warmup: 30,
    slots: [
      {
        id: "tool",
        greedy: true,
        required: {
          tool: 1,
        },
        forbidden: {
          "mariner.tool.broken": 1,
        },
      },
    ],
    furthermore: [
      {
        mutations: {
          tool: {
            mutate: "mariner.tool.broken",
            level: 1,
            additive: false,
          },
        },
      },
      { movements: { "~/tabletop": ["tool"] } },
    ],
    linked: [{ id: vb.vp("eoc.loop") }],
  });
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("eoc.exhaustcrew"),
    grandReqs: {
      "[~/exterior : {[mariner.crew.exhausted]=0} : mariner.crew]": 1,
    },
    label: "The Very Earth Resist Us",
    startdescription:
      "The ground crumbles under foot. While most of us can duck for cover, one of my sailors strains their limbs.",
    warmup: 30,
    slots: [
      {
        id: "crew",
        greedy: true,
        required: {
          "mariner.crew": 1,
        },
        forbidden: {
          "mariner.crew.exhausted": 1,
        },
      },
    ],
    furthermore: [
      {
        mutations: {
          "mariner.crew": {
            mutate: "mariner.crew.exhausted",
            level: 5,
          },
        },
      },
      { movements: { "~/tabletop": ["mariner.crew"] } },
    ],
    linked: [{ id: vb.vp("eoc.loop") }],
  });
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("eoc.killcrew"),
    tablereqs: { "mariner.crew": 1 },
    label: "Where Did They Go",
    startdescription:
      "In this dense vegetation, it is hard to keep everyone accounted for... Who knows what lead them astray, or where it lead them to.",
    warmup: 15,
    slots: [
      {
        id: "crew",
        greedy: true,
        required: {
          "mariner.crew.exhausted": 1,
        },
      },
    ],
    effects: { "mariner.crew": -1 },
    linked: [{ id: vb.vp("eoc.killcrew.post") }],
  });
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("eoc.killcrew.post"),
    label: "Where Did They Go",
    startdescription:
      '"They were right behind me, they were right here!"<br><br>The forest has claimed someone, it seems. We must push onward as quickly as possible, before more go the same way.',
    warmup: 15,
    linked: [{ id: vb.vp("eoc.loop") }],
  });
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("eoc.killcrew.gameover"),
    label: "Eyes Set On Me",
    startdescription:
      "The forest is alive, and I am sure that soon I will not be.",
    warmup: 15,
    ending: "mariner.endings.disappearedduringadventure",
    linked: [{ id: vb.vp("eoc.loop") }],
  });
}
