import { generateEdgeEvents, generateEdgeLayer } from "./edge.mjs";
import { generateCityEvents, generateCityLayer } from "./city.mjs";
import { generateDikeLayer } from "./seawall.mjs";
import { VaultBuilder } from "../../vaultbuilder.mjs";
import { generateGauntletLayer } from "./gauntlet.mjs";

export const ELEMENTS_FILE = "vaults.sunkencity";
export const RECIPES_FILE = "recipes.vaults.sunkencity";
export const ASPECTS_FILE = "aspects.vaults.sunkencity";

export const vaultPrefix = (end) => `mariner.vaults.sunkencity.${end}`;

export function generateSunkencityVault() {
  const vb = new VaultBuilder("sunkencity", null, true);

  generateEdgeEvents(vb);
  generateEdgeLayer(vb);

  generateCityEvents(vb);
  generateCityLayer(vb);

  // generateDikeEvents(vb);
  generateDikeLayer(vb);

  generateGauntletLayer(vb);
}
