import { INJURE_CREWMEMBER, requireAndDestroy } from "../../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS, SHIP_MANOEUVERING_SLOTS } from "../../../slots_helpers.mjs";
import { generateTresholdChallengeTask } from "../../vault_generation.mjs";
import { RECIPES_FILE, ASPECTS_FILE, vaultPrefix } from "./sunken_city.mjs";

// EVENTS
export function generateEdgeEvents(vb) {
  // terror TOKEN
  mod.setRecipe(RECIPES_FILE, {
    actionId: "mariner.haltable.terror",
    id: vb.vp("token.terror.appears"),
    label: "A Sea of Fright",
    startdescription:
      "We have sailed out of the common routes and onto tides much fiercer. Here, an enternal storm swirls around hole in the sea's surface. Sailors are hearty folk, but this experience lies beyond their ken.",
    description:
      "A first panicked whimper can be heard on the deck, though it is is impossible to tell where it originated from. But soon the panic will spread, oil-slick quick...",
    warmup: 120,
    linked: [{ id: vb.vp("token.terror.windup") }],
  });
  mod.setRecipe(RECIPES_FILE, {
    actionId: "mariner.haltable.terror",
    id: vb.vp("token.terror.windup"),
    label: "Rising Terror",
    startdescription:
      "Panic is gripping around my ship like wildfire, and just like a spreading flame, it is probable to sink us, ship, crew and all.",
    description: "Static crackles through the air, and one of my sailors is about to snap.",
    warmup: 60,
    linked: [{ id: vb.vp("token.terror.wardedoff") }, { id: vb.vp("token.terror.attack") }],
  });
  mod.setRecipe(RECIPES_FILE, {
    actionId: "mariner.haltable.terror",
    id: vb.vp("token.terror.wardedoff"),
    label: "Unperturbed",
    startdescription: "Horror has no grip on a heart filled with purpose.",
    extantreqs: {
      [vb.vp("courage")]: 1,
    },
    description: "Stalward, through grit and mettle, the crew remains balanced even on the edge of the abyss.",
    warmup: 30,
    linked: [{ id: vb.vp("token.terror.windup") }],
  });
  mod.setRecipe(RECIPES_FILE, {
    actionId: "mariner.haltable.terror",
    id: vb.vp("token.terror.attack"),
    label: "The Breaking Point",
    startdescription:
      "A spray of salt hits over the edge of the ship, and something shifts in the mentality of my men... one has reached his point of no return.",
    description: "White knuckles grip the railing as if to break it. One more sailor will respond to my commands no longer.",
    warmup: 30,
    linked: [{ id: vb.vp("token.terror.windup") }],
    slots: [
      {
        id: "victim",
        label: "Pillar of Salt",
        description: "Whimpering or raving, or stuck in awe-struck petrification, this sailor is no use to anyone anymore",
        greedy: true,
        required: { "mariner.crew": 1 },
      },
    ],
  });

  // winter song stealer
  mod.setRecipe(RECIPES_FILE, {
    actionId: "mariner.haltable.wintersong",
    id: vb.vp("mariner.haltable.wintersong.pre"),
    label: "A Perilous Tranquility",
    startdescription:
      "Until my breath runs out, this song will maintain the stability of the plane, the portal and the passage. The Moon herself will stand guard.",
    warmup: 30,
    linked: [{ id: vb.vp("mariner.haltable.wintersong.cycle") }],
  });
  mod.setRecipe(RECIPES_FILE, {
    actionId: "mariner.haltable.wintersong",
    id: vb.vp("mariner.haltable.wintersong.cycle"),
    label: "A Perilous Tranquility",
    description: "Even now a note lingers...",
    warmup: 120,
    linked: [{ id: vb.vp("mariner.haltable.wintersong.cycle") }],
    slots: [
      {
        id: "song",
        label: "Moon",
        description: "Only under a peculiar moon, is the passage down stable. But her light cannot aid me where I am going.",
        greedy: true,
        required: { "mariner.moon": 1 },
      },
    ],
  });

  // ship hostage
  mod.setRecipe(RECIPES_FILE, {
    actionId: "mariner.haltable.ship",
    id: vb.vp("mariner.haltable.ship.pre"),
    label: "Left Above",
    startdescription: "I have left my ship above.",
    warmup: 30,
    linked: [{ id: vb.vp("mariner.haltable.ship.cycle") }],
  });
  mod.setRecipe(RECIPES_FILE, {
    actionId: "mariner.haltable.ship",
    id: vb.vp("mariner.haltable.ship.cycle"),
    label: "Left Above",
    description: "I have left my ship above.",
    warmup: 120,
    linked: [{ id: vb.vp("mariner.haltable.ship.cycle") }],
    slots: [
      {
        id: "ship",
        label: "My Ship",
        description: "My vessel across the seas.",
        greedy: true,
        required: { "mariner.ship": 1 },
      },
    ],
  });

  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.heart",
    task: {
      id: vb.vp("events.stormtakesus"),
      craftable: false,
      label: "Storm Takes us!",
      startdescription:
        "A dangerous eddy in the water is suddenly met with a violent shift in the storm. The swell of water threatens to overtake us. [Approach this challenge with Forge or Heart.]",
    },
    success: [
      { recipe: { requirements: { heart: 4 } }, suffix: "heart" },
      { recipe: { requirements: { forge: 4 } }, suffix: "forge" },
    ],
    successEnd: {
      label: "Reasserted Control",
      description: "The current strong, but my crew is trained. Everyone is in their place and the ship's course is righted. ",
    },
    failure: {
      label: "Man overboard!",
      description:
        "Through the listing ship and the snapping lines, one of our crewmembers is swept of the ship. We see them go down between the waves, and then see them no more. ",
      inductions: [INJURE_CREWMEMBER],
    },
  });

  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.moth",
    actionId: "gauntlet",
    task: {
      id: vb.vp("events.seagulls"),
      craftable: false,
      label: "Seagull Choir",
      startdescription:
        "Thousands of swirling seagulls in an ever-tightening spiral follow the currents below, as the birds too plunge themselves into the depths. Our ship is on course with the cacophonous choir. [Approach this challenge with Moth or Winter.]",
    },
    success: [
      { recipe: { requirements: { moth: 4 } }, suffix: "moth" },
      { recipe: { requirements: { winter: 4 } }, suffix: "winter" },
    ],
    successEnd: {
      label: "Weather the beaks and talons",
      description:
        "We plug our ears and duck low, and when sailstunned birds drop on deck we quickly over them back to the storm and the sea. We make it through, and continue with our journey unscathed",
    },
    failure: {
      label: "Rapt in the sky",
      description:
        "Our attention was rapt, eyes raised. As the flurry of white birds screached overhead before it streaked down below. I wished to soar away with them, and it took everything I had to return our attentions to sailing.",
      effects: { "mariner.experiences.pain.standby": 1 },
      inductions: [{ id: vb.vp("events.stormtakesus"), chance: 75 }],
    },
  });
}

// TASKS
export function generateEdgeLayer(vb) {
  vb.defineLayer("1");

  vb.defineHelpElement({
    id: "linedown",
    unique: true,
    label: "The Line Down",
    description: "A single rope dangling of the edge of the abyss. With this we can descend...",
  });

  vb.definePassiveHelpElement({
    id: "courage",
    unique: true,
    icon: "temptation.defiance",
    label: "Courage",
    description:
      "You cannot freeze in fear, if you know you are backed by the greater force. My presence can act as their tailwind tonight. [This card protects your sailors from Terror as long as it is on the board.]",
  });

  vb.defineExplorationTask({
    taskId: "edge",
    locationLabel: "At the Abyss' Edge",
    locationDescription:
      "Here we witness a miracle one might expect at the edge of the world. A churning mass of water going straight down. It is not a waterfall but simply a door to the depths. As long as we are near it, we have to withstand that proximity, and it's effects on the currents.",
    tasks: ["sailors", "siphon"],
    events: ["events.stormtakesus", "events.seagulls"],
  });

  mod.setHiddenAspect(ASPECTS_FILE, { id: `mariner.moon.fixed` });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("makesupersitious"),
    label: "A Seed of Fear",
    description:
      "A fear has found purchase in this sailor's heart, and soon it will blossom into Terror. They will still be capable, but they will not be capable of standing up to Events tinged by the Mansus or the Woods.",
    mutations: [
      {
        filter: "mariner.crew",
        mutate: "mariner.crew.traits.superstitious",
        level: 1,
      },
    ],
  });

  vb.defineTask({
    taskId: "sailors",
    label: "Sailors on the Edge",
    icon: "tasks/mariner.tasks.crowd.blue",
    description:
      "Slack-mouthed and with gaping eyes, the sailors stare at the horrific miracle besides them. This is more than normal men have to bear. Many of them may prefer to flee, instead of following me into the depths.",
    actions: {
      talk: {
        challenge: {
          task: {
            label: "Embolden my crew",
            startdescription:
              "They may be scared, but they are sailors still, and I, their captain. If I press my authority I can remind them of their duty, and they may overcome their fright.",
          },
          success: {
            label: "Steeled Resolve",
            description:
              "Their mouths tighten into a resolute grimace, their eyes narrow, focussing on the task at hand. They will not let their captain down. They will follow their captain into the abyss.",
            effects: { [vb.vp("courage")]: 1 },
          },
          failure: {
            label: "Fraying Nerves",
            description:
              "My crew looks at that wretched hole, than at each other, and come to a silent accord. They do not want to come down, and may abandon me at any moment.",
            mutations: [
              {
                filter: "mariner.crew",
                mutate: "mariner.crew.exhausted",
                limit: 1,
                level: 3,
                additive: true,
              },
            ],
            linked: [{ id: vb.vp("makesupersitious") }],
          },
          template: probabilityChallengeTemplates.COMMAND,
        },
        slots: CHALLENGE_SLOTS(3, "talk"),
      },
    },
  });

  vb.defineTask({
    taskId: "siphon",
    label: "A Siphon of Wind and Water",
    icon: "tasks/mariner.tasks.whirlpool.vertdegris",
    description:
      "A raging torrent plummeting down into the depths. Some power opened a keyhole towards that sunken city, perhaps in an acknowledgement of her involvement. The sea parts unwillingly, however, and us sailors must reckon with the sea.",
    actions: {
      "mariner.sing": {
        challenge: {
          task: {
            label: "Stabilize the Portal",
            startdescription:
              "By some confluence of the moon and the tides and whims of an Hour, a whirling whirlpool has laid Ker Ys dry at the bottom of the Channel. But it is not a stable, and the churning winds and waters threaten the Kite. If I go down, the ship would be in danger. [Fulfilling this task will take your Winter song, it will not be returned to you for the entirety of The Sunken City.]",
          },
          success: {
            label: "A Tranquility",
            description:
              "The world needs not listen to pleas of a sailor, but in this place, at this time, it listens. Perhaps it has been rendered receptive by the same power that has rend the seas. Perhaps the moon is feeling kind today. Regardless, the moon settles right above the whirlpool, and will not move, not for a while.",
            inductions: [
              {
                id: vb.vp("mariner.haltable.wintersong.pre"),
              },
            ],
            furthermore: [
              {
                rootAdd: {
                  "mariner.moon.fixed": 1,
                },
              },
            ],
          },
          failure: {
            label: "The Unwilling World",
            description:
              "This time, my voice is lost to the storm. This time, the ocean seems to vast to hear me. This time, a tip-tap feeling on the back of my skull that tells me to try, try, try again, and make my voice heard. ",
          },
          template: probabilityChallengeTemplates.PLEAD,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.sing"),
      },
      "mariner.sail": {
        challenge: {
          hint: {
            label: "Manoeuver the ship?",
            startdescription:
              "We must maneuver to where we can let down a cable into the hole, but where our ship can remain save while we are down exploring. I will need to use the Kite to start maneuvering.",
            requirements: { "mariner.ship": -1 },
          },
          task: {
            label: "Maneuver the ship",
            startdescription:
              "We must maneuver to where we can let down a cable into the hole, but where our ship can remain save while we are down exploring. This will require a tricky configuration of sails and anchors and currents and wind.",
            requirements: { "mariner.ship": 1 },
          },
          success: {
            label: "A Delicate Balance",
            description:
              "The anchors have found purchase, and with the lines taut, the ship now hangs close to the edge. From our overhanging mast, a cable can be lowered. Our way down is set...",
            effects: vb.vp({ linedown: 1 }),
          },
          failure: {
            label: "A Slip and a Crack",
            description:
              "The anchor dragged and then slipped. The ship lurched, the sail snapped and a great creaking of wood and canvas. The Kite has sustained an injury in this attempt, and there is no option but to try again.",
            mutations: [
              {
                filter: "mariner.ship",
                mutate: "mariner.ship.damage",
                level: 1,
                additive: true,
              },
            ],
          },
          template: probabilityChallengeTemplates.OVERTAKE,
        },
        slots: [
          ...SHIP_MANOEUVERING_SLOTS(),
          {
            id: "shipslot",
            actionid: "mariner.sail",
            label: "My Ship",
            description: "To travel the sea, I require my vessel.",
            required: { "mariner.ship": 1 },
          },
        ],
      },
      explore: {
        challenge: {
          hint: {
            label: "Descend Into the Depths?",
            startdescription:
              "To progress, we must descend. But how do we ensure we do not descend at terminal velocity? The Moon must be in it's proper alignment with the sea, and our ship in the proper alignment with the Edge. only then can we attempt this.",
            requirements: { [vb.vp("linedown")]: -1 },
          },
          task: {
            label: "Descend Into the Depths",
            startdescription:
              "While this is not the normal diving experience, much of the anticipation is the same... and the dread. This will just as much be a challenge mentally as it is physically.",
            requirements: { [vb.vp("linedown")]: 1 },
            grandReqs: { "root/mariner.moon.fixed": 1 },
          },
          success: {
            label: "Step on the Seafloor",
            description:
              "Down we go into darkness. Time and distance have no meaning during the descent. Then, pale shapes arise in the darkness. Broken-teeth buildings of ancient, waterbitten stone. We descend between them, and step onto the silty sand.",
            effects: vb.vp({ siphon: -1, city: 1 }),
            purge: { [vb.currentLayerAspectId()]: 5 },
            inductions: [{ id: vb.vp("token.horror.appears") }, { id: vb.vp("mariner.haltable.ship.pre") }],
            haltverb: {
              "mariner.haltable.terror": 1,
            },
          },
          failure: {
            label: "Panic and Confusion",
            description:
              "Halfway down the line jossles, and someone loses their grip. We hear nothing of them again, until there is a soft thud below. We take a moment to recover and then must continue our descent. ",
            effects: { "mariner.experiences.rattling.standby": 1 },
            inductions: [INJURE_CREWMEMBER],
          },
          template: probabilityChallengeTemplates.DIVE,
        },
        slots: [
          ...CHALLENGE_SLOTS(3, "explore"),
          {
            id: "lnedown",
            actionid: "explore",
            label: "Stable Position",
            description: "A stable position on an unstable sea, to travel to destinations unimaginable.",
            required: { [vb.vp("linedown")]: 1 },
          },
        ],
      },
    },
  });
}
