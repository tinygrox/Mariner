import { CALL_FUNCTION, CALL_FUNCTIONS } from "../../generate_function_calls.mjs";
import { INJURE_CREWMEMBER, requireAndDestroy } from "../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS } from "../../slots_helpers.mjs";
import { SHIP_NAME } from "../ship_wrecks_names.mjs";
import { generateTresholdChallengeTask } from "../vault_generation.mjs";
import { RECIPES_FILE, vaultPrefix } from "./salvage_mission.mjs";

// EVENTS
export function generateShipwreckEvents(vb) {
  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.lantern",
    task: {
      id: vaultPrefix("events.crush"),
      craftable: false,
      label: "The Crush of Emptiness",
      startdescription:
        "Suddenly, it all comes bearing down on me. I am devastatingly aware of the uncaring vastness of the sea above me, and the weight of each individual icy drop. I could ward it of with bright thoughts or faith in my air in my longs",
    },
    success: [
      { recipe: { requirements: { lantern: 4 } }, suffix: "lantern" },
      { recipe: { requirements: { heart: 4 } }, suffix: "heart" },
    ],
    successEnd: {
      label: "Memories of Daylight",
      description:
        "There is a gleam of hope in the back of our mind, and the darkness recedes far enough for me to continue moving. I mustn’t stay here long. I must stay focussed on my task.",
      purge: vb.vp({ air: 1 }),
      linked: [{ id: vb.vp("drown") }],
    },
    failure: {
      label: "The Stare of the Abyss",
      description:
        "It is not that I am gazing into the waters around me. It is that I am staring into nothing. And the nothing around is only matchied by the emptyness inside.",
      effects: { "mariner.experiences.dreadful.standby": 1 },
      purge: vb.vp({ air: 1 }),
      linked: [{ id: vb.vp("drown") }],
    },
  });

  generateTresholdChallengeTask({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.knock",
    task: {
      id: vaultPrefix("events.drowneddead"),
      craftable: false,
      label: "The Drowned Dead",
      startdescription: `Shapes drift in the darkness. The crew of the ${SHIP_NAME}, entangled in strands of kelp and trapped in their final moments of despair. Lantern could give them a guidelight, or Knock could free their bonds.`,
    },
    success: [
      { recipe: { requirements: { lantern: 4 } }, suffix: "lantern" },
      { recipe: { requirements: { knock: 4 } }, suffix: "knock" },
    ],
    successEnd: {
      label: "Floating Free",
      description:
        "Their bonds slide off and the figures float. We cast our eyes up and watch them rise to the azure light above. Much later, when we ask our cohorts on the ship, they will not have seen a single body surface.",
      purge: vb.vp({ air: 1 }),
      linked: [{ id: vb.vp("drown") }],
    },
    failure: {
      label: "The Toll for the Death.",
      description:
        "A roll of underwater currents, and the shapes disappear down into the murk. Were they ever even there? But what is clear is that in their leaving, they have taken that vital part of me.",
      purge: vb.vp({ air: 1 }),
      linked: [{ id: vb.vp("drown") }],
    },
  });
}

// TASKS
export function generateShipwreckLayer(vb) {
  vb.defineLayer("3");

  vb.defineExplorationTask({
    taskId: "shipwreck",
    locationLabel: `The Wreck of the ${SHIP_NAME}`,
    icon: "mariner.vaults.salvagemission.shipwreck",
    locationDescription: `The ${SHIP_NAME} was once a proud vessel, with a caring captain and an intrepid crew. Now she lies, sloped and punctured, so deep that the surface is just a distant glimmer.`,
    tasks: ["lightlesshold", "clamcrustedsafe", "captainsquarters"],
    events: ["events.crush", "events.drowneddead"],
    next: [{ id: "mariner.applyshipnameid" }],
  });

  vb.defineTask({
    taskId: "lightlesshold",
    label: "The Lightless Hold",
    icon: "tasks/mariner.tasks.delve.darkpurple",
    description: `The tight corridors, cramped, twisted, decayed. But in that darkness, the goods the ${SHIP_NAME} was carrying.`,
    actions: {
      explore: {
        challenge: {
          task: {
            label: "Gather the Scraps",
            startdescription:
              "Something may be down there. But carrying the salvageable goods will be arduous labor, and will cost us a precious few breaths... If we get any out at all.",
          },
          success: {
            label: "Salvaged Bounty",
            description:
              "We labor our way through the tilted hallways, and wade through half digested debris in the belly of the ship. But with salvageable goods in tow we emerge back on deck, and tie them to our rope.",
            deckeffects: { "mariner.decks.cargo": 1 },
            purge: vb.vp({ air: 1 }),
            furthermore: CALL_FUNCTION("drawreward.jumble"),
            linked: [{ id: vb.vp("drown") }],
          },
          failure: {
            label: "Jammed in a Doorway",
            description:
              "Most boxes were rotten, most barrels were shot. When we finally found a salvagable barrel, it got jammed in the door when we carried it out. We had to smash it so we could return back to the tilted deck.",
            purge: vb.vp({ air: 1 }),
            linked: [{ id: vb.vp("drown") }],
          },
          template: probabilityChallengeTemplates.OVERCOME,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });

  vb.defineTask({
    taskId: "clamcrustedsafe",
    label: "The Clam-Crusted Safe",
    icon: "tasks/mariner.tasks.locked.yellow",
    description:
      "The inches thick steel is too heavy to lift out of the water. If we want at the valuables inside, we will have to pry it open down here in the dark.",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Open the Safe",
            startdescription:
              "The work conditions are, to be understated, not ideal here. Either approach will be difficult. But with the safe weighing a literal tonne, there is no way for us to move it. We select our tools, and begin.",
          },
          success: {
            label: "Cracked the Safe",
            description:
              "The safe swings open, and the insides, so long protected, now are exposed to the hungry sea. We gather it quickly before she claims it as her price.",
            effects: vb.vp({ clamcrustedsafe: -1 }),
            furthermore: CALL_FUNCTIONS("drawreward.precious", "drawreward.scholarly.2"),
            linked: [{ id: vb.vp("drown") }],
            purge: vb.vp({ air: 1 }),
          },
          failure: {
            label: "Wasted Time",
            description:
              "After the safe has survives the bite of salt and water for so long, did we really think our futile ministrations were going to be any different?",
            purge: vb.vp({ air: 1 }),
            linked: [{ id: vb.vp("drown") }],
          },
          template: probabilityChallengeTemplates.BREAK_IN,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
    },
  });

  vb.defineTask({
    taskId: "captainsquarters",
    label: "The Captain's Quarters",
    icon: "tasks/mariner.tasks.door.vertdegris",
    description: "I keep my most precious possessions in my quarter, maybe this captain did as well...",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Loot the Chambers of a Fallen Peer",
            startdescription:
              "I must consider only what useful items I might find, and not how closely it resembles my own quarters, my own desk and my own chair.",
          },
          success: {
            label: "A Glint in the Gloom",
            description:
              "I search as much with my gloved fingers as with my blinded eyes. From beneath a chewed up, water torn drape I uncover something, small, sterling, and shining still. I take my prize and leave this haunted place.",
            effects: {
              [vb.vp("captainsquarters")]: -1,
              "mariner.tool.lantern.1": 1,
            },
            purge: vb.vp({ air: 1 }),
            linked: [{ id: vb.vp("drown") }],
          },
          failure: {
            label: "A Meeting of Eyes",
            description:
              "As I stir up the water that has been stagnant for a long time, its current stirs up what remained of this room's occupant. I meet its gaze, and through my helmet, I suddenly feel all the water come crashing down on me.",
            effects: { "mariner.experiences.sobering.standby": 1 },
            purge: vb.vp({ air: 1 }),
            linked: [{ id: vb.vp("drown") }],
          },
          template: probabilityChallengeTemplates.PERCEIVE,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
    },
  });

  vb.defineTask({
    taskId: "abyssalplains",
    label: "The Abbyssal Plains",
    icon: "tasks/mariner.tasks.bubbles.blue",
    description: "Below the sea lies a desert, where your eyes strain for light and each step is an ordeal.",
    actions: {
      explore: {
        challenge: {
          task: {
            label: "Traverse to the Wreck",
            startdescription:
              "Below the sea lies a desert, where your eyes strain for light and each movement is an ordeal.. The shipwreck is the only landmark we have, a shaped shadow in the formless darkness. We set off.",
          },
          success: {
            label: "At the Foot of the Wreck",
            description:
              "Vague contours become solid, and the slick seafloor gets interrupted by the sloped shape of the wreck. We have reached her. Now, our true task begins.",
            furthermore: [
              {
                effects: vb.vp({ shipwreck: 1 }),
              },
              {
                mutations: [
                  {
                    filter: vb.vp("shipwreck"),
                    mutate: "mariner.shipnameid",
                    level: "mariner.shipnameid",
                  },
                ],
              },
            ],
            purge: vb.vp({ air: 1 }),
            linked: [{ id: vb.vp("drown") }],
          },
          failure: {
            label: "A Stumbling Journey",
            description:
              "Each step is a struggle, each moment an eternity. We get halfway to giving up before we muster the courage again. But this took a long time, and we breathed fast.",
            effects: vb.vp({ shipwreck: 1 }),
            purge: vb.vp({ air: 2 }),
            linked: [{ id: vb.vp("drown") }],
          },
          template: probabilityChallengeTemplates.ENDURE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("drown"),
    label: "The Agony Below",
    actionId: "mariner.drowning",
    startdescription:
      "My body struggles, trying to find one last gasp of livegiving breath. But all my trashing and clawing will not even make a ripple on the waters surface.",
    ending: "deathofthebody",
    signalendingflavour: "Melancholy",
    craftable: false,
    warmup: 30,
    extantreqs: vb.vp({ air: -1 }),
  });
}
