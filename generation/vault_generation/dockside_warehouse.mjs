import { probabilityChallengeTemplates } from "../probability_challenge_templates.mjs";
import { LOCAL_REPUTATION, requireAndDestroy } from "../generation_helpers.mjs";
import { CREW_TYPES } from "../crew_generation/generate_crewmen.mjs";
import { generateProbabilityChallengeTask, generateExplorationTask, generateTresholdChallengeTask } from "./vault_generation.mjs";
import { CHALLENGE_SLOT, CHALLENGE_SLOTS } from "../slots_helpers.mjs";

function generatePrisoners() {
  mod.setHiddenAspect("aspects.vaults.docksidewarehouse", {
    id: "mariner.aspects.vaults.docksidewarehouse.imprisoncrewmember",
  });
  mod.setHiddenAspect("aspects.vaults.docksidewarehouse", {
    id: "mariner.aspects.vaults.docksidewarehouse.freecrewmember",
  });
  mod.setAspect("aspects.vaults.docksidewarehouse", {
    id: "mariner.aspects.vaults.docksidewarehouse.imprisoned",
    label: "Imprisoned",
    description: "That one was captured.",
  });
  // Basic
  const imprisonedId = `mariner.vaults.docksidewarehouse.prisoners.basic`;
  const freeId = `mariner.crew.basic`;
  mod.setElement("vaults.docksidewarehouse", {
    id: `mariner.vaults.docksidewarehouse.prisoners.basic`,
    icon: "generic_p",
    label: "Imprisoned Crewmember",
    description: "Someone spotted us. This one wasn't as lucky as the others.",
    aspects: {
      "mariner.aspects.vaults.docksidewarehouse.imprisoned": 1,
      "mariner.local": 1,
    },
    xtriggers: {
      "mariner.aspects.vaults.docksidewarehouse.freecrewmember": `mariner.crew.basic`,
    },
  });
  mod.addToXtriggers("crewmen", freeId, {
    "mariner.aspects.vaults.docksidewarehouse.imprisoncrewmember": imprisonedId,
  });

  // Specialists
  for (const crewmemberType of CREW_TYPES) {
    const imprisonedId = `mariner.vaults.docksidewarehouse.prisoners.${crewmemberType}`;
    const freeId = `mariner.crew.specialist.${crewmemberType}`;
    mod.setElement("vaults.docksidewarehouse", {
      id: imprisonedId,
      icon: "generic_p",
      label: "Imprisoned Crewmember",
      description: "Someone spotted us. This one wasn't as lucky as the others.",
      aspects: {
        "mariner.aspects.vaults.docksidewarehouse.imprisoned": 1,
        "mariner.local": 1,
      },
      xtriggers: {
        "mariner.aspects.vaults.docksidewarehouse.freecrewmember": freeId,
      },
    });
    mod.addToXtriggers("crewmen", freeId, {
      "mariner.aspects.vaults.docksidewarehouse.imprisoncrewmember": imprisonedId,
    });
  }
}

function guardsEvent() {
  // router
  mod.setRecipe("recipes.vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.events.guards",
    actionId: "mariner.guards",
    requirements: {
      "mariner.aspects.vaults.docksidewarehouse.events.guards": 1,
    },
    effects: { "mariner.aspects.vaults.docksidewarehouse.events.guards": -1 },
    linked: [
      { id: "mariner.vaults.docksidewarehouse.events.guards.fight" },
      { id: "mariner.vaults.docksidewarehouse.events.guards.wandering" },
    ],
  });

  // Simple wandering
  generateTresholdChallengeTask({
    recipesFile: "recipes.vaults.docksidewarehouse",
    actionId: "mariner.event.grail",
    task: {
      id: "mariner.vaults.docksidewarehouse.events.guards.wandering",
      craftable: false,
      label: "A Wandering Guard!",
      startdescription:
        '"Is someone skulkin’ ‘ere?" A wandering guard came upon our gang. They seem interested in capitalizing on the situation... one way or another.',
      grandReqs: { [LOCAL_REPUTATION]: -3 },
    },
    success: [
      { recipe: { requirements: { edge: 4 } }, suffix: "edge" },
      { recipe: { requirements: { grail: 4 } }, suffix: "grail" },
    ],
    successEnd: {
      label: "Onward",
      description: "That unsavory creature is left behind us, and we continue on.",
    },
    failure: {
      label: "Raised the alarm",
      description: "We managed to get away, but we made noise and we took some losses.",
      effects: { "mariner.notoriety": 1 },
      alt: [
        {
          id: "mariner.vaults.docksidewarehouse.events.imprisoncrewmember",
          additional: true,
        },
      ],
    },
  });

  // Fight
  mod.setRecipe("recipes.vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.events.guards.triggerrep4",
    actionId: "mariner.guards",
    grandReqs: { [LOCAL_REPUTATION]: 4 },
    linked: [{ id: "mariner.vaults.docksidewarehouse.events.guards" }],
  });
  mod.setRecipe("recipes.vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.events.guards.triggerrep3",
    actionId: "mariner.guards",
    grandReqs: { [LOCAL_REPUTATION]: 3 },
    linked: [{ id: "mariner.vaults.docksidewarehouse.events.guards" }],
  });
  generateTresholdChallengeTask({
    recipesFile: "recipes.vaults.docksidewarehouse",
    actionId: "mariner.edge",
    task: {
      id: "mariner.vaults.docksidewarehouse.events.guards.fight",
      craftable: false,
      label: "Guards!",
      startdescription: "Unsavory hoards have come to stop their illegitimate gains from becoming illegitimately mine.",
      grandReqs: { [LOCAL_REPUTATION]: 3 },
    },
    success: [
      { recipe: { requirements: { edge: 4 } }, suffix: "edge" },
      { recipe: { requirements: { moth: 4 } }, suffix: "moth" },
    ],
    successEnd: {
      label: "Cracked Bones",
      description: "The grins of the victors, the moans of the defeated. We stride onward, unopposed.",
    },
    failure: {
      label: "One Soul Lost",
      startdescription: "We’ve fended them off, but one of mine was taken.",
      // no reward at tier 3 or 4, chance 10/30 percent chance to immediately spawn another guards! Event
      linked: [
        {
          id: "mariner.vaults.docksidewarehouse.events.guards.triggerrep4",
          chance: 20,
        },
        {
          id: "mariner.vaults.docksidewarehouse.events.guards.triggerrep3",
          chance: 10,
        },
      ],
    },
  });
}

function imprisonEvent() {
  mod.setRecipe("recipes.vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.events.imprisoncrewmember",
    actionId: "mariner.imprison",
    warmup: 1,
    label: "Bundled away!",
    startdescription:
      "These villains do not intend to kill, but to capture. Right this moment they struggle to seperate one from us.",
    description: "No one to imprison.",
    slots: [
      {
        id: "victim",
        label: "Crewmember",
        description: "One of our own.",
        greedy: true,
        required: { "mariner.crew": 1 },
      },
    ],
    linked: [
      {
        id: "mariner.vaults.docksidewarehouse.events.imprisoncrewmember.success",
      },
    ],
  });
  mod.setRecipe("recipes.vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.events.imprisoncrewmember.success",
    actionId: "mariner.imprison",
    label: "Bundled away!",
    startdescription: "One of mine was taken, and kept somewhere dark and safe.",
    aspects: {
      "mariner.aspects.vaults.docksidewarehouse.imprisoncrewmember": 1,
    },
  });
}

function generateEvents() {
  // EVENT imprison crewmember
  imprisonEvent();

  // EVENT Guards event wiring
  guardsEvent();

  // EVENT (Don Approaches)
  generateTresholdChallengeTask({
    recipesFile: "recipes.vaults.docksidewarehouse",
    actionId: "mariner.event.heart",
    task: {
      id: "mariner.vaults.docksidewarehouse.events.don",
      craftable: false,
      label: "The Don Approaches",
      startdescription: "A lieutenant came upon us. Fighting seems foolish, but he is willing to talk.",
    },
    bypass: requireAndDestroy("mariner.vaults.docksidewarehouse.insideinformation"),
    success: [
      { recipe: { requirements: { heart: 4 } }, suffix: "heart" },
      { recipe: { requirements: { grail: 4 } }, suffix: "grail" },
    ],
    successEnd: {
      label: "An Understanding",
      description:
        "They were mollified, for now. They’ll do their best to stop the buzzing of the bee-hive Warren. But this arrangement is no promise for the future.",
    },
    failure: {
      label: "Friends in High Places",
      description: '"Oh, I’ll make sure they know about you. I’ll make sure everyone knows about you."',
      // at tier 0, 1, 2 \". Give Reputation in a city in the sea\"  at tier 3 or 4, 10/30 chance to lead to a guard event.
      alt: [
        {
          id: "mariner.vaults.docksidewarehouse.events.guards.triggerrep4",
          chance: 20,
          additional: true,
        },
        {
          id: "mariner.vaults.docksidewarehouse.events.guards.triggerrep3",
          chance: 10,
          additional: true,
        },
      ],
      effects: {
        "mariner.lastingnotoriety": 5,
      },
    },
  });
}

function generateAlleyLayer() {
  // Layer 1 elements
  mod.setHiddenAspect("aspects.vaults.docksidewarehouse", {
    id: "mariner.aspects.vaults.docksidewarehouse.layer.1",
  });
  mod.setHiddenAspect("aspects.vaults.docksidewarehouse", {
    id: "mariner.aspects.vaults.docksidewarehouse.activelayer.1",
  });
  mod.setHiddenAspect("aspects.vaults.docksidewarehouse", {
    id: "mariner.aspects.vaults.docksidewarehouse.activeexclusivelayer.1",
  });

  mod.setElement("vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.crookedalley",
    label: "Crooked Alley",
    description: "Within the unsavory part of town lies a hidden empire. This street demarks its border.",
    aspects: {
      "mariner.local": 1,
      "mariner.aspects.vaults.docksidewarehouse.layer.1": 1,
      modded_explore_allowed: 1,
    },
  });

  mod.setElement("vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.drunkengangsters",
    icon: "tasks/mariner.tasks.charm.moth",
    label: "Drunken Gangsters",
    description:
      "In the crooked little alley is a crooked little bar, where the crooked little men celebrate their crooked little lives.",
    aspects: {
      "mariner.local": 1,
      "mariner.aspects.vaults.docksidewarehouse.layer.1": 1,
      modded_talk_allowed: 1,
    },
    slots: [
      {
        actionId: "talk",
        label: "Money",
        description: "Funds",
        required: { funds: 1 },
      },
      ...CHALLENGE_SLOTS(3, "talk"),
    ],
  });
  mod.setElement("vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.cornerwithaview",
    icon: "tasks/mariner.tasks.observe.darkred",
    label: "A Corner with a View",
    description: "An unassuming corner with a sightline to the warehouse. ",
    aspects: {
      "mariner.local": 1,
      "mariner.aspects.vaults.docksidewarehouse.layer.1": 1,
      modded_explore_allowed: 1,
    },
    slots: CHALLENGE_SLOTS(3, "explore"),
  });
  mod.setElement("vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.lot",
    icon: "tasks/mariner.tasks.door.knock",
    label: "Walled-in Warehouse Lot",
    description:
      "Next to the alley, a wall. Beyond the wall, a courtyard. Inside the courtyard, a warehouse. Inside the warehouse, a vault. And inside the vault, treasure.",
    aspects: {
      "mariner.local": 1,
      "mariner.aspects.vaults.docksidewarehouse.layer.1": 1,
      modded_explore_allowed: 1,
      "mariner.vaults.exclusivetask": 1,
    },
    slots: CHALLENGE_SLOTS(3, "explore"),
  });

  // Layer 1 recipes
  generateExplorationTask({
    vaultId: "docksidewarehouse",
    taskCompleteId: "mariner.vaults.docksidewarehouse.crookedalley",
    taskId: "crookedalley",
    label: "Explore the Crooked Alley",
    description: "What can we accomplish here? And how do we advance?",
    activityAspects: {
      "mariner.aspects.vaults.docksidewarehouse.activelayer.1": 1,
    },
    exclusionAspects: {
      "mariner.aspects.vaults.docksidewarehouse.activeexclusivelayer.1": -1,
    },
    exhaustedLabel: "Crooked Alley Explored",
    exhaustedDescription: "We’ve seen every inch of this place. There is nothing more for us here.",
    tasks: [
      "mariner.vaults.docksidewarehouse.drunkengangsters",
      "mariner.vaults.docksidewarehouse.cornerwithaview",
      "mariner.vaults.docksidewarehouse.lot",
    ],
  });

  mod.setRecipe("recipes.vaults.docksidewarehouse", {
    actionId: "talk",
    hintonly: true,
    id: "mariner.vaults.docksidewarehouse.drunkengangsters.hint",
    label: "Ply Information from the Gangsters",
    startdescription:
      "In the crooked little alley is a crooked little bar, where the crooked little men celebrate their crooked little lives. Perhaps we can cause their tongues to slip.",
    requirements: {
      "mariner.vaults.docksidewarehouse.drunkengangsters": 1,
      funds: -1,
    },
  });
  generateProbabilityChallengeTask({
    recipesFile: "recipes.vaults.docksidewarehouse",
    actionId: "talk",
    activityAspects: {
      "mariner.aspects.vaults.docksidewarehouse.activelayer.1": 1,
    },
    exclusionAspects: {
      "mariner.aspects.vaults.docksidewarehouse.activeexclusivelayer.1": -1,
    },
    task: {
      id: "mariner.vaults.docksidewarehouse.drunkengangsters.task",
      label: "Ply Information from the Gangsters",
      startdescription:
        "In the crooked little alley is a crooked little bar, where the crooked little men celebrate their crooked little lives. Perhaps we can cause their tongues to slip.",
      requirements: {
        "mariner.vaults.docksidewarehouse.drunkengangsters": 1,
        funds: 1,
      },
    },
    success: {
      label: "Meek As Lambs",
      description: '"No way. Do tell? Here, have another..."',
      effects: {
        "mariner.vaults.docksidewarehouse.insideinformation": 1,
        funds: -1,
      },
    },
    failure: {
      label: "Suspicion Raised",
      description: "Our questions were too obvious, our words too sweet. They know someone is watching.",
      effects: { "mariner.notoriety": 1 },
    },
    template: probabilityChallengeTemplates.HONEYED_TONGUE,
  });

  generateProbabilityChallengeTask({
    recipesFile: "recipes.vaults.docksidewarehouse",
    actionId: "explore",
    activityAspects: {
      "mariner.aspects.vaults.docksidewarehouse.activelayer.1": 1,
    },
    exclusionAspects: {
      "mariner.aspects.vaults.docksidewarehouse.activeexclusivelayer.1": -1,
    },
    task: {
      id: "mariner.vaults.docksidewarehouse.cornerwithaview.task",
      label: "Observe the Base",
      startdescription:
        "A quiet shadowed corner where we can spend sleepless nights to plan our entry. The more we see, the better we can prepare.",
      requirements: { "mariner.vaults.docksidewarehouse.cornerwithaview": 1 },
    },
    success: {
      label: "Patterns Perceived ",
      description: "Our eyes water and our mind is cotton, but we got what we need.",
      effects: { "mariner.vaults.docksidewarehouse.guardschedule": 1 },
    },
    failure: {
      label: "The Observer Observed",
      description: '"Oi, who\'s there? Get em!"',
      effects: { "mariner.notoriety": 1 },
    },
    template: probabilityChallengeTemplates.STUDY,
  });

  generateProbabilityChallengeTask({
    recipesFile: "recipes.vaults.docksidewarehouse",
    actionId: "explore",
    activityAspects: {
      "mariner.aspects.vaults.docksidewarehouse.activeexclusivelayer.1": 1,
    },
    exclusionAspects: {
      "mariner.aspects.vaults.docksidewarehouse.activelayer.1": -1,
    },
    bypass: requireAndDestroy("mariner.vaults.docksidewarehouse.guardschedule"),
    task: {
      id: "mariner.vaults.docksidewarehouse.lot.task",
      label: "Break into the Lot",
      startdescription: "Creep through corners and shimmy the lock. Let’s enter this compound unnoticed.",
      requirements: { "mariner.vaults.docksidewarehouse.lot": 1 },
    },
    success: {
      label: "Over the Wall",
      description: "We passed the gate, and entered the Yard.",
      effects: {
        "mariner.vaults.docksidewarehouse.lot": -1,
        "mariner.vaults.docksidewarehouse.craneyard": 1,
      },
      purge: { "mariner.aspects.vaults.docksidewarehouse.layer.1": 5 },
    },
    failure: {
      label: "Spotted!",
      description: "We were not as quiet as we ought to have been. We never even made it over the wall.",
      effects: {
        "mariner.notoriety": 1,
        "mariner.vaults.docksidewarehouse.guardschedule": -1,
      },
    },
    template: probabilityChallengeTemplates.SNEAK_IN,
  });
}

function generateYardLayer() {
  // Layer 2 elements
  mod.setHiddenAspect("aspects.vaults.docksidewarehouse", {
    id: "mariner.aspects.vaults.docksidewarehouse.layer.2",
  });
  mod.setHiddenAspect("aspects.vaults.docksidewarehouse", {
    id: "mariner.aspects.vaults.docksidewarehouse.activelayer.2",
  });
  mod.setHiddenAspect("aspects.vaults.docksidewarehouse", {
    id: "mariner.aspects.vaults.docksidewarehouse.activeexclusivelayer.2",
  });
  mod.setElement("vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.craneyard",
    label: "The Crane Yard",
    description: "A shipping and construction site. A legitimate face to an illegitimate operation.",
    aspects: {
      "mariner.local": 1,
      modded_explore_allowed: 1,
      "mariner.aspects.vaults.docksidewarehouse.layer.2": 1,
    },
  });
  mod.setElement("vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.sleepingmachines",
    label: "Sleeping Machines",
    icon: "tasks/mariner.tasks.tinker.forge",
    description: "The giants of industry rest here. We may be able to scavenge something from between their sleeping bones.",
    aspects: {
      "mariner.local": 1,
      "mariner.aspects.vaults.docksidewarehouse.layer.2": 1,
      modded_explore_allowed: 1,
    },
    slots: CHALLENGE_SLOTS(3, "explore"),
  });
  mod.setElement("vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.warehouse",
    label: "The Warehouse Itself",
    icon: "tasks/mariner.tasks.hiddenentrance.knock",
    description:
      "These cavernous brick halls and networked office building have been stripped of their original owners, yet lots of products still move through.",
    aspects: {
      "mariner.local": 1,
      "mariner.aspects.vaults.docksidewarehouse.layer.2": 1,
      modded_explore_allowed: 1,
    },
    slots: CHALLENGE_SLOTS(3, "explore"),
  });

  generateExplorationTask({
    vaultId: "docksidewarehouse",
    taskCompleteId: "mariner.vaults.docksidewarehouse.craneyard",
    taskId: "craneyard",
    label: "Explore the Crane Yard",
    description: "What can we accomplish here? And how do we advance?",
    activityAspects: {
      "mariner.aspects.vaults.docksidewarehouse.activelayer.2": 1,
    },
    exclusionAspects: {
      "mariner.aspects.vaults.docksidewarehouse.activeexclusivelayer.2": -1,
    },
    exhaustedLabel: "Crane Yard Explored",
    exhaustedDescription: "We’ve seen every inch of this place. There is nothing more for us here.",
    tasks: ["mariner.vaults.docksidewarehouse.sleepingmachines", "mariner.vaults.docksidewarehouse.warehouse"],
    events: ["mariner.vaults.docksidewarehouse.events.guards"],
  });

  // Tasks
  generateProbabilityChallengeTask({
    recipesFile: "recipes.vaults.docksidewarehouse",
    actionId: "explore",
    activityAspects: {
      "mariner.aspects.vaults.docksidewarehouse.activelayer.2": 1,
    },
    exclusionAspects: {
      "mariner.aspects.vaults.docksidewarehouse.activeexclusivelayer.2": -1,
    },
    task: {
      id: "mariner.vaults.docksidewarehouse.sleepingmachines.task",
      label: "Scrap the Machine",
      startdescription:
        "There are useful things here in the Yard. If we take the time, we could come out with some useful stuff. It will be noisy though.",
      requirements: { "mariner.vaults.docksidewarehouse.sleepingmachines": 1 },
    },
    success: {
      label: "Tools of the Trade",
      description:
        "We work quickly and we work quietly. Like scurrying rodents, we take what we can grab. But like foranging rats.",
      effects: {
        "mariner.trappings.forge.1": 1,
        "mariner.tool.forge.1": 1,
        "mariner.notoriety": 1,
      },
    },
    failure: {
      label: "A Commotion",
      description: "A mistake was made. Metal against metal, or the sound of breaking glass. Soon the guards will be upon us.",
      effects: {
        "mariner.notoriety": 1,
      },
      alt: [
        {
          id: "mariner.vaults.docksidewarehouse.events.guards",
          additional: true,
        },
      ],
    },
    template: probabilityChallengeTemplates.TAMPER,
  });
  generateProbabilityChallengeTask({
    recipesFile: "recipes.vaults.docksidewarehouse",
    actionId: "explore",
    task: {
      id: "mariner.vaults.docksidewarehouse.warehouse.task",
      label: "Gain Entry to the Warehouse",
      startdescription: "By brawn or guile, we need to get into the fortified warehouse itself.",
      requirements: { "mariner.vaults.docksidewarehouse.warehouse": 1 },
    },
    activityAspects: {
      "mariner.aspects.vaults.docksidewarehouse.activeexclusivelayer.2": 1,
    },
    exclusionAspects: {
      "mariner.aspects.vaults.docksidewarehouse.activelayer.2": -1,
    },
    success: {
      label: "Security breached",
      description: "Their hold on the stronghold is no longer strong.",
      effects: {
        "mariner.vaults.docksidewarehouse.warehouse": -1,
        "mariner.vaults.docksidewarehouse.warren": 1,
      },
      purge: { "mariner.aspects.vaults.docksidewarehouse.layer.2": 10 },
    },
    failure: {
      label: "Caught",
      description: "We found a route, but on the other side we were greeted with torch light and raised voices.",
      effects: { "mariner.notoriety": 1 },
      alt: [
        {
          id: "mariner.vaults.docksidewarehouse.events.guards",
          additional: true,
        },
      ],
    },
    template: probabilityChallengeTemplates.AVOID,
  });
}

function generateWarrenLayer() {
  // Layer 3 elements
  mod.setHiddenAspect("aspects.vaults.docksidewarehouse", {
    id: "mariner.aspects.vaults.docksidewarehouse.activelayer.3",
  });
  mod.setElement("vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.warren",
    label: "The Warren",
    description: "A labyrinth of passages, offices and refurbished store-rooms. Somewhere within these passages, lies a Vault.",
    aspects: {
      "mariner.local": 1,
      modded_explore_allowed: 1,
    },
  });
  mod.setElement("vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.safe",
    label: "The Safe",
    icon: "tasks/mariner.tasks.locked.goldenyellow",
    description: "A wrought iron box, man-high. Its number wheel stares at me, an unblinking eye lined with glyphs.",
    aspects: {
      "mariner.local": 1,
      modded_explore_allowed: 1,
    },
    slots: CHALLENGE_SLOTS(3, "explore"),
  });
  mod.setElement("vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.lockup",
    label: "The Lock-Up",
    icon: "tasks/mariner.tasks.cell",
    description:
      "The place where bad people keep those who wronged them. Some of my more careless friends might have found their way here.",
    aspects: {
      "mariner.local": 1,
      modded_explore_allowed: 1,
    },
    slots: CHALLENGE_SLOTS(3, "explore"),
  });

  generateExplorationTask({
    vaultId: "docksidewarehouse",
    taskCompleteId: "mariner.vaults.docksidewarehouse.warren",
    taskId: "warren",
    label: "Explore the Warren",
    description:
      "We tread lightly, peer around corners and listen at doors. We know where we want to go, but not where to find it.",
    activityAspects: {
      "mariner.aspects.vaults.docksidewarehouse.activelayer.3": 1,
    },
    exhaustedLabel: "The Warren Explored",
    exhaustedDescription: "We’ve seen every inch of this place. There is nothing more for us here.",
    tasks: ["mariner.vaults.docksidewarehouse.safe", "mariner.vaults.docksidewarehouse.lockup"],
    events: ["mariner.vaults.docksidewarehouse.events.guards", "mariner.vaults.docksidewarehouse.events.don"],
  });

  // Layer 3 Tasks
  generateProbabilityChallengeTask({
    recipesFile: "recipes.vaults.docksidewarehouse",
    actionId: "explore",
    task: {
      id: "mariner.vaults.docksidewarehouse.safe.task",
      label: "Open the Vault",
      startdescription: "With the right tools, or the right powders, we can gain entry to this vault.",
      requirements: { "mariner.vaults.docksidewarehouse.safe": 1 },
      grandReqs: { [LOCAL_REPUTATION]: -4 },
    },
    activityAspects: {
      "mariner.aspects.vaults.docksidewarehouse.activelayer.3": 1,
    },
    success: {
      label: "A Door Yields",
      description: "Hinges groan in submission. The contents are ours.",
      effects: { funds: 5, "mariner.vaults.docksidewarehouse.safe": -1 },
      linked: [{ id: "mariner.drawreward.precious.2" }],
    },
    failure: {
      label: "The Victory of Iron",
      description: "The barrier remains, taunting us... But this vault will not be left alone for long.",
      linked: [{ id: "mariner.vaults.docksidewarehouse.safe.failure.router" }],
    },
    template: probabilityChallengeTemplates.BREAK_IN,
  });
  // Safe task failure (guard or Don)
  mod.setRecipe("recipes.vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.safe.failure.router",
    actionId: "explore",
    linked: [
      { id: "mariner.vaults.docksidewarehouse.safe.failure.spawndon" },
      { id: "mariner.vaults.docksidewarehouse.safe.failure.spawnreputation" },
    ],
  });
  mod.setRecipe("recipes.vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.safe.failure.spawndon",
    actionId: "explore",
    alt: [{ id: "mariner.vaults.docksidewarehouse.events.don", additional: true }],
  });
  mod.setRecipe("recipes.vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.safe.failure.spawnreputation",
    actionId: "explore",
    grandReqs: { [LOCAL_REPUTATION]: -3 },
    effects: { "mariner.notoriety": 1 },
  });

  // Lock-up
  mod.setRecipe("recipes.vaults.docksidewarehouse", {
    actionId: "explore",
    hintonly: true,
    id: "mariner.vaults.docksidewarehouse.lockup.hint",
    label: "Prison Break",
    startdescription: "The Warehouse is at to high alarm for me to free the prisoners.",
    requirements: { "mariner.vaults.docksidewarehouse.lockup": 1 },
    grandReqs: { [LOCAL_REPUTATION]: 4 },
  });
  generateTresholdChallengeTask({
    recipesFile: "recipes.vaults.docksidewarehouse",
    actionId: "explore",
    bypass: requireAndDestroy("mariner.vaults.docksidewarehouse.insideinformation"),
    task: {
      id: "mariner.vaults.docksidewarehouse.lockup.task",
      label: "Prison Break",
      startdescription:
        "Friendly faces faced behind bars. They would be grateful, if we could get them. [I can try to tackle this using Knock.]",
      requirements: { "mariner.vaults.docksidewarehouse.lockup": 1 },
      grandReqs: { [LOCAL_REPUTATION]: -4 },
    },
    activityAspects: {
      "mariner.aspects.vaults.docksidewarehouse.activelayer.3": 1,
    },
    success: { requirements: { knock: 5 } },
    successEnd: {
      label: "Liberation",
      startdescription: "A satisfying click. A sigh of relief. A clasping of hands.",
      effects: { "mariner.vaults.docksidewarehouse.lockup": -1 },
      deckeffects: {
        "mariner.decks.specialists.opportunities.intellectual.free": 1,
      },
      linked: [{ id: "mariner.vaults.docksidewarehouse.lockup.freecrewmember" }],
    },
    failure: {
      label: "Foiled",
      description:
        "Hollow eyes stare at us, from the hollow between the iron. The door won’t budge. But someone heard the rattle of our efforts.",
      alt: [
        {
          id: "mariner.vaults.docksidewarehouse.events.guards",
          additional: true,
        },
      ],
    },
  });
  // free crewmember
  mod.setRecipe("recipes.vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.lockup.freecrewmember",
    actionId: "explore",
    warmup: 10,
    label: "Free the captives",
    startdescription: "Freeing the captives...",
    description: "They're all free now.",
    tablereqs: {
      "mariner.aspects.vaults.docksidewarehouse.imprisoned": 1,
    },
    aspects: { "mariner.aspects.vaults.docksidewarehouse.freecrewmember": 1 },
    slots: [
      {
        id: "victim",
        label: "Crewmember",
        description: "One of our Own",
        greedy: true,
        required: { "mariner.aspects.vaults.docksidewarehouse.imprisoned": 1 },
      },
    ],
    linked: [{ id: "mariner.vaults.docksidewarehouse.lockup.freecrewmember" }],
  });
}

export function generateDocksideWarehouseVault() {
  mod.initializeElementFile("vaults.docksidewarehouse", ["vaults", "docksidewarehouse"]);
  mod.initializeAspectFile("aspects.vaults.docksidewarehouse", ["vaults", "docksidewarehouse"]);
  mod.initializeRecipeFile("recipes.vaults.docksidewarehouse", ["vaults", "docksidewarehouse"]);

  // Items
  mod.setElement("vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.insideinformation",
    label: "Inside Information",
    description: "A useful scrap that can be used in a tight corner.",
    aspects: { "mariner.local": 1, "mariner.vaults.help": 1 },
  });
  mod.setElement("vaults.docksidewarehouse", {
    id: "mariner.vaults.docksidewarehouse.guardschedule",
    label: "Guard Schedule",
    description: "We know when the night is darkest, we know when the guards change, we know who drinks to pass the time.",
    aspects: { "mariner.local": 1, "mariner.vaults.help": 1 },
  });

  generatePrisoners();

  generateEvents();

  generateAlleyLayer();
  generateYardLayer();
  generateWarrenLayer();
}
