import { generateBleachedHeartCurse } from "./curses/bleached_heart.mjs";
import { generateLeadHeartCurse } from "./curses/lead_heart.mjs";
import { generateMisfortuneCurse } from "./curses/misfortune.mjs";
import { generateParchedThroatCurse } from "./curses/parched_throat.mjs";

export function CONCEALED_TEXT(concealedText, text) {
  return `@#mariner.curses.concealer|${concealedText}#|${text}@`;
}

export function generateCurses() {
  generateParchedThroatCurse();
  generateLeadHeartCurse();
  generateBleachedHeartCurse();
  generateMisfortuneCurse();
}

export function generateConcealedStateScaffhold(
  curseId,
  curseLabel,
  sleepingCurseDescription
) {
  const RECIPE_FILE = `recipes.curses.${curseId}`;
  const ELEMENT_FILE = `aspects.curses.${curseId}`;
  mod.initializeRecipeFile(RECIPE_FILE, ["curses", curseId]);
  mod.initializeElementFile(ELEMENT_FILE, ["curses", curseId]);

  // Concealer element
  mod.setElement(ELEMENT_FILE, {
    verbIcon: "mariner.curses.concealed",
    id: `mariner.curses.${curseId}.concealer`,
    isHidden: true,
    label: `${curseId} concealer element.`,
    aspects: { "mariner.curses.concealer": 1 },
  });

  mod.setRecipe(RECIPE_FILE, {
    id: `mariner.curses.${curseId}.start`,
    actionId: `mariner.curses.${curseId}`,
    linked: [{ id: `mariner.curses.${curseId}.sleep` }],
    effects: { [`mariner.curses.${curseId}.concealer`]: 1 },
  });

  mod.setRecipe(
    RECIPE_FILE,
    {
      id: `mariner.curses.${curseId}.sleep`,
      warmup: 800,
      label: CONCEALED_TEXT("Concealed Curse", curseLabel),
      startdescription: CONCEALED_TEXT(
        "This curse is concealed.",
        sleepingCurseDescription
      ),
      effects: { "mariner.curses.concealer": -1 },
      linked: [
        { id: `mariner.curses.${curseId}.applyeffect` },
        { id: `mariner.curses.${curseId}` },
      ],
    },
    true
  );
}
