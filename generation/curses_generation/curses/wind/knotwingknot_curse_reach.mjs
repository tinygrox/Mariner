import { generateConcealedStateScaffhold } from "../../generate_curses.mjs";

export function generateBaseCurseCurse() {
  const CURSE_ID = "knotwingknotsreach";
  const RECIPE_FILE = `recipes.curses.${CURSE_ID}`;
  generateConcealedStateScaffhold(
    CURSE_ID,
    "KnotwingKnot's Reach Curse",
    "KnotwingKnot's Reach is approaching..."
  );

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.knotwingknotsreach",
    warmup: 20,
    effects: { "mariner.curses.lifetimecounter": 1 },
    startdescription: "The KnotwingKnot's Reach is approaching...",
    linked: [
      { id: "mariner.curses.knotwingknotsreach.applyeffect" },
      { id: "mariner.curses.knotwingknotsreach.end" },
      { id: "mariner.curses.knotwingknotsreach" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.knotwingknotsreach.applyeffect",
    grandReqs: {
      "~/tabletop:{verb/sing}:recipeaspect/mariner.signal.performing": 1,
    },
    furthermore: [
      { target: "~/tabletop!sing[situationstorage]+", effects: { dread: 1 } },
    ],
    linked: [
      { id: "mariner.curses.knotwingknotsreach.end" },
      { id: "mariner.curses.knotwingknotsreach" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.knotwingknotsreach.end",
    label: "KnotwingKnot's Reach Curse Clearing",
    description: "nnn like nnn...",
    requirements: { "mariner.curses.lifetimecounter": 50 },
    effects: { "mariner.curses.lifetimecounter": -50 },
  });
}

export const SPAWN_PARCHED_THROAT_CURSE = {
  id: "mariner.curses.knotwingknotsreach.start",
  additional: true,
};
