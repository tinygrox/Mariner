import { generateConcealedStateScaffhold } from "../../generate_curses.mjs";

export function generateBaseCurseCurse() {
  const CURSE_ID = "courserspull";
  const RECIPE_FILE = `recipes.curses.${CURSE_ID}`;
  generateConcealedStateScaffhold(
    CURSE_ID,
    "The Courser's Pull Curse",
    "The Courser's Pull is approaching..."
  );

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.courserspull",
    warmup: 20,
    effects: { "mariner.curses.lifetimecounter": 1 },
    startdescription: "The Courser's Pull is approaching...",
    linked: [
      { id: "mariner.curses.courserspull.applyeffect" },
      { id: "mariner.curses.courserspull.end" },
      { id: "mariner.curses.courserspull" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.courserspull.applyeffect",
    furthermore: [
      { target: "~/tabletop!sing[situationstorage]+", effects: { dread: 1 } },
    ],
    linked: [
      { id: "mariner.curses.courserspull.end" },
      { id: "mariner.curses.courserspull" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.courserspull.end",
    label: "The Courser's Pull Clearing",
    description: "nnn like nnn...",
    requirements: { "mariner.curses.lifetimecounter": 50 },
    effects: { "mariner.curses.lifetimecounter": -50 },
  });
}

export const SPAWN_PARCHED_THROAT_CURSE = {
  id: "mariner.curses.courserspull.start",
  additional: true,
};
