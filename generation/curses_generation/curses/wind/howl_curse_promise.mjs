import { generateConcealedStateScaffhold } from "../../generate_curses.mjs";

export function generateBaseCurseCurse() {
  const CURSE_ID = "howlspromise";
  const RECIPE_FILE = `recipes.curses.${CURSE_ID}`;
  generateConcealedStateScaffhold(
    CURSE_ID,
    "Howl's Promise Curse",
    "Howl's Promise is approaching..."
  );

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.howlspromise",
    warmup: 20,
    effects: { "mariner.curses.lifetimecounter": 1 },
    startdescription: "The Howl's Promise is approaching...",
    linked: [
      { id: "mariner.curses.howlspromise.applyeffect" },
      { id: "mariner.curses.howlspromise.end" },
      { id: "mariner.curses.howlspromise" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.howlspromise.applyeffect",
    grandReqs: {
      "~/tabletop:{verb/sing}:recipeaspect/mariner.signal.performing": 1,
    },
    furthermore: [
      { target: "~/tabletop!sing[situationstorage]+", effects: { dread: 1 } },
    ],
    linked: [
      { id: "mariner.curses.howlspromise.end" },
      { id: "mariner.curses.howlspromise" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.howlspromise.end",
    label: "Howl's Promise Clearing",
    description: "nnn like nnn...",
    requirements: { "mariner.curses.lifetimecounter": 50 },
    effects: { "mariner.curses.lifetimecounter": -50 },
  });
}

export const SPAWN_PARCHED_THROAT_CURSE = {
  id: "mariner.curses.howlspromise.start",
  additional: true,
};
