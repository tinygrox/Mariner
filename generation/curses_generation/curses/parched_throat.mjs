import { generateConcealedStateScaffhold } from "../generate_curses.mjs";

export function generateParchedThroatCurse() {
  const CURSE_ID = "parchedthroat";
  const RECIPE_FILE = `recipes.curses.${CURSE_ID}`;
  generateConcealedStateScaffhold(
    CURSE_ID,
    "Parched Throat Curse",
    "The Parched Throat Curse is approaching..."
  );

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.parchedthroat",
    warmup: 20,
    effects: { "mariner.curses.lifetimecounter": 1 },
    startdescription: "The Parched Throat Curse is approaching...",
    linked: [
      { id: "mariner.curses.parchedthroat.applyeffect.performing" },
      { id: "mariner.curses.parchedthroat.applyeffect.exhauststory" },
      { id: "mariner.curses.parchedthroat.end" },
      { id: "mariner.curses.parchedthroat" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.parchedthroat.applyeffect.performing",
    grandReqs: {
      "~/tabletop:{verb/sing}:recipeaspect/mariner.signal.performing": 1,
    },
    furthermore: [
      { target: "~/tabletop!sing[situationstorage]+", effects: { "mariner.experiences.rattling.standby": 1 }, },
    ],
    linked: [
      { id: "mariner.curses.parchedthroat.end" },
      { id: "mariner.curses.parchedthroat" },
    ],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.parchedthroat.applyeffect.exhauststory",
    grandReqs: {
      "~/tabletop:{verb/sing}:recipeaspect/mariner.signal.exhauststory": 1,
    },
    furthermore: [
      { target: "~/tabletop!sing[situationstorage]+", effects: { "mariner.experiences.rattling.standby": 1 }, },
    ],
    linked: [
      { id: "mariner.curses.parchedthroat.end" },
      { id: "mariner.curses.parchedthroat" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.parchedthroat.end",
    label: "Parched Throat Curse Clearing",
    description: "Scattered like soft sand...",
    requirements: { "mariner.curses.lifetimecounter": 50 },
    effects: { "mariner.curses.lifetimecounter": -50 },
  });
}

export const SPAWN_PARCHED_THROAT_CURSE = {
  id: "mariner.curses.parchedthroat.start",
  additional: true,
};
