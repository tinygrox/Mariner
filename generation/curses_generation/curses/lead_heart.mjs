import { SIGNALS } from "../../generate_signal_aspects.mjs";
import { generateConcealedStateScaffhold } from "../generate_curses.mjs";

export function generateLeadHeartCurse() {
  const CURSE_ID = "leadheart";
  const RECIPE_FILE = `recipes.curses.${CURSE_ID}`;
  generateConcealedStateScaffhold(
    CURSE_ID,
    "Lead Heart Curse",
    "The Lead Heart Curse is approaching..."
  );

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.leadheart",
    warmup: 20,
    effects: { "mariner.curses.lifetimecounter": 1 },
    startdescription: "The Lead Heart Curse is approaching...",
    linked: [
      { id: "mariner.curses.leadheart.applyeffect" },
      { id: "mariner.curses.leadheart.end" },
      { id: "mariner.curses.leadheart" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.leadheart.applyeffect",
    grandReqs: { "[~/tabletop[situationstorage]+:mariner.lackheart]": 1 },
    furthermore: [
      {
        target: "~/tabletop[situationstorage]+",
        aspects: { [SIGNALS.EXHAUST_LH]: 1 },
      },
    ],
    linked: [
      { id: "mariner.curses.leadheart.end" },
      { id: "mariner.curses.leadheart" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.leadheart.end",
    label: "Lead Heart Curse Clearing",
    description: "Crumbling like rusting ore...",
    requirements: { "mariner.curses.lifetimecounter": 50 },
    effects: { "mariner.curses.lifetimecounter": -50 },
  });
}

export const SPAWN_LEAD_HEART_CURSE = {
  id: "mariner.curses.leadheart.start",
  additional: true,
};
