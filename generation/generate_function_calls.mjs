import { ME_RECIPE } from "./generation_helpers.mjs";

const ASPECTS_FILE = "aspects.functions";
const ELEMENTS_FILE = "functions.callers";
const CATALYST_ID = "mariner.function.caller";

export function CALLER_ID(id) {
  return `mariner.${id}.caller`;
}

export function CALL_FUNCTION(id, recipeId) {
  if (!mod.aspects[ASPECTS_FILE]) mod.initializeAspectFile(ASPECTS_FILE, ["functions"]);
  if (!mod.elements[ELEMENTS_FILE]) mod.initializeElementFile(ELEMENTS_FILE, ["functions"]);

  mod.setAspect(ASPECTS_FILE, { id: CATALYST_ID });

  const CALLER_ELEMENT_ID = CALLER_ID(id);

  if (!mod.getElement(ELEMENTS_FILE, CALLER_ELEMENT_ID)) {
    mod.setElement(ELEMENTS_FILE, {
      id: CALLER_ELEMENT_ID,
      lifetime: 1,
      xtriggers: {
        [CATALYST_ID]: [{ morpheffect: "destroy" }, { morpheffect: "apply" }, ME_RECIPE(`mariner.${id}`)],
      },
    });
  }

  return [
    {
      effects: { [CALLER_ELEMENT_ID]: 1 },
    },
    {
      aspects: { [CATALYST_ID]: 1 },
    },
  ];
}

export function CALL_FUNCTIONS(...ids) {
  const finalFurthermore = [{ effects: {} }, { aspects: { [CATALYST_ID]: 1 } }];
  const furthermoreObjects = ids.map((id) => CALL_FUNCTION(id));
  for (const obj of furthermoreObjects) {
    finalFurthermore[0].effects[Object.keys(obj[0].effects)[0]] = 1;
  }
  return finalFurthermore;
}
