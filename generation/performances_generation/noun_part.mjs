export const performanceNounPart = {
  grail: {
    moth: {
      label: "No magick here!",
      description: "No magick here!",
    },
    lantern: {
      label: "No magick here!",
      description: "No magick here!",
    },
    forge: {
      label: "No magick here!",
      description: "No magick here!",
    },
    edge: {
      label: "No magick here!",
      description: "No magick here!",
    },
    winter: {
      label: "No magick here!",
      description: "No magick here!",
    },
    heart: {
      label: "No magick here!",
      description: "No magick here!",
    },
    knock: {
      label: "No magick here!",
      description: "No magick here!",
    },
  },
  moth: {
    grail: {
      label: "No magick here!",
      description: "No magick here!",
    },
    lantern: {
      label: "No magick here!",
      description: "No magick here!",
    },
    forge: {
      label: "No magick here!",
      description: "No magick here!",
    },
    edge: {
      label: "No magick here!",
      description: "No magick here!",
    },
    winter: {
      label: "No magick here!",
      description: "No magick here!",
    },
    heart: {
      label: "No magick here!",
      description: "No magick here!",
    },
    knock: {
      label: "No magick here!",
      description: "No magick here!",
    },
  },
  lantern: {
    grail: {
      label: "No magick here!",
      description: "No magick here!",
    },
    moth: {
      label: "The Ballad to Sunset Cealia",
      description: [
        "Caelia, my great mystery<br>Do you enjoy the way you're chased by me<br>Is that why I glimpse your glances<br>At the fêtes and at the dances<br><br>Caelia I know what is said of you<br>You’re a player and a dreamer too<br>In the moonlight you will lead us on<br>But every time by sunrise you are gone",
        "Caelia please enlighten me<br>Only with you am I truly free<br>Lift this veil that clouds my being<br>Only in your eyes I'm truly seeing<br><br>Caelia’s can’t you hear I'm blue<br>I even wrote this song for you",
      ],
    },
    forge: {
      label: "No magick here!",
      description: "No magick here!",
    },
    edge: {
      label: "No magick here!",
      description: "No magick here!",
    },
    winter: {
      label: "No magick here!",
      description: "No magick here!",
    },
    heart: {
      label: "No magick here!",
      description: "No magick here!",
    },
    knock: {
      label: "No magick here!",
      description: "No magick here!",
    },
  },
  forge: {
    grail: {
      label: "No magick here!",
      description: "No magick here!",
    },
    moth: {
      label: "No magick here!",
      description: "No magick here!",
    },
    lantern: {
      label: "No magick here!",
      description: "No magick here!",
    },
    edge: {
      label: "No magick here!",
      description: "No magick here!",
    },
    winter: {
      label: "No magick here!",
      description: "No magick here!",
    },
    heart: {
      label: "No magick here!",
      description: "No magick here!",
    },
    knock: {
      label: "No magick here!",
      description: "No magick here!",
    },
  },
  edge: {
    grail: {
      label: "Counting The Winds",
      description:
      [
        "Who runs the Winds, Who runs the Wild Hunt?<br>Who herds the clouds, who whips up the stormfronts?<br><br><br>",
        "Coarser hunts the Seawinds, the Wild Winds<br><br>Föhn!<br>Mistral!<br>Kataburan!<br>",
        "Imago sails the Trade Winds, the Scorch Winds<br><br>Sirocco!<br>No'western!<br>Harmattan!<br>",
        "Knotwing rides the Dream Winds, the Mad Winds<br><br>Doctor!<br>st Anna's!<br>Pampero!<br>",
        "The Howl stalks the Chill Winds, the Death Winds<br><br>Citanoq!<br>Boran!<br>Kamigari!<br>"]
    },
    moth: {
      label: "No magick here!",
      description: "No magick here!",
    },
    lantern: {
      label: "No magick here!",
      description: "No magick here!",
    },
    forge: {
      label: "No magick here!",
      description: "No magick here!",
    },
    winter: {
      label: "No magick here!",
      description: "No magick here!",
    },
    heart: {
      label: "The Concord of Queens",
      description:
      [
        "A knight he marches on he marches on <br>Through the lands he marches on...<br>",
        "The stone queen speaks<br>She speaks the words of Duty<br>Of the ties that bind<br>And the lines that divide <br>And of those borders which never should be crossed at all.<br> <br>But the knight he marches on, he marches on<br>To his doom he marches on<br> ",
        "As the wild queen sings<br>She sings the whimper of desire<br>Of the whims on wings of whimsy<br>To be cherished and discarded<br>But which now hurt more than they should<br> <br> As the knight he marches on, he marches on<br> For his love he marches on<br>",
        "As the red queen speaks<br>She speaks of demands of true devotion<br>Of her aspirations for their future<br>And sacrifices now required,<br>An accord that must be made <br> <br>So the pine knight marches on he marches on <br>To a knotted noose he marches on",
      ]
      },
    knock: {
      label: "No magick here!",
      description: "No magick here!",
    },
  },
  winter: {
    grail: {
      label: "No magick here!",
      description: "No magick here!",
    },
    moth: {
      label: "Yuki to Ame",
      description:
        "Snow hides bloodstained earth<br>Or spring showers rinse the soil<br>Both seasons cleanse me",
    },
    lantern: {
      label: "No magick here!",
      description: "No magick here!",
    },
    forge: {
      label: "No magick here!",
      description: "No magick here!",
    },
    edge: {
      label: "No magick here!",
      description: "No magick here!",
    },
    heart: {
      label: "Today We Mourn",
      description:
        ["Quiet the clocks, stops the gallows swinging<br>Shutter the windows, still the bells a-ringing<br>Today we mourn a king<br><br>Freeze the forests, break the waves to mirrors flat<br>Shroud the sky, veil the sun in pale regret<br>Today we mourn the oldest king<br>",
        "No more shall the forest rustle, no more shall the mountains quake<br> No more shall he set the reeds a shiver, no more plains shall rumble in his wake<br><br>Quell the usurpers rumble<br>Let not a single fall leave tumble<br>Today we mark a loss the world has had<br>For the thunderking is dead",  
        ]
      },
    knock: {
      label: "No magick here!",
      description: "No magick here!",
    },
  },
  heart: {
    grail: {
      label: "No magick here!",
      description: "No magick here!",
    },
    moth: {
      label: "No magick here!",
      description: "No magick here!",
    },
    lantern: {
      label: "No magick here!",
      description: "No magick here!",
    },
    forge: {
      label: "No magick here!",
      description: "No magick here!",
    },
    edge: {
      label: "No magick here!",
      description: "No magick here!",
    },
    winter: {
      label: "No magick here!",
      description: "No magick here!",
    },
    knock: {
      label: "No magick here!",
      description: "No magick here!",
    },
  },
  knock: {
    grail: {
      label: "No magick here!",
      description: "No magick here!",
    },
    moth: {
      label: "No magick here!",
      description: "No magick here!",
    },
    lantern: {
      label: "No magick here!",
      description: "No magick here!",
    },
    forge: {
      label: "No magick here!",
      description: "No magick here!",
    },
    edge: {
      label: "No magick here!",
      description: "No magick here!",
    },
    winter: {
      label: "No magick here!",
      description: "No magick here!",
    },
    heart: {
      label: "No magick here!",
      description: "No magick here!",
    },
  },
};
