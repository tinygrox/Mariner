import { LORE_ASPECTS } from "../generation_helpers.mjs";
import { generateQuellWeatherPerformance } from "./effects/quell_weather.mjs";
import { generateRevealCursesPerformance } from "./effects/reveal_curses.mjs";
import { generateRouseWeatherPerformance } from "./effects/rouse_weather.mjs";
import { generateStopCursePerformance } from "./effects/stop_curse.mjs";
import { generateMendingCharacterPerformance } from "./effects/mend_character.mjs";
import { performanceVerbHints } from "./hints.mjs";
import { performanceVerbPart } from "./verb_part.mjs";
import { performanceNounPart } from "./noun_part.mjs";
import { performanceVerbPreview } from "./previews.mjs";
import { SIGNALS } from "../generate_signal_aspects.mjs";
import { generateCrewTraumaClearingPerformance } from "../crew_generation/generate_trauma_system.mjs";

function computeLinkedNounRecipe(verbAspect) {
  return LORE_ASPECTS.filter((lore) => lore !== verbAspect).map((aspect) => ({
    id: `mariner.performances.${verbAspect}.${aspect}`,
  }));
}

let performanceAspectsCombinations = null;

function computeLinkedPerformanceChecks(verbAspect, nounAspect, nounSuccessId) {
  if (!nounSuccessId) return [{ id: "mariner.performances.failure" }];
  return [
    {
      id: nounSuccessId,
      chance: `45 + ((Min([${verbAspect}], [${nounAspect}]) + Round(Abs([${verbAspect}]-[${nounAspect}])/2)) * 50) / 20`,
      // chance: 100,
    },
    { id: "mariner.performances.failure" },
  ];
}

// Generate all the recipes for performances
export function generatePerformances() {
  mod.initializeRecipeFile("recipes.performances", ["performances"]);
  mod.setRecipe("recipes.performances", {
    id: "mariner.performances.failure",
    label: "An Untranscendent Performance",
    description:
      "I did not reach beyond today. The performance did not transcend, and the only thing that comes to me from this night will be a scathing review in the morning. The audience shuffles out, feeling disapointed in ways they cannot understand.",
    effects: { "mariner.notoriety": 1 },
  });

  performanceAspectsCombinations = {
    grail: {},
    moth: {},
    lantern: {
      moth: generateRevealCursesPerformance(),
    },
    forge: {},
    edge: {
      heart: generateMendingCharacterPerformance(),
      grail: generateRouseWeatherPerformance(),
    },
    winter: {
      heart: generateQuellWeatherPerformance(),
      moth: generateStopCursePerformance(),
      lantern: generateCrewTraumaClearingPerformance(),
    },
    heart: {},
    knock: {},
  };

  // For each lore aspect
  for (const verbAspect of LORE_ASPECTS) {
    mod.initializeRecipeFile(`recipes.performances.${verbAspect}`, ["performances"]);

    mod.setRecipe(`recipes.performances.${verbAspect}`, {
      id: `mariner.performances.${verbAspect}.hint`,
      label: performanceVerbHints[verbAspect]?.label ?? `Try to do a ${mod.helpers.capitalize(verbAspect)} Performance?`,
      startdescription:
        performanceVerbHints[verbAspect]?.description ??
        `Do I wish to grace this theater with a performance laced with ${mod.helpers.capitalize(verbAspect)}?`,
      actionid: "mariner.sing",
      hintOnly: true,
      requirements: {
        "mariner.theater": 1,
        [`mariner.song.${verbAspect}`]: 1,
        [verbAspect]: -4,
      },
    });
    mod.setRecipe(`recipes.performances.${verbAspect}`, {
      id: `mariner.performances.${verbAspect}.start`,
      previewLabel: ` Start A Performance of ${mod.helpers.capitalize(verbAspect)}`,
      previewDescription:
        performanceVerbPreview[verbAspect] ?? "<[This text should not appear! If it did, contact the creators of Mariner]",
      label: `Perform a Show laced with the Intent of ${mod.helpers.capitalize(verbAspect)}`,
      startdescription: performanceVerbPart[verbAspect] ?? "First part of performance",
      warmup: 10,
      actionId: "mariner.sing",
      craftable: true,
      requirements: {
        "mariner.theater": 1,
        [`mariner.song.${verbAspect}`]: 1,
        [verbAspect]: 4,
      },
      aspects: { [SIGNALS.PERFORMING]: 1 },
      linked: [{ id: `mariner.performances.${verbAspect}.addnoun` }],
    });

    mod.setRecipe(`recipes.performances.${verbAspect}`, {
      id: `mariner.performances.${verbAspect}.addnoun`,
      warmup: 20,
      startdescription: "Now comes the time for me to channel this energy into whatever target I desire.",
      slots: [
        {
          id: "noun",
          label: "Direction",
          description: "Now that my Intent is set, I have to set my Direction. What do I wish to affect?",
          required: {
            "mariner.song": 1,
          },
        },
      ],
      aspects: { [SIGNALS.PERFORMING]: 1 },
      linked: [...computeLinkedNounRecipe(verbAspect), { id: "mariner.performances.failure" }],
    });

    // For each noun
    for (const nounAspect of LORE_ASPECTS) {
      if (verbAspect === nounAspect) continue;
      const nounSuccessId = performanceAspectsCombinations[verbAspect][nounAspect]
        ? `mariner.performances.${verbAspect}.${nounAspect}.success.1`
        : "mariner.performances.failure";

      mod.setRecipe(`recipes.performances.${verbAspect}`, {
        id: `mariner.performances.${verbAspect}.${nounAspect}`,
        requirements: {
          [`mariner.song.${verbAspect}`]: 1,
          [`mariner.song.${nounAspect}`]: 1,
        },
        aspects: { [SIGNALS.PERFORMING]: 1 },
        linked: computeLinkedPerformanceChecks(verbAspect, nounAspect, nounSuccessId),
      });

      if (performanceAspectsCombinations[verbAspect][nounAspect]) {
        const label = performanceNounPart[verbAspect][nounAspect].label;
        let descriptions = performanceNounPart[verbAspect][nounAspect].description;
        if (typeof descriptions === "string") descriptions = [descriptions];

        let totalWarmups = descriptions.length === 1 ? 30 : 40;

        for (let i = 0; i < descriptions.length; i++) {
          mod.setRecipe(`recipes.performances.${verbAspect}`, {
            id: `mariner.performances.${verbAspect}.${nounAspect}.success.${i + 1}`,
            label,
            startdescription: descriptions[i],
            warmup: totalWarmups / descriptions.length,
            aspects: { [SIGNALS.PERFORMING]: 1 },
            linked: [
              {
                id:
                  i < descriptions.length - 1
                    ? `mariner.performances.${verbAspect}.${nounAspect}.success.${i + 2}`
                    : performanceAspectsCombinations[verbAspect][nounAspect],
              },
            ],
          });
        }
      }
    }
  }
}
