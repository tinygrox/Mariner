export function generateStopCursePerformance() {
  const RECIPE_FILE = "recipes.performances.stopcurse";
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "stopcurse"]);

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.stopcurse",
    label: "Try To Rid Myself Of A Curse",
    startdescription:
      "Here on the stage, I align myself to the rhythms of the world and it's stern king above. The curse holds on to my soul like a barnacle bitten deep into the rocks. I must leave the theater with this blemish still scarring me.",
    warmup: 20,
    slots: [
      {
        id: "trapping",
        label: "The Trappings to Channel",
        required: {
          "mariner.trapping": 1,
        },
      },
    ],
    linked: [
      { id: "mariner.performances.stopcurse.parchedthroat" },
      { id: "mariner.performances.stopcurse.leadheart" },
      { id: "mariner.performances.stopcurse.bleachedheart" },
      { id: "mariner.performances.stopcurse.misfortune" },
    ],
    achievements: ["mariner.achievements.firstsuccessfulperformance"],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.stopcurse.cleanup",
    furthermore: [
      {
        target: "~/exterior",
        decays: ["mariner.curses.lifetimecounter"],
      },
    ],
    effects: { "mariner.trapping": -1 },
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.stopcurse.parchedthroat",
    requirements: { "mariner.trappings.grail": 1 },
    label: "Unseat the Parched Troat Curse",
    startdescription:
      "On my final exhale, I feel dust clatter against my teeth and push passed my lips. I can only hope that those many breathing souls in the audience will not carry home what I just expulsed onto them.",
    haltverb: { "mariner.curses.parchedthroat": 1 },
    linked: [{ id: "mariner.performances.stopcurse.cleanup" }],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.stopcurse.leadheart",
    requirements: { "mariner.trappings.edge": 1 },
    label: "Unburden Myself of the Lead Heart Curse",
    startdescription:
      "I topple to the stage, heaving. While I produce nothing but bile, I feel a lightness where there was lead before.",
    haltverb: { "mariner.curses.leadheart": 1 },
    linked: [{ id: "mariner.performances.stopcurse.cleanup" }],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.stopcurse.bleachedheart",
    requirements: { "mariner.trappings.lantern": 1 },
    label: "Undo the Bleached Heart Curse",
    startdescription:
      "My vision clears and I almost cry at the intensities of blues and richness of reds that again fill the tapestry of the world.",
    haltverb: { "mariner.curses.bleachedheart": 1 },
    linked: [{ id: "mariner.performances.stopcurse.cleanup" }],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.stopcurse.misfortune",
    requirements: { "mariner.trappings.heart": 1 },
    label: "Unravel the Curse of Misfortunies",
    startdescription:
      "Knots untangle in my back, and I can almost hear the shackles slump to the floor. This night, all around the theaters mirror crack and milk curdles. Misfortune now holds court, and will until the west winds comes in sweeping.",
    haltverb: { "mariner.curses.misfortune": 1 },
    linked: [{ id: "mariner.performances.stopcurse.cleanup" }],
  });

  return "mariner.performances.stopcurse";
}
