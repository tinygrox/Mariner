import { SIGNALS } from "../../generate_signal_aspects.mjs";

export function generateRouseWeatherPerformance() {
  const RECIPE_FILE = "recipes.performances.rouseweather";
  const ASPECT_FILE = "aspects.performances.rouseweather";
  mod.initializeAspectFile(ASPECT_FILE, ["performances", "rouseweather"]);
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "rouseweather"]);

  mod.setAspect(ASPECT_FILE, { id: "mariner.performanceeffects.rouseweather" });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.rouseweather",
    label: "The Hunt-Pack Courses In",
    startdescription:
      "One does not tame the winds. They are not your servants, for you to command at your beck and call. But if you learn their Names you can implore them...and how they ache to be unleashed.",
    warmup: 20,
    furthermore: [
      {
        target: "~/exterior",
        aspects: { [SIGNALS.PERFORMANCE_ROUSE_WEATHER]: 1 },
      },
    ],
    achievements: ["mariner.achievements.firstsuccessfulperformance"],
  });
  return "mariner.performances.rouseweather";
}
