import { SIGNALS } from "./generate_signal_aspects.mjs";
import { simpleBuyItem } from "./generate_store_actions.mjs";
import { optionSelectionRecipe } from "./option_selector.mjs";
import { SHIP_STAT } from "./stats_generation.mjs";

function addAspectsAndRemoveOthers(aspects, removalSignal) {
  return [
    {
      aspects: { [removalSignal]: 1 },
      mutations: Object.entries(aspects).map(([id, quantity]) => ({
        filter: "mariner.ship",
        mutate: id,
        level: quantity,
        additive: true,
      })),
    },
  ];
}

function installAspects(aspects) {
  return Object.entries(aspects).map(([id, quantity]) => ({
    morpheffect: "mutate",
    id: id,
    level: quantity,
  }));
}

function removeAspects(aspects) {
  return Object.entries(aspects).map(([id, quantity]) => ({
    morpheffect: "mutate",
    id: id,
    level: -quantity,
  }));
}

function configurationOption({
  id,
  category,
  label,
  aspectDescription,
  slotDescription,
  description,
  cost,
  appliedAspects,
  installationSignal,
  removalSignal,
}) {
  cost = cost ?? 10;

  const configurationId = `mariner.shipconfigurations.${id}`;
  const configurationAspects = {
    [configurationId]: 1,
    ...(appliedAspects ?? {}),
  };

  mod.setAspects("aspects.shipconfigurations", {
    id: configurationId,
    label: label,
    description: `<size=120%><color=#201d59>Ship Configuration: ${category}</color></size>\n\n${aspectDescription ?? "<>"}`,
    xtriggers: {
      [installationSignal]: installAspects(appliedAspects),
      [removalSignal]: removeAspects(configurationAspects),
    },
  });

  return {
    slot: { label, description: slotDescription },
    recipes: [
      {
        label: `<Not Enough Funds To Install: ${label}>`,
        startdescription: "<Not enough funds to change that configuration.>",
        grandReqs: { "[~/exterior : funds]": -cost },
        linked: [{ id: "mariner.configureship" }],
      },
      {
        label: `<Install: ${label}>`,
        startdescription: description ?? slotDescription,
        grandReqs: { "[~/exterior : funds]": cost },
        furthermore: [
          // Remove potential previous configuration
          {
            aspects: { [removalSignal]: 1 },
          },
          // Mutate the new configuration
          {
            mutations: [
              {
                filter: "mariner.ship",
                mutate: configurationId,
                level: 1,
              },
            ],
          },
          // Signal this new aspect to apply the configuration aspects
          {
            aspects: { [installationSignal]: 1 },
          },
          // Pay for the change
          {
            target: "~/exterior",
            effects: { funds: -cost },
          },
        ],
        linked: [{ id: "mariner.configureship" }],
      },
    ],
  };
}

const riggingConfigurationOption = (args) =>
  configurationOption({
    category: "Rigging",
    installationSignal: SIGNALS.INSTALL_RIGGING_CONFIGURATION,
    removalSignal: SIGNALS.CLEAR_RIGGING_CONFIGURATION,
    ...args,
  });

const holdConfigurationOption = (args) =>
  configurationOption({
    category: "Hold",
    installationSignal: SIGNALS.INSTALL_HOLD_CONFIGURATION,
    removalSignal: SIGNALS.CLEAR_HOLD_CONFIGURATION,
    ...args,
  });

const hullConfigurationOption = (args) =>
  configurationOption({
    category: "Hull",
    installationSignal: SIGNALS.INSTALL_HULL_CONFIGURATION,
    removalSignal: SIGNALS.CLEAR_HULL_CONFIGURATION,
    ...args,
  });

function generateWharfShipConfigurationAction() {
  mod.initializeAspectFile("aspects.shipconfigurations", ["ship"]);

  mod.initializeRecipeFile("recipes.configureship", ["locations", "wharves"]);
  mod.setRecipes(
    "recipes.configureship",
    // Main option picker (configuration to change)
    {
      id: "mariner.configureship.start",
      actionId: "explore",
      craftable: true,
      grandReqs: {
        "mariner.wharf": 1,
        "mariner.ship": 1,
        "mariner.halfheart": 1,
      },
      label: "<Reconfigure Parts of my Ship>",
      startdescription:
        "<Each part of the Kite can be tweaked to suit its current goal better. Each part can only be set in a single configuration at a time.<br>Which part do I want to reconfigure?>",
      movements: {
        "~/tabletop": "mariner.halfheart",
      },
      linked: [{ id: "mariner.configureship" }],
    },
    ...optionSelectionRecipe({
      id: "mariner.configureship",
      label: "<Reconfigure Parts of my Ship>",
      startdescription:
        "<Each part of the Kite can be tweaked to suit its current goal better. Each part can only be set in a single configuration at a time.<br>Which part do I want to reconfigure?>",
      description: "<All good then.>",
      options: [
        {
          slot: {
            label: "Rigging",
            description: "<where the architecture of rope and canvas meets the geometry of aviation>",
          },
          recipe: {
            label: "Reconfigure the Rigging",
            startdescription:
              "Each set of sails and masts fits a purpose. By changing them, I could catch the wind better, make myself less visible at sea, or even prepare to accelerate more on my longer voyages.",
            linked: [{ id: "mariner.configureship.rigging" }],
          },
        },
        {
          slot: {
            label: "Hold",
            description: "<The hollow heart spaces whose function borders on the titular.>",
          },
          recipe: {
            label: "Reconfigure the Entrails of the Kite",
            startdescription:
              "<How do I allot the the precious space pf my ship? Do I expend the hold, convert additional cabins or expand our private quarters for more personal and private pursuits.>",
            linked: [{ id: "mariner.configureship.hold" }],
          },
        },
        {
          slot: {
            label: "Hull",
            description: "<The peel of planks, the shell of wood, the rind of tar.>",
          },
          recipe: {
            label: "Reconfigure the Hull",
            startdescription:
              "<The hull determines how your ship glides through the water, as well how she keeps it out. Do we optimize the ship for studyness or speed, or do we seek the balance between.>",
            linked: [{ id: "mariner.configureship.hull" }],
          },
        },
      ],
    }),
    // Rigging options
    ...optionSelectionRecipe({
      id: "mariner.configureship.rigging",
      label: "<Reconfigure the Rigging>",
      startdescription:
        "<Each set of sails and masts fits a purpose. By changing them, I could catch the wind better, make myself less visible at sea, or even prepare to accelerate more on my longer voyages. I can decide what direction I want with my Half-heart, as long as I have the cash to spend.>",
      options: [
        riggingConfigurationOption({
          id: "rigging.floating",
          label: "<Floating Sails>",
          slotDescription:
            "<Better suited to catch the wind at any angle. This is offers great speed whenever the wind rises above a sigh. Place my Half-heart here to pick this.>", //puy in half heart to make decission.
          description: "<>",
          appliedAspects: { edge: 1 },
        }),
        riggingConfigurationOption({
          id: "rigging.amoviblemast",
          label: "<Removable Mast>",
          slotDescription:
            "<Less adapted for high thrill races, but they a clever system at the base of the mast allows us to rotate and lower it a bit, to have a lower profile on the open sea. Place my Half-heart here to pick this.>",
          description: "<>",
          appliedAspects: { moth: 1, "mariner.ship.discretion": 1 },
        }),
        riggingConfigurationOption({
          id: "rigging.oceanrigging",
          label: "<Ocean Rigging>",
          slotDescription:
            "<Wide canvas for the wide flat seas. This rigging is optimized for long tracks in one direction. The longer a course is held, the more she accelerates. Place my Half-heart here to pick this.>",
          description: "<>",
          appliedAspects: { heart: 1 },
        }),
      ],
      linked: [{ id: "mariner.configureship" }],
    }),
    // Hold options
    ...optionSelectionRecipe({
      id: "mariner.configureship.hold",
      label: "<Reconfigure the Hold>",
      startdescription:
        "<How do I allot the the precious space pf my ship? Do I expend the hold, convert additional cabins or expand our private quarters for more personal and private pursuits. Place my Half-heart here to pick this.>",
      options: [
        holdConfigurationOption({
          id: "hold.cargo",
          label: "<Cargo Ship Layout>",
          slotDescription:
            "<The more cargo we can carry, the more we money we can make, and the more space we have to prepare for inevitabilities. Place my Half-heart here to pick this.>",
          description: "<>",
          appliedAspects: {
            forge: 1,
            "mariner.ship.holdsize": 3,
            "mariner.ship.cabinsize": 8,
            "mariner.ship.sanctumsize": 3,
          },
        }),
        holdConfigurationOption({
          id: "hold.passenger",
          label: "<Passenger Ship Layout>",
          slotDescription:
            "<With addtional state rooms i can take on additional passengers to ferry over the sea, or i can use them to take on additional crew to prepare for arduous challenges. Place my Half-heart here to pick this.>",
          description: "<>",
          appliedAspects: {
            heart: 1,
            "mariner.ship.holdsize": 1,
            "mariner.ship.cabinsize": 12,
            "mariner.ship.sanctumsize": 3,
          },
        }),
        holdConfigurationOption({
          id: "hold.sanctum",
          label: "<Private Pleasure Yaght Layout>",
          slotDescription:
            "<My vessel need not serves any purpose but mine. If I allocate additional space for my private designs, more spaces to practice my music, pamper my pets and prepare shrines for bound spirits.>",
          description: "<>",
          appliedAspects: {
            grail: 1,
            "mariner.ship.holdsize": 1,
            "mariner.ship.cabinsize": 8,
            "mariner.ship.sanctumsize": 9,
          },
        }),
      ],
      linked: [{ id: "mariner.configureship" }],
    }),
    // Hull options
    ...optionSelectionRecipe({
      id: "mariner.configureship.hull",
      label: "<Reconfigure the Hull>",
      startdescription:
        "<The hull determines how your ship glides through the water, as well how she keeps it out. Do we optimize the ship for studyness or speed, or do we seek the balance between. Place my Half-heart here to pick this.>",
      options: [
        hullConfigurationOption({
          id: "hull.plated",
          label: "<Plated Hull>",
          slotDescription:
            "<Hardwood shiplap does not lay on the water the lightest, but it takes more than a scratch to penetrate.>",
          description: "<>",
          appliedAspects: { forge: 1, "mariner.ship.durability": 2 },
        }),
        hullConfigurationOption({
          id: "hull.balanced",
          label: "<Balanced Hull>",
          slotDescription:
            "<The familiar curves of reliable teak. standards are a standard and sail any sea and lake and river for a reason. Place my Half-heart here to pick this.>",
          description: "<>",
          appliedAspects: {
            heart: 1,
            "mariner.ship.durability": 1,
            [SHIP_STAT("speed")]: 1,
          },
        }),
        hullConfigurationOption({
          id: "hull.sleek",
          label: "<Sleek Hull>",
          slotDescription:
            "<A tapered hull of fragrant cedar wood. Be the blade that carves the water, but better avoid any stress to the wood. Place my Half-heart here to pick this.>",
          description: "<>",
          appliedAspects: { edge: 1, [SHIP_STAT("speed")]: 2 },
        }),
      ],
      linked: [{ id: "mariner.configureship" }],
    })
  );
}

export function generateWharfBuyCargoAction(portRegion, portId, cargoOptions) {
  const buyCargoOption = (cargoOption) =>
    simpleBuyItem(
      cargoOption.id,
      `<Cargo [${cargoOption.label}]>`,
      "<Exchange stocks for stock>",
      "<Not Enough Funds To Buy Cargo>",
      "<Not Enough Space in the Hold>",
      "<Commerce occurs on the edge between want and surplus. I can buy low here and sell at a profit where demand is higher>",
      "<Buy Cargo>",
      "<Barrels are rolled and crates are stowed and the ship is crammed with wealth in potentia.>",
      `mariner.buycargo.from.${portRegion}.${portId}`
    );

  mod.setRecipes(
    "recipes.buycargo",
    ...optionSelectionRecipe({
      id: `mariner.buycargo.from.${portRegion}.${portId}`,
      craftable: true,
      actionId: "talk",
      grandReqs: {
        [`mariner.locations.${portRegion}.${portId}.wharf`]: 1,
        "mariner.location.hostile": -1,
      },
      label: `<Chartering Cargo in ${portId}>`,
      startdescription: `Here I can establish contracts for goods that need to go elsewhere. Once I have reached that elsewhere, they will be good for some money. Available Wares:<br><br>-[1 <sprite name=funds>] Cargo (${cargoOptions[0].label})<br><br>-[1 <sprite name=funds>] Cargo (${cargoOptions[1].label})<br><br>-[1 <sprite name=funds>] Cargo (${cargoOptions[2].label})<i>[Stop making choices to leave the store.]</i>`,
      linked: [{ id: "mariner.buycargo.leaving" }],
      options: [buyCargoOption(cargoOptions[0]), buyCargoOption(cargoOptions[1]), buyCargoOption(cargoOptions[2])],
    }),
    {
      id: "mariner.buycargo.leaving",
      startdescription: "<Alright then.>",
      linked: [
        {
          id: "mariner.addgroundedness",
        },
      ],
    }
  );
}

export function generateWharfActions() {
  generateWharfShipConfigurationAction();
}
