import "./generation/mod.mjs";
import { generateSongs } from "./generation/song_generation/generate_songs.mjs";
import { generateSeas } from "./generation/sea_generation/generate_seas.mjs";
import { generateMoons } from "./generation/generate_moons.mjs";
import { CREW_TYPES, generateCrewmen } from "./generation/crew_generation/generate_crewmen.mjs";
import { generateTools } from "./generation/tools_generation/generate_tools.mjs";
import { generatePassengerSystem } from "./generation/generate_passengers.mjs";
import { generateWeatherBase } from "./generation/generate_weather.mjs";
import { generateTrappings } from "./generation/generate_trappings.mjs";
import { generateRituals } from "./generation/generate_rituals.mjs";
import { generateSeaEventsSystem } from "./generation/sea_events_generation/generate_seaevents.mjs";
import { generateDeliverySystem } from "./generation/packages_generation/generate_packages.mjs";
import { generateHostilitySystem } from "./generation/hostility_system/generate_hostility.mjs";
import { generateStories } from "./generation/stories_generation/generate_stories.mjs";
import { generatePerformances } from "./generation/performances_generation/generate_performances.mjs";
import { generateSeaRewardFunctionBase } from "./generation/generate_sea_reward_functions.mjs";
import { generateCurses } from "./generation/curses_generation/generate_curses.mjs";
import { generateCrewFunctions } from "./generation/crew_generation/generate_crew_functions.mjs";
import { generateInstrumentsContent } from "./generation/instruments_generation/generate_instruments.mjs";
import { generateGeneralShelves } from "./generation/shelves_generation/generate_shelves.mjs";
import { generateGeneralCodexEntries } from "./generation/codex_generation/codex_generation.mjs";
import { generateRootEnums } from "./generation/generate_enums.mjs";
import { generateAllTunes } from "./generation/song_generation/tune_generation.mjs";
import { setLengtheningTunesRecipes } from "./generation/song_generation/lengthening_tunes.mjs";
import { generateExperiences } from "./generation/generate_experiences.mjs";
import { generatePets } from "./generation/pets_generation/generate_pets.mjs";
import { generateGeneralReputationData } from "./generation/port_generation/notoriety_generation.mjs";
import { generateAISystems } from "./generation/port_generation/generate_ais/generate_ais.mjs";
import { generateHearts } from "./generation/hearts_generation/generate_hearts.mjs";
import { generateOptionSelectionRecipe } from "./generation/option_selector.mjs";
import { generateWharfActions } from "./generation/generate_wharf_actions.mjs";
import { generateStoreActions } from "./generation/generate_store_actions.mjs";

import "./generation/quest_generation.mjs";
import { generateSummons } from "./generation/summons_generation/generate_summons.mjs";
import { generateSummonBodies } from "./generation/crew_generation/generate_summons.mjs";
import { generateSailingTestData } from "./generate_sailing_test.mjs";
import { generateShipStats } from "./generation/stats_generation.mjs";
import { generateSavingSystem } from "./generation/generate_saving_system.mjs";

mod.options.displayHiddenAspects = false;
mod.options.displayRecipeIds = false;
mod.options.forceMinimalWarmup = false;

mod.packageGenerationDescriptionStats = {};
mod.packageGenerationInspectionEndStats = {};

generateRootEnums();
generateShipStats();
generateOptionSelectionRecipe();
generateWharfActions();
generateStoreActions();

generateHearts();

generateGeneralCodexEntries();
// generateGeneralShelves();

generatePets();

// Songs
generateSongs();
generateAllTunes();
setLengtheningTunesRecipes();

// Tide
mod.readDecksFile("decks.tides", ["tide"]);
mod.readRecipesFile("recipes.tide.suspicion", ["tide"]);

// Package delivery system
generateDeliverySystem();

// Reputation system
generateGeneralReputationData();
generateHostilitySystem();

// travel system
mod.readRecipesFile("recipes.sailing.arriving", ["sailing"]);

// Generate Moons
generateMoons();

// Generate crewmen
generateCrewFunctions();
generateCrewmen();
generateSummons();
generateSummonBodies();

// Generate tools
generateTools();

// Generate instrument systems
generateInstrumentsContent();

// generate Trappings
generateTrappings();

// generate Rituals
generateRituals();

// generate base reward generic function content
generateSeaRewardFunctionBase();

generateWeatherBase();

generateCurses();

generateStories();
generateExperiences();
// Generate Seas
generateSeas();

// Generate passengers
// Has to be generated after the seas
generatePassengerSystem();

generateSeaEventsSystem();

generatePerformances();

generateAISystems();

generateSavingSystem();

// Write to disk
mod.save();

console.log("Generation stats:");
console.log(mod.generationStats);
