"use strict";

import fs from "fs";
import path from "path";
import { readdir } from "fs/promises";

const EXCEPTIONS = ["ability", "memory.liminal", "vault", "jumble", "locksmithsdream2", "onthewhite", "generic_p"];
const EXCEPTION_FOLDERS = ["_composables", "codex"];

const getDirectories = async (source) =>
  (await readdir(source, { withFileTypes: true })).filter((dirent) => dirent.isDirectory()).map((dirent) => dirent.name);

const getFiles = async (source) =>
  (await readdir(source, { withFileTypes: true })).filter((dirent) => !dirent.isDirectory()).map((dirent) => dirent.name);

const isAspectFile = (fileName, fileObj) => fileName.startsWith("aspects.");
const isElementFile = (fileName, fileObj) => !fileName.startsWith("aspects.") && fileObj["elements"] != undefined;

async function logFile(directories, file, missingIcons) {
  const fileObj = JSON.parse(fs.readFileSync(directories.join(path.sep) + path.sep + file));
  let type = null;
  if (isAspectFile(file, fileObj)) type = "aspects";
  else if (isElementFile(file, fileObj)) type = "elements";

  if (type === null) return;

  for (const obj of fileObj.elements) {
    if (obj.isHidden) continue;
    let fileName = obj.icon ?? obj.id;
    let filePath = "images/" + type;
    if (fileName.includes("/")) {
      const index = fileName.lastIndexOf("/");
      filePath += "/" + fileName.substring(0, index);
      fileName = fileName.substring(index + 1);
    }

    let fullPath = filePath + "/" + fileName + ".png";
    if (EXCEPTIONS.includes(fileName)) continue;
    try {
      const files = await getFiles(filePath);
      if (!files.includes(fileName + ".png")) throw Error();
    } catch (err) {
      missingIcons.add(fullPath);
    }
  }
}

async function readAndLogDirectory(directoryPath, missingIcons) {
  const files = await getFiles(directoryPath.join(path.sep));
  const directories = await getDirectories(directoryPath.join(path.sep));
  // console.log("╔ READING DIRECTORY", directoryPath.join("/"));
  // if (directories.length) console.log("║ Found directories:", directories);

  for (const file of files) {
    await logFile(directoryPath, file, missingIcons);
  }

  // console.log("╚ Done");

  for (const directory of directories) {
    if (EXCEPTION_FOLDERS.includes(directory)) continue;
    await readAndLogDirectory([...directoryPath, directory], missingIcons);
  }
}

let missingIcons = new Set();
await readAndLogDirectory(["content"], missingIcons);
fs.writeFileSync("missing_icons.txt", [...missingIcons].join("\n"));
console.log("Done. Results are in missing_icons.txt");
