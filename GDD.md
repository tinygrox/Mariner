# Game Design Document of Mariner

## Goal/story

You play as the Mariner, a sailor with a particular sensibility. Narratively, you are a Halfheart: like the greek myth on the origin of the soulmates, you share "one Soul" with someone else you never knew, and because of that, you are a unique individual. You feel constant Wanderlust, the need to be "elsewhere", to stay on the move. You occasionally work for the Raconteur Network, a somewhat informal network of contraband of illegal goods, often used by occultists to buy and transport forbidden objects.

Two main goals can be reached, and are mutually exclusive.

### "The Vagabond Path"

One of the divinities (Hours) of this world, called the Vagabond, is known in the tales to have been banned from the Mansus, the "House of the Hours". She wanders and it is said she's been everywhere. She seems to be collecting something, peculiar words describing specific emotions, for some unknown purpose. She leaves trails behind her, forming a web its followers wander. It is said that some of them might even find a backdoor she left open to reenter the Mansus when the time is right: a Painted Waterfall flowing back up to the fabled Painted River.

Your goal, if you pursue this temptation, is to collect one Word in each sea, describing a specific story the Vagabond collected. These would give you access to the Painted Waterfall, allowing you forever sail onto the rivers of the Mansus, never having to settle anywhere ever again.

### "The Twins Path"

Known as Halfhearts, the Twins were two Hours born far apart, reunited in drowning together and ascending as Names and then Hours into the Mansus.

Your goal, if you pursue this temptation, is to uncover every part of your soul that is damaged (the soul parts of the CS universe) and mend them by performing a ritual in each of the seas, an occult performance made of the most charged symbolism, narrating similar issues found in tales of the Hours. Each performance gives you a clue of who your Other Half is, until you're both reunited in a final act into the ocean, and ascend under the patronage of the Twins.

### Structure

Each main quest is made of seven milestones, one per sea. The first sea is always the north sea, opening up with the med sea being the next intended sea. But it's not an absolute requirement, beyond the north sea any order is acceptable. Seas are contained narratives with a main storyline for each main quest (milestones) and a lot of secondary content.

"Secondary content" isn't to be understood as "side" or "optional", as a lot of milestones require a good enough set of "stats" and equipment, upgrades to your cards, which is usually a reward of secondary content.

As a sailor, you're never fully accepted by the local people, and your dabbing into the occult is explicitely forbidden by most local authorities. Sail from port to port with your crew, moving cargo and people to stay financially afloat, recruit the right crew members to explore "vaults/adventures/dungeons" to gather the knowledge and items you need to advance on your Quest.

### Narrative Intent

## Gameplay Overview

Born as a CS mod, the UX is pretty similar: a table, onto which every known thing is either a card, or a stat (aspect) on a card. The actions are verbs, boxes you fill with cards to start actions. Actions are a string of texts and decisions to make, eventually stopping to allow you to collect the result.

The action verbs are Explore, Talk, Navigate ("Study"), Sing, Sail. There is an additional verb used as a ticking clock, Tide. It is never available to do things with it, it is only a source of things happening.

The gameplay is split into three phases: In Port, At Sea, and In Adventure. You usually go from In Port to At Sea, from In Adventure to At Sea, and from At Sea to any other context based on where you sail to.

### In Port:

Docking to a port gives you up to 3 Thrill cards, the max on the board being 3.
While docked in a port, you have several options:

- exploring the port:
  - with your LH to look for tunes to listen to
  - with your HH to look for new places and people to meet
  - [if you have enough Knock influence] with your HH, you can meet a member of the Raconteur Network. This will start a sort of minigame where the person will tell you a tale, which is clearly of some kind. You need to answer with one of the stories you know, possessing the matching story type aspect. The more packages you collect and keep with you, the more questions the member will ask before giving you a package to deliver to someone.
- interacting with local elements (bar, wharf, npcs, ...)
- specifically, with the local bar, you can:
  - look for people to recruit as part of your crew. You can either find an Intellectual, Manual, or Basic recruitment opportunity, based on luck and the amount of Lantern and Edge influence.
  - roam the place buying drinks to people, trying to acquire rumours (chance based loop, biased by the Heart influence)
  - look for potential passager clients to bring on board and deliver somewhere else (docking to the right destination converts them into funds and a rumour) (chance based loop, biased by the Graal influence)
  - talk about an experience combined with a matching tale to clear it before its effect triggers
- specifically with the wharf, you can:
  - pay to repair your ship
  - acquire cargo
- specifically with NPCs, you can:
  - talk to them about a lot of things (advance specific quests that way)
  - deliver them a package if they're a recipient of the network (they'll ask which reward you'd like to get, LH gives intel, HH gives funds)
  - pay them to petsit a pet for you, if they are marked as potential petsitter and do not already keep one of your pets
  - talk to them about any story to get their personal story in return
- other local elements and places can have unique interactions.
- theaters allow you to perform

Almost every port has a local bar and wharf.

While docked in a port, the Tide will regularly deliver "seasons", events happening. Among them:

- Season of Reckoning: eats untreated experiences and wounds your hearts. If any heart reaches 3 wounds, it is game over.
- Season of Wanderings: either gives you a Wanderlust card, or an event. Some special quest events happen that way (they take top priority). If you already have Wanderlust, has a chance to transform into Season of Heartache instead.
- Season of Heartache: eats a Wanderlust, then, if it finds any, a Thrill to consume both. If it reaches 3 Wanderlust, and you do not leave in time, it is game over.
- Season of Suspicion: if notoriety is present on the table, will eat it to upgrade the level of reputation you have in this port.

### At Sea

### In Adventure

## Systems Overview

General description of all the systems.

### The Kite (the ship)

The ship is a card on the table. It has stats: cargo hold size, number of cabins, durability, current amount of damage. If the amount of damages reaches the durability value while at sea, the ship sinks and it is game over.

#### _Future features_

The Kite can receive a number of upgrades, modifying its stats and unlocking specific outcomes during sea events.

### Half-Heart and Lack-Heart

The Half-heart and Lack-heart are two cards on the table. They represent the two parts of your heart.

> Half-Heart: My passions, my drive, my charm, my thrumming blood, my clever fingers. If I present this to the world, I may be able to bend things to my will. [Use this while Exploring, Talking or Performing, and you may find or discuss things of interest to you]

> Lack-Heart: A wound in the soul, a raw nerve, a longing. If I leave this exposed, the universe will try to fill it. [Use this while Exploring, Talking or Performing and you may find Tunes, Stories or other Inspirations]

These two hearts can sometimes be used in challenges during adventures, but they are mostly used to express intent. HH is the more active one, LH is the more passive one. Several actions in the game ask the player to provide one of them to make a decision.

Each heart can be wounded up to three times and lead to a game over. See the Experiences and Wounds section for more info on that.

### Tide

The Tide is the Mariner equivalent of the Time verb from Cultist SImulator. It is a looping verb, with a pace of 60s outside adventures, longer in Adventures (they usually run at 180s per cycle). Every cycle, they lead to a bunch of stuff updating (reputation heat in ports decreases, for instance)

Every cycle, it spawns a Season or an Event. In Ports, it spawns most seasons (and events happening there happen during the Season of Wandering). At Sea, the Tide doesn't spawn anything. In Adventures, it can spawn Adventure-specific events.

The cycle also advances a hidden clock (a simple increasing counter) that a lot of things use. For instance, most Wandering events have a timer to avoid them repeating too frequently or even on loop. Some events also use this to appear in sequence but with a good distance in time between them. (avoid the syndrome of "all these events have their requirements met so a lot of stuff happens at once". Also, the Tide has a global rate limit between special events)

### Stories and Personal Stories

Stories are unique cards describing a myth or tale. When used to do something, they are usually exhausted. They refresh when docking to a port. They're a resource you slowly acquire as you progress.

Stories have aspects describing their type, usually several.

Example:

> How Anansi The Spider Came To Own All Stories  
> "Once, there were no stories told in the world. The Skyfather, Sun of Suns, held them all. Anansi bartered for them, and though the Skyfather set an impossible price, Anansi delivered. He tricked wise Snake, caught the familial Hornets, trapped the fierce Leopard and fooled the furtive fairy. With these prizes Anansi earned the respect of the Skyfather, and all the stories of the world."  
> **Aspects:** Apollonian, Wise, Witty

Personal Stories are obtained by talking to a NPC about any story you know. In return, they exhaust your story and provide a unique story. Each NPC has a unique story to provide, with a specific combination of aspects. Personal stories are local (they are destroyed when you leave the port/adventure).

### Experiences and Wounds

At various points in the game, you can receive "experiences" card, e.g. "Dreadful Experience". They represent the fact that something happened that fundamentally impacted you. Experiences usually have a lifetime, and then decay.

A decayed experience can be picked up by the Season of Reckoning to apply an effect (usually, a wound onto one of the hearts, but experiences have a wide array of lifetimes and effects, from making crew want to leave you to hurting both hearts, ...)

Once a heart has at least one wound, any menial action (getting cargo, talking to people, wandering the port, finding clients, ...) adds some amount of groundedness to it. Once it reaches 100, the Season of Reckoning, when it happens, deletes a wound and resets groundedness to 0.

If a heart reaches 3 wounds, it is game over.

Experiences have aspects matching the story types. You can talk about an experience + matching story in a bar to delete the wound, exhausting the story in the process.

### Influences

Influences are cards that influence the randomness of a lot of actions (usually in town):

- graal influence increases the probability of finding a potential client (traveller)
- heart influence increases the probability of convincing people in bars to tell you about rumours
- lantern influence increases the probability of finding an Intellectual Crewmember opportunity
- edge influence increases the probability of finding a Manual Crewmember opportunity
- knock influence increases the probability of meeting a Raconteur member while wandering the port
- forge influence increases the probability of getting one round of repair of the ship for free
- moth influence increases the probability of the authorities being confused and not upgrade your reputation when eating notoriety
- winter influence decreases the probability of getting sea events while at sea

All influences except Winter are local to a port (they disappear as soon as you leave) and have no lifetime. The Winter influence sin't local but has a lifetime of a few hundred seconds. When the effect of an influence triggers, 2 of the influence are consumed.

Each influence usually increases the probability of triggering the effect by 10%.

### Song Inspirations

Several things in the game are considered "Song inspirations":

- Muses
- Nature (weather/moon)
- Remarkable Melodies
- Remarkable Locations
- Remarkable Experiences
- Ur-Stories
- Remarkable Instruments

These are special (occult) things that, when used on the right song (same principle) can upgrade it. You can only use an inspiration type once to upgrade a song of a given Principle (a Muse having Edge as its aspects can only be used once to upgrade the Edge song).

### Songs & Singing

Songs are card on the table. You can only compose one song per Lore Principle.

Songs are created ("composed") via Navigate + tune + inspiration. A type of inspiration can only be used once to compose or upgrade a song.

Songs can be upgraded via Navigate + song + inspiration. Upgrading a song improves its stats, which improve the general effects and chances of success when doing actions related to them.

Singing is done via Sing + song + crowd.

> "Crowd" is an aspect representing a potentially attentive group of people. It is currently present on the port cards, bars, and theaters.

The chance of success depends on the song level. It creates a number of influences of the same principle as the song. The quantity depends on the song level. If the action is successful, no notoriety is created in the process. If not, influence is still created, but notoriety is also created in the process.

In addition, you can Sing + Song + Trapping as a "quick ritual" to consume a trapping and produce influence quickly.

### Performances

Performances are entire...performances, performed at Theaters. They require to input a song and allow to also put a few cards to help the general stats of the performance. The performance will then demand a second song.

The first song is the "verb" or "action", and the second is the "noun" or "direction". Each verb+noun combination has a specific effect. They are basically spells with a more specific and powerful effect than Singing.

Theoretical example: a successful Edge (rouse) + Graal (nature) performance "rouses the weather", conjuring an unnatural storm.

The chance of success is based on the stats of the songs, boosted by whatever assistance you might have added at the start (the "stats of the songs" are just the amount of the Principle they embody, as aspect on them).

### Cargo

We can obtain cargo when interacting with wharves in ports (talk + wharf). They're automatically converted to funds when docking to the next port.

### Reputation in Ports

In ports, some actions can create Notoriety cards. These are local (they disappear when you leave).

When the Season of Suspicion happens, if it finds Notoriety on the board, it will upgrade the reputation level of the current port.

Weither it found suspicion or not, if the current port has a reputation level, it has a chance of making one of the local elements "hostile". The probability is 30% for rep level 1, 60% for rep level 2, 100% for rep 3 and 4.
The higher the reputation level, the quicker the port shuts down bit by bit.

The maximum reputation level is 4.

When the reputation level of a port changes, it generates some amount of Reputation Heat on the port. The amount is ([reputation level] \* 2) + 3.

Every Tide cycle (60s in port and at sea, longer in adventures, see Tide), all ports see their heat decreased by 1. If a port has reputation but zero heat, its reputation level decreases by 1 and its heat is regenerated based on its newly lowered reputation level.

### Local Elements Hostility and Consequences of Trouble

Some local elements (notably bars, wharves, theaters, npcs) can be made "hostile" by the Season of Suspicion. This represents the increaasing pressure of the city's authority. NPCs become "hard to meet", bars become hostile to your presence, etc. You need to clear them first if you want to use them again. The clearing action is a Challenge just like most actions in adventures:

- Forked Tongue for Theaters
- Overcome for Wharves
- Honeyed Tongue for Bars
- Sneak In for Markets
- Sneak In for NPCs

Failing the action will still clear the place, but generate 1 Notoriety in the process. Also, if the rep was at level 3 or 4, it spawns a verb called Consequences of Trouble.

At rep level 3, this verb will force you to leave (leaving port halts it) under 60s or suffer a game over.

At rep level 4, this verb will force you to leave (leaving port halts it) under 60s or suffer a game over, AND will grab one of your crew members at random, as a scapegoat, that will be lost in the process.

Hostile locations clear automatically when you leave the port.

### Crew

Crewmembers are cards on the table. Each crewmember comes with three informations: a specialty and two traits. Specialists have higher stats but of only one or two Principle aspects, Basic crewmembers have lower stats spread over a few more Principle aspects.

Crewmembers have two traits, describing their name "A [Tall] [Grumpy] [Navigator]" (Trait, Trait, Specialty).

The majority of traits are cosmetic, but around a quarter of them have actual gameplay changes. A few examples:

- The Mute Trait makes a crewmember unusable in any action in the Talk verb
- The Superstitious Trait makes a crewmember impossible to use to help against supernatural threats
- the Wanted Trait can lead to reputation increase even without Notoriety on the board
- the Loyal Trait makes a crewmember never wanting to leave the crew on their own
- the Charming Trait increases the probability of finding passenger clients

Finding a crewmember opportunity is increased by the amount of Lantern influence (to find "intellectual" specialists) and Edge influence (to find "manual" specialists). Then, you can talk to the opportunity card with a number of funds to recruit it. Basic crewmembers cost 1 fund, specialists usually cost three to five funds. When trying to recruit someone, they might refuse based on the level of reputation in the port and then the opportunity is lost.

### Crew longing

With each travel to a destination, crewmembers gain 1 longing. Every time you dock to a port, if the highest amount of longing on a crewmember is above 7, there is a 25% chance of someone wanting to leave. The crewmember with the highest amount is picked and now has the aspect "wants to leave". When leaving a port, crew members with this aspect disappear, they "leave the crew".

You try to convince them to stay. Crewmembers track how many times you're bargained with them to convince them to stay. This value never resets (aspect: mariner.crew.bargained).

To bargain with them, talk to a crewmember willing to leave. The action allows you to input up to 5 funds.

> The chance of success of the bargaining is computed via the formula: 25 + ((50*[funds]) / ((100*[mariner.crew.bargained] + 1) + 25*[funds])) * 100

The more you've bargained , the harder it is to make them change their mind. This means it is fairly easy to ask them to stick around the first or second time, but after that the cost and probability becomes pretty prohibitive. This also means the more you fail to convince someone, the harder it becomes to convince them to change their mind.

Eventually, they'll leave. Crewmembers are, as a general rule, always only part of the crew for a while.

### Pets

Pets are unique rewards you can obtain as quest rewards. You can have up to 1 pet at a time on the table ("on board"). Some NPCs are marked as potential petsitters. You can talk to them with the pet card, provided they do not already keep one of your pets. In exchange for a fund, they'll keep it (visible on their card's portrait, and as an aspect on their card).

Pets provide passive bonuses and bonus outcomes to sea events and wandering events.

Pets' label and description depends on a secret level of bonding present on them.

Example on the "ferret pet":

> A Watchful Ferret  
> Sneaky shadow, claw sounds on the planks, pinky snout peeking from under the furnitures. Ever watching, ever expecting something to happen. Some could argue all these are just a play of their imagination, were it not for the fact that it leaves little food stashes all over the ship.

> "Hadashi"  
> Someone named it "Hadashi", and it stuck since. It accepts little nuggets of food, and likes to follow the crew around, at a distance. It seems to always be aware of anything happening an instant before we do.

> Hadashi, Ever Watching  
> Hadashi is as much part of the crew as anyone else. Silent as the shadows, alerting when guards approach, ever watching for threats and keeping us safe. The crew, in turn, keeps blankets in all the corners of the ship, always full of food.

Pets aren't considered "crew members": they never want to leave, can never be injured or die and aren't affected by effects attacking the crew.

### Sailing

### Sea Event Opportunities and Sea Events

While at Sea, either sailing towards a destination wandering the open sea, you get a series of sea events. When sailing towards a destination, distance is counted in sea event opportunities. Sailing to a destination is basically a cycle, processing each sea event opportunity.

Winter influences increase the probability of a sea event opportunity not leading to a sea event to resolve.

When you get a sea event, it is usually a simple situation asking for you to input resources (crewmembers, hearts, tools, ...) to solve them. They have a set of outcomes and then the cycle continues.

Sea events often have conditions of appearance: the right weather, the presence of an item on board, a crewmember having a specific trait, etc. Because of that, "when" to sail and the path to reach it can sometimes be quite important, as leaving a port during stormy weather will usually lead to more trouble at sea.

Once no more sea event opporunities are left to process, sailing ends and the player arrives (and docks) to the destination.

Sailing the open sea without destination gives an infinite amount of sea events until the player decides to change course. Some events are specific to sailing the open sea.

### Wandering Events

When the Season of Wanderings happen in ports, it can either give Wanderlust, or an event. These don't usually require decisionmaking and are more things happening to make ports more lively. They also sometimes depend on the port you're docked to and the general state of the game.

### Special Events

Special Events are events that take absolute priority when processing sea events and wandering events. They always happen if their condition is valid. They're usually one time events happening for story reasons, related to some questline. This is one method often used by quests: they automatically advance if the conditions are met.

Example: at the beginning of the first milestone for the Twins Path in the north sea, Mr Agdistis gives you a doll and asks you to throw it into the ocean at the heart of a storm. During sailing, assuming you have the doll and this quest flag active, if the weather is stormy, a special event will happen, deleting the doll and advancing the quest.

### Instruments

### Markets

### Tools

### Passengers

Talking in bars + ship let you look for passenger opportunities. The probability to find some is increased by the amount of Graal influence, and decreased by the level of reputation in the port.
